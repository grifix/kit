<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace {
    
    return [
        'presenter' => [
            'views' => [
                'text/html' => 'grifix.kit.html.tpl.exception',
                'application/json' => 'grifix.kit.json.tpl.exception',
            ],
            'debug' => true,
            'enabled' => true
        ],
    ];
}

