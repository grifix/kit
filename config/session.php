<?php
declare(strict_types=1);

namespace {

    return [
        'handler' => 'native',
        'options' => [
            'name' => 'grifix',
            'cookie_lifetime' => 1800
        ],
    ];
}