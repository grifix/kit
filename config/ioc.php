<?php
declare(strict_types=1);

namespace {

    use Grifix\Kit\Config\ConfigInterface;
    use Grifix\Kit\EventBus\EventBusFactoryInterface;
    use Grifix\Kit\EventBus\EventBusInterface;
    use Grifix\Kit\Ioc\IocContainerInterface;
    use Grifix\Kit\Queue\QueueFactoryInterface;
    use Grifix\Kit\Queue\QueueInterface;

    return [
        QueueInterface::class => function () {
            /**@var $this IocContainerInterface */
            if ($this->get(ConfigInterface::class)->get('grifix.kit.queue.async')) {
                return $this->get(QueueFactoryInterface::class)->createRabbitMqQueue();
            } else {
                return $this->get(QueueFactoryInterface::class)->createSyncQueue();
            }
        },

        EventBusInterface::class => function () {
            /**@var  $this IocContainerInterface */
            return ($this->get(EventBusFactoryInterface::class)->createEventBus());
        }
    ];
}
