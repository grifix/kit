<?php
declare(strict_types=1);

namespace {

    return [
        'async' => true,
        'rabbit' => [
            'host' => getenv('RABBITMQ_HOST'),
            'port' => 5672,
            'user' => getenv('RABBITMQ_USER'),
            'pass' => getenv('RABBITMQ_PASS')
        ]
    ];
}
