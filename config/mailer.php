<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace {
    
    use Grifix\Kit\Mailer\MailerFactoryInterface;
    
    return [
        'fromEmail' => 'no-reply@grifix.net',
        'fromName' => 'Grifix mailer',
        'transport' => MailerFactoryInterface::TRANSPRORT_SMTP,
        'host' => 'localhost',
        'port' => 25,
        'userName' => 'test',
        'password' => 'test',
    ];
}