<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace {
    
    return [
        'email' => 'Email',
        'password' => 'Password',
        'msg_test' => 'Test message, do not change it!',
        'msg_invalidInt' => '{field} must be integer!',
        'msg_somethingWrong' => 'Something wrong, call to administrator!',
        'error' => 'Error',
        'msg_unauthorized' => 'Yoy must sign in to access for this resource!',
        'msg_accessDenied' => 'Access denied!',
        'msg_successfullySaved' => 'Data successfully saved!',
        'msg_404' => 'Page not found!',
        'id' => 'Id',
        'name' => 'Name',
    ];
}
