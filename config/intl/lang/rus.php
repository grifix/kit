<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace {
    return [
        'email' => 'Email',
        'password' => 'Пароль',
        'letter' => [
            'cases' => [
                'nom' => ['буква', 'буквы'],
                'gen' => ['буквы', 'букв'],
                'dat' => ['букве', 'буквам'],
                'acc' => ['буквы', 'букв'],
                'abl' => ['буквой', 'буквами'],
                'prep' => ['букве', 'буквах'],
            ],
        ],
        'number' => [
            'cases' => [
                'nom' => ['цифра', 'цифры'],
                'gen' => ['цифры', 'цифр'],
                'dat' => ['цифру', 'цифрам'],
                'acc' => ['цифры', 'цифр'],
                'abl' => ['цифрой', 'цифрами'],
                'prep' => ['цифре', 'цифрах'],
            ],
        ],
        'symbol' => [
            'cases' => [
                'nom' => ['символ', 'символы'],
                'gen' => ['символа', 'символов'],
                'dat' => ['символу', 'символам'],
                'acc' => ['символа', 'символов'],
                'abl' => ['символом', 'символами'],
                'prep' => ['символе', 'символах'],
            ],
        ],
        'special' => [
            'cases' => [
                'nom' => ['спецальный', 'специальные'],
                'gen' => ['специального', 'специальных'],
                'dat' => ['специальному', 'специальным'],
                'acc' => ['специального', 'специальных'],
                'abl' => ['специальным', 'специальными'],
                'prep' => ['специальном', 'специалныих'],
            ],
        ],
        'msg_invalidIntInField' => 'Занчение в поле {field} должно быть целым числом!',
        'msg_invalidDateInField' => 'Значение в поле  {field} содержит дату в неправильном формате!',
        'msg_invalidMinNumOfCapsInField' => 'Значение в поле {field} должно содержать минимум {num} _(grifix.kit.letter.{num}) в верхнем регстре!',
        'msg_invalidMinNumOfNumbersInField' => 'Значение в поле {field} должно содержать минимум {num} _(grifix.kit.number.{num})!',
        'msg_invalidMinNumOfLettersInField' => 'Значение в поле {field} должно содержать минимум {num} _(grifix.kit.letter.{num})!',
        'msg_invalidMinNumOfSpecialSymbolsInField' => 'Значение в поле {field} должно содержать минимум {num} _(grifix.kit.special.{num}) _(grifix.kit.symbol.{num})!',
        'msg_invalidNotEmptyInField' => 'Значение в поле {field} не может быть пустым!',
        'msg_invalidEmailInField' => 'Значение в поле {field} содежит email в неправильном формате!',
        'msg_invalidFloatInField' => 'Значение в поле {field} должно быть числом с плавающей точкой!',
        'msg_invalidMinSymbolsInField' => 'Значение в поле {field} должно содержать минимум {num} _(grifix.kit.symbol.{num})!',
        'msg_invalidMaxNumOfSymbolsInField' => 'Занчение в поле {field} может содержать максимум {num} _(grifix.kit.symbol.{num})!',
        'msg_invalidMinNumOfSymbolsInField' => 'Занчение в поле {field} должно содержать минимум {num} _(grifix.kit.symbol.{num})!',
        'msg_invalidMoneyInField' => 'Значение в поле {field} не являетя корректной денежной суммой',
        'msg_invalidNoTagsInField' => 'Значение в поле {field} не может содержать теги!',
        'msg_invalidNoEmptyInField' => 'Значение в поле {field} не может быть пустым!',
        'msg_invalidRomanAlphabetInField' => 'Значение в поле {field} может содержать только буквы, цифры и символ подчеркивания!',
        'msg_invalidUrlInField' => 'Значение в поле {field} содежрит адрес URL в неправильном формате!',
        'msg_invalidInArrayInField' => 'Значение в поле {field} должно быть одним из значений {values}!',
        'id' => 'Ид',
        'name' => 'Имя',
        'createdAt' => 'Дата создания',
        'date' => 'Дата',
        'type' => 'Тип',
        'msg_somethingWrong' => 'Что-то пошло не так, обратитесь к администатору!',
        'error' => 'Ошибка',
        'msg_unauthorized' => 'Для доступа к данному ресурсу необходимо выполнить вход в систему!',
        'msg_accessDenied' => 'Недостаточно прав для доступа к данному ресурсу!',
        'msg_successfullySaved' => 'Данные успешно сохранены!',
        'msg_404' => 'Страница не найдена!'
    ];
}