<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
return [
    'default' => [
        'type' => 'postgres',
        'host' => getenv('DB_HOST'),
        'user' => getenv('POSTGRES_USER'),
        'password' => getenv('POSTGRES_PASSWORD'),
        'db' => getenv('POSTGRES_DB'),
        'port' => '5432',
    ]
];
