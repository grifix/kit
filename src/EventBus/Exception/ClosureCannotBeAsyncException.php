<?php
declare(strict_types=1);

namespace Grifix\Kit\EventBus\Exception;

use Throwable;

/**
 * Class ClosureCannotBeAsycnException
 * @package Grifix\Kit\EventBus\Exception
 */
class ClosureCannotBeAsyncException extends \RuntimeException
{
    protected $message = 'Closure listener cannot be async!';

    public function __construct()
    {
        parent::__construct();
    }
}
