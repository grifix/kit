<?php
declare(strict_types=1);

namespace Grifix\Kit\EventBus;

/**
 * Class EventBus
 * @package Grifix\Kit\EventBus
 */
interface EventBusInterface
{
    /**
     * @param object $event
     */
    public function trigger($event): void;

    /**
     * @param string $eventClass
     * @param callable $listener
     * @param bool $async
     */
    public function listen(string $eventClass, callable $listener, bool $async = false): void;
}
