<?php
declare(strict_types=1);

namespace Grifix\Kit\EventBus;


use Grifix\Kit\EventBus\Exception\ClosureCannotBeAsyncException;
use Grifix\Kit\EventBus\Queue\ExecuteListenerMessage;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Queue\QueueInterface;

/**
 * Class EventBus
 * @package Grifix\Kit\EventBus
 */
class EventBus implements EventBusInterface
{

    public const QUEUE_NAME = 'event_bus';

    /**
     * @var array
     */
    protected $listeners = [];


    /**
     * @var ClassHelperInterface
     */
    protected $classHelper;

    /**
     * @var array
     */
    protected $asyncListeners = [];

    /**
     * @var QueueInterface
     */
    protected $queue;

    /**
     * EventBus constructor.
     * @param ClassHelperInterface $classHelper
     * @param QueueInterface $queue
     * @param array $asyncListeners <cfg:grifix.kit.eventBus.asyncListeners>
     */
    public function __construct(
        ClassHelperInterface $classHelper,
        QueueInterface $queue
    ) {
        $this->classHelper = $classHelper;
        $this->queue = $queue;
    }

    /**
     * @param object $event
     */
    public function trigger($event): void
    {
        $eventType = $this->classHelper->makeFullClassName(get_class($event));
        if (isset($this->listeners[$eventType])) {
            foreach ($this->listeners[$eventType] as $listener) {
                if ($this->isAsync($listener)) {
                    $this->queue->publish(new ExecuteListenerMessage($event, get_class($listener)), self::QUEUE_NAME);
                } else {
                    $listener($event);
                }

            }
        }
    }

    /**
     * @param object $listener
     * @return bool
     */
    protected function isAsync(object $listener): bool
    {
        return in_array(get_class($listener), $this->asyncListeners);
    }

    /**
     * @param string $eventClass
     * @param callable $listener
     * @param bool $async
     */
    public function listen(string $eventClass, callable $listener, ?bool $async = false): void
    {
        $eventClass = $this->classHelper->makeFullClassName($eventClass);
        if (!isset($this->listeners[$eventClass])) {
            $this->listeners[$eventClass] = [];
        }
        $this->listeners[$eventClass][] = $listener;
        if ($async) {
            if (get_class($listener) === 'Closure') {
                throw new ClosureCannotBeAsyncException();
            }
            $this->asyncListeners[] = get_class($listener);
        }
    }
}
