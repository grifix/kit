<?php
declare(strict_types=1);

namespace Grifix\Kit\EventBus;

/**
 * Class EventBusFactory
 * @package Grifix\Kit\EventBus
 */
interface EventBusFactoryInterface
{
    /**
     * @return EventBusInterface
     */
    public function createEventBus(): EventBusInterface;
}