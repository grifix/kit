<?php
declare(strict_types=1);

namespace Grifix\Kit\EventBus;

/**
 * Class Definition
 * @package Grifix\Kit\EventBus
 */
class Definition
{
    /**
     * @var string
     */
    protected $eventClass;

    /**
     * @var string
     */
    protected $listenerClass;

    /**
     * @var bool
     */
    protected $async = false;

    /**
     * Definition constructor.
     * @param string $eventClass
     * @param string $listenerClass
     * @param bool $async
     */
    public function __construct(string $eventClass, string $listenerClass, bool $async)
    {
        $this->eventClass = $eventClass;
        $this->listenerClass = $listenerClass;
        $this->async = $async;
    }

    /**
     * @return string
     */
    public function getEventClass(): string
    {
        return $this->eventClass;
    }

    /**
     * @return string
     */
    public function getListenerClass(): string
    {
        return $this->listenerClass;
    }

    /**
     * @return bool
     */
    public function isAsync(): bool
    {
        return $this->async;
    }
}
