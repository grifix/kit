<?php
declare(strict_types=1);

namespace Grifix\Kit\EventBus;

use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Queue\QueueInterface;

/**
 * Class EventBusFactory
 * @package Grifix\Kit\EventBus
 */
class EventBusFactory extends AbstractFactory implements EventBusFactoryInterface
{

    /**
     * @var ClassHelperInterface
     */
    protected $classHelper;

    /**
     * @var QueueInterface
     */
    protected $queue;

    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * EventBusFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param ClassHelperInterface $classHelper
     * @param QueueInterface $queue
     * @param IocContainerInterface $iocContainer
     * @param KernelInterface $kernel
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        ClassHelperInterface $classHelper,
        QueueInterface $queue,
        IocContainerInterface $iocContainer,
        KernelInterface $kernel
    ) {
        parent::__construct($classMaker);
        $this->classHelper = $classHelper;
        $this->queue = $queue;
        $this->iocContainer = $iocContainer;
        $this->kernel = $kernel;
    }

    /**
     * @return EventBusInterface
     * @throws \ReflectionException
     */
    public function createEventBus(): EventBusInterface
    {
        $class = $this->makeClassName(EventBus::class);

        /**@var EventBusInterface $eventBus */
        $eventBus = new $class($this->classHelper, $this->queue);
        $this->setConfig($eventBus);
        return $eventBus;
    }

    /**
     * @param EventBusInterface $eventBus
     * @throws \ReflectionException
     */
    protected function setConfig(EventBusInterface $eventBus)
    {
        foreach ($this->kernel->getModules() as $module) {
            $config = $module->getConfig();
            if (isset($config['events'])) {
                foreach ($config['events'] as $definition) {
                    /**@var Definition $definition */
                    $eventBus->listen(
                        $definition->getEventClass(),
                        $this->iocContainer->createNewInstance($definition->getListenerClass()),
                        $definition->isAsync()
                    );
                }
            }
        }
    }
}
