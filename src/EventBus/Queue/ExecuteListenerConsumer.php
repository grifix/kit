<?php
declare(strict_types=1);

namespace Grifix\Kit\EventBus\Queue;

use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class ExecuteListenerConsumer
 * @package Grifix\Kit\EventBus\Queue
 */
class ExecuteListenerConsumer
{

    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    /**
     * @var ClassMakerInterface
     */
    protected $classMaker;

    /**
     * ExecuteListenerConsumer constructor.
     * @param IocContainerInterface $iocContainer
     * @param ClassMakerInterface $classMaker
     */
    public function __construct(
        IocContainerInterface $iocContainer,
        ClassMakerInterface $classMaker
    ) {
        $this->iocContainer = $iocContainer;
        $this->classMaker = $classMaker;
    }

    /**
     * @param ExecuteListenerMessage $message
     * @throws \ReflectionException
     */
    public function __invoke(ExecuteListenerMessage $message)
    {
        $listenerClass = $this->classMaker->makeClassName($message->getListenerClass());
        $listener = $this->iocContainer->createNewInstance($listenerClass);
        $listener($message->getEvent());
    }
}
