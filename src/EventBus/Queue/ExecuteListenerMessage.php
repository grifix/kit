<?php
declare(strict_types=1);

namespace Grifix\Kit\EventBus\Queue;

/**
 * Class ExecuteListenerMessage
 * @package Grifix\Kit\EventBus\Queue
 */
class ExecuteListenerMessage
{
    /**
     * @var object
     */
    protected $event;

    /**
     * @var string
     */
    protected $listenerClass;

    /**
     * ExecuteListenerMessage constructor.
     * @param object $event
     * @param string $listenerClass
     */
    public function __construct(object $event, string $listenerClass)
    {
        $this->event = $event;
        $this->listenerClass = $listenerClass;
    }

    /**
     * @return object
     */
    public function getEvent(): object
    {
        return $this->event;
    }

    /**
     * @return string
     */
    public function getListenerClass(): string
    {
        return $this->listenerClass;
    }
}
