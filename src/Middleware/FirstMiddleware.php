<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Middleware;


use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;

/**
 * Class FirstMiddleware
 *
 * @category Grifix
 * @package  Grifix\FirstMiddleware
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FirstMiddleware extends AbstractMiddleware
{
    /**
     * {@inheritdoc}
     */
    protected function after(ServerRequestInterface $request, ResponseInterface $response)
    {
        if (!($response instanceof ResponseInterface)) {
            throw new InvalidResponseException();
        }
        return $response;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function before(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $response;
    }
}
