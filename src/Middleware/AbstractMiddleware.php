<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Middleware;

use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;

/**
 * Class AbstractSpecialClass
 *
 * @category Grifix
 * @package  Grifix\FirstMiddleware
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractMiddleware implements MiddlewareInterface
{
    
    /**
     * @var MiddlewareInterface
     */
    protected $nextMiddleware;
    
    /**
     * @param MiddlewareInterface $nextMiddleware
     *
     * @return void
     */
    public function setNextMiddleware(MiddlewareInterface $nextMiddleware)
    {
        $this->nextMiddleware = $nextMiddleware;
    }
    
    /**
     * {@inheritdoc}
     */
    public function process(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        $response = $this->before($request, $response);
        if ($this->nextMiddleware) {
            $response = $this->nextMiddleware->process($request, $response);
        }
        $response = $this->after($request, $response);
        
        return $response;
    }
    
    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return ResponseInterface
     */
    abstract protected function before(ServerRequestInterface $request, ResponseInterface $response);
    
    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return mixed
     */
    abstract protected function after(ServerRequestInterface $request, ResponseInterface $response);
}
