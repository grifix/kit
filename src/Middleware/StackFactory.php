<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Middleware;

use Grifix\Kernel\FactoryTrait;

/**
 * Class StackFactory
 *
 * @category Grifix
 * @package  Grifix\FirstMiddleware
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class StackFactory implements StackFactoryInterface
{
    
    use FactoryTrait;
    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $Stack = $this->makeClassName(Stack::class);
        return new $Stack();
    }
}
