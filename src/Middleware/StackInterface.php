<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Middleware;

use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;

/**
 * Interface StackInterface
 *
 * @category Grifix
 * @package  Grifix\FirstMiddleware
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface StackInterface
{
    
    /**
     * @param MiddlewareInterface $middleware
     *
     * @return static
     */
    public function add(MiddlewareInterface $middleware);
    
    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return ResponseInterface
     */
    public function call(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface;
}
