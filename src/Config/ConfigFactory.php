<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Config;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class ConfigFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Config
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ConfigFactory extends AbstractFactory implements ConfigFactoryInterface
{
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * ConfigFactory constructor.
     *
     * @param ArrayHelperInterface $arrayHelper
     * @param ClassMakerInterface $classMaker
     */
    public function __construct(
        ArrayHelperInterface $arrayHelper,
        ClassMakerInterface $classMaker
    ) {
        $this->arrayHelper = $arrayHelper;
        parent::__construct($classMaker);
    }

    /**
     * {@inheritdoc}
     */
    public function createConfig(array $values = []): ConfigInterface
    {
        $className = $this->makeClassName(Config::class);
        return new $className($values, $this->arrayHelper);
    }
}
