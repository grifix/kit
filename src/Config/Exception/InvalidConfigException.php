<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Config\Exception;

use Exception;

/**
 * Class InvalidConfigException
 *
 * @category Grifix
 * @package  Grifix\Kit\Config\Exceptions
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InvalidConfigException extends Exception
{
    protected $path;
    
    /**
     * InvalidConfigException constructor.
     *
     * @param string         $path
     * @param Exception|null $previous
     */
    public function __construct($path, Exception $previous = null)
    {
        $this->path = $path;
        $this->message = 'Config '.$path.' not returns an array!';
        parent::__construct($previous);
    }
}
