<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Config;

use Grifix\Kit\Helper\ArrayHelperInterface;

/**
 * Class Config
 *
 * @category Grifix
 * @package  Grifix\Kit\Config
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Config implements ConfigInterface
{
    protected $values = [];
    protected $arrayHelper;
    
    /**
     *
     * Config constructor.
     *
     * @param array                $values
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(array $values, ArrayHelperInterface $arrayHelper)
    {
        $this->values = $values;
        $this->arrayHelper = $arrayHelper;
    }
    
    /**
     * Returns config value
     *
     * @param $key
     * @param $defaultValue
     *
     * @return mixed
     */
    public function get($key, $defaultValue = null)
    {
        return $this->arrayHelper->get($this->values, $key, $defaultValue);
    }
    
    /**
     * Sets config value
     *
     * @param $key
     * @param $val
     *
     * @return static
     */
    public function set($key, $val)
    {
        $this->arrayHelper->set($this->values, $key, $val);
        
        return $this;
    }
    
    /**
     * Clears config
     *
     * @return static
     */
    public function clear()
    {
        $this->values = [];
        
        return $this;
    }
    
    /**
     * @param array $values
     *
     * @return $this
     */
    public function merge(array $values)
    {
        $this->values = $this->arrayHelper->merge($this->values, $values);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function toArray(): array
    {
        return $this->values;
    }
}
