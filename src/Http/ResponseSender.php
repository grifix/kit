<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http;

use Grifix\Kit\Http\Exception\HeadersSentException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ResponseSender
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ResponseSender implements ResponseSenderInterface
{
    /**
     * @inheritdoc
     */
    public function send(ResponseInterface $response)
    {
        if (headers_sent()) {
            throw new HeadersSentException($response);
        }
        
        header(
            sprintf(
                'HTTP/%s %s %s',
                $response->getProtocolVersion(),
                $response->getStatusCode(),
                $response->getReasonPhrase()
            ),
            true,
            $response->getStatusCode()
        );
        
        foreach ($response->getHeaders() as $header => $values) {
            foreach ($values as $value) {
                header($header . ': ' . $value, false, $response->getStatusCode());
            }
        }
        
        echo $response->getBody();
    }
}
