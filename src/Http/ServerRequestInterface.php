<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http;

use Psr\Http\Message\ServerRequestInterface as PsrServerRequestInterface;

/**
 * Interface ServerRequestInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ServerRequestInterface extends PsrServerRequestInterface
{
    const HTTP_ACCEPT_LANGUAGE = 'HTTP_ACCEPT_LANGUAGE';
    
    /**
     * Checks if request is ajax
     *
     * @return bool
     */
    public function isAjax();
    
    /**
     * @return bool
     */
    public function isSecured();
    
    /**
     * @return string
     */
    public function getHost();
    
    /**
     * @param      $name
     * @param null $defaultValue
     *
     * @return mixed
     */
    public function getQueryParam(string $name, $defaultValue = null);
    
    /**
     * @param string $name
     * @param null   $defaultValue
     *
     * @return mixed
     */
    public function getCookieParam(string $name, $defaultValue = null);
    
    /**
     * @return bool
     */
    public function isGet(): bool;
    
    /**
     * @return bool
     */
    public function isPost(): bool;
    
    /**
     * @param string $name
     * @param null   $defaultValue
     *
     * @return mixed
     */
    public function getParsedBodyParam(string $name, $defaultValue = null);
    
    /**
     * @param string $paramName
     * @param mixed   $defaultValue
     *
     * @return mixed
     */
    public function getServerParam(string $paramName, $defaultValue = null);
    
    /**
     * @return mixed|null
     */
    public function getClientIp();
    
    /**
     * @return string
     */
    public function getRelativeUri(): string;
}
