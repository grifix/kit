<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http;

/**
 * Class Server
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Server implements ServerInterface
{
    /**
     * @var ServerRequestInterface
     */
    protected $request;
    
    /**
     * Server constructor.
     *
     * @param ServerRequestInterface $request
     */
    public function __construct(ServerRequestInterface $request)
    {
        $this->request = $request;
    }
    
    /**
     * @param string $name
     * @param null   $defaultValue
     *
     * @return mixed
     */
    public function get(string $name, $defaultValue = null)
    {
        return $this->request->getServerParam($name, $defaultValue);
    }
    
    /**
     * @return mixed|null
     */
    public function getClientIp()
    {
        return $this->request->getClientIp();
    }
}