<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http;

use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

/**
 * Class ServerRequestFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ServerRequestFactoryInterface
{
    /**
     * Creates request
     *
     * @param string               $method  request method
     * @param UriInterface|string  $uri     request uri
     * @param array                $headers request headers
     * @param StreamInterface|null $body    request body
     * @param string               $version protocol version
     *
     * @return ServerRequestInterface
     */
    public function create(string $method, $uri, array $headers = [], $body = null, $version = null);
    
    /**
     * Creates request from global php variables
     *
     * @return ServerRequestInterface
     */
    public function createFromGlobals();
}
