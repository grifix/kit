<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

/**
 * Class Response
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Response implements ResponseInterface
{
    
    use MessageTrait;
    
    /**
     * @var ResponseInterface
     */
    protected $psr;
    
    /**
     * @var StreamFactoryInterface
     */
    protected $streamFactory;
    
    /**
     * Response constructor.
     *
     * @param PsrResponseInterface   $psrResponse
     * @param StreamFactoryInterface $streamFactory
     */
    public function __construct(PsrResponseInterface $psrResponse, StreamFactoryInterface $streamFactory)
    {
        $this->psr = $psrResponse;
        
        $this->streamFactory = $streamFactory;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getStatusCode()
    {
        return $this->psr->getStatusCode();
    }
    
    /**
     * {@inheritdoc}
     */
    public function withStatus($code, $reasonPhrase = '')
    {
        $new = clone($this);
        $new->psr = $this->psr->withStatus($code, $reasonPhrase);
        
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function withContent(string $content)
    {
        $new = clone ($this);
        $new->psr = $this->psr->withBody($this->streamFactory->createFormString($content));
        
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getReasonPhrase()
    {
        return $this->psr->getReasonPhrase();
    }
    
    /**
     * @param string $name
     * @param string $value
     *
     * @return Response
     */
    public function withCookie(string $name, string $value)
    {
        $new = clone($this);
        $new->psr = $this->psr->withAddedHeader('Set-Cookie', $name . '=' . $value);
        
        return $new;
    }
}
