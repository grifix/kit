<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Message\ServerRequestInterface as PsrServerRequestInterface;

/**
 * Class ServerRequest
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ServerRequest implements ServerRequestInterface
{
    /**
     * @var ServerRequestInterface
     */
    protected $psr;
    
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;
    
    use MessageTrait;
    
    /**
     * ServerRequest constructor.
     *
     * @param ServerRequestInterface $serverRequest
     * @param ArrayHelperInterface   $arrayHelper
     */
    public function __construct(PsrServerRequestInterface $serverRequest, ArrayHelperInterface $arrayHelper)
    {
        $this->psr = $serverRequest;
        $this->arrayHelper = $arrayHelper;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getAttribute($name, $default = null)
    {
        return $this->psr->getAttribute($name, $default);
    }
    
    /**
     * @inheritdoc
     */
    public function withAttribute($name, $value)
    {
        $new = clone($this);
        $new->psr = $this->psr->withAttribute($name, $value);
        
        return $new;
    }
    
    /**
     * @inheritdoc
     */
    public function withCookieParams(array $cookies)
    {
        $new = clone($this);
        $new->psr = $this->psr->withCookieParams($cookies);
        
        return $new;
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function withoutAttribute($name)
    {
        $new = clone($this);
        $new->psr = $this->psr->withoutAttribute($name);
        
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function withParsedBody($data)
    {
        $new = clone($this);
        $new->psr = $this->psr->withParsedBody($data);
        
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function withQueryParams(array $query)
    {
        $new = clone($this);
        $new->psr = $this->psr->withQueryParams($query);
        
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function withUploadedFiles(array $uploadedFiles)
    {
        $new = clone($this);
        $new->psr = $this->psr->withUploadedFiles($uploadedFiles);
        
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function withMethod($method)
    {
        $new = clone($this);
        $new->psr = $this->psr->withMethod($method);
        
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function withRequestTarget($requestTarget)
    {
        $new = clone($this);
        $new->psr = $this->psr->withRequestTarget($requestTarget);
        
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function withUri(UriInterface $uri, $preserveHost = false)
    {
        $new = clone($this);
        $new->psr = $this->psr->withUri($uri, $preserveHost);
        
        return $new;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getUri()
    {
        return $this->psr->getUri();
    }
    
    /**
     * @return string
     */
    public function getRelativeUri(): string
    {
        $result = $this->getUri()->getPath();
        if ($this->getUri()->getQuery()) {
            $result .= '?' . $this->getUri()->getQuery();
        }
        
        return $result;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getAttributes()
    {
        return $this->psr->getAttributes();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCookieParams()
    {
        return $this->psr->getCookieParams();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getParsedBody()
    {
        return $this->psr->getParsedBody();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getQueryParams()
    {
        return $this->psr->getQueryParams();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getServerParams()
    {
        return $this->psr->getServerParams();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getUploadedFiles()
    {
        return $this->psr->getUploadedFiles();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMethod()
    {
        return $this->psr->getMethod();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getRequestTarget()
    {
        return $this->psr->getRequestTarget();
    }
    
    /**
     * {@inheritdoc}
     */
    public function isAjax()
    {
        return in_array('XMLHttpRequest', $this->getHeader('X-Requested-With'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function isSecured()
    {
        return isset($this->getServerParams()['HTTPS']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getHost()
    {
        return $this->getServerParams()['HTTP_HOST'];
    }
    
    /**
     * {@inheritdoc}
     */
    public function getServerParam(string $paramName, $defaultValue = null)
    {
        return $this->arrayHelper->get($this->getServerParams(), $paramName, $defaultValue);
    }
    
    /**
     * @return bool
     */
    public function isGet(): bool
    {
        return $this->getMethod() === 'GET';
    }
    
    /**
     * @return mixed|null
     */
    public function getClientIp()
    {
        if ($this->getServerParam('HTTP_CLIENT_IP')) {
            return $this->getServerParam('HTTP_CLIENT_IP');
        }
        if ($this->getServerParam('HTTP_X_FORWARDED_FOR')) {
            return $this->getServerParam('HTTP_X_FORWARDED_FOR');
        }
        if ($this->getServerParam('REMOTE_ADDR')) {
            return $this->getServerParam('REMOTE_ADDR');
        }
        
        return null;
    }
    
    /**
     * @return bool
     */
    public function isPost(): bool
    {
        return $this->getMethod() === 'POST';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getQueryParam(string $name, $defaultValue = null)
    {
        return $this->arrayHelper->get($this->getQueryParams(), $name, $defaultValue);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getParsedBodyParam(string $name, $defaultValue = null)
    {
        return $this->arrayHelper->get($this->getParsedBody(), $name, $defaultValue);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCookieParam(string $name, $defaultValue = null)
    {
        return $this->arrayHelper->get($this->getCookieParams(), $name, $defaultValue);
    }
    
}
