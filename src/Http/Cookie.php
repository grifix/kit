<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http;

/**
 * Class Cookie
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Cookie implements CookieInterface
{
    protected $request;
    
    protected $data = [];
    
    /**
     * Cookie constructor.
     *
     * @param ServerRequestInterface $request
     */
    public function __construct(ServerRequestInterface $request)
    {
        $this->request = $request;
    }
    
    /**
     * {@inheritdoc}
     */
    public function set($name, $value): CookieInterface
    {
        $this->data[$name] = $value;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function get($name, $defaultValue = null)
    {
        return $this->request->getCookieParam($name, $defaultValue);
    }
    
    /**
     * {@inheritdoc}
     */
    public function processResponse(ResponseInterface $response): ResponseInterface
    {
        if ($this->data) {
            foreach ($this->data as $key => $val) {
                $response = $response->withCookie($key, $val);
            }
        }
        
        return $response;
    }
}