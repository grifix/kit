<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http;

use \Psr\Http\Message\ResponseInterface AS PsrResponseInterface;

/**
 * Interface ResponseInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ResponseInterface extends PsrResponseInterface
{
    
    /**
     * @param string $content
     *
     * @return Response
     */
    public function withContent(string $content);
    
    /**
     * @param string $name
     * @param string $value
     *
     * @return Response
     */
    public function withCookie(string $name, string $value);
}
