<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Http;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use GuzzleHttp\Psr7\Response as PsrResponse;

/**
 * Class ResponseFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ResponseFactory extends AbstractFactory implements ResponseFactoryInterface
{
    
    protected $streamFactory;
    
    /**
     * ResponseFactory constructor.
     *
     * @param StreamFactoryInterface $streamFactory
     * @param ClassMakerInterface    $classMaker
     */
    public function __construct(StreamFactoryInterface $streamFactory, ClassMakerInterface $classMaker)
    {
        $this->streamFactory = $streamFactory;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createResponse(
        $status = 200,
        array $headers = [],
        $body = null,
        $version = '1.1',
        $reason = null
    ) {
        $ResponseWrapper = $this->makeClassName(Response::class);
        
        return new $ResponseWrapper(new PsrResponse(
            $status,
            $headers,
            $body,
            $version,
            $reason
        ), $this->streamFactory);
    }
}
