<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Http;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;
use GuzzleHttp\Psr7\ServerRequest as PsrServerRequest;

/**
 * Class ServerRequestFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ServerRequestFactory extends AbstractFactory implements ServerRequestFactoryInterface
{

    protected $arrayHelper;

    /**
     * ServerRequestFactory constructor.
     *
     * @param ClassMakerInterface $classMaker
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(ClassMakerInterface $classMaker, ArrayHelperInterface $arrayHelper)
    {
        parent::__construct($classMaker);
        $this->arrayHelper = $arrayHelper;
    }

    /**
     * Create new instance of server request
     *
     * @param string $method HTTP method
     * @param string|UriInterface $uri URI
     * @param array $headers Request headers
     * @param string|null|resource|StreamInterface $body Request body
     * @param string $version Protocol version
     * @param array $serverParams Typically the $_SERVER superglobal
     *
     *
     * @return ServerRequestInterface
     */
    public function create(
        string $method,
        $uri,
        array $headers = [],
        $body = null,
        $version = null,
        $serverParams = []
    )
    {
        $ServerRequestWrapper = $this->makeClassName(ServerRequest::class);
        return new $ServerRequestWrapper(
            new PsrServerRequest($method, $uri, $headers, $body, $version, $serverParams),
            $this->arrayHelper
        );
    }

    /**
     * {@inheritDoc}
     *
     * @return ServerRequestInterface
     */
    public function createFromGlobals()
    {
        $ServerRequestWrapper = $this->makeClassName(ServerRequest::class);
        return new $ServerRequestWrapper(PsrServerRequest::fromGlobals(), $this->arrayHelper);
    }
}
