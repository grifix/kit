<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ui\Request;

use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Ioc\ServiceLocatorTrait;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class RequestHandlerFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RequestHandlerFactory implements RequestHandlerFactoryInterface
{
    use ServiceLocatorTrait;
    /**
     * @param string $requestAlias
     * @param array  $request
     *
     * @return RequestHandlerInterface
     */
    public function createRequestHandler(string $requestAlias, array $request = []): RequestHandlerInterface
    {
        $arr = explode('.', $requestAlias);
        foreach ($arr as &$v) {
            $v = ucfirst($v);
        }
        $vendor = array_shift($arr);
        $module = array_shift($arr);
        
        $handlerClass = $this->getShared(ClassMakerInterface::class)->makeClassName(
            $vendor . '\\' . $module . '\\Ui\\Http\\Request\\' . implode('\\', $arr) . 'RequestHandler'
        );
        
        return new $handlerClass($this->getShared(IocContainerInterface::class));
    }
}