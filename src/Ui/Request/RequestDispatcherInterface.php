<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Ui\Request;

use Grifix\Kit\Ui\Request\Exception\InvalidRequestMethodException;


/**
 * Class RequestDispatcher
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface RequestDispatcherInterface
{
    /**
     * @param string      $alias
     * @param array       $request
     * @param string|null $method
     *
     * @return array
     * @throws InvalidRequestMethodException
     */
    public function dispatch(string $alias, array $request = [], string $method = null): array;
}