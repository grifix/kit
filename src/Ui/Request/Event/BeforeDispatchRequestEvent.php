<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ui\Request\Event;

use Grifix\Kit\Ui\Request\RequestDispatcherInterface;

/**
 * Class BeforeDispatchRequestEvent
 *
 * @category Grifix
 * @package  Grifix\Admin\Ui\Http\Request\Event
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class BeforeDispatchRequestEvent
{
    /**
     * @var string
     */
    protected $requestAlias;
    
    /**
     * @var array
     */
    protected $request;
    
    /**
     * @var string
     */
    protected $method;

    /**
     * BeforeDispatchRequestEvent constructor.
     *
     * @param string $requestAlias
     * @param array $request
     * @param string|null $method
     */
    public function __construct(
        string $requestAlias,
        array $request = [],
        string $method = null
    ) {
        $this->requestAlias = $requestAlias;
        $this->request = $request;
        $this->method = $method;
    }
    
    /**
     * @return string
     */
    public function getRequestAlias(): string
    {
        return $this->requestAlias;
    }
    
    /**
     * @return array
     */
    public function getRequest(): array
    {
        return $this->request;
    }
    
    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }
    
}