<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ui\Request\Exception;

use Exception;

/**
 * Class InvalidRequestMethodException
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InvalidRequestMethodException extends Exception
{
    protected $givenMethod;
    
    protected $validMethod;
    
    protected $requestAlias;
    
    /**
     * InvalidRequestMethodException constructor.
     *
     * @param string $givenMethod
     * @param string $validMethod
     * @param string $requestAlias
     */
    public function __construct(string $givenMethod, string $validMethod, string $requestAlias)
    {
        $this->givenMethod = $givenMethod;
        $this->validMethod = $validMethod;
        $this->requestAlias = $requestAlias;
        
        $this->message = 'Request method for "'.$this->requestAlias.'" request handler must be "'.$validMethod.'" not "'.$givenMethod.'"!';
        parent::__construct();
    }
}