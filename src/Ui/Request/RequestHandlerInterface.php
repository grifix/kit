<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ui\Request;

/**
 * Class RequestHandlerInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface RequestHandlerInterface
{
    
    const METHOD_GET = 'GET'; //Handler can be run only using GET request or internal
    const METHOD_POST = 'POST'; //Handler can be run only using POST request or internal
    const METHOD_INTERNAL = 'INTERNAL'; //Handler cannot be run directly from HTTP, only from php code
    /**
     * @param array $request
     *
     * @return array
     */
    public function handle(array $request = []): array;
    
    /**
     * @return string
     */
    public function getMethod():string;
}