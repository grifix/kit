<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Ui\Request;

use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Ui\Request\Event\BeforeDispatchRequestEvent;
use Grifix\Kit\Ui\Request\Exception\InvalidRequestMethodException;

/**
 * Class RequestDispatcher
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RequestDispatcher implements RequestDispatcherInterface
{

    /**
     * @var RequestHandlerFactoryResolverInterface
     */
    protected $requestHandlerFactoryResolver;

    /**
     * @var EventBusInterface
     */
    protected $eventBus;

    /**
     * RequestDispatcher constructor.
     *
     * @param RequestHandlerFactoryResolverInterface $requestHandlerFactoryResolver
     * @param EventBusInterface $eventBus
     */
    public function __construct(
        RequestHandlerFactoryResolverInterface $requestHandlerFactoryResolver,
        EventBusInterface $eventBus
    ) {
        $this->requestHandlerFactoryResolver = $requestHandlerFactoryResolver;
        $this->eventBus = $eventBus;
    }

    /**
     * @param string $alias
     * @param array $request
     * @param string|null $method
     *
     * @return array
     * @throws InvalidRequestMethodException
     */
    public function dispatch(string $alias, array $request = [], string $method = null): array
    {
        $requestHandler = $this->requestHandlerFactoryResolver
            ->resolveFactory($alias, $request)
            ->createRequestHandler($alias, $request);
        if ($method && $requestHandler->getMethod() && $requestHandler->getMethod() != $method) {
            throw new InvalidRequestMethodException($method, $requestHandler->getMethod(), $alias);
        }

        $this->eventBus->trigger(new BeforeDispatchRequestEvent($alias, $request, $method));
        return $requestHandler->handle($request);
    }
}