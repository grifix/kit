<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ui\Request;

use Grifix\Kit\Cqrs\CqrsClientTrait;

/**
 * Class AbstractRequestHandler
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractRequestHandler implements RequestHandlerInterface
{
    use CqrsClientTrait;
    
    /**
     * @var string
     */
    protected $method = self::METHOD_INTERNAL;
    
    /**
     * {@inheritdoc}
     */
    public function getMethod(): string
    {
        return $this->method;
    }
}