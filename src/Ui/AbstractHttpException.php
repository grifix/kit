<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Ui;

use Grifix\Kit\Intl\TranslatorInterface;
use Throwable;

/**
 * Class AbstractHttpException
 *
 * @category Grifix
 * @package  Grifix\Kit\Ui\Request\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractHttpException extends \Exception
{
    
    /**
     * @var int
     */
    protected $code = 422;
    
    /**
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     * AbstractHttpException constructor.
     *
     * @param TranslatorInterface $translator
     * @param Throwable|null      $previous
     */
    public function __construct(TranslatorInterface $translator, Throwable $previous = null)
    {
        $this->translator = $translator;
        parent::__construct($this->createMessage(), $this->code, $previous);
    }
    
    /**
     * @return string
     */
    abstract protected function createMessage():string;
}
