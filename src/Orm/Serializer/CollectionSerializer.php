<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Collection\GenericCollectionInterface;
use Grifix\Kit\Orm\Collection\ArrayCollection;

/**
 * Class CollectionSerializer
 *
 * @package Grifix\Kit\OldRepository\Serializer\Property
 */
class CollectionSerializer implements SerializerInterface
{
    protected $collectionFactory;

    /**
     * CollectionSerializer constructor.
     * @param CollectionFactoryInterface $collectionFactory
     */
    public function __construct(CollectionFactoryInterface $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize($value)
    {
        if ($value instanceof GenericCollectionInterface) {
            return $value->toArray();
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function unSerialize($value)
    {
        if (is_array($value)) {
            return $this->collectionFactory->createCollection($value);
        }

        return $this->collectionFactory->createCollection();
    }
}