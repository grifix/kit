<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

/**
 * Class Serializer
 *
 * @package Grifix\Kit\OldRepository\Serializer
 */
interface ObjectSerializerInterface extends SerializerInterface
{
    /**
     * @return string
     */
    public function getObjectClass(): string;
}