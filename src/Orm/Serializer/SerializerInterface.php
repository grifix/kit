<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

use Grifix\Kit\OldRepository\Serializer\Type\Exception\PropertyIsNotExistsException;
use Grifix\Kit\OldRepository\Serializer\Type\TypeInterface;

/**
 * Class BlueprintInterface
 *
 * @package Grifix\Kit\OldRepository
 */
interface SerializerInterface
{
    /**
     * @param $value
     *
     * @return mixed
     */
    public function serialize($value);

    /**
     * @param $value
     *
     * @return mixed
     */
    public function unSerialize($value);
}