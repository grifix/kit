<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer\Exception;

use Throwable;

/**
 * Class UndefinedTypeException
 * @package Grifix\Kit\Orm\Serializer\Exception
 */
class UndefinedTypeException extends \Exception
{
    public function __construct()
    {
        parent::__construct('UndefinedType');
    }
}
