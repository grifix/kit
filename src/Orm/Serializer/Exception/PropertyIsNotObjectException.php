<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer\Exception;

/**
 * Class PropertyIsNotObjectException
 * @package Grifix\Kit\Orm\Serializer\Exception
 */
class PropertyIsNotObjectException extends \Exception
{
    protected $property;

    /***
     * PropertyIsNotObjectException constructor.
     * @param string $property
     */
    public function __construct(string $property)
    {
        parent::__construct(sprintf('Property "%s" is not object!', $property));
        $this->property = $property;
    }
}
