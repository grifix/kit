<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Orm\Serializer\Exception\ArgumentMustBeArrayException;
use Grifix\Kit\Orm\Serializer\Exception\ArgumentMustBeObjectException;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Type\EmailInterface;
use Grifix\Kit\Type\IpAddressInterface;
use Grifix\Kit\Type\TimeZoneInterface;
use Grifix\Kit\Type\UrlInterface;

/**
 * Class Serializer
 *
 * @package Grifix\Kit\OldRepository\Serializer
 */
class ObjectSerializer implements ObjectSerializerInterface
{

    /**
     * @var SerializerInterface[]
     */
    protected $propertySerializers = [];

    /**
     * @var string
     */
    protected $objectClass;

    /**
     * @var \ReflectionClass
     */
    protected $reflection;

    /**
     * @var SerializerFactoryInterface
     */
    protected $serializerFactory;

    /**
     * @var ClassHelperInterface
     */
    protected $objectHelper;

    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    protected $singleValueTypes = [
        EmailInterface::class,
        IpAddressInterface::class,
        UrlInterface::class,
        TimeZoneInterface::class
    ];

    /**
     * ObjectSerializer constructor.
     * @param string $objectClass
     * @param SerializerFactoryInterface $serializerFactory
     * @param ClassHelperInterface $objectHelper
     * @param IocContainerInterface $iocContainer
     * @param EntityManagerInterface $entityManager
     * @param ArrayHelperInterface $arrayHelper
     * @param array $propertySerializers
     */
    public function __construct(
        string $objectClass,
        SerializerFactoryInterface $serializerFactory,
        ClassHelperInterface $objectHelper,
        IocContainerInterface $iocContainer,
        EntityManagerInterface $entityManager,
        ArrayHelperInterface $arrayHelper,
        array $propertySerializers = []
    ) {
        $this->objectClass = $objectClass;
        $this->serializerFactory = $serializerFactory;
        $this->objectHelper = $objectHelper;
        $this->iocContainer = $iocContainer;
        $this->propertySerializers = $propertySerializers;
        $this->entityManager = $entityManager;
        $this->arrayHelper = $arrayHelper;
        $this->init();
    }


    protected function init(): void
    {
    }

    /**
     * @return \ReflectionClass
     * @throws \ReflectionException
     */
    protected function getReflection(): \ReflectionClass
    {
        if (!$this->reflection) {
            $this->reflection = new \ReflectionClass($this->objectClass);
        }
        return $this->reflection;
    }

    /**
     * @param string $property
     * @param SerializerInterface $serializer
     * @return ObjectSerializer
     */
    protected function addPropertySerializer(string $property, SerializerInterface $serializer): self
    {
        $this->propertySerializers[$property] = $serializer;
        return $this;
    }

    /**
     * @param string $property
     * @param string $objectClass
     * @param string|null $serializerClass
     * @return ObjectSerializer
     */
    protected function addObjectSerializer(
        string $property,
        string $objectClass,
        string $serializerClass = ObjectSerializer::class
    ): self {
        $this->addPropertySerializer(
            $property,
            $this->serializerFactory->createObjectSerializer($this->entityManager, $objectClass, $serializerClass)
        );
        return $this;
    }

    /**
     * @param string $property
     * @param array $mapping
     * @param string $typeColumn
     */
    protected function addMultiObjectSerializer(string $property, array $mapping, string $typeColumn = 'type')
    {
        $serializers = [];
        foreach ($mapping as $type => $class) {
            $serializers[$type] = $this->serializerFactory->createObjectSerializer($this->entityManager, $class);
        }
        $this->addPropertySerializer(
            $property,
            $this->serializerFactory->createMultiObjectSerializer($serializers, $typeColumn)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getObjectClass(): string
    {
        return $this->objectClass;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize($object = null)
    {
        if ($object === null) {
            return null;
        }
        if (!is_object($object)) {
            throw new ArgumentMustBeObjectException();
        }
        $result = [];
        foreach ($this->getReflection()->getProperties() as $property) {
            $serializer = $this->getPropertySerializer($property);
            if ($serializer) {
                $property->setAccessible(true);
                $result[$property->getName()] = $serializer->serialize($property->getValue($object));
                $property->setAccessible(false);
            }
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function unSerialize($data)
    {
        if (!is_array($data)) {
            throw new ArgumentMustBeArrayException();
        }
        $result = $this->getReflection()->newInstanceWithoutConstructor();
        foreach ($this->getReflection()->getProperties() as $property) {
            $serializer = $this->getPropertySerializer($property);
            if ($serializer) {
                $property->setAccessible(true);
                $property->setValue(
                    $result,
                    $serializer->unSerialize($this->arrayHelper->get($data, $property->getName()))
                );
                $property->setAccessible(false);
            }
            if ($this->isDependency($property)) {
                $this->injectDependency($property, $result);
            }
        }
        return $result;
    }


    /**
     * @param \ReflectionProperty $property
     * @param object $object
     */
    protected function injectDependency(\ReflectionProperty $property, $object): void
    {
        $type = $this->detectType($property);
        if (strpos($type, '\\') === 0) {
            $type = substr($type, 1);
        }
        $dependency = $this->iocContainer->get($type);
        $property->setAccessible(true);
        $property->setValue($object, $dependency);
        $property->setAccessible(false);
    }

    /**
     * @param \ReflectionProperty $property
     * @return SerializerInterface|null
     */
    protected function getPropertySerializer(\ReflectionProperty $property): ?SerializerInterface
    {
        if (isset($this->propertySerializers[$property->getName()])) {
            return $this->propertySerializers[$property->getName()];
        }
        if ($this->isDependency($property) || $this->isRelatedCollection($property)) {
            return null;
        }

        $type = $this->detectType($property);
        if ($this->isCollection($type)) {
            return $this->serializerFactory->createCollectionSerializer();
        }

        if ($this->isScalarType($type)) {
            return $this->serializerFactory->createSimpleSerializer(ScalarPropertySerializer::class);
        }

        if ($this->objectHelper->isInstanceOf($type, \DateTimeInterface::class)) {
            return $this->serializerFactory->createSimpleSerializer(DatePropertySerializer::class);
        }

        if ($this->isSingleValueType($type)) {
            return $this->serializerFactory->createSingleValuePropertySerializer($type);
        }

        return $this->serializerFactory->createObjectSerializer($this->entityManager, $type);
    }

    /**
     * @param \ReflectionProperty $property
     * @return bool
     */
    protected function isDependency(\ReflectionProperty $property): bool
    {
        if (!$property->getDocComment()) {
            return false;
        }
        return strpos($property->getDocComment(), '@dependency') !== false;
    }

    /**
     * @param \ReflectionProperty $property
     * @return bool
     */
    protected function isRelatedCollection(\ReflectionProperty $property): bool
    {
        if (!$property->getDocComment()) {
            return false;
        }
        return strpos($property->getDocComment(), '@relation') !== false;
    }

    /**
     * @param $type
     * @return bool
     */
    protected function isCollection($type): bool
    {
        return strpos($type, 'CollectionInterface') !== false;
    }

    /**
     * @param string $type
     * @return bool
     */
    protected function isScalarType(string $type)
    {
        return in_array($type, ['string', 'float', 'int', 'bool']);
    }

    /**
     * @param \ReflectionProperty $property
     * @return mixed|string
     */
    protected function detectType(\ReflectionProperty $property)
    {
        if (!$property->getDocComment()) {
            return 'string';
        }
        $matches = [];
        preg_match('/@var\s(\S*)/', $property->getDocComment(), $matches);
        if (!$matches || !isset($matches[1])) {
            return 'string';
        }
        return $matches[1];
    }

    /**
     * @param string $type
     * @return bool
     */
    protected function isSingleValueType(string $type)
    {
        foreach ($this->singleValueTypes as $singleValueType) {
            if ($this->objectHelper->isInstanceOf($type, $singleValueType)) {
                return true;
            }
        }
        return false;
    }
}
