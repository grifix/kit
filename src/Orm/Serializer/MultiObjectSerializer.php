<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

use Grifix\Kit\Orm\Serializer\Exception\ArgumentMustBeArrayException;
use Grifix\Kit\Orm\Serializer\Exception\ArgumentMustBeObjectException;
use Grifix\Kit\Orm\Serializer\Exception\SerializerNotExistsException;
use Grifix\Kit\Orm\Serializer\Exception\UndefinedTypeException;

/**
 * Class MultiObjectSerialzer
 * @package Grifix\Kit\Orm\Serializer
 */
class MultiObjectSerializer implements SerializerInterface
{

    /**
     * @var ObjectSerializerInterface[]
     */
    protected $objectSerializers;

    /**
     * @var string
     */
    protected $typeColumn;

    /**
     * MultiObjectSerializer constructor.
     * @param array $objectSerializers
     * @param string $typeColumn
     */
    public function __construct(array $objectSerializers, $typeColumn = 'type')
    {
        $this->objectSerializers = $objectSerializers;
        $this->typeColumn = $typeColumn;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize($value)
    {
        if (!is_object($value)) {
            throw new ArgumentMustBeObjectException();
        }
        $class = get_class($value);
        $result = $this->getSerializerByClass($class)->serialize($value);
        $result[$this->typeColumn] = $this->getType($class);
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function unSerialize($value)
    {
        if (!is_array($value)) {
            throw new ArgumentMustBeArrayException();
        }
        if (!isset($value[$this->typeColumn])) {
            throw new UndefinedTypeException();
        }
        return $this->getSerializerByType($value[$this->typeColumn])->unSerialize($value);
    }

    /**
     * @param string $objectClass
     * @return ObjectSerializerInterface
     * @throws SerializerNotExistsException
     */
    protected function getSerializerByClass(string $objectClass): ObjectSerializerInterface
    {
        foreach ($this->objectSerializers as $serializer) {
            if ($serializer->getObjectClass() == $objectClass) {
                return $serializer;
            }
        }

        throw new SerializerNotExistsException($objectClass);
    }

    /**
     * @param string $type
     * @return ObjectSerializerInterface
     * @throws SerializerNotExistsException
     */
    protected function getSerializerByType(string $type): ObjectSerializerInterface
    {
        if (isset($this->objectSerializers[$type])) {
            return $this->objectSerializers[$type];
        }

        throw new SerializerNotExistsException($type);
    }

    /**
     * @param string $objectClass
     * @return string
     * @throws SerializerNotExistsException
     */
    protected function getType(string $objectClass): string
    {
        foreach ($this->objectSerializers as $type => $serializer) {
            if ($serializer->getObjectClass() == $objectClass) {
                return $type;
            }
        }

        throw new SerializerNotExistsException($objectClass);
    }
}
