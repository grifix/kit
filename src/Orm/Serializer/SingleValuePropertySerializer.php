<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

use Grifix\Kit\Type\ValuableTypeInterface;

/**
 * Class DateSerializer
 *
 * @package Grifix\Kit\OldRepository\Serializer\Property
 */
class SingleValuePropertySerializer implements SerializerInterface
{

    protected $class;

    /**
     * TypePropertySerializer constructor.
     * @param string $class
     */
    public function __construct(string $class)
    {
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize($value)
    {
        if (!is_null($value) && !is_null($value->getValue())) {
            assert($value instanceof ValuableTypeInterface);
            return $value->getValue();
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function unSerialize($value)
    {
        $class = $this->class;
        if ($value) {
            return new $class($value);
        }

        return null;
    }
}
