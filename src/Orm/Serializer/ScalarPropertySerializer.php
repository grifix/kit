<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

/**
 * Class StringPropertySerializer
 * @package Grifix\Kit\OldRepository\Serializer
 */
class ScalarPropertySerializer implements SerializerInterface
{

    /**
     * {@inheritdoc}
     */
    public function serialize($value)
    {
        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function unSerialize($value)
    {
        return $value;
    }
}