<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class SerializerFactory
 *
 * @package Grifix\Kit\OldRepository\Serializer\Property
 */
interface SerializerFactoryInterface
{
    /**
     * @param string $serializerClass
     * @return SerializerInterface
     */
    public function createSimpleSerializer(string $serializerClass): SerializerInterface;

    /**
     * @param EntityManagerInterface $entityManager
     * @param string $objectClass
     * @param string $serializerClass
     * @return ObjectSerializerInterface
     */
    public function createObjectSerializer(
        EntityManagerInterface $entityManager,
        string $objectClass,
        string $serializerClass = ObjectSerializer::class
    ): ObjectSerializerInterface;

    /**
     * @param callable $serialize
     * @param callable $unSerialize
     * @return SerializerInterface
     */
    public function createCallableSerializer(callable $serialize, callable $unSerialize): SerializerInterface;

    /**
     * @param string $propertyClass
     * @return SerializerInterface
     */
    public function createSingleValuePropertySerializer(string $propertyClass): SerializerInterface;

    /**
     * @param array $serializers
     * @param string $typeColumn
     * @return SerializerInterface
     */
    public function createMultiObjectSerializer(array $serializers, string $typeColumn = 'type'): SerializerInterface;

    /**
     * @return SerializerInterface
     */
    public function createCollectionSerializer(): SerializerInterface;
}