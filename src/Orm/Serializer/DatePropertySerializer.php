<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

/**
 * Class DateSerializer
 *
 * @package Grifix\Kit\OldRepository\Serializer\Property
 */
class DatePropertySerializer implements SerializerInterface
{

    protected $format = 'Y-m-d H:i:s';

    /**
     * {@inheritdoc}
     */
    public function serialize($value)
    {
        if ($value instanceof \DateTimeInterface) {
            return $value->format($this->format);
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function unSerialize($value)
    {
        if ($value) {
            return new \DateTimeImmutable($value);
        }

        return null;
    }

    /**
     * @param string $format
     */
    public function setFormat(string $format)
    {
        $this->format = $format;
    }
}