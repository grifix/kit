<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class SerializerFactory
 *
 * @package Grifix\Kit\OldRepository\Serializer\Property
 */
class SerializerFactory extends AbstractFactory implements SerializerFactoryInterface
{

    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    /**
     * @var ClassHelperInterface
     */
    protected $objectHelper;

    /**
     * @var CollectionFactoryInterface
     */
    protected $collectionFactory;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * SerializerFactory constructor.
     *
     * @param ClassMakerInterface $classMaker
     * @param IocContainerInterface $iocContainer
     * @param ClassHelperInterface $objectHelper
     * @param CollectionFactoryInterface $collectionFactory
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        IocContainerInterface $iocContainer,
        ClassHelperInterface $objectHelper,
        CollectionFactoryInterface $collectionFactory,
        ArrayHelperInterface $arrayHelper
    ) {
        parent::__construct($classMaker);
        $this->iocContainer = $iocContainer;
        $this->objectHelper = $objectHelper;
        $this->collectionFactory = $collectionFactory;
        $this->arrayHelper = $arrayHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function createSimpleSerializer(string $serializerClass): SerializerInterface
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->iocContainer->createNewInstance($this->makeClassName($serializerClass));
    }

    /**
     * {@inheritdoc}
     */
    public function createObjectSerializer(
        EntityManagerInterface $entityManager,
        string $objectClass,
        string $serializerClass = ObjectSerializer::class
    ): ObjectSerializerInterface {
        /** @var ObjectSerializer $class */
        $class = $this->makeClassName($serializerClass);
        return new $class(
            $objectClass,
            $this,
            $this->objectHelper,
            $this->iocContainer,
            $entityManager,
            $this->arrayHelper
        );
    }

    /**
     * {@inheritdoc}
     */
    public function createMultiObjectSerializer(array $serializers, string $typeColumn = 'type'): SerializerInterface
    {
        $class = $this->makeClassName(MultiObjectSerializer::class);
        return new $class($serializers, $typeColumn);
    }

    /**
     * {@inheritdoc}
     */
    public function createCallableSerializer(callable $serialize, callable $unSerialize): SerializerInterface
    {
        $class = $this->makeClassName(PropertySerializer::class);
        return new $class($serialize, $unSerialize);
    }

    /**
     * {@inheritdoc}
     */
    public function createSingleValuePropertySerializer(string $propertyClass): SerializerInterface
    {
        $class = $this->makeClassName(SingleValuePropertySerializer::class);
        return new $class($propertyClass);
    }

    /**
     * @return SerializerInterface
     */
    public function createCollectionSerializer(): SerializerInterface
    {
        $class = $this->makeClassName(CollectionSerializer::class);
        return new $class($this->collectionFactory);
    }
}
