<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Serializer;

/**
 * Class Property
 */
class PropertySerializer implements SerializerInterface
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var callable|null
     */
    protected $serializer;

    /**
     * @var callable|null
     */
    protected $unSerializer;

    /**
     * Property constructor.
     *
     * @param callable|null $serializer
     * @param callable|null $unSerializer
     */
    public function __construct(
        callable $serializer,
        callable $unSerializer
    ) {
        $this->serializer = $serializer;
        $this->unSerializer = $unSerializer;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize($object)
    {
        return call_user_func($this->serializer, [$object]);
    }

    /**
     * {@inheritdoc}
     */
    public function unSerialize($data)
    {
        return call_user_func($this->unSerializer, [$data]);
    }
}
