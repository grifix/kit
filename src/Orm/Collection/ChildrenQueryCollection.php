<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection;

use Grifix\Kit\Collection\Finder\CollectionFinderInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Orm\Collection\Exception\ParentEntityNotMatchException;
use Grifix\Kit\Specification\Injector\SpecificationInjectorFactoryInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class ChildrenCollection
 * @package Grifix\Kit\Orm\Collection
 */
class ChildrenQueryCollection extends QueryCollection
{
    /**
     * @var object
     */
    protected $parentEntity;

    /**
     * ChildrenCollection constructor.
     * @param string $entityClass
     * @param QueryInterface $query
     * @param EntityManagerInterface $entityManager
     * @param ArrayHelperInterface $arrayHelper
     * @param CollectionFactoryInterface $collectionFactory
     * @param $parentEntity
     * @param SpecificationInjectorFactoryInterface $specificationInjectorFactory
     * @param CollectionFinderInterface $collectionFinder
     * @param array $entities
     * @throws \Grifix\Kit\Orm\Blueprint\Exception\RelationToParentNotExists
     */
    public function __construct(
        string $entityClass,
        QueryInterface $query,
        EntityManagerInterface $entityManager,
        ArrayHelperInterface $arrayHelper,
        CollectionFactoryInterface $collectionFactory,
        $parentEntity,
        SpecificationInjectorFactoryInterface $specificationInjectorFactory,
        CollectionFinderInterface $collectionFinder,
        array $entities = []
    ) {
        $this->entityManager = $entityManager;
        $blueprint = $this->entityManager->getBlueprint($entityClass);
        $parentBlueprint = $this->entityManager->getBlueprint(get_class($parentEntity));
        $parentId = $this->entityManager->getEntityId($parentEntity);
        $relation = $blueprint->getRelationToParent($parentBlueprint->getEntityClass());

        $query->where('"' . $relation->getChildColumn() . '"' . '= :parentId')->bindValue('parentId', $parentId);
        $this->parentEntity = $parentEntity;
        parent::__construct(
            $entityClass,
            $query,
            $entityManager,
            $arrayHelper,
            $collectionFactory,
            $specificationInjectorFactory,
            $collectionFinder,
            $entities
        );
    }

    /**
     * {@inheritdoc}
     */
    public function add($entity): void
    {
        parent::add($entity);
        $this->entityManager->setParent($entity, $this->parentEntity);
    }

    /**
     * {@inheritdoc}
     */
    public function remove($entity): void
    {
        if ($this->entityManager->getParent($entity) !== $this->parentEntity) {
            throw new ParentEntityNotMatchException($this->entityManager->getParent($entity), $this->parentEntity);
        }
        parent::remove($entity);
    }
}
