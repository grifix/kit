<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection;

use Grifix\Kit\Collection\ArrayWrapperTrait;
use Grifix\Kit\Collection\Finder\CollectionFinderInterface;
use Grifix\Kit\Specification\SpecificationInterface;
use Grifix\Kit\Type\ValuableTypeInterface;

/**
 * Class ArrayCollection
 * @package Grifix\Kit\Orm\Collection
 */
class ArrayCollection implements CollectionInterface
{
    use ArrayWrapperTrait {
        __construct as traitConstruct;
    }

    /**
     * @var CollectionFinderInterface
     */
    protected $collectionFinder;

    /**
     * ArrayCollection constructor.
     * @param CollectionFinderInterface $collectionFinder
     * @param array $elements
     */
    public function __construct(CollectionFinderInterface $collectionFinder, array $elements = [])
    {
        $this->traitConstruct($elements);
        $this->collectionFinder = $collectionFinder;
    }

    /**
     * {@inheritdoc}
     */
    public function find(string $id)
    {
        foreach ($this->elements as $element) {
            if ($this->getId($element) === $id) {
                return $element;
            }
        }
        return null;
    }

    /**
     * @param string $property
     * @param $value
     * @return ArrayCollection
     * @throws \ReflectionException
     */
    public function findBy(string $property, $value): CollectionInterface
    {
        return new self($this->collectionFinder, $this->collectionFinder->find($this, $property, $value)->toArray());
    }

    /**
     * @param $entity
     * @return string
     * @throws \ReflectionException
     */
    private function getId($entity): string
    {
        $reflection = new \ReflectionClass($entity);
        $property = $reflection->getProperty('id');
        $property->setAccessible(true);
        $result = $property->getValue($entity);
        $property->setAccessible(false);
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function match(SpecificationInterface $specification): CollectionInterface
    {
        $array = [];
        foreach ($this->elements as $element) {
            if ($specification->isSatisfiedBy($element)) {
                $array[] = $element;
            }
        }
        return new self($this->collectionFinder, $array);
    }
}
