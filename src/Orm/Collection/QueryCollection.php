<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection;

use Grifix\Kit\Collection\ArrayWrapperTrait;
use Grifix\Kit\Collection\Finder\CollectionFinderInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Orm\Collection\Exception\EntityNotExistsException;
use Grifix\Kit\Orm\Collection\Exception\InvalidEntityClassException;
use Grifix\Kit\Specification\Injector\SpecificationInjectorFactoryInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Specification\SpecificationInterface;

/**
 * Class Orm
 * @package Grifix\Kit\Orm
 */
class QueryCollection implements CollectionInterface
{
    use ArrayWrapperTrait;

    /**
     * @var array
     */
    protected $entities = [];

    /**
     * @var QueryInterface
     */
    protected $query;

    /**
     * @var bool
     */
    protected $loaded = false;

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var CollectionFactoryInterface
     */
    protected $collectionFactory;

    /**
     * @var SpecificationInjectorFactoryInterface
     */
    protected $specificationInjectorFactory;

    /**
     * @var CollectionFinderInterface
     */
    protected $collectionFinder;

    /**
     * Collection constructor.
     * @param string $entityClass
     * @param QueryInterface $query
     * @param EntityManagerInterface $entityManager
     * @param ArrayHelperInterface $arrayHelper
     * @param CollectionFactoryInterface $collectionFactory
     * @param SpecificationInjectorFactoryInterface $specificationInjectorFactory
     * @param CollectionFinderInterface $collectionFinder
     * @param array $entities
     */
    public function __construct(
        string $entityClass,
        QueryInterface $query,
        EntityManagerInterface $entityManager,
        ArrayHelperInterface $arrayHelper,
        CollectionFactoryInterface $collectionFactory,
        SpecificationInjectorFactoryInterface $specificationInjectorFactory,
        CollectionFinderInterface $collectionFinder,
        array $entities = []
    ) {
        $this->entityClass = $entityClass;
        $this->query = $query;
        $this->entityManager = $entityManager;
        $this->arrayHelper = $arrayHelper;
        $this->collectionFactory = $collectionFactory;
        $this->specificationInjectorFactory = $specificationInjectorFactory;
        $this->collectionFinder = $collectionFinder;
        foreach ($entities as $entity) {
            $this->add($entity);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function findBy(string $property, $value): CollectionInterface
    {
        $query = clone $this->query;
        $query->where("data->'" . $property . "' = :value")->bindValue('value', json_encode($value));
        $result = new static(
            $this->entityClass,
            $query,
            $this->entityManager,
            $this->arrayHelper,
            $this->collectionFactory,
            $this->specificationInjectorFactory,
            $this->collectionFinder
        );

        $result->entities = $this->collectionFinder->find($this, $property, $value)->toArray();
        if (count($result->entities) > 0) {
            $result->loaded = true;
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    protected function setCollectionArray(
        array $elements
    ) {
        $this->entities = $elements;
    }

    /**
     * {@inheritdoc}
     */
    protected function getCollectionArray(): array
    {
        $this->load();
        return $this->entities;
    }

    protected function load(): void
    {
        if (!$this->loaded) {
            $records = $this->query->fetchAll();
            foreach ($records as $record) {
                $data = json_decode($record['data'], true);
                $entityClass = $this->entityManager->getBlueprint($this->entityClass)->detectEntityClass($data);
                if (!$this->entityManager->hasEntity($entityClass, $record['id'])) {
                    $this->addEntity(
                        $this->entityManager->unSerialize($data, $this->entityClass),
                        false
                    );
                } else {
                    $this->addEntity($this->entityManager->getEntity($entityClass, $record['id']));
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function add(
        $entity
    ): void {
        $this->checkEntityClassOrFail($entity);

        $this->addEntity(
            $entity,
            !$this->entityManager->hasEntity(get_class($entity), $this->entityManager->getEntityId($entity))
        );
    }

    /**
     * @param $entity
     * @param bool $isNew
     */
    protected function addEntity(
        $entity,
        $isNew = false
    ) {
        $this->entities[$this->entityManager->getEntityId($entity)] = $entity;
        $this->entityManager->watch($entity, $isNew);
    }

    /**
     * @param $entity
     * @throws InvalidEntityClassException
     */
    protected function checkEntityClassOrFail(
        $entity
    ): void {
        if (!($entity instanceof $this->entityClass)) {
            throw new InvalidEntityClassException(get_class($entity), $this->entityClass);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function find(
        string $id
    ) {
        if (!isset($this->entities[$id])) {
            $query = clone ($this->query);
            $query->where('id = :id')->bindValue('id', $id);
            $record = $query->fetch();
            if ($record) {
                $this->entities[$id] = $this->entityManager->unSerialize(
                    json_decode($record['data'], true),
                    $this->entityClass
                );
                $this->entityManager->watch($this->entities[$id], false);
            }
        }
        return $this->arrayHelper->get($this->entities, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function remove(
        $entity
    ): void {
        $this->checkEntityClassOrFail($entity);
        $id = $this->entityManager->getEntityId($entity);
        if (!$this->find($id)) {
            throw new EntityNotExistsException($this->entityClass, $id);
        }
        $this->entityManager->delete($entity);
        unset($this->entities[$this->entityManager->getEntityId($entity)]);
    }



    /**
     * {@inheritdoc}
     */
    public function match(
        SpecificationInterface $specification
    ): CollectionInterface {
        $query = clone $this->query;
        $injector = $this->specificationInjectorFactory->createJsonbSpecificationInjector(
            $query
        );
        $injector->inject($specification);
        return new static(
            $this->entityClass,
            $query,
            $this->entityManager,
            $this->arrayHelper,
            $this->collectionFactory,
            $this->specificationInjectorFactory,
            $this->collectionFinder
        );
    }
}
