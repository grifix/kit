<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection\Exception;

/**
 * Class EntityNotExistsException
 * @package Grifix\Kit\Orm\Collection\Exception
 */
class EntityNotExistsException extends \Exception
{
    protected $id;

    protected $class;

    public function __construct(string $id, string $class)
    {
        $this->id = $id;
        $this->class = $class;
        parent::__construct(sprintf('Entity "%s" with id "%s" not exists!', $class, $id));
    }
}
