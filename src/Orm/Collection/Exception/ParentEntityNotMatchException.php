<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection\Exception;

/**
 * Class ParentEntityNotMatchException
 * @package Grifix\Kit\Orm\Collection\Exception
 */
class ParentEntityNotMatchException extends \Exception
{
    protected $givenParentEntity;

    protected $collectionParentEntity;

    /**
     * ParentEntityNotMatchException constructor.
     * @param $givenParentEntity
     * @param $collectionParentEntity
     */
    public function __construct($givenParentEntity, $collectionParentEntity)
    {
        parent::__construct('Parent entity does not match!');
        $this->givenParentEntity = $givenParentEntity;
        $this->collectionParentEntity = $collectionParentEntity;
    }
}
