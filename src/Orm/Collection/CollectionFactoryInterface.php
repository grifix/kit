<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection;

use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class CollectionFactory
 * @package Grifix\Kit\Orm\Collection
 */
interface CollectionFactoryInterface
{
    /**
     * @param EntityManagerInterface $entityManager
     * @param string $entityClass
     * @param array $elements
     * @param null $parentEntity
     * @return CollectionInterface
     */
    public function createQueryCollection(
        EntityManagerInterface $entityManager,
        string $entityClass,
        array $elements = [],
        $parentEntity = null
    ): CollectionInterface;

    /**
     * @param EntityManagerInterface $entityManager
     * @param string $wrapperClass
     * @param string $entityClass
     * @param array $elements
     * @param $parentEntity
     * @return mixed
     */
    public function createQueryCollectionWrapper(
        EntityManagerInterface $entityManager,
        string $wrapperClass,
        string $entityClass,
        array $elements = [],
        $parentEntity = null
    );

    /**
     * @param object[] $elements
     * @return CollectionInterface
     */
    public function createArrayCollection(array $elements = []): CollectionInterface;

    /**
     * @param string $wrapperClass
     * @param array $elements
     * @return mixed
     */
    public function createArrayCollectionWrapper(string $wrapperClass, array $elements = []);
}
