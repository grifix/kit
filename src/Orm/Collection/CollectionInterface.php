<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection;

use Grifix\Kit\Collection\GenericCollectionInterface;
use Grifix\Kit\Specification\SpecificationInterface;

/**
 * Interface RepositoryInterace
 * @package Grifix\Kit\Orm
 */
interface CollectionInterface extends GenericCollectionInterface
{
    /**
     * @param string $id
     * @return object
     */
    public function find(string $id);

    /**
     * @param SpecificationInterface $specification
     * @return CollectionInterface
     */
    public function match(SpecificationInterface $specification): CollectionInterface;

    /**
     * @param object $entity
     * @return void
     */
    public function add($entity): void;

    /**
     * @param $entity
     * @return void
     */
    public function remove($entity): void;

    /**
     * @param string $property
     * @param $value
     * @return ArrayCollection
     * @throws \ReflectionException
     */
    public function findBy(string $property, $value): CollectionInterface;


    /**
     * @return mixed
     */
    public function first();

    /**
     * @return mixed
     */
    public function last();
}
