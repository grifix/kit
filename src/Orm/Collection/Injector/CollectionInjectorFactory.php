<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection\Injector;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Orm\Collection\CollectionFactoryInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class CollectionInjectorFactory
 * @package Grifix\Kit\Orm\Collection\Injector
 */
class CollectionInjectorFactory extends AbstractFactory implements CollectionInjectorFactoryInterface
{
    /**
     * @var CollectionFactoryInterface
     */
    protected $collectionFactory;

    /**
     * CollectionInjectorFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param CollectionFactoryInterface $collectionFactory
     */
    public function __construct(ClassMakerInterface $classMaker, CollectionFactoryInterface $collectionFactory)
    {
        parent::__construct($classMaker);
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @return CollectionInjectorInterface
     */
    public function createCollectionInjector(EntityManagerInterface $entityManager): CollectionInjectorInterface
    {
        $class = $this->makeClassName(CollectionInjector::class);
        return new $class($entityManager, $this->collectionFactory);
    }
}
