<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection\Injector;

/**
 * Class CollectionInjector
 * @package Grifix\Kit\Orm\Collection
 */
interface CollectionInjectorInterface
{
    /**
     * @param $entity
     * @throws \ReflectionException
     */
    public function injectQueryCollections($entity);
}