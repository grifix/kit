<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection\Injector;

use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class CollectionInjectorFactory
 * @package Grifix\Kit\Orm\Collection\Injector
 */
interface CollectionInjectorFactoryInterface
{
    /**
     * @param EntityManagerInterface $entityManager
     * @return CollectionInjectorInterface
     */
    public function createCollectionInjector(EntityManagerInterface $entityManager): CollectionInjectorInterface;
}