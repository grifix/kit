<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Collection;

use Grifix\Kit\Collection\Finder\CollectionFinderInterface;
use Grifix\Kit\Db\Query\QueryFactoryInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Specification\Injector\SpecificationInjectorFactoryInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Collection\CollectionFactoryInterface as BaseCollectionFactoryInterface;

/**
 * Class CollectionFactory
 * @package Grifix\Kit\Orm\Collection
 */
class CollectionFactory extends AbstractFactory implements CollectionFactoryInterface
{

    /**
     * @var BaseCollectionFactoryInterface
     */
    protected $baseCollectionFactory;

    /**
     * @var QueryFactoryInterface
     */
    protected $queryFactory;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var SpecificationInjectorFactoryInterface
     */
    protected $specificationInjectorFactory;

    /**
     * @var CollectionFinderInterface
     */
    protected $collectionFinder;

    /**
     * CollectionFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param BaseCollectionFactoryInterface $baseCollectionFactory
     * @param QueryFactoryInterface $queryFactory
     * @param ArrayHelperInterface $arrayHelper
     * @param SpecificationInjectorFactoryInterface $specificationInjectorFactory
     * @param CollectionFinderInterface $collectionFinder
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        BaseCollectionFactoryInterface $baseCollectionFactory,
        QueryFactoryInterface $queryFactory,
        ArrayHelperInterface $arrayHelper,
        SpecificationInjectorFactoryInterface $specificationInjectorFactory,
        CollectionFinderInterface $collectionFinder
    ) {
        parent::__construct($classMaker);
        $this->baseCollectionFactory = $baseCollectionFactory;
        $this->queryFactory = $queryFactory;
        $this->arrayHelper = $arrayHelper;
        $this->specificationInjectorFactory = $specificationInjectorFactory;
        $this->collectionFinder = $collectionFinder;
    }

    /**
     * {@inheritdoc}
     */
    public function createQueryCollection(
        EntityManagerInterface $entityManager,
        string $entityClass,
        array $elements = [],
        $parentEntity = null
    ): CollectionInterface {
        $blueprint = $entityManager->getBlueprint($entityClass);
        $query = $this->queryFactory->createQuery($entityManager->getConnection());
        $query->select('*')->from($blueprint->getTable());
        if (!$parentEntity) {
            $class = $this->makeClassName(QueryCollection::class);
            return new $class(
                $entityClass,
                $query,
                $entityManager,
                $this->arrayHelper,
                $this,
                $this->specificationInjectorFactory,
                $this->collectionFinder,
                $elements
            );
        } else {
            $class = $this->makeClassName(ChildrenQueryCollection::class);
            return new $class(
                $entityClass,
                $query,
                $entityManager,
                $this->arrayHelper,
                $this,
                $parentEntity,
                $this->specificationInjectorFactory,
                $this->collectionFinder,
                $elements
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function createArrayCollection(array $elements = []): CollectionInterface
    {
        $class = $this->makeClassName(ArrayCollection::class);
        return new $class($this->collectionFinder, $elements);
    }

    /**
     * {@inheritdoc}
     */
    public function createQueryCollectionWrapper(
        EntityManagerInterface $entityManager,
        string $wrapperClass,
        string $entityClass,
        array $elements = [],
        $parentEntity = null
    ) {
        $class = $this->makeClassName($wrapperClass);
        return new $class($this->createQueryCollection($entityManager, $entityClass, $elements, $parentEntity));
    }

    /**
     * {@inheritdoc}
     */
    public function createArrayCollectionWrapper(string $wrapperClass, array $elements = [])
    {
        $class = $this->makeClassName($wrapperClass);
        return new $class($this->createArrayCollection($elements));
    }
}
