<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Repository;


use Grifix\Kit\Collection\GenericCollectionInterface;
use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Specification\SpecificationInterface;

/**
 * Class Repository
 * @package Grifix\Kit\Orm\Repository
 */
interface RepositoryInterface extends GenericCollectionInterface
{
    /**
     * @param string $id
     * @return object|null
     */
    public function find(string $id): ?object;

    /**
     * @param object $entity
     */
    public function save(object $entity): void;

    /**
     * @param object $entity
     */
    public function delete(object $entity): void;

    /**
     * @param SpecificationInterface $specification
     * @return CollectionInterface
     */
    public function match(SpecificationInterface $specification): CollectionInterface;

    /**
     * @return mixed
     */
    public function first();

    public function last();

    /**
     * @param string $property
     * @param $value
     * @return CollectionInterface
     * @throws \ReflectionException
     */
    public function findBy(string $property, $value): CollectionInterface;
}
