<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Repository;

use Grifix\Kit\Collection\CollectionWrapperTrait;
use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Specification\SpecificationInterface;

/**
 * Class Repository
 * @package Grifix\Kit\Orm\Repository
 */
class Repository implements RepositoryInterface
{
    use CollectionWrapperTrait;

    /**
     * @var CollectionInterface
     */
    protected $collection;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * Repository constructor.
     * @param CollectionInterface $collection
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(CollectionInterface $collection, EntityManagerInterface $entityManager)
    {
        $this->collection = $collection;
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $id
     * @return object|null
     */
    public function find(string $id): ?object
    {
        return $this->collection->find($id);
    }

    /**
     * @param object $entity
     */
    public function save(object $entity): void
    {
        if (!$this->find($this->entityManager->getEntityId($entity))) {
            $this->collection->add($entity);
        }
        $this->entityManager->flush($entity);
    }

    /**
     * @param object $entity
     */
    public function delete(object $entity): void
    {
        $this->collection->remove($entity);
        $this->entityManager->flush($entity);
    }

    /**
     * @param SpecificationInterface $specification
     * @return CollectionInterface
     */
    public function match(SpecificationInterface $specification): CollectionInterface
    {
        return $this->collection->match($specification);
    }

    /**
     * @return mixed
     */
    public function first()
    {
        return $this->collection->first();
    }

    /**
     * @return mixed
     */
    public function last()
    {
        return $this->collection->last();
    }

    /**
     * @param string $property
     * @param $value
     * @return CollectionInterface
     * @throws \ReflectionException
     */
    public function findBy(string $property, $value): CollectionInterface
    {
        return $this->collection->findBy($property, $value);
    }
}
