<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Repository;

/**
 * Class RepositoryFactory
 * @package Grifix\Kit\Orm\Repository
 */
interface RepositoryFactoryInterface
{
    /**
     * @param string $entityClass
     * @param string $wrapperClass
     * @return mixed
     */
    public function createRepositoryWrapper(string $entityClass, string $wrapperClass);

    /**
     * @param string $entityClass
     * @return RepositoryInterface
     */
    public function createRepository(string $entityClass): RepositoryInterface;
}
