<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Repository;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Orm\Collection\CollectionFactoryInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class RepositoryFactory
 * @package Grifix\Kit\Orm\Repository
 */
class RepositoryFactory extends AbstractFactory implements RepositoryFactoryInterface
{
    /**
     * @var CollectionFactoryInterface
     */
    protected $collectionFactory;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * RepositoryFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param CollectionFactoryInterface $collectionFactory
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        CollectionFactoryInterface $collectionFactory,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct($classMaker);
        $this->collectionFactory = $collectionFactory;
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $entityClass
     * @param string $wrapperClass
     * @return mixed
     */
    public function createRepositoryWrapper(string $entityClass, string $wrapperClass)
    {
        $entityClass = $this->makeClassName($entityClass);
        $wrapperClass = $this->makeClassName($wrapperClass);
        return new $wrapperClass($this->createRepository($entityClass));
    }

    /**
     * @param string $entityClass
     * @return RepositoryInterface
     */
    public function createRepository(string $entityClass): RepositoryInterface
    {
        $entityClass = $this->makeClassName($entityClass);
        $repositoryClass = $this->makeClassName(Repository::class);
        return new $repositoryClass($this->collectionFactory->createQueryCollection(
            $this->entityManager,
            $entityClass
        ), $this->entityManager);
    }
}
