<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\IdentityMap;


use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class IdentyMapFactory
 * @package Grifix\Kit\Orm\IdentityMap
 */
interface IdentityMapFactoryInterface
{
    /**
     * @param EntityManagerInterface $entityManager
     * @return IdentityMapInterface
     */
    public function createIdentityMap(EntityManagerInterface $entityManager): IdentityMapInterface;
}