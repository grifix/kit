<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\IdentityMap;

use Grifix\Kit\Collection\ArrayWrapperTrait;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException;
use Grifix\Kit\Orm\IdentityMap\Exception\SameEntityAlreadyExistsException;

/**
 * Class IdentityMap
 * @package Grifix\Kit\Orm\IdentityMap
 */
class IdentityMap implements IdentityMapInterface
{

    /**
     * @var IdentityItem[]
     */
    protected $items = [];

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var IdentityItemFactoryInterface
     */
    protected $identityItemFactory;

    /**
     * @var ClassHelperInterface
     */
    protected $classHelper;

    /**
     * IdentityMap constructor.
     * @param IdentityItemFactoryInterface $identityItemFactory
     * @param ArrayHelperInterface $arrayHelper
     * @param EntityManagerInterface $entityManager
     * @param ClassHelperInterface $classHelper
     */
    public function __construct(
        IdentityItemFactoryInterface $identityItemFactory,
        ArrayHelperInterface $arrayHelper,
        EntityManagerInterface $entityManager,
        ClassHelperInterface $classHelper
    ) {
        $this->identityItemFactory = $identityItemFactory;
        $this->arrayHelper = $arrayHelper;
        $this->entityManager = $entityManager;
        $this->classHelper = $classHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function addEntity($entity, bool $isNew = true): void
    {
        $this->checkExistedEntity($entity);

        $this->arrayHelper->set(
            $this->items,
            $this->makeEntityKey(get_class($entity), $this->entityManager->getEntityId($entity)),
            $this->identityItemFactory->createIdentityItem($this->entityManager, $entity, $isNew)
        );
    }

    /**
     * @param $entity
     * @throws \Exception
     */
    private function checkExistedEntity($entity)
    {
        $id = $this->entityManager->getEntityId($entity);
        if ($this->hasEntityWithId(get_class($entity), $id)) {
            $existedEntity = $this->getEntity(get_class($entity), $id);
            if ($existedEntity && $existedEntity !== $entity) {
                throw new SameEntityAlreadyExistsException(get_class($entity), $id);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDiff($entity): array
    {
        return $this->getItem(get_class($entity), $this->entityManager->getEntityId($entity))->getDiff();
    }

    /**
     * {@inheritdoc}
     */
    public function isNew($entity): bool
    {
        return $this->getItem(get_class($entity), $this->entityManager->getEntityId($entity))->isNew();
    }

    /**
     * {@inheritdoc}
     */
    public function isDirty($entity): bool
    {
        return $this->getItem(get_class($entity), $this->entityManager->getEntityId($entity))->isDirty();
    }

    /**
     * {@inheritdoc}
     */
    public function markForDeletion($entity): void
    {
        $this->getItem(get_class($entity), $this->entityManager->getEntityId($entity))->markForDeletion();
    }

    /**
     * {@inheritdoc}
     */
    public function markDeleted($entity): void
    {
        $this->getItem(get_class($entity), $this->entityManager->getEntityId($entity))->markDeleted();
    }

    /**
     * {@inheritdoc}
     */
    public function isMarkedForDeletion($entity): bool
    {
        return $this->getItem(get_class($entity), $this->entityManager->getEntityId($entity))->isMarkedForDeletion();
    }

    /**
     * @param string $entityClass
     * @param string $id
     * @return IdentityItem
     * @throws ItemNotExistsException
     */
    protected function getItem(string $entityClass, string $id): IdentityItemInterface
    {
        $result = $this->arrayHelper->get($this->items, $this->makeEntityKey($entityClass, $id));
        if (!$result) {
            throw new ItemNotExistsException($entityClass, $id);
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity(string $entityClass, string $id)
    {
        return $this->getItem($entityClass, $id)->getEntity();
    }

    /**
     * {@inheritdoc}
     */
    public function visitDirtyItems(callable $visitor): void
    {
        foreach ($this->items as $item) {
            if ($item->isDirty()) {
                $visitor($item);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function visitNewItems(callable $visitor): void
    {
        foreach ($this->items as $item) {
            if ($item->isNew()) {
                $visitor($item);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function visitAllItems(callable $visitor): void
    {
        foreach ($this->items as $item) {
            $visitor($item);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function commitChanges($entity): void
    {
        $this->getItem(get_class($entity), $this->entityManager->getEntityId($entity))->commitChanges();
    }

    /**
     * @param string $entityClass
     * @param string $id
     * @return string
     */
    protected function makeEntityKey($entityClass, string $id): string
    {
        return $this->classHelper->makeFullClassName($entityClass) . '_' . $id;
    }

    /**
     * {@inheritdoc}
     */
    public function hasEntity($entity): bool
    {
        return $this->arrayHelper->has(
            $this->items,
            $this->makeEntityKey(get_class($entity), $this->entityManager->getEntityId($entity))
        );
    }

    /**
     * {@inheritdoc}
     */
    public function hasEntityWithId(string $entityClass, string $id): bool
    {
        return $this->arrayHelper->has($this->items, $this->makeEntityKey($entityClass, $id));
    }
}
