<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\IdentityMap;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class IdentyMapFactory
 * @package Grifix\Kit\Orm\IdentityMap
 */
class IdentityMapFactory extends AbstractFactory implements IdentityMapFactoryInterface
{

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var IdentityItemFactoryInterface
     */
    protected $identityItemFactory;

    /**
     * @var ClassHelperInterface
     */
    protected $classHelper;

    /**
     * IdentityMapFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param ArrayHelperInterface $arrayHelper
     * @param IdentityItemFactoryInterface $identityItemFactory
     * @param ClassHelperInterface $classHelper
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        ArrayHelperInterface $arrayHelper,
        IdentityItemFactoryInterface $identityItemFactory,
        ClassHelperInterface $classHelper
    ) {
        parent::__construct($classMaker);
        $this->arrayHelper = $arrayHelper;
        $this->identityItemFactory = $identityItemFactory;
        $this->classHelper = $classHelper;
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @return IdentityMapInterface
     */
    public function createIdentityMap(EntityManagerInterface $entityManager): IdentityMapInterface
    {
        $class = $this->makeClassName(IdentityMap::class);
        return new $class($this->identityItemFactory, $this->arrayHelper, $entityManager, $this->classHelper);
    }
}
