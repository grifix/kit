<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\IdentityMap;

/**
 * Class IdentyItem
 * @package Grifix\Kit\Orm\IdentityMap
 */
interface IdentityItemInterface
{
    /**
     * @return bool
     */
    public function isDirty(): bool;

    public function commitChanges(): void;

    /**
     * @return bool
     */
    public function isNew(): bool;

    /**
     * @return object
     */
    public function getEntity();

    /**
     * @return array
     */
    public function getDiff(): array;

    /**
     * @return bool
     */
    public function isMarkedForDeletion(): bool;

    public function markForDeletion(): void;

    public function markDeleted(): void;

    /**
     * @return bool
     */
    public function isDeleted(): bool;
}