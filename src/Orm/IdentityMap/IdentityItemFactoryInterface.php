<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\IdentityMap;

use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class IdentyItemFactory
 * @package Grifix\Kit\Orm\IdentityMap
 */
interface IdentityItemFactoryInterface
{
    /**
     * @param EntityManagerInterface $entityManager
     * @param $entity
     * @param bool $isNew
     * @return IdentityItemInterface
     */
    public function createIdentityItem(
        EntityManagerInterface $entityManager,
        $entity,
        bool $isNew = true
    ): IdentityItemInterface;
}