<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\IdentityMap;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class IdentyItemFactory
 * @package Grifix\Kit\Orm\IdentityMap
 */
class IdentityItemFactory extends AbstractFactory implements IdentityItemFactoryInterface
{

    protected $arrayHelper;

    /**
     * IdentyItemFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(ClassMakerInterface $classMaker, ArrayHelperInterface $arrayHelper)
    {
        parent::__construct($classMaker);
        $this->arrayHelper = $arrayHelper;
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param $entity
     * @param bool $isNew
     * @return IdentityItemInterface
     */
    public function createIdentityItem(
        EntityManagerInterface $entityManager,
        $entity,
        bool $isNew = true
    ): IdentityItemInterface {
        $class = $this->makeClassName(IdentityItem::class);
        return new $class($this->arrayHelper, $entityManager, $entity, $isNew);
    }
}
