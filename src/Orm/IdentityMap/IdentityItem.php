<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\IdentityMap;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class IdentyItem
 * @package Grifix\Kit\Orm\IdentityMap
 */
class IdentityItem implements IdentityItemInterface
{

    /**
     * @var object
     */
    protected $entity;

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var bool
     */
    protected $isNew;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var bool
     */
    protected $isDeleted = false;

    /**
     * @var bool
     */
    protected $isMarkedForDeletion = false;

    /**
     * IdentityItem constructor.
     * @param ArrayHelperInterface $arrayHelper
     * @param EntityManagerInterface $entityManager
     * @param $entity
     * @param bool $isNew
     */
    public function __construct(
        ArrayHelperInterface $arrayHelper,
        EntityManagerInterface $entityManager,
        $entity,
        bool $isNew = true
    ) {
        $this->entityManager = $entityManager;
        $this->entity = $entity;
        $this->data = $this->entityManager->serialize($entity);
        $this->isNew = $isNew;
        $this->arrayHelper = $arrayHelper;
    }

    /**
     * @return bool
     */
    public function isDirty(): bool
    {
        if ($this->entityManager->serialize($this->entity) !== $this->data
            || ($this->isMarkedForDeletion && !$this->isDeleted)) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isMarkedForDeletion(): bool
    {
        return $this->isMarkedForDeletion;
    }

    public function markForDeletion(): void
    {
        if (!$this->isDeleted) {
            $this->isMarkedForDeletion = true;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isDeleted(): bool
    {
        return $this->isDeleted;
    }

    public function markDeleted(): void
    {
        $this->isDeleted = true;
    }

    public function commitChanges(): void
    {
        $this->data = $this->entityManager->serialize($this->entity);
        $this->isNew = false;
        if ($this->isMarkedForDeletion) {
            $this->isDeleted = true;
        }
    }

    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->isNew;
    }

    /**
     * @return object
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return array
     */
    public function getDiff(): array
    {
        return $this->arrayHelper->diff($this->entityManager->serialize($this->entity), $this->data);
    }
}
