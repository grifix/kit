<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\EventCollector;

/**
 * Interface EventPublisherInterface
 * @package Grifix\Kit\Orm\EventCollector
 */
interface EventPublisherInterface
{
    /**
     * @param object $event
     */
    public function publishEvent($event): void;
}
