<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\EventCollector;

/**
 * Class EventManager
 * @package Grifix\Kit\OldRepository
 */
interface EventCollectorInterface
{
    /**
     * @param object $event
     */
    public function collect($event): void;

    /**
     *
     */
    public function flush(): void;
}