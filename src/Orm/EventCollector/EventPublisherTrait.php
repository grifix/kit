<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\EventCollector;

/**
 * Trait EventCollectorTrait
 * @package Grifix\Kit\Orm\EventCollector
 */
trait EventPublisherTrait
{
    /***
     * @var EventCollectorInterface
     */
    protected $eventCollector;

    /**
     * @param object $event
     */
    public function publishEvent($event): void
    {
        $this->eventCollector->collect($event);
    }
}
