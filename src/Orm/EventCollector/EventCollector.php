<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\EventCollector;

use Grifix\Kit\EventBus\EventBusInterface;

/**
 * Class EventManager
 * @package Grifix\Kit\OldRepository
 */
class EventCollector implements EventCollectorInterface
{
    protected $events = [];

    /**
     * @var EventBusInterface
     */
    protected $eventBus;

    /**
     * EventManager constructor.
     * @param EventBusInterface $eventBus
     */
    public function __construct(EventBusInterface $eventBus)
    {
        $this->eventBus = $eventBus;
    }

    /**
     * {@inheritdoc}
     */
    public function collect($event): void
    {
        $this->events[] = $event;
    }

    /**
     *
     */
    public function flush(): void
    {
        foreach ($this->events as $i => $event) {
            $this->eventBus->trigger($event);
            unset($this->events[$i]);
        }
    }
}
