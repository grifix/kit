<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Persister;

/**
 * Class AbstractPersister
 * @package Grifix\Kit\Orm\Persister
 */
interface PersisterInterface
{
    /**
     * @param object $entity
     */
    public function persist($entity): void;
}
