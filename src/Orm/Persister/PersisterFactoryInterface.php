<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Persister;

use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class PersisterFactory
 * @package Grifix\Kit\Orm\Blueprint\Persister
 */
interface PersisterFactoryInterface
{
    /**
     * @param string $persisterClass
     * @param string $table
     * @param EntityManagerInterface $entityManager
     * @return PersisterInterface
     */
    public function createPersister(
        string $persisterClass,
        string $table,
        EntityManagerInterface $entityManager
    ): PersisterInterface;
}