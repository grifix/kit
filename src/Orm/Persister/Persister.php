<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Persister;

use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Orm\Persister\JsonbUpdate\JsonbUpdateFactoryInterface;

/**
 * Class AbstractPersister
 * @package Grifix\Kit\Orm\Persister
 */
class Persister implements PersisterInterface
{

    /**
     * @var string
     */
    protected $table;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var JsonbUpdateFactoryInterface
     */
    protected $jsonUpdateFactory;

    /**
     * Persister constructor.
     * @param JsonbUpdateFactoryInterface $jsonbUpdateFactory
     * @param EntityManagerInterface $entityManager
     * @param string $table
     */
    public function __construct(
        JsonbUpdateFactoryInterface $jsonbUpdateFactory,
        EntityManagerInterface $entityManager,
        string $table
    ) {
        $this->table = $table;
        $this->entityManager = $entityManager;
        $this->jsonUpdateFactory = $jsonbUpdateFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function persist($entity): void
    {
        if ($this->entityManager->isNew($entity)) {
            $this->insert($entity);

        } elseif ($this->entityManager->isDirty($entity)) {
            if ($this->entityManager->isMarkedForDeletion($entity)) {
                $this->delete($entity);
            } else {
                $this->update($entity);
            }
        }
    }

    /**
     * @param $entity
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     * @throws \Grifix\Kit\Orm\Blueprint\Exception\RelationToParentNotExists
     * @throws \Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException
     * @throws \Orm2\RelationManager\Exception\EntityHasNoParentException
     */
    protected function insert($entity): void
    {
        $record = [
            'id' => $this->entityManager->getEntityId($entity),
            'data' => json_encode($this->entityManager->serialize($entity))
        ];
        if ($this->entityManager->hasParent($entity)) {
            $parentEntity = $this->entityManager->getParent($entity);
            if ($this->entityManager->isNew($parentEntity)) {
                $this->entityManager->persist($parentEntity);
            }
            $relation = $this->entityManager->getBlueprint(get_class($entity))
                ->getRelationToParent(get_class($parentEntity));
            $record[$relation->getChildColumn()] = $this->entityManager->getEntityId($parentEntity);
        }
        $query = $this->entityManager->getConnection()->createQuery();
        $query->insert($record)->into($this->table);
        $query->execute();
        $this->entityManager->commitChanges($entity);
    }

    /**
     * @param object $entity
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    protected function delete($entity): void
    {
        $this->entityManager->getConnection()->delete(
            ['id' => $this->entityManager->getEntityId($entity)],
            $this->table
        );
    }

    /**
     * @param $entity
     * @throws \Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException
     */
    protected function update($entity): void
    {
        $updates = $this->jsonUpdateFactory->createUpdates($this->entityManager->getDiff($entity));
        foreach ($updates as $update) {
            $this->entityManager->getConnection()->execute(
                'UPDATE ' . $this->table . ' SET data = jsonb_set(data, :path, :value) WHERE "id" = :id',
                [
                    'id' => $this->entityManager->getEntityId($entity),
                    'path' => $update->getPathString(),
                    'value' => json_encode($update->getValue())
                ]
            );
        }

        $this->entityManager->commitChanges($entity);
    }
}
