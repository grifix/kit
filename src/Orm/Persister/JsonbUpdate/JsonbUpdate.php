<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Persister\JsonbUpdate;

/**
 * Class Update
 * @package Grifix\Kit\Orm\Persister\JsonbUpdate
 */
class JsonbUpdate
{
    /**
     * @var array
     */
    protected $path;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * Update constructor.
     * @param array $path
     * @param mixed $value
     */
    public function __construct(array $path, $value)
    {
        $this->path = $path;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getPath(): array
    {
        return $this->path;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getPathString(): string
    {
        return '{' . implode(',', $this->path) . '}';
    }
}
