<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\Relation;

/**
 * Class RelationToChild
 * @package Grifix\Kit\Orm\Blueprint\Relation
 */
abstract class AbstractRelation
{
    /**
     * @var string
     */
    protected $childClass;

    /**
     * @var string
     */
    protected $childColumn;

    /**
     * @var string
     */
    protected $parentProperty;

    /**
     * @var string
     */
    protected $parentClass;

    /**
     * RelationToChild constructor.
     * @param string $parentClass
     * @param string $parentProperty
     * @param string $childClass
     * @param string $childColumn
     */
    public function __construct(string $parentClass, string $parentProperty, string $childClass, string $childColumn)
    {
        $this->childClass = $childClass;
        $this->childColumn = $childColumn;
        $this->parentClass = $parentClass;
        $this->parentProperty = $parentProperty;
    }

    /**
     * @return string
     */
    public function getChildClass(): string
    {
        return $this->childClass;
    }

    /**
     * @return string
     */
    public function getChildColumn(): string
    {
        return $this->childColumn;
    }

    /**
     * @return string
     */
    public function getParentProperty(): string
    {
        return $this->parentProperty;
    }

    /**
     * @return string
     */
    public function getParentClass(): string
    {
        return $this->parentClass;
    }
}
