<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\Exception;

/**
 * Class BlueprintAlreadyRegisteredException
 * @package Grifix\Kit\Orm\EntityManager\Exception
 */
class BlueprintAlreadyRegisteredException extends \Exception
{
    protected $entityClass;

    /**
     * BlueprintAlreadyRegisteredException constructor.
     * @param string $entityClass
     */
    public function __construct(string $entityClass)
    {
        parent::__construct(sprintf('Blueprint is already registered for entity "%s"!', $entityClass));
        $this->entityClass = $entityClass;
    }

    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return $this->entityClass;
    }
}
