<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\Exception;

/**
 * Class EntityClassIsNotDefinedException
 * @package Grifix\Kit\Orm\Blueprint\Exception
 */
class EntityClassIsNotDefinedException extends \Exception
{
    protected $blueprintClass;

    /**
     * EntityClassIsNotDefinedException constructor.
     * @param string $blueprintClass
     */
    public function __construct(string $blueprintClass)
    {
        $this->blueprintClass;
        parent::__construct(sprintf('Entity class is not defined for blueprint "%s"', $blueprintClass));
    }
}