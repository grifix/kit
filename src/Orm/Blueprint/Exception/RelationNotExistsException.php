<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\Exception;

/**
 * Class RelationNotExistsException
 * @package Grifix\Kit\Orm\Blueprint\Exception
 */
class RelationNotExistsException extends \RuntimeException
{
    protected $entityClass;

    /**
     * RelationNotExistsException constructor.
     * @param string $entityClass
     */
    public function __construct(string $entityClass)
    {
        $this->entityClass = $entityClass;
        parent::__construct(sprintf('Relation to "%s" not exists!', $entityClass));
    }
}
