<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\Exception;

/**
 * Class RelationToChildrenNotExists
 * @package Grifix\Kit\Orm\Blueprint\Exception
 */
class RelationToChildrenNotExists extends \Exception
{

    /**
     * @var string
     */
    protected $property;

    /**
     * RelationToChildrenNotExists constructor.
     * @param string $property
     */
    public function __construct(string $property)
    {
        $this->property = $property;
        parent::__construct(sprintf('Relation "%s" not exists!', $property));
    }
}