<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\ClassDetector;

use Grifix\Kit\Orm\Blueprint\ClassDetector\Exception\TypeColumnNotExists;
use Grifix\Kit\Orm\Blueprint\ClassDetector\Exception\UnknownClassException;
use Grifix\Kit\Orm\Blueprint\ClassDetector\Exception\UnknownTypeException;

/**
 * Class ClassDetector
 * @package Grifix\Kit\Orm\Blueprint\ClassDetector
 */
interface ClassDetectorInterface
{
    /**
     * @param array $data
     * @return string
     * @throws TypeColumnNotExists
     * @throws UnknownTypeException
     */
    public function detectClass(array $data): string;

    /**
     * @param string $className
     * @return string
     * @throws UnknownClassException
     */
    public function detectType(string $className): string;

    /**
     * @return string
     */
    public function getColumn(): string;
}