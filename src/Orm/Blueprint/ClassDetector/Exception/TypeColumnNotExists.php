<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\ClassDetector\Exception;

/**
 * Class TypeColumnNotExists
 * @package Grifix\Kit\Orm\Blueprint\ClassDetector\Exception
 */
class TypeColumnNotExists extends \Exception
{
    protected $message = 'Type column not exists!';
}
