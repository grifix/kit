<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\ClassDetector\Exception;

/**
 * Class UnknownClassException
 * @package Grifix\Kit\Orm\Blueprint\ClassDetector\Exception
 */
class UnknownClassException extends \Exception
{
    /**
     * @var string
     */
    protected $class;

    /**
     * UnknownTypeException constructor.
     * @param string $class
     */
    public function __construct(string $class)
    {
        $this->class = $class;
        parent::__construct(sprintf('Unknown class "%s"', $class));
    }
}
