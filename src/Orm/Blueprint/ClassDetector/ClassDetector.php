<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint\ClassDetector;

use Grifix\Kit\Orm\Blueprint\ClassDetector\Exception\TypeColumnNotExists;
use Grifix\Kit\Orm\Blueprint\ClassDetector\Exception\UnknownClassException;
use Grifix\Kit\Orm\Blueprint\ClassDetector\Exception\UnknownTypeException;

/**
 * Class ClassDetector
 * @package Grifix\Kit\Orm\Blueprint\ClassDetector
 */
class ClassDetector implements ClassDetectorInterface
{
    /**
     * @var array
     */
    protected $map;

    /**
     * @var string
     */
    protected $column;

    /**
     * ClassDetector constructor.
     * @param array $map
     * @param string $column
     */
    public function __construct(array $map, string $column = 'type')
    {
        $this->map = $map;
        $this->column = $column;
    }

    /**
     * @param array $data
     * @return string
     * @throws TypeColumnNotExists
     * @throws UnknownTypeException
     */
    public function detectClass(array $data): string
    {
        if (!isset($data[$this->column])) {
            throw new TypeColumnNotExists();
        }
        $type = $data[$this->column];
        if (!isset($this->map[$type])) {
            throw new UnknownTypeException($type);
        }
        return $this->map[$type];
    }

    /**
     * @param string $className
     * @return string
     * @throws UnknownClassException
     */
    public function detectType(string $className): string
    {
        $map = array_combine($this->map, array_keys($this->map));
        if (!isset($map[$className])) {
            throw new UnknownClassException($className);
        }
        return $map[$className];
    }

    /**
     * @return string
     */
    public function getColumn(): string
    {
        return $this->column;
    }
}
