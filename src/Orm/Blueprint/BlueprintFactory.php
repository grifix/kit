<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint;

use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Orm\Blueprint\ClassDetector\ClassDetectorFactoryInterface;
use Grifix\Kit\Orm\Blueprint\Exception\NotBlueprintInterfaceException;
use Grifix\Kit\Orm\Serializer\SerializerFactoryInterface;

/**
 * Class BlueprintFactory
 * @package Grifix\Kit\Orm\Blueprint
 */
class BlueprintFactory extends AbstractFactory implements BlueprintFactoryInterface
{
    /**
     * @var ClassHelperInterface
     */
    protected $objectHelper;

    /**
     * @var SerializerFactoryInterface
     */
    protected $serializerFactory;

    /**
     * @var ClassDetectorFactoryInterface
     */
    protected $classDetectorFactory;

    /**
     * BlueprintFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param ClassHelperInterface $objectHelper
     * @param ClassDetectorFactoryInterface $classDetectorFactory
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        ClassHelperInterface $objectHelper,
        ClassDetectorFactoryInterface $classDetectorFactory
    ) {
        $this->objectHelper = $objectHelper;
        $this->classDetectorFactory = $classDetectorFactory;
        parent::__construct($classMaker);
    }

    /**
     * @param string $blueprintClass
     * @return BlueprintInterface
     */
    public function createBlueprint(string $blueprintClass): BlueprintInterface
    {
        $className = $this->makeClassName($blueprintClass);
        if (!$this->objectHelper->isInstanceOf($blueprintClass, BlueprintInterface::class)) {
            throw new NotBlueprintInterfaceException($blueprintClass);
        }
        return new $className($this->classDetectorFactory, $this->objectHelper);
    }
}