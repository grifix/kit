<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint;

use foo\Foo;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Orm\Blueprint\ClassDetector\ClassDetectorFactoryInterface;
use Grifix\Kit\Orm\Blueprint\ClassDetector\ClassDetectorInterface;
use Grifix\Kit\Orm\Blueprint\Exception\EntityClassIsNotDefinedException;
use Grifix\Kit\Orm\Blueprint\Exception\RelationNotExistsException;
use Grifix\Kit\Orm\Blueprint\Exception\RelationToChildrenNotExists;
use Grifix\Kit\Orm\Blueprint\Exception\RelationToParentNotExists;
use Grifix\Kit\Orm\Blueprint\Exception\TableIsNotDefinedException;
use Grifix\Kit\Orm\Blueprint\Relation\AbstractRelation;
use Grifix\Kit\Orm\Persister\Persister;
use Grifix\Kit\Orm\Blueprint\Relation\RelationToChild;
use Grifix\Kit\Orm\Blueprint\Relation\RelationToParent;
use Grifix\Kit\Orm\Serializer\ObjectSerializer;
use Grifix\Kit\Orm\Serializer\SerializerFactoryInterface;
use Grifix\Kit\Orm\Serializer\SerializerInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\TechnicalInspection\TechnicalInspectionCollectionFactory;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\TechnicalInspection\TechnicalInspectionCollectionFactoryInterface;

/**
 * Class AbstractBlueprint
 * @package Grifix\Kit\Orm\Blueprint
 */
abstract class AbstractBlueprint implements BlueprintInterface
{
    /**
     * @var string
     */
    protected $table;

    /**
     * @var string
     */
    protected $entityClass;

    /**
     * @var array
     */
    protected $properties = [];

    /**
     * @var AbstractRelation[]
     */
    protected $relations = [];

    /**
     * @var \ReflectionClass
     */
    protected $reflection;

    /**
     * @var string
     */
    protected $serializerClass = ObjectSerializer::class;

    /**
     * @var string
     */
    protected $persisterClass = Persister::class;

    /**
     * @var ClassDetectorFactoryInterface
     */
    protected $classDetectorFactory;

    /**
     * @var ClassDetectorInterface|null
     */
    protected $classDetector;

    /**
     * @var ClassHelperInterface
     */
    protected $classHelper;

    /**
     * AbstractBlueprint constructor.
     * @param ClassDetectorFactoryInterface $classDetectorFactory
     * @param ClassHelperInterface $classHelper
     * @throws EntityClassIsNotDefinedException
     * @throws TableIsNotDefinedException
     */
    final public function __construct(
        ClassDetectorFactoryInterface $classDetectorFactory,
        ClassHelperInterface $classHelper
    ) {
        $this->classDetectorFactory = $classDetectorFactory;
        $this->classHelper = $classHelper;
        $this->init();
        if (!$this->table) {
            throw new TableIsNotDefinedException(get_class($this));
        }

        $this->hasEntityClassOrFail();
        $this->entityClass = $this->classHelper->makeFullClassName($this->entityClass);
    }


    protected function hasEntityClassOrFail()
    {
        if (!$this->entityClass) {
            throw new EntityClassIsNotDefinedException(get_class($this));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSerializerClass(): string
    {
        return $this->serializerClass;
    }

    /**
     * {@inheritdoc}
     */
    public function getPersisterClass(): string
    {
        return $this->persisterClass;
    }

    /**
     * @return void
     */
    abstract protected function init(): void;

    /**
     * @param string $entityClass
     * @return AbstractRelation
     */
    public function getRelationTo(string $entityClass): AbstractRelation
    {
        $entityClass = $this->classHelper->makeFullClassName($entityClass);
        foreach ($this->relations as $relation) {
            if ($this->classHelper->makeFullClassName($relation->getChildClass()) === $entityClass
                || $this->classHelper->makeFullClassName($relation->getParentClass()) === $entityClass) {
                return $relation;
            }
        }
        throw new RelationNotExistsException($entityClass);
    }

    /**
     * @param string $entityClass
     * @return bool
     */
    public function hasRelationTo(string $entityClass): bool
    {
        try {
            $this->getRelationTo($entityClass);
        } catch (RelationNotExistsException $exception) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getId($entity): string
    {
        $property = $this->getReflection()->getProperty('id');
        $property->setAccessible(true);
        $result = $property->getValue($entity);
        $property->setAccessible(false);
        return $result;
    }

    /**
     * @return \ReflectionClass
     * @throws \ReflectionException
     */
    protected function getReflection(): \ReflectionClass
    {
        if (!$this->reflection) {
            $this->reflection = new \ReflectionClass($this->entityClass);
        }
        return $this->reflection;
    }

    /**
     * @param string $property
     * @param string $childClass
     * @param string $childColumn
     * @param string $childCollectionClass
     * @return AbstractBlueprint
     */
    protected function addRelationToChild(
        string $property,
        string $childClass,
        string $childColumn,
        string $childCollectionClass
    ): self {
        $this->relations[] = new RelationToChild(
            $this->getEntityClass(),
            $property,
            $childClass,
            $childColumn,
            $childCollectionClass
        );
        return $this;
    }

    /**
     * @param string $parentClass
     * @param string $parentProperty
     * @param string $column
     * @return AbstractBlueprint
     */
    protected function addRelationToParent(
        string $parentClass,
        string $parentProperty,
        string $column
    ): self {
        $this->relations[] = new RelationToParent($parentClass, $parentProperty, $this->getEntityClass(), $column);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRelationToParent(string $parentClass): RelationToParent
    {
        foreach ($this->relations as $relation) {
            if (($relation instanceof RelationToParent)
                && ($this->classHelper->isInstanceOf($parentClass, $relation->getParentClass()))) {
                return $relation;
            }
        }

        throw new RelationToParentNotExists($parentClass);
    }

    /**
     * @param string $childrenProperty
     * @return mixed
     * @throws RelationToChildrenNotExists
     */
    public function getRelationToChildren(string $childrenProperty)
    {
        foreach ($this->relations as $relation) {
            if (($relation instanceof RelationToChild) && $childrenProperty == $relation->getParentProperty()) {
                return $relation;
            }
        }
        throw new RelationToChildrenNotExists($childrenProperty);
    }

    /**
     * {@inheritdoc}
     */
    public function hasRelationToChildren(string $childrenProperty): bool
    {
        try {
            $this->getRelationToChildren($childrenProperty);
        } catch (RelationToChildrenNotExists $e) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getRelationsToChildren(): array
    {
        $result = [];
        foreach ($this->relations as $relation) {
            if ($relation instanceof RelationToChild) {
                $result[] = $relation;
            }
        }
        return $result;
    }

    /**
     * @return string
     */
    public function getEntityClass(): string
    {
        return $this->entityClass;
    }

    /**
     * {@inheritdoc}
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * @param array $data
     * @return string
     * @throws ClassDetector\Exception\TypeColumnNotExists
     * @throws ClassDetector\Exception\UnknownTypeException
     */
    public function detectEntityClass(array $data): string
    {
        if ($this->classDetector) {
            return $this->classDetector->detectClass($data);
        }
        return $this->entityClass;
    }

    /**
     * @param string $entityClass
     * @return null|string
     * @throws ClassDetector\Exception\UnknownClassException
     */
    public function detectEntityType(string $entityClass)
    {
        if ($this->classDetector) {
            return $this->classDetector->detectType($entityClass);
        }

        return null;
    }

    /**
     * @param array $types
     * @param string $column
     * @return AbstractBlueprint
     */
    protected function setClassDetector(array $types, $column = 'type'): self
    {
        $this->classDetector = $this->classDetectorFactory->createClassDetector($types, $column);
        return $this;
    }

    /**
     * @return null|string
     */
    public function getTypeColumn()
    {
        if ($this->classDetector) {
            return $this->classDetector->getColumn();
        }
        return null;
    }
}
