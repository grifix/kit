<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint;

use Grifix\Kit\Orm\Blueprint\Exception\RelationNotExistsException;
use Grifix\Kit\Orm\Blueprint\Exception\RelationToChildrenNotExists;
use Grifix\Kit\Orm\Blueprint\Exception\RelationToParentNotExists;
use Grifix\Kit\Orm\Blueprint\Relation\AbstractRelation;
use Grifix\Kit\Orm\Blueprint\Relation\RelationToChild;
use Grifix\Kit\Orm\Blueprint\Relation\RelationToParent;
use Grifix\Kit\Orm\Serializer\SerializerInterface;

/**
 * @internal
 * Class BlueprintInterface
 * @package Grifix\Kit\Orm\Blueprint
 */
interface BlueprintInterface
{
    /**
     * @internal
     * @param $entity
     * @return string
     */
    public function getId($entity): string;

    /**
     * @internal
     * @return string
     */
    public function getTable(): string;

    /**
     * @internal
     * @param string $parentClass
     * @return mixed
     * @throws RelationToParentNotExists
     */
    public function getRelationToParent(string $parentClass): RelationToParent;

    /**
     * @internal
     * @return string
     */
    public function getEntityClass(): string;

    /**
     * @return string
     */
    public function getSerializerClass(): string;

    /**
     * @internal
     * @return string
     */
    public function getPersisterClass(): string;

    /**
     * @param array $data
     * @return string
     * @throws ClassDetector\Exception\TypeColumnNotExists
     * @throws ClassDetector\Exception\UnknownTypeException
     */
    public function detectEntityClass(array $data): string;

    /**
     * @param string $entityClass
     * @return null|string
     */
    public function detectEntityType(string $entityClass);

    /**
     * @return null|string
     */
    public function getTypeColumn();

    /**
     * @param string $childrenProperty
     * @return mixed
     * @throws RelationToChildrenNotExists
     */
    public function getRelationToChildren(string $childrenProperty);

    /**
     * @return RelationToChild[]
     */
    public function getRelationsToChildren(): array;

    /**
     * @param string $childrenProperty
     * @return bool
     */
    public function hasRelationToChildren(string $childrenProperty): bool;

    /**
     * @param string $entityClass
     * @return bool
     */
    public function hasRelationTo(string $entityClass): bool;

    /**
     * @param string $entityClass
     * @return AbstractRelation
     */
    public function getRelationTo(string $entityClass): AbstractRelation;
}
