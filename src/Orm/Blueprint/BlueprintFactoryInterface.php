<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\Blueprint;

/**
 * Class BlueprintFactory
 * @package Grifix\Kit\Orm\Blueprint
 */
interface BlueprintFactoryInterface
{
    /**
     * @param string $blueprintClass
     * @return BlueprintInterface
     */
    public function createBlueprint(string $blueprintClass): BlueprintInterface;
}