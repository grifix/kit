<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\RelationMap;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class RelationManagerFactory
 * @package Orm\RelationMap
 */
class RelationMapFactory extends AbstractFactory implements RelationMapFactoryInterface
{
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * RelationManagerFactory constructor.
     * @param ClassMakerInterface $classMaker
     */
    public function __construct(ClassMakerInterface $classMaker, ArrayHelperInterface $arrayHelper)
    {
        parent::__construct($classMaker);
        $this->arrayHelper = $arrayHelper;
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @return RelationMap
     */
    public function createRelationMap(EntityManagerInterface $entityManager): RelationMapInterface
    {
        $class = $this->makeClassName(RelationMap::class);
        return new $class($entityManager, $this->arrayHelper);
    }
}
