<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\RelationMap;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Orm2\RelationManager\Exception\EntityHasNoParentException;

/**
 * This class contains information about all entities relations in current request
 * @package Orm\RelationMap
 */
class RelationMap implements RelationMapInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var array
     */
    protected $relations = [];

    /**
     * RelationMap constructor.
     * @param EntityManagerInterface $entityManager
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ArrayHelperInterface $arrayHelper
    ) {
        $this->entityManager = $entityManager;
        $this->arrayHelper = $arrayHelper;
    }

    /**
     * @param object $childEntity
     * @param object $parentEntity
     */
    public function setParent($childEntity, $parentEntity): void
    {
        $this->arrayHelper->set(
            $this->relations,
            $this->makeEntityKey($childEntity),
            $parentEntity
        );
    }

    /**
     * @param object $childEntity
     */
    public function unsetParent($childEntity): void
    {
        $this->arrayHelper->set($this->relations, $this->makeEntityKey($childEntity), null);
    }

    /**
     * @param $childEntity
     * @return mixed
     * @throws EntityHasNoParentException
     * @return object
     */
    public function getParent($childEntity)
    {
        if (!$this->hasParent($childEntity)) {
            throw new EntityHasNoParentException($childEntity);
        }
        return $this->arrayHelper->get($this->relations, $this->makeEntityKey($childEntity));
    }

    /**
     * @param $parentEntity
     * @return bool
     */
    public function hasChildren($parentEntity): bool
    {
        return boolval(array_search($parentEntity, $this->relations, true));
    }

    /**
     * @param $child
     * @return string
     * @throws EntityHasNoParentException
     */
    public function getParentId($child): string
    {
        $this->entityManager->getEntityId($this->getParent($child));
    }

    /**
     * @param object $child
     * @return bool
     */
    public function hasParent($child): bool
    {
        return $this->arrayHelper->has($this->relations, $this->makeEntityKey($child));
    }

    /**
     * @param object $entity
     * @return string
     */
    protected function makeEntityKey($entity): string
    {
        return get_class($entity) . '_' . $this->entityManager->getEntityId($entity);
    }
}
