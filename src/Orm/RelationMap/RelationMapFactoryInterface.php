<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\RelationMap;

use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;

/**
 * Class RelationManagerFactory
 * @package Orm\RelationMap
 */
interface RelationMapFactoryInterface
{
    /**
     * @param EntityManagerInterface $entityManager
     * @return RelationMap
     */
    public function createRelationMap(EntityManagerInterface $entityManager): RelationMapInterface;
}