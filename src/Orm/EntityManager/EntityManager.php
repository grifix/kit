<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\EntityManager;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Helper\ReflectionHelperInterface;
use Grifix\Kit\Orm\Blueprint\BlueprintFactoryInterface;
use Grifix\Kit\Orm\Blueprint\BlueprintInterface;
use Grifix\Kit\Orm\Blueprint\BlueprintMapInterface;
use Grifix\Kit\Orm\Blueprint\Relation\RelationToParent;
use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Orm\Collection\Injector\CollectionInjectorFactory;
use Grifix\Kit\Orm\Collection\Injector\CollectionInjectorInterface;
use Grifix\Kit\Orm\EventCollector\EventCollectorInterface;
use Grifix\Kit\Orm\EventCollector\EventPublisherInterface;
use Grifix\Kit\Orm\IdentityMap\IdentityItemInterface;
use Grifix\Kit\Orm\IdentityMap\IdentityMapFactoryInterface;
use Grifix\Kit\Orm\IdentityMap\IdentityMapInterface;
use Grifix\Kit\Orm\Persister\PersisterFactoryInterface;
use Grifix\Kit\Orm\Persister\PersisterInterface;
use Grifix\Kit\Orm\RelationMap\RelationMapFactoryInterface;
use Grifix\Kit\Orm\RelationMap\RelationMapInterface;
use Grifix\Kit\Orm\Serializer\ObjectSerializerInterface;
use Grifix\Kit\Orm\Serializer\SerializerFactoryInterface;

/**
 * Class EntityManager
 * @package Grifix\Kit\Orm\EntityManager
 */
class EntityManager implements EntityManagerInterface
{
    /**
     * @var IdentityMapInterface
     */
    protected $identityMap;

    /**
     * @var BlueprintMapInterface
     */
    protected $blueprintMap;

    /**
     * @var RelationMapInterface
     */
    protected $relationMap;

    /**
     * @var ConnectionInterface
     */
    protected $connection;

    /**
     * @var BlueprintFactoryInterface
     */
    protected $blueprintFactory;

    /**
     * @var SerializerFactoryInterface
     */
    protected $serializerFactory;

    /**
     * @var PersisterFactoryInterface
     */
    protected $persisterFactory;

    /**
     * @var ObjectSerializerInterface[]
     */
    protected $serializers = [];

    /**
     * @var PersisterInterface[]
     */
    protected $persisters = [];

    /**
     * @var CollectionInjectorInterface
     */
    protected $collectionInjector;

    /**
     * @var ReflectionHelperInterface
     */
    protected $reflectionHelper;

    /**
     * EntityManager constructor.
     * @param RelationMapFactoryInterface $relationMapFactory
     * @param IdentityMapFactoryInterface $identityMapFactory
     * @param BlueprintMapInterface $blueprintMap
     * @param ConnectionInterface $connection
     * @param BlueprintFactoryInterface $blueprintFactory
     * @param SerializerFactoryInterface $serializerFactory
     * @param PersisterFactoryInterface $persisterFactory
     * @param CollectionInjectorFactory $collectionInjectorFactory
     * @param ReflectionHelperInterface $reflectionHelper
     */
    public function __construct(
        RelationMapFactoryInterface $relationMapFactory,
        IdentityMapFactoryInterface $identityMapFactory,
        BlueprintMapInterface $blueprintMap,
        ConnectionInterface $connection,
        BlueprintFactoryInterface $blueprintFactory,
        SerializerFactoryInterface $serializerFactory,
        PersisterFactoryInterface $persisterFactory,
        CollectionInjectorFactory $collectionInjectorFactory,
        ReflectionHelperInterface $reflectionHelper
    ) {
        $this->identityMap = $identityMapFactory->createIdentityMap($this);
        $this->blueprintMap = $blueprintMap;
        $this->relationMap = $relationMapFactory->createRelationMap($this);
        $this->connection = $connection;
        $this->blueprintFactory = $blueprintFactory;
        $this->serializerFactory = $serializerFactory;
        $this->persisterFactory = $persisterFactory;
        $this->collectionInjector = $collectionInjectorFactory->createCollectionInjector($this);
        $this->reflectionHelper = $reflectionHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityId($entity): string
    {
        return $this->getBlueprint(get_class($entity))->getId($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlueprint(string $entityClass): BlueprintInterface
    {
        return $this->blueprintMap->getBlueprint($entityClass);
    }

    /**
     * {@inheritdoc}
     */
    public function hasBlueprint(string $entityClass): bool
    {
        return $this->blueprintMap->hasBlueprint($entityClass);
    }

    /**
     * {@inheritdoc}
     */
    public function registerBlueprint(string $blueprintClass): EntityManagerInterface
    {
        $this->blueprintMap->registerBlueprint($this->blueprintFactory->createBlueprint($blueprintClass));
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent($entity)
    {
        return $this->relationMap->getParent($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function hasParent($entity): bool
    {
        return $this->relationMap->hasParent($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function hasChildren($entity): bool
    {
        return $this->relationMap->hasChildren($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function isNew($entity): bool
    {
        return $this->identityMap->isNew($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function setParent($entity, $parent): void
    {
        $this->relationMap->setParent($entity, $parent);
    }

    /**
     * {@inheritdoc}
     */
    public function serialize($entity): array
    {
        $blueprint = $this->getBlueprint(get_class($entity));
        $result = $this->getSerializer(get_class($entity))->serialize($entity);
        if ($blueprint->getTypeColumn()) {
            $result[$blueprint->getTypeColumn()] = $blueprint->detectEntityType(get_class($entity));
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function unSerialize(array $data, string $entityClass)
    {
        return $this->getSerializer($this->getBlueprint($entityClass)->detectEntityClass($data))->unSerialize($data);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity(string $entityClass, string $id)
    {
        return $this->identityMap->getEntity($entityClass, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function hasEntity(string $entityClass, string $id)
    {
        return $this->identityMap->hasEntityWithId($entityClass, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function injectQueryCollections($entity): void
    {
        $this->collectionInjector->injectQueryCollections($entity);
    }

    /**
     * @param string $entityClass
     * @return ObjectSerializerInterface
     */
    public function getSerializer(string $entityClass): ObjectSerializerInterface
    {
        if (!isset($this->serializers[$entityClass])) {
            $this->serializers[$entityClass] = $this->serializerFactory->createObjectSerializer(
                $this,
                $entityClass,
                $this->getBlueprint($entityClass)->getSerializerClass()
            );
        }
        return $this->serializers[$entityClass];
    }

    /**
     * @param string $entityClass
     * @return PersisterInterface
     */
    protected function getPersister(string $entityClass): PersisterInterface
    {
        if (!isset($this->persisters[$entityClass])) {
            $blueprint = $this->getBlueprint($entityClass);
            $this->persisters[$entityClass] = $this->persisterFactory->createPersister(
                $blueprint->getPersisterClass(),
                $blueprint->getTable(),
                $this
            );
        }
        return $this->persisters[$entityClass];
    }

    /**
     * {@inheritdoc}
     */
    public function isDirty($entity): bool
    {
        return $this->identityMap->isDirty($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function isMarkedForDeletion($entity): bool
    {
        return $this->identityMap->isMarkedForDeletion($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function unsetParent($entity): void
    {
        $this->relationMap->unsetParent($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function watch($entity, bool $isNew = true): void
    {
        $this->injectQueryCollections($entity);
        $this->identityMap->addEntity($entity, $isNew);
    }

    /**
     * {@inheritdoc}
     */
    public function commitChanges($entity): void
    {
        $this->identityMap->commitChanges($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getDiff($entity): array
    {
        return $this->identityMap->getDiff($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function persist($entity): void
    {
        $this->getPersister(get_class($entity))->persist($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function flush(?object $entity = null): void
    {
        $this->getConnection()->beginTransaction();
        $that = $this;

        $needAction = function (IdentityItemInterface $item) use ($that, $entity) {
            return $entity === null || $entity === $item->getEntity() || $that->areRelated($item->getEntity(), $entity);
        };

        $this->identityMap->visitNewItems(function (IdentityItemInterface $item) use ($that, $needAction) {
            if ($needAction($item)) {
                $that->persist($item->getEntity());
            }
        });
        $this->identityMap->visitDirtyItems(function (IdentityItemInterface $item) use ($that, $needAction) {

            if ($needAction($item)) {
                $that->persist($item->getEntity());
            }
        });

        $this->getConnection()->commitTransaction();

        $this->identityMap->visitAllItems(function (IdentityItemInterface $item) use ($that, $needAction) {
            if ($needAction($item)) {
                $that->publishEvents($item->getEntity());
            }
        });
    }

    /**
     * @param object $childEntity
     * @param object $parentEntity
     * @return bool
     * @throws \ReflectionException
     */
    protected function areRelated(object $childEntity, object $parentEntity)
    {
        $childBlueprint = $this->getBlueprint(get_class($childEntity));
        $parentBlueprint = $this->getBlueprint(get_class($parentEntity));
        if (!$childBlueprint->hasRelationTo($parentBlueprint->getEntityClass())) {
            return false;
        }
        $relation = $childBlueprint->getRelationTo($parentBlueprint->getEntityClass());
        if (!($relation instanceof RelationToParent)) {
            return false;
        }

        /**@var $children CollectionInterface */
        $children = $this->reflectionHelper->getPropertyValue($parentEntity, $relation->getParentProperty());
        foreach ($children as $child) {
            if ($child === $childEntity) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $entity
     * @throws \ReflectionException
     */
    protected function publishEvents($entity): void
    {
        $reflection = new \ReflectionClass($entity);
        if (!$reflection->hasProperty('infrastructure')) {
            return;
        }
        $property = $reflection->getProperty('infrastructure');
        $property->setAccessible(true);
        $infrastructure = $property->getValue($entity);
        $property->setAccessible(false);
        if (!($infrastructure instanceof EventPublisherInterface)) {
            return;
        }
        $reflection = new \ReflectionClass($infrastructure);

        if ($reflection->hasProperty('_mockery_partial')) {
            $partialProperty = $reflection->getProperty('_mockery_partial');
            $partialProperty->setAccessible(true);
            $infrastructure = $partialProperty->getValue($infrastructure);
            $reflection = new \ReflectionClass($infrastructure);
            $partialProperty->setAccessible(false);
        }

        if (!$reflection->hasProperty('eventCollector')) {
            return;
        }
        $property = $reflection->getProperty('eventCollector');
        $property->setAccessible(true);
        $eventCollector = $property->getValue($infrastructure);
        if ($eventCollector instanceof EventCollectorInterface) {
            $eventCollector->flush();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function delete($entity): void
    {
        $this->identityMap->markForDeletion($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getConnection(): ConnectionInterface
    {
        return $this->connection;
    }
}
