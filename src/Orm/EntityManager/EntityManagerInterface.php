<?php
declare(strict_types=1);

namespace Grifix\Kit\Orm\EntityManager;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Orm\Blueprint\BlueprintInterface;
use Grifix\Kit\Orm\Serializer\ObjectSerializerInterface;

/**
 * Interface EntityManager
 * @package Grifix\Kit\Orm
 */
interface EntityManagerInterface
{
    /**
     * @internal
     * @param $entity
     * @param bool $isNew
     */
    public function watch($entity, bool $isNew = true): void;

    /**
     * @api
     * @param null|object $entity
     */
    public function flush(?object $entity = null): void;

    /**
     * @internal
     * @param object $entity
     */
    public function delete($entity): void;

    /**
     * @internal
     * @param string $entityClass
     * @return BlueprintInterface
     */
    public function getBlueprint(string $entityClass): BlueprintInterface;

    /**
     * @internal
     * @param $entity
     * @return string
     */
    public function getEntityId($entity): string;

    /**
     * @internal
     * @param $entity
     * @return object
     * @throws \Orm2\RelationManager\Exception\EntityHasNoParentException
     */
    public function getParent($entity);

    /**
     * @internal
     * @param $entity
     * @return bool
     */
    public function hasParent($entity): bool;

    /**
     * @internal
     * @param object $entity
     * @param object $parent
     */
    public function setParent($entity, $parent): void;

    /**
     * @internal
     * @param object $entity
     */
    public function unsetParent($entity): void;

    /**
     * @internal
     * @return ConnectionInterface
     */
    public function getConnection(): ConnectionInterface;

    /**
     * @api
     * @param string $blueprintClass
     * @return mixed
     */
    public function registerBlueprint(string $blueprintClass): EntityManagerInterface;

    /**
     * @internal
     * @param object $entity
     * @return array
     */
    public function serialize($entity): array;

    /**
     * @internal
     * @param array $data
     * @param string $entityClass
     * @return object
     */
    public function unSerialize(array $data, string $entityClass);

    /**
     * @internal
     * @param object $entity
     * @return array
     * @throws \Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException
     */
    public function getDiff($entity): array;

    /**
     * @internal
     * @param $entity
     * @return bool
     * @throws \Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException
     */
    public function isNew($entity): bool;

    /**
     * @internal
     * @param object $entity
     * @throws \Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException
     */
    public function commitChanges($entity): void;

    /**
     * @internal
     * @param $entity
     * @return bool
     * @throws \Grifix\Kit\Orm\IdentityMap\Exception\ItemNotExistsException
     */
    public function isDirty($entity): bool;

    /**
     * @internal
     * @param object $entity
     */
    public function persist($entity): void;

    /**
     * @internal
     * @param object $entity
     * @return bool
     */
    public function hasChildren($entity): bool;

    /**
     * Injects QueryCollections instead of ArrayCollections or null
     * @internal
     * @param object $entity
     * @throws \ReflectionException
     */
    public function injectQueryCollections($entity): void;

    /**@internal
     * @param string $entityClass
     * @return bool
     */
    public function hasBlueprint(string $entityClass): bool;

    /**
     * @param object $entity
     * @return bool
     */
    public function isMarkedForDeletion($entity): bool;

    /**
     * @internal
     * @param string $entityClass
     * @return ObjectSerializerInterface
     */
    public function getSerializer(string $entityClass): ObjectSerializerInterface;

    /**
     * @internal
     * @param string $entityClass
     * @param string $id
     * @return object
     */
    public function getEntity(string $entityClass, string $id);

    /**
     * @internal
     * @param string $entityClass
     * @param string $id
     * @return bool
     */
    public function hasEntity(string $entityClass, string $id);
}
