<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View;

use Grifix\Kit\View\Skin\SkinInterface;

/**
 * Interface ViewFactoryInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\View
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ViewFactoryInterface
{
    
    /**
     * @param       $path
     * @param array $vars
     *
     * @return ViewInterface
     */
    public function create($path, $vars = []): ViewInterface;
    
    /**
     * @param SkinInterface $skin
     *
     * @return void
     */
    public function setSkin(SkinInterface $skin);
    
    /**
     * @return SkinInterface
     */
    public function getSkin(): SkinInterface;
}
