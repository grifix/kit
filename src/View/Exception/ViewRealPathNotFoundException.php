<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Exception;

/**
 * Class ViewRealPathNotFoundException
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ViewRealPathNotFoundException extends \Exception
{
    
    /**
     * @var string
     */
    protected $viewPath;
    
    protected $searchedPaths;
    
    /**
     * ViewRealPathNotFoundException constructor.
     *
     * @param string $viewPath
     * @param array  $searchedPaths
     */
    public function __construct(string $viewPath, array $searchedPaths)
    {
        $this->viewPath = $viewPath;
        $this->searchedPaths = $searchedPaths;
        $this->message = 'Real path fo view '.$viewPath.' is not found in '.implode(', ',$searchedPaths).'!';
        parent::__construct();
    }
}