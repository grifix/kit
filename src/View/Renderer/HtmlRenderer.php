<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\View\Renderer;

use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\View\ViewInterface;

/**
 * Class HtmlRenderer
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Renderer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class HtmlRenderer implements RendererInterface
{
    /**
     * @param ResponseInterface $response
     * @param ViewInterface     $view
     *
     * @param array             $data
     *
     * @return ResponseInterface
     */
    public function render(ResponseInterface $response, ViewInterface $view, array $data = []): ResponseInterface
    {
        return $response->withHeader('content-type', 'text/html')->withContent($view->render($data));
    }
}