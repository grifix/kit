<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View;

use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Intl\Lang\LangInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Ioc\ServiceLocatorTrait;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Ui\Request\RequestDispatcherInterface;
use Grifix\Kit\View\Asset\AssetCombinerFactoryInterface;
use Grifix\Kit\View\Asset\AssetCombinerInterface;
use Grifix\Kit\View\Event\AfterRenderEvent;
use Grifix\Kit\View\Event\BeforeRenderEvent;
use Grifix\Kit\View\Exception\InvalidViewFileNameException;
use Grifix\Kit\View\Helper\ViewHelperFactoryInterface;
use Grifix\Kit\View\Helper\ViewHelperInterface;

/**
 * Class View
 *
 * @category Grifix
 * @package  Grifix\Kit\View
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class View implements ViewInterface
{
    use ServiceLocatorTrait {
        getShared as public _getShared;
    }

    const PARENT_TAG = '<!--parent-->';

    /**
     * @var string
     */
    protected $path;

    /**
     * @var array
     */
    protected $vars = [];

    /**
     * @var array
     */
    protected $blocks = [];

    /**
     * @var View
     */
    protected $parentView;

    /**
     * @var string
     */
    protected $currentBlock;

    /**
     * @var ViewFactoryInterface
     */
    protected $viewFactory;

    /**
     * @var EventBusInterface
     */
    protected $eventBus;

    /**
     * @var RequestDispatcherInterface
     */
    protected $requestDispatcher;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var AssetCombinerFactoryInterface
     */
    protected $assetCombinerFactory;

    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var array
     */
    protected $js = [];

    /**
     * @var array
     */
    protected $css = [];

    /**
     * @var array
     */
    protected $excludedJs = [];

    /**
     * @var array
     */
    protected $excludedCss = [];

    /**
     * @var View
     */
    protected $childView;

    /**
     * @var string
     */
    protected $assetsPath;

    /**
     * @var string
     */
    protected $publicPath;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var bool
     */
    protected $alwaysCombineAssets = true;

    /**
     * @var string
     */
    protected $cssClass;

    /**
     * @var string
     */
    protected $widgetClass;

    /**
     * @var ViewHelperInterface
     */
    protected $viewHelper;

    /**
     * @var array
     */
    protected $packJsPaths = [];


    /**
     * View constructor.
     *
     * @param EventBusInterface $eventBus
     * @param TranslatorInterface $translator
     * @param ViewFactoryInterface $viewFactory
     * @param RequestDispatcherInterface $requestDispatcher
     * @param ArrayHelperInterface $arrayHelper
     * @param AssetCombinerFactoryInterface $assetCombinerFactory
     * @param KernelInterface $kernel
     * @param IocContainerInterface $iocContainer
     * @param ViewHelperFactoryInterface $viewHelperFactory
     * @param bool $alwaysCombineAssets
     * @param string $path
     * @param string $assetsPath
     * @param string $publicPath
     * @param array $vars
     * @param string|null $cssClass
     *
     * @throws InvalidViewFileNameException
     */
    public function __construct(
        EventBusInterface $eventBus,
        TranslatorInterface $translator,
        ViewFactoryInterface $viewFactory,
        RequestDispatcherInterface $requestDispatcher,
        ArrayHelperInterface $arrayHelper,
        AssetCombinerFactoryInterface $assetCombinerFactory,
        KernelInterface $kernel,
        IocContainerInterface $iocContainer,
        ViewHelperFactoryInterface $viewHelperFactory,
        bool $alwaysCombineAssets,
        string $path,
        string $assetsPath,
        string $publicPath,
        array $vars = [],
        string $cssClass = null
    ) {

        $this->eventBus = $eventBus;
        $this->viewFactory = $viewFactory;
        $this->requestDispatcher = $requestDispatcher;
        $this->arrayHelper = $arrayHelper;
        $this->assetCombinerFactory = $assetCombinerFactory;
        $this->path = $path;
        $this->vars = $vars;
        $this->assetsPath = $assetsPath;
        $this->publicPath = $publicPath;
        $this->alwaysCombineAssets = $alwaysCombineAssets;
        $this->cssClass = $cssClass;
        $this->iocContainer = $iocContainer;
        $this->kernel = $kernel;
        $this->translator = $translator;
        $this->viewHelper = $viewHelperFactory->createViewHelper($this);
        $this->checkFilename();
    }

    /**
     * @return void
     * @throws InvalidViewFileNameException
     */
    protected function checkFilename()
    {
        $filename = basename($this->path);
        if (strpos($filename, self::PREFIX_LAYOUT) !== 0
            && strpos($filename, self::PREFIX_PARTIAL) !== 0
            && strpos($filename, self::PREFIX_TEMPLATE) !== 0
        ) {
            throw new InvalidViewFileNameException($this->path);
        }
    }

    /**
     * @return array
     */
    public function getVars(): array
    {
        return $this->vars;
    }

    /**
     * {@inheritdoc}
     */
    public function setBlocks(array $blocks): ViewInterface
    {
        $this->blocks = $blocks;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getShared(string $alias)
    {
        return $this->_getShared($alias);
    }

    /**
     * {@inheritdoc}
     */
    public function setBlock($name, $value): ViewInterface
    {
        $this->arrayHelper->set($this->blocks, $name, $value);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * {@inheritdoc}
     */
    public function getCssClass(): string
    {
        return $this->cssClass;
    }

    /**
     * @param string $suffix
     *
     * @return string
     */
    public function makeCssClass(string $suffix): string
    {
        return $this->cssClass . '-' . $suffix;
    }

    /**
     * {@inheritdoc}
     */
    public function getWidgetClass(): string
    {
        if (!$this->widgetClass) {
            $arr = explode('_', $this->cssClass);
            $widgetName = array_pop($arr);
            array_pop($arr);
            $this->widgetClass = 'wg-' . implode('_', $arr) . '_' . ucfirst($widgetName);
        }

        return $this->widgetClass;
    }

    /**
     * {@inheritdoc}
     */
    public function inherits($path): ViewInterface
    {
        $this->parentView = $this->viewFactory->create($path);

        return $this;
    }


    /**
     * @param array $vars
     *
     * @return string
     */
    public function render(array $vars = []): string
    {
        $this->vars = array_merge($this->vars, $vars);
        $this->eventBus->trigger(new BeforeRenderEvent());
        ob_start();

        /** @noinspection PhpIncludeInspection */
        require $this->path;

        if (!$this->parentView) {
            $this->eventBus->trigger(new AfterRenderEvent());

            return ob_get_clean();
        } else {
            ob_get_clean();
            $this->parentView->setChildView($this);
            $this->parentView->setBlocks($this->blocks);
            $this->eventBus->trigger(new AfterRenderEvent());


            return $this->parentView->render($this->vars);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function startBlock($blockName): ViewInterface
    {
        $this->currentBlock = $blockName;
        ob_start();

        return $this;
    }

    /**
     * @param string $path
     * @param array $vars
     *
     * @return string
     */
    public function renderPartial(string $path, array $vars = []): string
    {
        $vars = array_merge($this->vars, $vars);

        return $this->viewFactory->create($path, $vars)->render();
    }

    /**
     * {@inheritdoc}
     */
    public function includePartial(string $path): ViewInterface
    {
        include $this->viewFactory->create($path)->getPath();

        return $this;
    }

    /**
     * @return View
     */
    public function getParentView(): View
    {
        return $this->parentView;
    }

    /**
     * {@inheritdoc}
     */
    public function getParentBlock(): string
    {
        return self::PARENT_TAG;
    }

    /**
     * {@inheritdoc}
     */
    public function endBlock(): ViewInterface
    {
        if (!$this->parentView) {

            $blockContent = ob_get_clean();

            $arrContent[] = $blockContent;
            foreach ($this->getAllChildren() as $child) {
                $arrContent[] = $child->getBlockContent($this->currentBlock);
            }

            $blockContent = $arrContent[0];
            foreach ($arrContent as $i => &$item) {
                if ($i > 0 && $item) {
                    $item = $blockContent = str_replace(self::PARENT_TAG, $arrContent[$i - 1], $item);
                }
            }

            echo $blockContent;

        } else {
            $this->blocks[$this->currentBlock] = ob_get_clean();
        }
        $this->currentBlock = null;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addJs(string $path)
    {
        $this->js[] = $path;

        return $this;
    }

    /**
     * @param string $path
     *
     * @return ViewInterface
     */
    public function excludeJs(string $path): ViewInterface
    {
        $this->excludedJs[] = $path;

        return $this;
    }

    /**
     * @param string $path
     *
     * @return ViewInterface
     */
    public function excludeCss(string $path): ViewInterface
    {
        $this->excludedCss[] = $path;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addCss(string $path)
    {
        $this->css[] = $path;

        return $this;
    }

    /**
     * @return array
     */
    public function getExcludedJs(): array
    {
        return $this->excludedJs;
    }

    /**
     * @return array
     */
    public function getExcludedCss(): array
    {
        return $this->excludedCss;
    }

    /**
     * @return View[]
     */
    public function getAllChildren(): array
    {
        $result = [];
        if ($this->getChildView()) {
            $result[] = $this->getChildView();
            $result = array_merge($result, $this->getChildView()->getAllChildren());
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function includeViewJs(): void
    {
        $js = $this->js;
        $excludedJs = $this->excludedJs;


        foreach ($this->getAllChildren() as $child) {
            $js = array_merge($js, $child->getJs());
        }

        foreach ($this->getAllChildren() as $child) {
            $excludedJs = array_merge($excludedJs, $child->getExcludedJs());
        }

        if (count($js)) {
            echo '<script type="text/javascript" src="' .
                $this->createViewAssetsUrl($js, $excludedJs, 'js') . '"></script>';
        }
    }


    /**
     * {@inheritdoc}
     */
    public function includeViewCss(): void
    {
        $css = $this->css;
        $removedCss = $this->excludedCss;
        foreach ($this->getAllChildren() as $child) {
            $css = array_merge($css, $child->getCss());
        }
        foreach ($this->getAllChildren() as $child) {
            $removedCss = array_merge($removedCss, $child->getExcludedCss());
        }


        if (count($css)) {
            echo '<link rel="stylesheet" href="' .
                $this->createViewAssetsUrl($css, $removedCss, 'css') . '">';
        }
    }

    /**
     * @internal
     *
     * @param View $view
     *
     * @return void
     */
    public function setChildView(View $view)
    {
        $this->childView = $view;
    }

    /**
     * @internal
     *
     * @return View|null
     */
    public function getChildView()
    {
        return $this->childView;
    }

    /**
     * @internal
     *
     * @return array
     */
    public function getJs()
    {
        return $this->js;
    }

    /**
     * @internal
     *
     * @return array
     */
    public function getCss(): array
    {
        return $this->css;
    }

    /**
     * {@internal}
     *
     * @param $block
     *
     * @return mixed
     */
    public function getBlockContent($block)
    {
        return $this->arrayHelper->get($this->blocks, $block);
    }

    /**
     * {@inheritdoc}
     */
    public function includeJsPack(string $name, array $assets, array $excludedPaths = []): ViewInterface
    {
        echo '<script type="text/javascript" src="' .
            $this->createAssetsPack($name, 'js', $assets, $excludedPaths) . '"></script>';

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function includeCssPack(string $name, array $assets, array $excludedPaths = []): ViewInterface
    {
        echo '<link rel="stylesheet" href="' .
            $this->createAssetsPack($name, 'css', $assets, $excludedPaths) . '">';

        return $this;
    }

    /**
     * @param string $name
     * @param string $type
     * @param array $assets
     * @param array $excludedPaths
     *
     * @return string
     */
    protected function createAssetsPack(string $name, string $type, array $assets, array $excludedPaths = [])
    {
        $path = $this->assetsPath . DIRECTORY_SEPARATOR . base64_encode($this->path) . '-' . $name . '.' . $type;

        if (!is_file($path) || $this->alwaysCombineAssets) {
            $assetCombiner = $this->assetCombinerFactory->createAssetCombiner($this->viewFactory->getSkin());
            foreach ($assets as $asset) {
                $this->addAssetToCombiner($assetCombiner, $asset, $type);
            }
            foreach ($excludedPaths as $excludedPath) {
                $assetCombiner->excludePath($this->kernel->getRootDir() . DIRECTORY_SEPARATOR . $excludedPath);
            }
            $assetCombiner->combineToFile($path);
            $this->packJsPaths = array_merge($this->packJsPaths, $assetCombiner->getPaths());
        }

        return str_replace($this->publicPath, '', $path);
    }

    /**
     * @param array $assets
     * @param array $removedAssets
     * @param string $type
     *
     * @return string
     */
    protected function createViewAssetsUrl(
        array $assets,
        array $removedAssets,
        string $type
    ) {

        $assetCombiner = $this->assetCombinerFactory->createAssetCombiner($this->viewFactory->getSkin());

        if (!$this->childView) {
            $path = $this->assetsPath . DIRECTORY_SEPARATOR . base64_encode($this->path) . '.' . $type;
        } else {
            /**@var $lastChild View */
            $allChildren = $this->getAllChildren();
            $lastChild = array_pop($allChildren);
            $path = $this->assetsPath . DIRECTORY_SEPARATOR . base64_encode($lastChild->getPath()) . '.' . $type;
        }

        if ($this->alwaysCombineAssets || !is_file($path)) {
            foreach ($assets as $asset) {
                $this->addAssetToCombiner($assetCombiner, $asset, $type);
            }
            foreach ($removedAssets as $removedAsset) {
                $assetCombiner->excludePath($this->kernel->getRootDir() . DIRECTORY_SEPARATOR . $removedAsset);
            }

            //Deleting js that was included in pack
            if ($type === 'js') {
                foreach ($this->packJsPaths as $packJsPath) {
                    $assetCombiner->excludePath($packJsPath);
                }
            }
            $assetCombiner->combineToFile($path);
        }

        return str_replace($this->publicPath, '', $path);
    }


    /**
     * @param AssetCombinerInterface $assetCombiner
     * @param string $asset
     * @param string $type
     *
     * @return void
     */
    protected function addAssetToCombiner(AssetCombinerInterface $assetCombiner, string $asset, string $type)
    {
        $assetFirstPoint = explode('/', $asset)[0];
        if (in_array($assetFirstPoint, ['{src}', 'vendor', 'app'])) {
            $assetCombiner->addAsset($asset, $type);
        } else {
            $assetCombiner->addPath($this->assetsPath . DIRECTORY_SEPARATOR . $asset);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function renderComponent($queryAlias, $view, array $params = []): string
    {
        return $this->viewFactory->create(
            $view,
            array_merge($params, $this->requestDispatcher->dispatch($queryAlias, $params, null))
        )->render();
    }

    /**
     * {@inheritdoc}
     */
    public function getVar($key, $default = null)
    {
        return $this->arrayHelper->get($this->vars, $key, $default);
    }

    /**
     * {@inheritdoc}
     */
    public function getTextVar($key, $default = null): string
    {
        $result = $this->getVar($key, $default);
        if (!is_null($result)) {
            return $this->entitle($result);
        }

        return '';
    }

    public function jsonEncode(array $var)
    {
        return htmlspecialchars(json_encode($var));
    }

    /**
     * {@inheritdoc}
     */
    public function setVar($key, $val): ViewInterface
    {
        $this->arrayHelper->set($this->vars, $key, $val);

        return $this;
    }

    /**
     * @param string $key
     * @param array $vars
     * @param int $quantity
     * @param string $case
     * @param int $form
     *
     * @return string
     */
    public function translate(
        string $key,
        array $vars = [],
        int $quantity = 1,
        string $case = LangInterface::CASE_NOMINATIVE,
        int $form = LangInterface::FORM_SINGULAR
    ) {
        return $this->entitle($this->translator->translate($key, $vars, $quantity, $case, $form));
    }

    /**
     * {@inheritdoc}
     */
    public function getHelper(): ViewHelperInterface
    {
        return $this->viewHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function entitle(string $var)
    {
        return htmlentities($var);
    }
}
