<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View;

use Grifix\Kit\Intl\Lang\LangInterface;
use Grifix\Kit\View\Helper\ViewHelperInterface;
use Grifix\Kit\View\Input\InputBuilderInterface;

/**
 * Interface ViewInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\View
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ViewInterface
{
    const PREFIX_PARTIAL = 'prt';
    const PREFIX_LAYOUT = 'lyt';
    const PREFIX_TEMPLATE = 'tpl';
    
    /**
     * @param array $vars
     *
     * @return string
     */
    public function render(array $vars = []): string;
    
    /**
     * @param array $blocks
     *
     * @return ViewInterface
     */
    public function setBlocks(array $blocks): ViewInterface;
    
    /**
     * @return string
     */
    public function getPath(): string;
    
    /**
     * @param $path
     *
     * @return ViewInterface
     */
    public function inherits($path): ViewInterface;
    
    /**
     * @param $blockName
     *
     * @return ViewInterface
     */
    public function startBlock($blockName): ViewInterface;
    
    /**
     * @return ViewInterface
     *
     */
    public function endBlock(): ViewInterface;
    
    /**
     * @param string $path
     * @param array  $vars
     *
     * @return string
     */
    public function renderPartial(string $path, array $vars = []): string;
    
    /**
     * @param       $queryAlias
     * @param       $view
     * @param array $params
     *
     * @return string
     */
    public function renderComponent($queryAlias, $view, array $params = []): string;
    
    /**
     * @param      $key
     * @param null $default
     *
     * @return mixed
     */
    public function getVar($key, $default = null);
    
    /**
     * @param $key
     * @param $val
     *
     * @return ViewInterface
     */
    public function setVar($key, $val): ViewInterface;
    
    /**
     * @return string
     */
    public function getParentBlock(): string;
    
    /**
     * @param $name
     * @param $value
     *
     * @return ViewInterface
     */
    public function setBlock($name, $value): ViewInterface;
    
    /**
     * @param string $path
     *
     * @return $this
     */
    public function addJs(string $path);
    
    /**
     * @param string $path
     *
     * @return $this
     */
    public function addCss(string $path);
    
    /**
     * Подключает js вида и его потомков добавленные с помодью addJs
     *
     * @return void
     */
    public function includeViewJs(): void;
    
    /**
     * Подключает css вида и его потомков добавленные с помощью addCss
     *
     * @return void
     */
    public function includeViewCss(): void;
    
    /**
     * @return string
     */
    public function getCssClass(): string;
    
    /**
     * @return string
     */
    public function getWidgetClass(): string;
    
    /**
     * @param string $suffix
     *
     * @return string
     */
    public function makeCssClass(string $suffix): string;
    
    /**
     * @param string $path
     *
     * @return ViewInterface
     */
    public function excludeJs(string $path): ViewInterface;
    
    /**
     * @param string $path
     *
     * @return ViewInterface
     */
    public function excludeCss(string $path): ViewInterface;
    
    /**
     * @param string $alias
     *
     * @return mixed
     */
    public function getShared(string $alias);
    
    /**
     * @param string $name
     * @param array  $assets
     * @param array  $excludedPaths
     *
     * @return ViewInterface
     */
    public function includeJsPack(string $name, array $assets, array $excludedPaths = []): ViewInterface;
    
    /**
     * @param       $name
     * @param array $assets
     * @param array $excludedPaths
     *
     * @return ViewInterface
     */
    public function includeCssPack(string $name, array $assets, array $excludedPaths = []): ViewInterface;
    
    /**
     * @param string $path
     *
     * @return ViewInterface
     */
    public function includePartial(string $path): ViewInterface;
    
    /**
     * @param string $key
     * @param array  $vars
     * @param int    $quantity
     * @param string $case
     * @param int    $form
     *
     * @return string
     */
    public function translate(
        string $key,
        array $vars = [],
        int $quantity = 1,
        string $case = LangInterface::CASE_NOMINATIVE,
        int $form = LangInterface::FORM_SINGULAR
    );
    
    /**
     * @param string $var
     *
     * @return string
     */
    public function entitle(string $var);
    
    /**
     * @param      $key
     * @param null $default
     *
     * @return string
     */
    public function getTextVar($key, $default = null): string;
    
    /**
     * @return ViewHelperInterface
     */
    public function getHelper(): ViewHelperInterface;
    
    /**
     * @return array
     */
    public function getVars(): array;
}
