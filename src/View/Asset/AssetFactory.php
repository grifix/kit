<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Asset;

use Grifix\Kit\Helper\FilesystemHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\View\Skin\SkinInterface;

/**
 * Class AssetFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Asset
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class AssetFactory extends AbstractFactory implements AssetFactoryInterface
{
    
    /**
     * @var string
     */
    protected $rootDir;
    
    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;
    
    /**
     * AssetFactory constructor.
     *
     * @param ClassMakerInterface       $classMaker
     * @param string                    $rootDir <root_dir>
     * @param FilesystemHelperInterface $filesystemHelper
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        string $rootDir,
        FilesystemHelperInterface $filesystemHelper
    ) {
        $this->rootDir = $rootDir;
        $this->filesystemHelper = $filesystemHelper;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createAsset(string $path, SkinInterface $skin, string $extension = null): AssetInterface
    {
        $class = $this->makeClassName(Asset::class);
        
        return new $class($this->rootDir . DIRECTORY_SEPARATOR . $path, $skin, $this->filesystemHelper, $extension);
    }
}