<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Asset;

use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\View\Skin\SkinInterface;

/**
 * Class AssetCombinerFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Asset
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class AssetCombinerFactory extends AbstractFactory implements AssetCombinerFactoryInterface
{
    /**
     * @var AssetFactoryInterface
     */
    protected $assetFactory;

    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;

    /**
     * @var EventBusInterface
     */
    protected $eventBus;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * AssetCombinerFactory constructor.
     *
     * @param ClassMakerInterface $classMaker
     * @param AssetFactoryInterface $assetFactory
     * @param FilesystemHelperInterface $filesystemHelper
     * @param EventBusInterface $eventBus
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        AssetFactoryInterface $assetFactory,
        FilesystemHelperInterface $filesystemHelper,
        EventBusInterface $eventBus,
        ArrayHelperInterface $arrayHelper
    ) {
        $this->assetFactory = $assetFactory;
        $this->filesystemHelper = $filesystemHelper;
        $this->eventBus = $eventBus;
        $this->arrayHelper = $arrayHelper;
        parent::__construct($classMaker);
    }

    /**
     * @param SkinInterface $skin
     *
     * @return AssetCombiner
     */
    public function createAssetCombiner(SkinInterface $skin)
    {
        $class = $this->makeClassName(AssetCombiner::class);

        return new $class($skin, $this->assetFactory, $this->filesystemHelper, $this->eventBus, $this->arrayHelper);
    }
}