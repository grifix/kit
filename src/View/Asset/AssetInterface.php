<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\View\Asset;


/**
 * Class Asset
 *
 * @category Grifix
 * @package  Grifix\Kit\View\AssetCombiner
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface AssetInterface
{
    /**
     * @return string
     */
    public function getPath(): string;
    
    /**
     * Возвращает реальный пути к файлам ассета, с учетом расширения по скинам и вендору
     *
     * @return array
     */
    public function getRealPaths(): array;
}