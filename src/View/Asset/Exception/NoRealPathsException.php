<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Asset\Exception;


use Grifix\Kit\View\Asset\AssetInterface;

/**
 * Class NoRealPathsException
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Asset\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class NoRealPathsException extends \Exception
{
    protected $asset;
    
    /**
     * NoRealPathsException constructor.
     *
     * @param AssetInterface $asset
     */
    public function __construct(AssetInterface $asset)
    {
        $this->asset = $asset;
        $this->message = 'No real paths for asset path "'.$asset->getPath().'""!';
        parent::__construct();
    }
}