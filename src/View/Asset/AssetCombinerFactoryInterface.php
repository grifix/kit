<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\View\Asset;

use Grifix\Kit\View\Skin\SkinInterface;


/**
 * Class AssetCombinerFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Asset
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface AssetCombinerFactoryInterface
{
    /**
     * @param SkinInterface $skin
     *
     * @return AssetCombiner
     */
    public function createAssetCombiner(SkinInterface $skin);
}