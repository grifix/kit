<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Asset;

use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelperInterface;
use Grifix\Kit\View\Asset\Event\CombineAssetEvent;
use Grifix\Kit\View\Skin\SkinInterface;

/**
 * Class AssetCombiner
 *
 * @category Grifix
 * @package  Grifix\Kit\Asset
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class AssetCombiner implements AssetCombinerInterface
{
    /**
     * @var Asset[]
     */
    protected $assets = [];
    
    /**
     * @var Asset[]
     */
    protected $excludedPaths = [];
    
    /**
     * @var SkinInterface
     */
    protected $skin;
    
    /**
     * @var AssetFactoryInterface
     */
    protected $assetFactory;
    
    /**
     * @var FilesystemHelperInterface
     */
    protected $fileSystemHelper;
    
    /**
     * @var array
     */
    protected $paths = [];
    
    /**
     * @var EventBusInterface
     */
    protected $eventBus;
    
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;
    
    /**
     * AssetCombiner constructor.
     *
     * @param SkinInterface             $skin
     * @param AssetFactoryInterface     $assetFactory
     * @param FilesystemHelperInterface $filesystemHelper
     * @param EventBusInterface     $eventBus
     * @param ArrayHelperInterface      $arrayHelper
     */
    public function __construct(
        SkinInterface $skin,
        AssetFactoryInterface $assetFactory,
        FilesystemHelperInterface $filesystemHelper,
        EventBusInterface $eventBus,
        ArrayHelperInterface $arrayHelper
    ) {
        $this->skin = $skin;
        $this->assetFactory = $assetFactory;
        $this->fileSystemHelper = $filesystemHelper;
        $this->eventBus = $eventBus;
        $this->arrayHelper = $arrayHelper;
    }
    
    /**
     * {@inheritdoc}
     */
    public function addAsset(string $path, string $extension = null): AssetCombinerInterface
    {
        $this->assets[] = $this->assetFactory->createAsset($path, $this->skin, $extension);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function excludePath(string $path): AssetCombinerInterface
    {
        $this->excludedPaths[] = $path;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function addPath(string $path): AssetCombinerInterface
    {
        $this->paths[] = $path;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function combine(): string
    {
        foreach ($this->assets as $asset) {
            $this->paths = array_merge($this->paths, $asset->getRealPaths());
        }
        $this->paths = array_unique($this->paths);
        $result = '';
        
        foreach ($this->paths as $i => $path) {
            if (!in_array($this->paths, $this->excludedPaths)) {
                $this->resolveDependencies($path, $i);
            }
        }
        
        $this->paths = array_values(array_unique($this->paths));
        
        
        foreach ($this->paths as $path) {
            if (!in_array($path, $this->excludedPaths)) {
                $event = new CombineAssetEvent($path, file_get_contents($path));
                $this->eventBus->trigger($event);
                $result .= "\n".$event->getAssetContent();
            }
        }
        
        return $result;
    }
    
    /**
     * @return array
     */
    public function getPaths(): array
    {
        return $this->paths;
    }
    
    /**
     * @param $path
     * @param $index
     *
     * @return void
     */
    protected function resolveDependencies($path, $index)
    {
        $content = $this->fileSystemHelper->readFromFile($path);
        preg_match_all("/\/\*require (.*)\*\//", $content, $dependencies);
        foreach ($dependencies[1] as $dependency) {
            $asset = $this->assetFactory->createAsset($dependency, $this->skin);
            foreach ($asset->getRealPaths() as $realPath) {
                $this->arrayHelper->insert($this->paths, $realPath, $index);
                $this->resolveDependencies($realPath, $index);
            }
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function combineToFile(string $path): int
    {
        return $this->fileSystemHelper->writeToFile($path, $this->combine(), 'w');
    }
    
}