<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Asset;

use Grifix\Kit\Helper\FilesystemHelperInterface;
use Grifix\Kit\View\Asset\Exception\NoRealPathsException;
use Grifix\Kit\View\Skin\SkinInterface;

/**
 * Class Asset
 *
 * @category Grifix
 * @package  Grifix\Kit\View\AssetCombiner
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Asset implements AssetInterface
{
    const MODE_BY_SKIN = 1;
    const MODE_BY_VENDOR = 2;
    const MODE_BY_SKIN_AND_VENDOR = 3;
    const MODE_NOT_EXTEND = 4;
    
    /**
     * @var string
     */
    protected $path;
    
    /**
     * @var SkinInterface
     */
    protected $skin;
    
    /**
     * @var int
     */
    protected $extendMode;
    
    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;
    
    /**
     * @var array
     */
    protected $realPaths = [];
    
    /**
     * @var string
     */
    protected $extension;
    
    /**
     * Asset constructor.
     *
     * @param string                    $path
     * @param SkinInterface             $skin
     * @param FilesystemHelperInterface $filesystemHelper
     * @param string                    $extension расширение файлов если $path указывает на папку
     */
    public function __construct(
        string $path,
        SkinInterface $skin,
        FilesystemHelperInterface $filesystemHelper,
        string $extension = null
    ) {
        $this->path = $path;
        $this->skin = $skin;
        $this->extension = $extension;
        $this->extendMode = self::MODE_NOT_EXTEND;
        $this->filesystemHelper = $filesystemHelper;
        if (strpos($this->path, '{skin}')) {
            $this->extendMode = self::MODE_BY_SKIN;
        }
        if (strpos($this->path, '{src}')) {
            $this->extendMode = self::MODE_BY_VENDOR;
        }
        if (strpos($this->path, '{src}') && strpos($this->path, '{skin}')) {
            $this->extendMode = self::MODE_BY_SKIN_AND_VENDOR;
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function getPath(): string
    {
        return $this->path;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getRealPaths(): array
    {
        if (!$this->realPaths) {
            if ($this->extendMode == self::MODE_BY_SKIN_AND_VENDOR) {
                foreach ($this->skin->getParentSkinsNames() as $skin) {
                    foreach (['vendor', 'app'] as $src) {
                        $path = str_replace(['{skin}', '{src}'], [$skin, $src], $this->path);
                        if (file_exists($path)) {
                            $this->realPaths[] = $path;
                        }
                    }
                }
            } elseif ($this->extendMode == self::MODE_BY_SKIN) {
                foreach ($this->skin->getParentSkinsNames() as $skin) {
                    foreach (['vendor', 'app'] as $src) {
                        $path = str_replace(['{skin}', '{src}'], [$skin, $src], $this->path);
                        if (file_exists($path)) {
                            $this->realPaths[] = $path;
                            break;
                        }
                    }
                }
            } elseif ($this->extendMode == self::MODE_BY_VENDOR) {
                foreach (['vendor', 'app'] as $src) {
                    foreach ($this->skin->getParentSkinsNames() as $skin) {
                        $path = str_replace(['{skin}', '{src}'], [$skin, $src], $this->path);
                        if (file_exists($path)) {
                            $this->realPaths[] = $path;
                            break;
                        }
                    }
                }
            } elseif ($this->extendMode == self::MODE_NOT_EXTEND) {
                if (file_exists($this->path)) {
                    $this->realPaths[] = $this->path;
                }
            }
            
            if (!$this->realPaths) {
                throw new NoRealPathsException($this);
            }
            
            foreach ($this->realPaths as $i => &$realPath) {
                if (is_dir($realPath)) {
                    unset($this->realPaths[$i]);
                    $this->realPaths = array_merge($this->realPaths, $this->scanDir($realPath));
                }
            }
        }
        
        return $this->realPaths;
    }
    
    /**
     * @param $dir
     *
     * @return array
     */
    protected function scanDir($dir)
    {
        
        $files = $this->filesystemHelper->scanDir($dir);
        $result = [];
        foreach ($files as $file) {
            if (isset($file['extension']) && $file['extension'] == $this->extension) {
                $result[] = $file['path'];
            }
        }
        
        return $result;
    }
}