<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\View\Input\InputBuilderFactoryInterface;
use Grifix\Kit\View\Input\InputBuilderInterface;

/**
 * Class ViewHelper
 *
 * @category Grifix
 * @package  Grifix\Kit\View
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ViewHelperInterface
{
    /**
     * @return InputBuilderInterface
     */
    public function createTextInputBuilder(): InputBuilderInterface;
    
    /**
     * @return InputBuilderInterface
     */
    public function createPasswordInputBuilder(): InputBuilderInterface;
    
    /**
     * @return InputBuilderInterface
     */
    public function createEmailInputBuilder(): InputBuilderInterface;
    
    /**
     * @return InputBuilderFactoryInterface
     */
    public function getInputBuilderFactory(): InputBuilderFactoryInterface;
    
    /**
     * @return InputBuilderInterface
     */
    public function createFileInputBuilder(): InputBuilderInterface;
    
    /**
     * @param string $vendor
     * @param string $module
     *
     * @return bool
     */
    public function hasModule(string $vendor, string $module): bool;
}