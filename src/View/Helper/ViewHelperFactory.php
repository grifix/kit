<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\View\Input\InputBuilderFactoryInterface;
use Grifix\Kit\View\ViewInterface;

/**
 * Class ViewHelperFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\View
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ViewHelperFactory extends AbstractFactory implements ViewHelperFactoryInterface
{
    protected $inputBuilderFactory;
    
    /**
     * ViewHelperFactory constructor.
     *
     * @param ClassMakerInterface          $classMaker
     * @param InputBuilderFactoryInterface $inputBuilderFactory
     */
    public function __construct(ClassMakerInterface $classMaker, InputBuilderFactoryInterface $inputBuilderFactory)
    {
        $this->inputBuilderFactory = $inputBuilderFactory;
        parent::__construct($classMaker);
    }
    
    /**
     * @param ViewInterface $view
     *
     * @return ViewHelperInterface
     */
    public function createViewHelper(ViewInterface $view): ViewHelperInterface
    {
        $class = $this->makeClassName(ViewHelper::class);
        
        return new $class($view, $this->inputBuilderFactory);
    }
}
