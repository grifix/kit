<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Helper;

use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\View\Input\InputBuilderFactoryInterface;
use Grifix\Kit\View\Input\InputBuilderInterface;
use Grifix\Kit\View\ViewInterface;

/**
 * Class ViewHelper
 *
 * @category Grifix
 * @package  Grifix\Kit\View
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ViewHelper implements ViewHelperInterface
{
    /**
     * @var ViewInterface
     */
    protected $view;

    /**
     * @var InputBuilderFactoryInterface
     */
    protected $inputBuilderFactory;

    /**
     * ViewHelper constructor.
     *
     * @param ViewInterface $view
     * @param InputBuilderFactoryInterface $inputBuilderFactory
     */
    public function __construct(
        ViewInterface $view,
        InputBuilderFactoryInterface $inputBuilderFactory
    )
    {
        $this->view = $view;
        $this->inputBuilderFactory = $inputBuilderFactory;
    }

    /**
     * @return InputBuilderFactoryInterface
     */
    public function getInputBuilderFactory(): InputBuilderFactoryInterface
    {
        return $this->inputBuilderFactory;
    }

    /**
     * @return InputBuilderInterface
     */
    public function createTextInputBuilder(): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createTextInputBuilder($this->view);
    }

    /**
     * @return InputBuilderInterface
     */
    public function createPasswordInputBuilder(): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createPasswordInputBuilder($this->view);
    }

    /**
     * @return InputBuilderInterface
     */
    public function createEmailInputBuilder(): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createEmailInputBuilder($this->view);
    }

    /**
     * @return InputBuilderInterface
     */
    public function createFileInputBuilder(): InputBuilderInterface
    {
        return $this->inputBuilderFactory->createFileInputBuilder($this->view);
    }

    /**
     * @param string $vendor
     * @param string $module
     *
     * @return bool
     */
    public function hasModule(string $vendor, string $module): bool
    {
        return $this->view->getShared(KernelInterface::class)->hasModule($vendor, $module);
    }
}
