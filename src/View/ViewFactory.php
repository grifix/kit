<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View;

use Grifix\Kit\Cache\CacheKeyTrait;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelperInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Ui\Request\RequestDispatcherInterface;
use Grifix\Kit\View\Asset\AssetCombinerFactoryInterface;
use Grifix\Kit\View\Exception\ViewRealPathNotFoundException;
use Grifix\Kit\View\Helper\ViewHelperFactoryInterface;
use Grifix\Kit\View\Skin\SkinFactoryInterface;
use Grifix\Kit\View\Skin\SkinInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class ViewFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\View
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ViewFactory extends AbstractFactory implements ViewFactoryInterface
{
    
    use CacheKeyTrait;
    
    /**
     * @var IocContainerInterface
     */
    protected $ioc;
    
    /**
     * @var SkinInterface
     */
    protected $skin;
    
    /**
     * @var EventBusInterface
     */
    protected $eventBus;
    
    /**
     * @var CacheInterface
     */
    protected $cache;
    
    /**
     * @var KernelInterface
     */
    protected $kernel;
    
    /**
     * @var SkinFactoryInterface
     */
    protected $skinFactory;
    
    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;
    
    /**
     * @var RequestDispatcherInterface
     */
    protected $requestDispatcher;
    
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;
    
    /**
     * @var AssetCombinerFactoryInterface
     */
    protected $assetCombinerFactory;
    
    /**
     * @var string
     */
    protected $assetsPath;
    
    /**
     * @var string
     */
    protected $publicPath;
    
    /**
     * @var bool
     */
    protected $alwaysCombineAssets = true;
    
    /**
     * @var TranslatorInterface
     */
    protected $translator;
    
    protected $viewHelperFactory;
    
    /**
     * ViewFactory constructor.
     *
     * @param IocContainerInterface         $ioc
     * @param TranslatorInterface           $translator
     * @param EventBusInterface         $eventManager
     * @param CacheInterface                $cache
     * @param KernelInterface               $kernel
     * @param SkinFactoryInterface          $skinFactory
     * @param AssetCombinerFactoryInterface $assetCombinerFactory
     * @param FilesystemHelperInterface     $filesystemHelper
     * @param RequestDispatcherInterface    $requestDispatcher
     * @param ArrayHelperInterface          $arrayHelper
     * @param ClassMakerInterface           $classMaker
     * @param ViewHelperFactoryInterface  $viewHelperFactory
     * @param string                        $assetsPath          <cfg:grifix.kit.view.assetsPath>
     * @param string                        $publicPath          <cfg:grifix.kit.view.publicPath>
     * @param bool                          $alwaysCombineAssets <cfg:grifix.kit.view.alwaysCombineAssets>
     */
    public function __construct(
        IocContainerInterface $ioc,
        TranslatorInterface $translator,
        EventBusInterface $eventManager,
        CacheInterface $cache,
        KernelInterface $kernel,
        SkinFactoryInterface $skinFactory,
        AssetCombinerFactoryInterface $assetCombinerFactory,
        FilesystemHelperInterface $filesystemHelper,
        RequestDispatcherInterface $requestDispatcher,
        ArrayHelperInterface $arrayHelper,
        ClassMakerInterface $classMaker,
        ViewHelperFactoryInterface $viewHelperFactory,
        string $assetsPath,
        string $publicPath,
        bool $alwaysCombineAssets
    ) {
        $this->ioc = $ioc;
        $this->translator = $translator;
        $this->eventBus = $eventManager;
        $this->cache = $cache;
        $this->kernel = $kernel;
        $this->skinFactory = $skinFactory;
        $this->filesystemHelper = $filesystemHelper;
        $this->skin = $this->skinFactory->createSkin();
        $this->requestDispatcher = $requestDispatcher;
        $this->arrayHelper = $arrayHelper;
        $this->assetCombinerFactory = $assetCombinerFactory;
        $this->assetsPath = $this->kernel->getRootDir() . DIRECTORY_SEPARATOR . $assetsPath;
        $this->alwaysCombineAssets = $alwaysCombineAssets;
        $this->publicPath = $this->kernel->getRootDir() . DIRECTORY_SEPARATOR . $publicPath;
        $this->viewHelperFactory = $viewHelperFactory;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function setSkin(SkinInterface $skin)
    {
        $this->skin = $skin;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getSkin(): SkinInterface
    {
        return $this->skin;
    }
    
    /**
     * {@inheritdoc}
     */
    public function create($path, $vars = []): ViewInterface
    {
        if (strpos($path, DIRECTORY_SEPARATOR) === false) {
            $path = $this->makePath($path);
        }
        /**@var $class View */
        $class = $this->makeClassName(View::class);
        
        return new $class(
            $this->eventBus,
            $this->translator,
            $this,
            $this->requestDispatcher,
            $this->arrayHelper,
            $this->assetCombinerFactory,
            $this->kernel,
            $this->ioc,
            $this->viewHelperFactory,
            $this->alwaysCombineAssets,
            $path,
            $this->assetsPath,
            $this->publicPath,
            $vars,
            $this->makeCssClass($path)
        );
    }
    
    /**
     * @param string $path
     *
     * @return string
     */
    protected function makeCssClass(string $path): string
    {
        $path = str_replace($this->kernel->getRootDir(), '', $path);
        $path = explode(DIRECTORY_SEPARATOR, $path);
        unset($path[0], $path[1], $path[4], $path[5]);
        $path = array_values($path);
        $tpl = explode('.', array_pop($path));
        $result = implode('_', $path) . '_' . $tpl[0] . '_' . $tpl[1];
        
        return $result;
    }
    
    /**
     * @param string $pathKey
     *
     * @return string
     * @throws ViewRealPathNotFoundException
     */
    protected function makePath(string $pathKey): string
    {
        $cacheKey = $this->makeCacheKey($pathKey);
        $result = $this->cache->get($cacheKey);
        if (!$result) {
            $arrKey = explode('.', $pathKey);
            $vendor = array_shift($arrKey);
            $package = array_shift($arrKey);
            $skinName = array_shift($arrKey);
            $viewName = array_pop($arrKey);
            $viewType = array_pop($arrKey);
            $skin = $this->skin;
            if ($skinName != '{skin}') {
                $skin = $this->skinFactory->createSkin($skinName);
            }
            
            $dirs = [$this->kernel->getAppDir(), $this->kernel->getVendorDir()];
            $keyPath = null;
            if ($arrKey) {
                $keyPath = implode('/', $arrKey) . '/';
            }
            $result = '{dir}/' . $vendor . '/' . $package . '/views/{skin}/' . $keyPath
                . $viewType . '.' . $viewName . '.php';
            
            $pathExists = false;
            $searchedPaths = [];
            $skins = array_reverse($skin->getParentSkinsNames());
            foreach ($skins as $sName) {
                foreach ($dirs as $dir) {
                    $path = str_replace('{dir}', $dir, $result);
                    $path = str_replace('{skin}', $sName, $path);
                    $searchedPaths[] = $path;
                    if ($this->filesystemHelper->fileExists($path)) {
                        $result = $path;
                        $pathExists = true;
                        break 2;
                    }
                }
            }
            if (!$pathExists) {
                throw new ViewRealPathNotFoundException($result, $searchedPaths);
            }
            $this->cache->set($cacheKey, $result);
        }
        
        return $result;
    }
}
