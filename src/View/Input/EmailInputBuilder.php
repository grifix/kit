<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Input;

use Grifix\Kit\View\Input\Exception\NotSupportedMethodException;

/**
 * Class FileInputBuilder
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Form
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class EmailInputBuilder extends AbstractInputBuilder
{
    /** @noinspection PhpMissingParentCallCommonInspection */
    
    /**
     * @return array
     */
    protected function prepareVars(): array
    {
        return array_merge(parent::prepareVars(), ['type' => 'email']);
    }
}
