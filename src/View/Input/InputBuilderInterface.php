<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Input;

/**
 * Class AbstractInputBuilder
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Form
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface InputBuilderInterface
{
    /**
     * @param string $partial
     *
     * @return InputBuilderInterface
     */
    public function setPartial(string $partial):InputBuilderInterface;
    
    /**
     * @param string $name
     *
     * @return InputBuilderInterface
     */
    public function setName(string $name): InputBuilderInterface;
    
    /**
     * @param string $class
     *
     * @return InputBuilderInterface
     */
    public function removeClass(string $class): InputBuilderInterface;
    
    /**
     * @param string $class
     *
     * @return InputBuilderInterface
     */
    public function addClass(string $class): InputBuilderInterface;
    
    /**
     * @param string $class
     *
     * @return InputBuilderInterface
     */
    public function setClass(string $class): InputBuilderInterface;
    
    /**
     * @return InputBuilderInterface
     */
    public function disabledBox(): InputBuilderInterface;
    
    /**
     * @return InputBuilderInterface
     */
    public function enableBox(): InputBuilderInterface;
    
    /**
     * @return string
     */
    public function build(): string;
    
    /**
     * @param string $value
     *
     * @return InputBuilderInterface
     */
    public function setValue(string $value): InputBuilderInterface;
    
    /**
     * @param string $label
     *
     * @return InputBuilderInterface
     */
    public function setLabel(string $label): InputBuilderInterface;
    
    /**
     * @param string $placeholder
     *
     * @return InputBuilderInterface
     */
    public function setPlaceholder(string $placeholder): InputBuilderInterface;
    
    /**
     * @param bool $required
     *
     * @return InputBuilderInterface
     */
    public function setRequired(bool $required = true): InputBuilderInterface;
    
    /**
     * @param string $description
     *
     * @return InputBuilderInterface
     */
    public function setDescription(string $description): InputBuilderInterface;
}