<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Input;

use Grifix\Kit\View\ViewInterface;


/**
 * Class InputBuilderFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Input
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface InputBuilderFactoryInterface
{
    /**
     * @param ViewInterface $view
     *
     * @return InputBuilderInterface
     */
    public function createTextInputBuilder(ViewInterface $view): InputBuilderInterface;
    
    /**
     * @param ViewInterface $view
     *
     * @return InputBuilderInterface
     */
    public function createPasswordInputBuilder(ViewInterface $view): InputBuilderInterface;
    
    /**
     * @param ViewInterface $view
     *
     * @return InputBuilderInterface
     */
    public function createEmailInputBuilder(ViewInterface $view): InputBuilderInterface;
    
    /**
     * @param ViewInterface $view
     *
     * @return InputBuilderInterface
     */
    public function createFileInputBuilder(ViewInterface $view): InputBuilderInterface;
}