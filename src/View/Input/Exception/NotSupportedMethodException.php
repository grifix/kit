<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Input\Exception;

use Throwable;

/**
 * Class NotSupportedMethodException
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Form\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class NotSupportedMethodException extends \RuntimeException
{
    /**
     * NotSupportedMethodException constructor.
     *
     * @param string $method
     * @param string $class
     */
    public function __construct(string $method, string $class)
    {
        $this->message = 'Method "' . $method . '" is not supported in "' . $class . "!";
    }
}
