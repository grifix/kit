<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\View\Input;

use Grifix\Kit\View\ViewInterface;

/**
 * Class AbstractInputBuilder
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Form
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractInputBuilder implements InputBuilderInterface
{
    protected $view;
    
    protected $boxEnabled = true;
    
    protected $placeholder;
    
    protected $class = ['form-input'];
    
    protected $dataRole;
    
    protected $name;
    
    protected $label;
    
    protected $partial = 'grifix.kit.{skin}.input.prt.input';
    
    protected $required = false;
    
    protected $value;
    
    protected $boxPartial = 'grifix.kit.{skin}.input.prt.inputBox';
    
    protected $description;
    
    /**
     * AbstractInputBuilder constructor.
     *
     * @param ViewInterface $view
     */
    public function __construct(ViewInterface $view)
    {
        $this->view = $view;
    }
    
    /**
     * @return array
     */
    protected function prepareVars(): array
    {
        return [
            'name' => $this->name,
            'class' => implode(' ', array_unique($this->class)),
            'placeholder' => $this->placeholder,
            'dataRole' => $this->dataRole,
            'value' => $this->value,
            'required' => $this->required,
            'description' => $this->description
        ];
    }
    
    /**
     * @return string
     */
    private function render(): string
    {
        return $this->view->renderPartial(
            $this->partial,
            $this->prepareVars()
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function build(): string
    {
        if ($this->boxEnabled) {
            return $this->renderBox();
        } else {
            return $this->render();
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function setLabel(string $label): InputBuilderInterface
    {
        $this->label = $label;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setRequired(bool $required = true): InputBuilderInterface
    {
        $this->required = $required;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setPlaceholder(string $placeholder): InputBuilderInterface
    {
        $this->placeholder = $placeholder;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setValue(string $value): InputBuilderInterface
    {
        $this->value = $value;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setDescription(string $description): InputBuilderInterface
    {
        $this->description = $description;
        
        return $this;
    }
    
    /**
     * @return string
     */
    private function renderBox(): string
    {
        return $this->view->renderPartial(
            $this->boxPartial,
            [
                'label' => $this->label,
                'input' => $this->render(),
                'description' => $this->description,
                'required' => $this->required,
                'name' => $this->name
            ]
        );
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function setPartial(string $partial): InputBuilderInterface
    {
        $this->partial = $partial;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setName(string $name): InputBuilderInterface
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function enableBox(): InputBuilderInterface
    {
        $this->boxEnabled = true;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function disabledBox(): InputBuilderInterface
    {
        $this->boxEnabled = false;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setClass(string $class): InputBuilderInterface
    {
        $this->class = [$class];
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function addClass(string $class): InputBuilderInterface
    {
        $this->class[] = $class;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function removeClass(string $class): InputBuilderInterface
    {
        foreach ($this->class as $i => $v) {
            if ($v === $class) {
                unset($this->class[$i]);
            }
        }
        
        return $this;
    }
}
