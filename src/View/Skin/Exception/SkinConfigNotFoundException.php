<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Skin\Exception;

use Exception;


/**
 * Class SkinConfigNotFoundException
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Skin\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SkinConfigNotFoundException extends \Exception
{
    protected $skinName;
    
    /**
     * SkinConfigNotFoundException constructor.
     *
     * @param string         $skinName
     * @param Exception|null $previous
     */
    public function __construct(string $skinName, \Exception $previous = null)
    {
        $this->skinName = $skinName;
        $this->message = 'Config for skin "' . $skinName . '" was not found!';
        parent::__construct($previous);
    }
}
