<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\View\Skin;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\View\Skin\Exception\SkinConfigNotFoundException;

/**
 * Class SkinFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Skin
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SkinFactory extends AbstractFactory implements SkinFactoryInterface
{
    
    /**
     * @var KernelInterface
     */
    protected $kernel;
    
    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;
    
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;
    
    /**
     * @var string
     */
    protected $defaultSkinName;
    
    /**
     * SkinFactory constructor.
     *
     * @param KernelInterface           $kernel
     * @param FilesystemHelperInterface $filesystemHelper
     * @param ArrayHelperInterface      $arrayHelper
     * @param ClassMakerInterface       $classMaker
     * @param string                    $defaultSkinName <cfg:grifix.kit.view.defaultSkin>
     */
    public function __construct(
        KernelInterface $kernel,
        FilesystemHelperInterface $filesystemHelper,
        ArrayHelperInterface $arrayHelper,
        ClassMakerInterface $classMaker,
        string $defaultSkinName
    ) {
        $this->kernel = $kernel;
        $this->filesystemHelper = $filesystemHelper;
        $this->arrayHelper = $arrayHelper;
        $this->defaultSkinName = $defaultSkinName;
        parent::__construct($classMaker);
    }
    
    /**
     * @param $skinName
     *
     * @return SkinInterface
     */
    public function createSkin(string $skinName = null)
    {
        if (!$skinName) {
            $skinName = $this->defaultSkinName;
        }
        $config = $this->loadSkinConfig($skinName);
        
        /**@var $class Skin */
        $class = $this->makeClassName(Skin::class);
        $parentSkin = null;
        if ($this->arrayHelper->get($config, 'parent')) {
            $parentSkin = $this->createSkin($config['parent']);
        }
        
        return new $class($skinName, $parentSkin, $this->arrayHelper->get($config, 'persistent', false),$this->arrayHelper->get($config, 'contentType', null));
    }
    
    /**
     * @param string $skinName
     *
     * @return array
     * @throws SkinConfigNotFoundException
     */
    protected function loadSkinConfig(string $skinName): array
    {
        $result = [
            'parents' => null,
            'persistent' => false,
        ];
        $appPath = $this->kernel->getAppDir() . '/grifix/kit/views/' . $skinName . '/config.php';
        $vendorPath = $this->kernel->getVendorDir() . '/grifix/kit/views/' . $skinName . '/config.php';
        if (!$this->filesystemHelper->fileExists($appPath) && !$this->filesystemHelper->fileExists($vendorPath)) {
            throw new SkinConfigNotFoundException($skinName);
        }
        if ($this->filesystemHelper->fileExists($vendorPath)) {
            /** @noinspection PhpIncludeInspection */
            $result = include $vendorPath;
        }
        
        if ($this->filesystemHelper->fileExists($appPath)) {
            /** @noinspection PhpIncludeInspection */
            $result = $this->arrayHelper->merge($result, include $appPath);
        }
        
        return $result;
    }
}
