<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\View\Skin;

/**
 * Class SkinFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Skin
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface SkinFactoryInterface
{
    /**
     * @param string|null $skinName
     *
     * @return SkinInterface
     */
    public function createSkin(string $skinName = null);
}
