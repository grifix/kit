<?php
declare(strict_types=1);

namespace Grifix\Kit\Collection\Finder;

use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Collection\GenericCollectionInterface;
use Grifix\Kit\Helper\ReflectionHelperInterface;
use Grifix\Kit\Type\ValuableTypeInterface;

/**
 * Class CollectionFinder
 * @package Grifix\Kit\Collection
 */
class CollectionFinder implements CollectionFinderInterface
{
    /**
     * @var CollectionFactoryInterface
     */
    protected $collectionFactory;

    /**
     * @var ReflectionHelperInterface
     */
    protected $reflectionHelper;

    /**
     * CollectionFinder constructor.
     * @param CollectionFactoryInterface $collectionFactory
     * @param ReflectionHelperInterface $reflectionHelper
     */
    public function __construct(
        CollectionFactoryInterface $collectionFactory,
        ReflectionHelperInterface $reflectionHelper
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->reflectionHelper = $reflectionHelper;
    }

    /**
     * @param GenericCollectionInterface $collection
     * @param string $property
     * @param $value
     * @return CollectionInterface
     * @throws \ReflectionException
     */
    public function find(GenericCollectionInterface $collection, string $property, $value): CollectionInterface
    {
        $array = [];
        foreach ($collection as $element) {
            $propertyValue = $this->reflectionHelper->getPropertyValue($element, $property);
            if ($propertyValue instanceof ValuableTypeInterface) {
                $propertyValue = $propertyValue->getValue();
            }
            if ($propertyValue === $value) {
                $array[] = $element;
            }
        }
        return $this->collectionFactory->createCollection($array);
    }
}
