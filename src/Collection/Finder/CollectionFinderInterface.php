<?php
declare(strict_types=1);

namespace Grifix\Kit\Collection\Finder;


use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Collection\GenericCollectionInterface;

/**
 * Class CollectionFinder
 * @package Grifix\Kit\Collection
 */
interface CollectionFinderInterface
{
    /**
     * @param GenericCollectionInterface $collection
     * @param string $property
     * @param $value
     * @return CollectionInterface
     * @throws \ReflectionException
     */
    public function find(GenericCollectionInterface $collection, string $property, $value): CollectionInterface;
}
