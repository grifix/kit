<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Collection;

use Grifix\Kit\Kernel\AbstractFactory;

/**
 * Class CollectionFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Collection
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CollectionFactory extends AbstractFactory implements CollectionFactoryInterface
{
    
    /**
     * @param array $elements
     *
     * @return CollectionInterface
     */
    public function createCollection(array $elements = []):CollectionInterface
    {
        $class = $this->makeClassName(Collection::class);
        
        return new $class($elements);
    }
}
