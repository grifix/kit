<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Collection;

use Ds\Collection;
use Grifix\Kit\Contract\ArrayableInterface;

/**
 * PHP Version 7
 *
 * @category Grifix
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface GenericCollectionInterface extends
    Collection,
    \ArrayAccess,
    \Serializable,
    \IteratorAggregate,
    ArrayableInterface
{

}
