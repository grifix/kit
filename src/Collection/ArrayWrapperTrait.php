<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Collection;

use Ds\Traits\Collection;

/**
 * Class CollectionTrait
 *
 * @category Grifix
 * @package  Grifix\Utils
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/doc/
 */
trait ArrayWrapperTrait
{
    use Collection;

    protected $elements = [];

    /**
     * CollectionTrait constructor.
     *
     * @param array $elements array of elements
     */
    public function __construct(array $elements = [])
    {
        $this->setCollectionArray($elements);
    }

    /**
     * Returns collection elements
     *
     * @return array
     */
    protected function getCollectionArray(): array
    {
        return $this->elements;
    }

    /**
     * Sets collection elements
     *
     * @param array $elements array of elements
     *
     * @return $this
     */
    protected function setCollectionArray(array $elements)
    {
        $this->elements = $elements;

        return $this;
    }

    /**
     * Clear the collection
     *
     * @return void
     */
    public function clear(): void
    {
        $this->setCollectionArray([]);
    }

    /**
     * Cont collection elements
     *
     * @return int
     */
    public function count(): int
    {
        return count($this->getCollectionArray());
    }

    /**
     * Convert collection to array
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->getCollectionArray();
    }

    /**
     * Returns iterator
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->getCollectionArray());
    }

    /**
     * Sets offset
     *
     * @param mixed $offset offset
     * @param mixed $value value
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        $elements = $this->getCollectionArray();
        if (is_null($offset)) {
            $elements[] = $value;
        } else {
            $elements[$offset] = $value;
        }
        $this->setCollectionArray($elements);
    }

    /**
     * Check offset exists
     *
     * @param mixed $offset offset
     *
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->getCollectionArray()[$offset]);
    }

    /**
     * Unset offset
     *
     * @param mixed $offset offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        $elements = $this->getCollectionArray();
        unset($elements[$offset]);
        $this->setCollectionArray($elements);
    }

    /**
     * Gets offset
     *
     * @param mixed $offset offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        $elements = $this->getCollectionArray();

        return isset($elements[$offset]) ? $elements[$offset] : null;
    }

    /**
     * Serialize
     *
     * @return string
     */
    public function serialize()
    {
        return \serialize($this->getCollectionArray());
    }

    /**
     * Unserialize
     *
     * @param mixed $elements serialized elements
     *
     * @return void
     */
    public function unserialize($elements)
    {
        $this->setCollectionArray(unserialize($elements));
    }

    /**
     * Returns the first  element
     *
     * @return mixed|null
     */
    public function first()
    {
        if ($this->count()) {
            $arr = $this->toArray();

            return array_shift($arr);
        }

        return null;
    }

    /**
     * @param $element
     *
     * @return void
     */
    public function add($element): void
    {
        $elements = $this->getCollectionArray();
        $elements[] = $element;
        $this->setCollectionArray($elements);
    }

    /**
     * @param $element
     *
     * @return void
     */
    public function remove($element): void
    {
        foreach ($this->getCollectionArray() as $i => $existedElement) {
            if ($element === $existedElement) {
                $this->offsetUnset($i);
            }
        }
    }

    /**
     * Returns the last element
     *
     * @return mixed|null
     */
    public function last()
    {
        if ($this->count()) {
            $arr = $this->toArray();

            return array_pop($arr);
        }

        return null;
    }

    /**
     * @param array $array
     *
     * @return void
     */
    public function set(array $array): void
    {
        $this->setCollectionArray($array);
    }
}
