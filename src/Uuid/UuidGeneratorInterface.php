<?php
declare(strict_types=1);

namespace Grifix\Kit\Uuid;

/**
 * Class Uuid
 * @package Grifix\Kit\Uuid
 */
interface UuidGeneratorInterface
{
    /**
     * @return string
     * @throws \Exception
     */
    public function generate(): string;
}