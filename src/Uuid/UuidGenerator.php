<?php
declare(strict_types=1);

namespace Grifix\Kit\Uuid;

use Ramsey\Uuid\Uuid;

/**
 * Class Uuid
 * @package Grifix\Kit\Uuid
 */
class UuidGenerator implements UuidGeneratorInterface
{
    /**
     * @return string
     * @throws \Exception
     */
    public function generate(): string
    {
        return Uuid::uuid4()->toString();
    }
}
