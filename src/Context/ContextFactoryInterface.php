<?php
declare(strict_types=1);

namespace Grifix\Kit\Context;


/**
 * Class ContextFactory
 * @package Grifix\Kit\Context
 */
interface ContextFactoryInterface
{
    /**
     * @return ContextInterface
     */
    public function createContext(): ContextInterface;
}