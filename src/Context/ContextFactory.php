<?php
declare(strict_types=1);

namespace Grifix\Kit\Context;

use Grifix\Kit\Kernel\AbstractFactory;

/**
 * Class ContextFactory
 * @package Grifix\Kit\Context
 */
class ContextFactory extends AbstractFactory implements ContextFactoryInterface
{
    /**
     * @return ContextInterface
     */
    public function createContext(): ContextInterface
    {
        $class = $this->makeClassName(Context::class);
        return new $class;
    }
}
