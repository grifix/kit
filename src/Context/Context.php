<?php
declare(strict_types=1);

namespace Grifix\Kit\Context;

use Grifix\Kit\Context\Exception\ContextPropertyAlreadyExistsException;
use Grifix\Kit\Context\Exception\ContextPropertyNotExistsException;

/**
 * Class Context
 * @package Grifix\Kit\Context
 */
class Context implements ContextInterface
{
    protected $properties = [];

    /**
     * {@inheritdoc}
     */
    public function set(string $property, $value, bool $force = false): void
    {
        if (!$force && array_key_exists($property, $this->properties)) {
            throw new ContextPropertyAlreadyExistsException($property);
        }
        $this->properties[$property] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $property)
    {
        if (!array_key_exists($property, $this->properties)) {
            throw new ContextPropertyNotExistsException($property);
        }
        return $this->properties[$property];
    }
}
