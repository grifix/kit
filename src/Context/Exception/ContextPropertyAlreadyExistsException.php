<?php
declare(strict_types=1);

namespace Grifix\Kit\Context\Exception;

/**
 * Class PropertyAlreadySetException
 * @package Grifix\Kit\Context\Exception
 */
class ContextPropertyAlreadyExistsException extends \Exception
{
    protected $property;

    /**
     * ContextPropertyAlreadySetException constructor.
     * @param string $property
     */
    public function __construct(string $property)
    {
        $this->property = $property;
        parent::__construct(sprintf('Context property "%s" is already exists!', $property));
    }
}
