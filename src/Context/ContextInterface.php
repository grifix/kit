<?php
declare(strict_types=1);

namespace Grifix\Kit\Context;

use Grifix\Kit\Context\Exception\ContextPropertyAlreadyExistsException;
use Grifix\Kit\Context\Exception\ContextPropertyNotExistsException;

/**
 * Class Context
 * @package Grifix\Kit\Context
 */
interface ContextInterface
{
    /**
     * @param string $property
     * @param $value
     * @param bool $force
     * @throws ContextPropertyAlreadyExistsException
     */
    public function set(string $property, $value, bool $force = false): void;

    /**
     * @param string $property
     * @return mixed
     * @throws ContextPropertyNotExistsException
     */
    public function get(string $property);
}