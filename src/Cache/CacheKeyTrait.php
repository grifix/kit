<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Cache;

/**
 * Class CacheKeyTrait
 *
 * @category Grifix
 * @package  Grifix\Kit\Helper
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
trait CacheKeyTrait
{
    /**
     * @param string|null $suffix
     *
     * @return string
     */
    protected function makeCacheKey(string $suffix = null): string
    {
        $result = str_replace('\\', '.', get_class($this));
        if ($suffix) {
            $result .= '.' . $suffix;
        }
        
        return $result;
    }
}
