<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Cache\Adapter;

/**
 * Interface AdapterInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Cache\Adapters
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface AdapterInterface
{
    /**
     * @param string $key
     * @param string $value
     * @param int $ttl
     * @return bool
     */
    public function set(string $key, string $value, int $ttl = 0): bool;

    /**
     * @param string $key
     * @return null|string
     */
    public function get(string $key): ?string;

    /**
     * @param string $key
     *
     * @return bool
     */
    public function delete(string $key): bool;

    /**
     * Clear all cache
     *
     * @return bool
     */
    public function clear(): bool;
}
