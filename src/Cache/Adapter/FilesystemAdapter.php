<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Cache\Adapter;

use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Serializer\SerializerInterface;

/**
 * Class FileSystemAdapter
 *
 * @category Grifix
 * @package  Grifix\Cache\Adapters
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FilesystemAdapter implements AdapterInterface
{
    protected $path;
    protected $filesystem;
    protected $serializer;
    protected $times = [];

    /**
     * FileSystemAdapter constructor.
     *
     * @param string $path
     * @param FilesystemInterface $filesystem
     */
    public function __construct(string $path, FilesystemInterface $filesystem)
    {
        $this->path = $path;
        $this->filesystem = $filesystem;
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $key, string $value, int $ttl = 0): bool
    {
        if ($ttl === 0) {
            $this->times[$key] = null;
        } else {
            $this->times[$key] = time() + $ttl;
        }
        return $this->filesystem->put($this->makePath($key), $value);
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key): ?string
    {
        $path = $this->makePath($key);
        if (!$this->filesystem->has($path)) {
            return null;
        }
        if ($this->isExpired($key)) {
            $this->delete($key);
            return null;
        }

        return $this->filesystem->read($path);
    }

    /**
     * @param $key
     * @return bool
     */
    protected function isExpired($key)
    {
        return $this->times[$key] !== null && $this->times[$key] <= time();
    }

    /**
     * @inheritdoc
     */
    public function delete(string $key): bool
    {
        $path = $this->makePath($key);
        if ($this->filesystem->has($path)) {
            return $this->filesystem->delete($path);
        }
        unset($this->times[$key]);

        return false;
    }

    /**
     * @param $key
     *
     * @return string
     */
    protected function makeFileName($key)
    {
        $result = str_replace('.', '_', $key);
        if (strlen($result) > 255) {
            $result = md5($result);
        }

        return $result;
    }

    /**
     * @param $key
     *
     * @return string
     */
    protected function makePath($key)
    {
        return $this->path . DIRECTORY_SEPARATOR . $this->makeFileName($key);
    }

    /**
     * {@inheritdoc}
     */
    public function clear(): bool
    {
        return $this->filesystem->deleteDir($this->path);
    }
}
