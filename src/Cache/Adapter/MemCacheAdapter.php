<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Cache\Adapter;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Serializer\SerializerInterface;

/**
 * Class MemCacheAdapter
 *
 * @category Grifix
 * @package  Grifix\Cache\Adapters
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MemCacheAdapter implements AdapterInterface
{

    /**
     * @var \Memcache
     */
    protected $memcache;


    /**
     * MemCacheAdapter constructor.
     * @param array $servers
     */
    public function __construct(\Memcache $memcache)
    {
        $this->memcache = $memcache;
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $key, string $value, int $ttl = 0): bool
    {
        return $this->memcache->set($key, $value, MEMCACHE_COMPRESSED, $ttl);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(string $key): bool
    {
        return $this->memcache->delete($key);
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key): ?string
    {
        $result = $this->memcache->get($key);
        if ($result === false) {
            return null;
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function clear(): bool
    {
        return $this->memcache->flush();
    }

    public function __destruct()
    {
        @$this->memcache->close();
    }
}
