<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Cache;

use Grifix\Kit\Cache\Adapter\MemCacheServerDto;
use Psr\SimpleCache\CacheInterface;

/**
 * Class SimpleCacheFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Cache
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface SimpleCacheFactoryInterface
{
    /**
     * @param string $path
     * @param \DateInterval, int       $ttl
     *
     * @return SimpleCache
     */
    public function createFileSystemCache(string $path): CacheInterface;

    /**
     * @param MemCacheServerDto[] $servers
     * @return CacheInterface
     */
    public function createMemCache(array $servers): CacheInterface;

    /**
     * @return CacheInterface
     */
    public function createNullCache(): CacheInterface;
}
