<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Cache;

use Grifix\Kit\Cache\Adapter\AdapterInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\DateHelperInterface;
use Grifix\Kit\Serializer\SerializerInterface;
use Psr\SimpleCache\CacheInterface;
use Grifix\Kit\Cache\Exception\InvalidArgumentException;

/**
 * Class Cache
 *
 * @category Grifix
 * @package  Grifix\Kit\Cache
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SimpleCache implements CacheInterface
{

    /**
     * @var AdapterInterface
     */
    protected $adapter;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var DateHelperInterface
     */
    protected $dateHelper;


    /**
     * SimpleCache constructor.
     * @param AdapterInterface $adapter
     * @param SerializerInterface $serializer
     * @param DateHelperInterface $dateHelper
     */
    public function __construct(
        AdapterInterface $adapter,
        SerializerInterface $serializer,
        DateHelperInterface $dateHelper
    ) {
        $this->adapter = $adapter;
        $this->serializer = $serializer;
        $this->dateHelper = $dateHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function get(
        $key,
        $default = null
    ) {
        $result = $this->adapter->get($key);
        if (!is_null($result)) {
            return $this->serializer->unSerialize($result);
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function set(
        $key,
        $value,
        $ttl = null
    ) {
        if (is_null($ttl)) {
            $ttl = 0;
        }
        if ($ttl instanceof \DateInterval) {
            $ttl = $this->dateHelper->dateIntervalToSeconds($ttl);
        }
        $this->adapter->set($key, $this->serializer->serialize($value), $ttl);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        $key
    ) {
        $this->isValidKeyOrFail($key);

        return $this->adapter->delete($key);
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        return $this->adapter->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function getMultiple(
        $keys,
        $default = null
    ) {
        $result = [];
        foreach ($keys as $key) {
            $result[$key] = $this->get($key, $default);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function setMultiple(
        $values,
        $ttl = null
    ) {
        foreach ($values as $key => $value) {
            if (!$this->set($key, $value, $ttl)) {
                return false;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteMultiple(
        $keys
    ) {
        foreach ($keys as $key) {
            if (!$this->delete($key)) {
                return false;
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function has(
        $key
    ) {
        return !is_null($this->get($key));
    }

    /**
     * @param $key
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    protected function isValidKeyOrFail(
        $key
    ) {
        if (!preg_match('/[A-Za-z0-9_.]/', $key)) {
            throw new InvalidArgumentException('Invalid key!');
        }
    }
}
