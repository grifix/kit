<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Cache;

use Grifix\Kit\Cache\Adapter\AdapterFactoryInterface;
use Grifix\Kit\Cache\Adapter\FilesystemAdapter;
use Grifix\Kit\Cache\Adapter\MemCacheAdapter;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\DateHelperInterface;
use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Serializer\SerializerInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class SimpleCacheFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Cache
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SimpleCacheFactory extends AbstractFactory implements SimpleCacheFactoryInterface
{

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var AdapterFactoryInterface
     */
    protected $adapterFactory;

    /**
     * @var DateHelperInterface
     */
    protected $dateHelper;

    /**
     * SimpleCacheFactory constructor.
     * @param SerializerInterface $serializer
     * @param ClassMakerInterface $classMaker
     * @param AdapterFactoryInterface $adapterFactory
     * @param DateHelperInterface $dateHelper
     */
    public function __construct(
        SerializerInterface $serializer,
        ClassMakerInterface $classMaker,
        AdapterFactoryInterface $adapterFactory,
        DateHelperInterface $dateHelper
    ) {
        $this->serializer = $serializer;
        $this->adapterFactory = $adapterFactory;
        $this->dateHelper = $dateHelper;
        parent::__construct($classMaker);
    }

    /**
     * {@inheritdoc}
     */
    public function createFileSystemCache(string $path): CacheInterface
    {
        $class = $this->makeClassName(SimpleCache::class);
        return new $class(
            $this->adapterFactory->createFileSystemAdapter($path),
            $this->serializer,
            $this->dateHelper
        );
    }

    /**
     * {@inheritdoc}
     */
    public function createMemCache(array $servers): CacheInterface
    {
        $class = $this->makeClassName(SimpleCache::class);
        return new $class(
            $this->adapterFactory->createMemcacheAdapter($servers),
            $this->serializer,
            $this->dateHelper
        );
    }

    /**
     * {@inheritdoc}
     */
    public function createNullCache(): CacheInterface
    {
        $class = $this->makeClassName(NullCache::class);
        return new $class;
    }
}
