<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Cqrs;

use Grifix\Kit\Cqrs\Command\CommandBusInterface;
use Grifix\Kit\Cqrs\Query\QueryBusInterface;
use Grifix\Kit\Ioc\ServiceLocatorTrait;

/**
 * Class CqrsClientTrait
 *
 * @category Grifix
 * @package  Grifix\Kit\Cqrs
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
trait CqrsClientTrait
{
    use ServiceLocatorTrait;
    
    /**
     * @param object $query
     *
     * @throws \Throwable
     *
     * @return mixed
     */
    protected function executeQuery($query)
    {
        return $this->getShared(QueryBusInterface::class)->execute($query);
    }
    
    /**
     * @param object $command
     *
     * @throws \Throwable
     *
     * @return mixed
     */
    protected function executeCommand($command)
    {
        return $this->getShared(CommandBusInterface::class)->execute($command);
    }
}
