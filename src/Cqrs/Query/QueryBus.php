<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Cqrs\Query;

use Grifix\Kit\Cqrs\Query\QueryBusInterface;
use Grifix\Kit\Cqrs\Query\QueryHandlerFactory;
use Grifix\Kit\Cqrs\Query\QueryHandlerFactoryInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class QueryDispatcher
 *
 * @category Grifix
 * @package  Grifix\Kit\Cqrs
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class QueryBus implements QueryBusInterface
{
    
    /**
     * @var ClassMakerInterface
     */
    protected $classMaker;
    
    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;
    
    /**
     * QueryDispatcher constructor.
     *
     * @param ClassMakerInterface   $classMaker
     * @param IocContainerInterface $iocContainer
     */
    public function __construct(ClassMakerInterface $classMaker, IocContainerInterface $iocContainer)
    {
        $this->classMaker = $classMaker;
        $this->iocContainer = $iocContainer;
    }
    
    /**
     * {@inheritdoc}
     */
    public function execute($query)
    {
        $handler = $this->resolveQueryHandlerFactory($query)->createQueryHandler($query);
        return $handler($query);
    }
    
    /**
     * @param object $query
     *
     * @return QueryHandlerFactoryInterface
     */
    protected function resolveQueryHandlerFactory($query)
    {
        $class = $this->classMaker->makeClassName(get_class($query) . 'RouteRouteHandlerFactory');
        if (!class_exists($class)) {
            $class = $this->classMaker->makeClassName(QueryHandlerFactory::class);
        }
        
        return new $class($this->iocContainer, $this->classMaker);
    }
}
