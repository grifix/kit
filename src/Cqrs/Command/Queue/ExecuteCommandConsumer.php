<?php
declare(strict_types=1);

namespace Grifix\Kit\Cqrs\Command\Queue;

use Grifix\Kit\Cqrs\Command\CommandHandlerFactoryInterface;
use Grifix\Kit\Ioc\IocContainerInterface;

/**
 * Class ExecuteCommandConsumer
 * @package Grifix\Kit\Cqrs\Command
 */
class ExecuteCommandConsumer
{
    protected $iocContainer;

    /**
     * ExecuteCommandConsumer constructor.
     * @param IocContainerInterface $iocContainer
     */
    public function __construct(IocContainerInterface $iocContainer)
    {
        $this->iocContainer = $iocContainer;
    }

    /**
     * @param ExecuteCommandMessage $message
     */
    public function __invoke(ExecuteCommandMessage $message)
    {
        /**@var $factory CommandHandlerFactoryInterface */
        $factory = $this->iocContainer->get($message->getCommandHandlerFactoryClass());
        $handler = $factory->createCommandHandler($message->getCommand());
        $handler($message->getCommand());
    }
}
