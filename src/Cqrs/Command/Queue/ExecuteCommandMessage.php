<?php
declare(strict_types=1);

namespace Grifix\Kit\Cqrs\Command\Queue;

/**
 * Class ExecuteCommandMessage
 * @package Grifix\Kit\Cqrs\Command
 */
class ExecuteCommandMessage
{
    /**
     * @var object
     */
    protected $command;

    /**
     * @var string
     */
    protected $commandHandlerFactoryClass;

    /**
     * ExecuteCommandMessage constructor.
     * @param object $command
     * @param string $commandHandlerFactoryClass
     */
    public function __construct(object $command, string $commandHandlerFactoryClass)
    {
        $this->command = $command;
        $this->commandHandlerFactoryClass = $commandHandlerFactoryClass;
    }

    /**
     * @return object
     */
    public function getCommand(): object
    {
        return $this->command;
    }

    /**
     * @return string
     */
    public function getCommandHandlerFactoryClass(): string
    {
        return $this->commandHandlerFactoryClass;
    }
}
