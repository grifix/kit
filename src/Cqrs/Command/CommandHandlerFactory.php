<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Cqrs\Command;

use Grifix\Kit\Cqrs\AbstractHandlerFactory;

/**
 * Class CommandHandlerFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Cqrs
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CommandHandlerFactory extends AbstractHandlerFactory implements CommandHandlerFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createCommandHandler($command): callable
    {
        $class = get_class($command) . 'Handler';
        /**@var callable $result */
        $result = $this->iocContainer->createNewInstance($class);
        return $result;
    }
}

