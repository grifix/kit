<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Cqrs\Command;

use Grifix\Kit\Cqrs\Command\Queue\ExecuteCommandMessage;
use Grifix\Kit\Helper\ClassHelperInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Queue\QueueInterface;

/**
 * Class CommandBus
 *
 * @category Grifix
 * @package  Grifix\Kit\Cqrs
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CommandBus implements CommandBusInterface
{
    public const QUEUE_NAME = 'command_bus';

    /**
     * @var ClassMakerInterface
     */
    protected $classMaker;

    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    /**
     * @var QueueInterface
     */
    protected $queue;

    /**
     * @var array
     */
    protected $asyncCommands;

    /**
     * @var ClassHelperInterface
     */
    protected $classHelper;

    /**
     * CommandBus constructor.
     * @param ClassMakerInterface $classMaker
     * @param IocContainerInterface $iocContainer
     * @param QueueInterface $queue
     * @param array $asyncCommands <cfg:grifix.kit.cqrs.asyncCommands>
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        IocContainerInterface $iocContainer,
        QueueInterface $queue,
        array $asyncCommands = []
    ) {
        $this->classMaker = $classMaker;
        $this->iocContainer = $iocContainer;
        $this->queue = $queue;
        $this->asyncCommands = $asyncCommands;
    }

    /**
     * @param object $command
     *
     * @return void
     */
    public function execute($command): void
    {
        if ($this->isAsyncCommand($command)) {
            $this->queue->publish(
                new ExecuteCommandMessage($command, $this->resolveHandlerFactoryClass($command)),
                self::QUEUE_NAME
            );
        } else {
            $handler = $this->resolveCommandHandlerFactory($command)->createCommandHandler($command);
            $handler($command);
        }
    }

    /**
     * @param object $command
     * @return bool
     */
    protected function isAsyncCommand(object $command): bool
    {
        return in_array(get_class($command), $this->asyncCommands);
    }

    /**
     * @param object $command
     * @return string
     */
    protected function resolveHandlerFactoryClass(object $command): string
    {
        $result = $this->classMaker->makeClassName(get_class($command) . 'HandlerFactory');
        if (!class_exists($result)) {
            $result = $this->classMaker->makeClassName(CommandHandlerFactory::class);
        }
        return $result;
    }


    /**
     * @param object $command
     *
     * @return CommandHandlerFactoryInterface
     */
    protected function resolveCommandHandlerFactory($command)
    {
        $class = $this->resolveHandlerFactoryClass($command);
        return new $class($this->iocContainer, $this->classMaker);
    }
}
