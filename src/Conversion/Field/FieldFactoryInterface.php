<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Conversion\Field;


/**
 * Class FieldFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Conversion
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface FieldFactoryInterface
{
    /**
     * @param string $name
     *
     * @param string $class
     *
     * @return FieldInterface
     */
    public function createField(string $name, string $class = null): FieldInterface;
}