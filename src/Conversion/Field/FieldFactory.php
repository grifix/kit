<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Conversion\Field;

use Grifix\Kit\Conversion\Converter\ConverterFactoryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class FieldFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Conversion
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FieldFactory extends AbstractFactory implements FieldFactoryInterface
{
    /**
     * @var ConverterFactoryInterface
     */
    protected $converterFactory;
    
    /**
     * FieldFactory constructor.
     *
     * @param ConverterFactoryInterface $converterFactory
     * @param ClassMakerInterface $classMaker
     */
    public function __construct(ConverterFactoryInterface $converterFactory, ClassMakerInterface $classMaker)
    {
        $this->converterFactory = $converterFactory;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createField(string $name, string $class = null): FieldInterface
    {
        if(is_null($class)){
            $class = Field::class;
        }
        
        $class = $this->makeClassName($class);
        
        return new $class($name, $this->converterFactory);
    }
}