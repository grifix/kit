<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Conversion\Field;

use Grifix\Kit\Conversion\Exception\ConverterAlreadyExistsException;
use Grifix\Kit\Conversion\Exception\ConverterDoesNotExistsException;
use Grifix\Kit\Conversion\Converter\ConverterFactoryInterface;
use Grifix\Kit\Conversion\Converter\ConverterInterface;

/**
 * Class Field
 *
 * @category Grifix
 * @package  Grifix\Kit\Field
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Field implements FieldInterface
{
    /**
     * @var string
     */
    protected $name;
    
    /**
     * @var ConverterInterface[]
     */
    protected $converters = [];
    
    /**
     * @var bool
     */
    protected $enabled = true;
    
    /**
     * @var ConverterFactoryInterface
     */
    protected $converterFactory;
    
    /**
     * Field constructor.
     *
     * @param string                    $name
     * @param ConverterFactoryInterface $converterFactory
     */
    public function __construct(string $name, ConverterFactoryInterface $converterFactory)
    {
        $this->name = $name;
        $this->converterFactory = $converterFactory;
    }
    
    /**
     * {@inheritdoc}
     */
    public function enable(): FieldInterface
    {
        $this->enabled = true;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function disable(): FieldInterface
    {
        $this->enabled = false;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setName(string $name): FieldInterface
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }
    
    /**
     * {@inheritdoc}
     */
    public function hasConverter(string $converterClass): bool
    {
        return in_array($converterClass, array_keys($this->converters));
    }
    
    /**
     * {@inheritdoc}
     */
    public function removeConverter(string $converterClass): FieldInterface
    {
        unset($this->converters[$converterClass]);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getConverter(string $converterClass): ConverterInterface
    {
        if (!$this->hasConverter($converterClass)) {
            throw new ConverterDoesNotExistsException($converterClass);
        }
        
        return $this->converters[$converterClass];
    }
    
    /**
     * {@inheritdoc}
     */
    public function addConverter(ConverterInterface $converter): FieldInterface
    {
        $converterClass = get_class($converter);
        if ($this->hasConverter($converterClass)) {
            throw new ConverterAlreadyExistsException($converterClass);
        }
        $this->converters[$converterClass] = $converter;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function createConverter(string $converterClass): ConverterInterface
    {
        $converter = $this->converterFactory->createConverter($converterClass);
        $this->addConverter($converter);
        
        return $converter;
    }
    
    /**
     * {@inheritdoc}
     */
    public function convert($value)
    {
        if ($this->isEnabled()) {
            foreach ($this->converters as $converter) {
                $value = $converter->convert($value);
            }
        }
        
        return $value;
    }
}