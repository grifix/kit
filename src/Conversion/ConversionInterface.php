<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Conversion;

use Grifix\Kit\Conversion\Exception\FieldAlreadyExistsException;
use Grifix\Kit\Conversion\Exception\FieldIsNotExistsException;
use Grifix\Kit\Conversion\Field\FieldInterface;

/**
 * Class Conversion
 *
 * @category Grifix
 * @package  Kit\Conversion
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ConversionInterface
{
    /**
     * @param FieldInterface $field
     *
     * @return ConversionInterface
     * @throws FieldAlreadyExistsException
     */
    public function addField(FieldInterface $field): ConversionInterface;
    
    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasField(string $name): bool;
    
    /**
     * @param string $name
     *
     * @return FieldInterface
     * @throws FieldIsNotExistsException
     */
    public function getField(string $name): FieldInterface;
    
    /**
     * @param string $name
     *
     * @param string $class
     *
     * @return FieldInterface
     */
    public function createField(string $name, string $class = null): FieldInterface;
    
    /**
     * @param array $values
     *
     * @return void
     */
    public function convert(array &$values);
}
