<?php

namespace Grifix\Kit\Conversion\Converter;

use Grifix\Kit\Collection\CollectionInterface;

/**
 * Class CollectionToJsonConverter
 * @package Grifix\Kit\Conversion\Serializer
 */
class CollectionToStringConverter extends AbstractConverter
{
    /**
     * {@inheritdoc}
     */
    protected function doConvert($value)
    {
        if ($value instanceof CollectionInterface) {
            return '{' . implode(',', $value->toArray()) . '}';
        }
        return $value;
    }
}