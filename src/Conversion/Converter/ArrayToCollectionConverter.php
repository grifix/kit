<?php


namespace Grifix\Kit\Conversion\Converter;

use Grifix\Kit\Collection\CollectionFactoryInterface;

/**
 * Class ArrayToCollectionConverter
 * @package Grifix\Kit\Conversion\Serializer
 */
class ArrayToCollectionConverter extends AbstractConverter
{

    /**
     * @var CollectionFactoryInterface
     */
    protected $collectionFactory;

    /**
     * ArrayToCollectionConverter constructor.
     * @param CollectionFactoryInterface $collectionFactory
     */
    public function __construct(CollectionFactoryInterface $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function doConvert($value)
    {
        if (is_array($value)) {
            return $this->collectionFactory->createCollection($value);
        }
        return $value;
    }
}
