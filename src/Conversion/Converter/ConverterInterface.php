<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Conversion\Converter;

/**
 * Class ConverterInterface
 *
 * @category Grifix
 * @package  Kit\Conversion\Serializer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ConverterInterface
{
    /**
     * @param mixed $value
     *
     * @return mixed
     */
    public function convert($value);
    
    /**
     * @return ConverterInterface
     */
    public function enable(): ConverterInterface;
    
    /**
     * @return ConverterInterface
     */
    public function disable(): ConverterInterface;
}