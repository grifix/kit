<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Conversion\Converter;

use Grifix\Kit\Type\IpAddress;

/**
 * Class StringToIpConverter
 *
 * @category Grifix
 * @package  Grifix\Kit\Conversion\Serializer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class StringToIpConverter extends AbstractConverter
{
    /**
     * {@inheritdoc}
     */
    protected function doConvert($value)
    {
        if (!is_null($value)) {
            return new IpAddress($value);
        }
        return null;
    }
}