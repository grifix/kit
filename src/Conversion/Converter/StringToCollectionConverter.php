<?php


namespace Grifix\Kit\Conversion\Converter;

use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Helper\StringHelperInterface;

/**
 * Class ArrayToCollectionConverter
 * @package Grifix\Kit\Conversion\Serializer
 */
class StringToCollectionConverter extends AbstractConverter
{

    /**
     * @var CollectionFactoryInterface
     */
    protected $collectionFactory;

    /**
     * @var StringHelperInterface
     */
    protected $stringHelper;

    /**
     * StringToCollectionConverter constructor.
     * @param CollectionFactoryInterface $collectionFactory
     * @param StringHelperInterface $stringHelper
     */
    public function __construct(
        CollectionFactoryInterface $collectionFactory,
        StringHelperInterface $stringHelper
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->stringHelper = $stringHelper;
    }

    /**
     * {@inheritdoc}
     */
    protected function doConvert($value)
    {
        if ($value) {
            $value = $this->stringHelper->crop($value, 1);
            $value = $this->stringHelper->crop($value, -1);
            $value = explode(',', $value);
        } else {
            $value = [];
        }
        return $this->collectionFactory->createCollection($value);
    }
}
