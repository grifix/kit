<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Conversion\Converter;

/**
 * Class DateToStringConverter
 *
 * @category Grifix
 * @package  Kit\Conversion\Serializer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class DateToStringConverter extends AbstractConverter
{
    
    /**
     * @var string
     */
    protected $format = 'Y-m-d';
    
    /**
     * {@inheritdoc}
     */
    public function doConvert($value)
    {
        if($value instanceof \DateTimeInterface){
            return $value->format($this->getFormat());
        }
        return $value;
    }
    
    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }
    
    /**
     * @param string $format
     *
     * @return DateToStringConverter
     */
    public function setFormat(string $format): DateToStringConverter
    {
        $this->format = $format;
        
        return $this;
    }
}