<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Conversion\Converter;

use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;


/**
 * Class ConverterFactory
 *
 * @category Grifix
 * @package  Kit\Conversion\Serializer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ConverterFactory extends AbstractFactory implements ConverterFactoryInterface
{

    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    /**
     * ConverterFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param IocContainerInterface $iocContainer
     */
    public function __construct(ClassMakerInterface $classMaker, IocContainerInterface $iocContainer)
    {
        $this->iocContainer = $iocContainer;
        parent::__construct($classMaker);
    }

    /**
     * @param $converterClass
     * @return mixed|object
     * @throws \ReflectionException
     */
    public function createConverter($converterClass)
    {
        $class = $this->makeClassName($converterClass);
        $this->isSubClassOrFail($converterClass, ConverterInterface::class);
        return $this->iocContainer->createNewInstance($class);
    }
}
