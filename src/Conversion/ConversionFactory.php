<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Conversion;

use Grifix\Kit\Conversion\Field\FieldFactoryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class ConversionFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Conversion
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ConversionFactory extends AbstractFactory implements ConversionFactoryInterface
{
    /**
     * @var FieldFactoryInterface
     */
    protected $fieldFactory;
    
    /**
     * ConversionFactory constructor.
     *
     * @param FieldFactoryInterface $fieldFactory
     * @param ClassMakerInterface   $classMaker
     */
    public function __construct(FieldFactoryInterface $fieldFactory, ClassMakerInterface $classMaker)
    {
        $this->fieldFactory = $fieldFactory;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createConversion(): ConversionInterface
    {
        $class = $this->makeClassName(Conversion::class);
        
        return new $class($this->fieldFactory);
    }
}
