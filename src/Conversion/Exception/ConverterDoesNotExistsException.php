<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Conversion\Exception;

/**
 * Class ConverterDoesNotExistsException
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Serializer\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ConverterDoesNotExistsException extends \Exception
{
    protected $converterClass;
    
    /**
     * ConverterDoesNotExistsException constructor.
     *
     * @param string $converterClass
     */
    public function __construct(string $converterClass)
    {
        $this->converterClass = $converterClass;
        $this->message = 'Serializer "'.$converterClass.'" does not exists!';
        parent::__construct();
    }
}