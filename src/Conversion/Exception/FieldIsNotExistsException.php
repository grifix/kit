<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Conversion\Exception;

/**
 * Class FieldNotExistsException
 *
 * @category Grifix
 * @package  Grifix\Kit\Field\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FieldIsNotExistsException extends \Exception
{
    protected $name;
    
    /**
     * FieldNotExistsException constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->message = 'Filed "' . $name . '" is not exists in this conversion!';
        parent::__construct();
    }
}