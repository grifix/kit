<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Conversion\Exception;

use Grifix\Kit\Conversion\Field\FieldInterface;

/**
 * Class FieldAlreadyExistsException
 *
 * @category Grifix
 * @package  Grifix\Kit\Field\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FieldAlreadyExistsException extends \Exception
{
    protected $field;
    
    /**
     * ValidatorIsNotExistsException constructor.
     *
     * @param \Grifix\Kit\Conversion\Field\FieldInterface $field
     */
    public function __construct(FieldInterface $field)
    {
        $this->message = 'Field with name "' . $field->getName() . '" already exists in this conversion!';
        parent::__construct();
    }
}