<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Conversion;

use Grifix\Kit\Conversion\Exception\FieldAlreadyExistsException;
use Grifix\Kit\Conversion\Exception\FieldIsNotExistsException;
use Grifix\Kit\Conversion\Field\FieldInterface;

/**
 * Class Conversion
 *
 * @category Grifix
 * @package  Kit\Conversion
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Conversion implements ConversionInterface
{
    
    /**
     * @var FieldInterface[]
     */
    protected $fields = [];
    
    /**
     * @var \Grifix\Kit\Conversion\Field\FieldFactoryInterface
     */
    protected $fieldFactory;
    
    /**
     * Conversion constructor.
     *
     * @param \Grifix\Kit\Conversion\Field\FieldFactoryInterface $fieldFactory
     */
    public function __construct(Field\FieldFactoryInterface $fieldFactory)
    {
        $this->fieldFactory = $fieldFactory;
    }
    
    /**
     * {@inheritdoc}
     */
    public function addField(FieldInterface $field): ConversionInterface
    {
        if ($this->hasField($field->getName())) {
            throw new FieldAlreadyExistsException($field);
        }
        $this->fields[] = $field;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function hasField(string $name): bool
    {
        foreach ($this->fields as $field) {
            if ($field->getName() == $name) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * {@inheritdoc}
     */
    public function createField(string $name, string $class = null): FieldInterface
    {
        $this->addField($this->fieldFactory->createField($name, $class));
        
        return $this->getField($name);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getField(string $name): FieldInterface
    {
        foreach ($this->fields as $field) {
            if ($field->getName() == $name) {
                return $field;
            }
        }
        
        throw new FieldIsNotExistsException($name);
    }

    /**
     * {@inheritdoc}
     */
    public function convert(array &$values)
    {
        foreach ($values as $key => $value) {
            if ($this->hasField($key) && $this->getField($key)) {
                $values[$key] = $this->getField($key)->convert($value);
            }
        }
    }
}
