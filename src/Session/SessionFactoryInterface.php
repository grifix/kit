<?php
declare(strict_types=1);

namespace Grifix\Kit\Session;


/**
 * Class SessionFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Session
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface SessionFactoryInterface
{
    /**
     * @param array $options
     *
     * @return Session
     */
    public function createNativeSession(array $options = []):SessionInterface;
}