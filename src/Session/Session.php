<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Session;

use Grifix\Kit\Helper\ArrayHelperInterface;

/**
 * Class Session
 *
 * @category Grifix
 * @package  Grifix\Kit\Session
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Session implements SessionInterface
{

    /**
     * @var \SessionHandlerInterface
     */
    protected $handler;

    /**
     * @var array
     */
    protected $vars;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * Session constructor.
     *
     * @param \SessionHandlerInterface $handler
     * @param ArrayHelperInterface $arrayHelper
     * @param array $options
     */
    public function __construct(
        \SessionHandlerInterface $handler,
        ArrayHelperInterface $arrayHelper,
        array $options = []
    ) {
        $this->handler = $handler;
        $this->arrayHelper = $arrayHelper;
        session_set_save_handler($handler, true);
        $this->setOptions($options);
        session_start();
        $this->vars = &$_SESSION;
    }


    /**
     * {@inheritdoc}
     */
    public function set(string $key, $val): SessionInterface
    {
        $this->arrayHelper->set($this->vars, $key, $val);

        return $this;
    }

    /**
     * @param array $options
     *
     * @return void
     */
    protected function setOptions(array $options)
    {
        $validOptions = array_flip([
            'cache_limiter',
            'cookie_domain',
            'cookie_httponly',
            'cookie_lifetime',
            'cookie_path',
            'cookie_secure',
            'entropy_file',
            'entropy_length',
            'gc_divisor',
            'gc_maxlifetime',
            'gc_probability',
            'hash_bits_per_character',
            'hash_function',
            'name',
            'referer_check',
            'serialize_handler',
            'use_cookies',
            'use_only_cookies',
            'use_trans_sid',
            'upload_progress.enabled',
            'upload_progress.cleanup',
            'upload_progress.prefix',
            'upload_progress.name',
            'upload_progress.freq',
            'upload_progress.min-freq',
            'url_rewriter.tags',
        ]);
        foreach ($options as $key => $value) {
            if (isset($validOptions[$key])) {
                ini_set('session.' . $key, strval($value));
            }
        }
    }


    /**
     * {@inheritdoc}
     */
    public function get(string $key)
    {
        return $this->arrayHelper->get($this->vars, $key);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return session_id();
    }
}
