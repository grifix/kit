<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Session;

/**
 * Class Session
 *
 * @category Grifix
 * @package  Grifix\Kit\Session
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface SessionInterface
{
    /**
     * @param string $key
     * @param mixed  $val
     *
     * @return SessionInterface
     */
    public function set(string $key, $val): SessionInterface;
    
    /**
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key);
    
    /**
     * @return string
     */
    public function getId();
}
