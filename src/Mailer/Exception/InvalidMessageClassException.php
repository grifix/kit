<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Mailer\Exception;

use Grifix\Kit\Mailer\Message;
use Grifix\Kit\Mailer\MessageInterface;

/**
 * Class InvalidMessageClassException
 *
 * @category Grifix
 * @package  Grifix\Kit\Mailer\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InvalidMessageClassException extends \Exception
{
    /**
     * InvalidMessageClassException constructor.
     *
     * @param MessageInterface $emailMessage
     */
    public function __construct(MessageInterface $emailMessage)
    {
        $this->message = 'Message must be instance of "'.Message::class.'" "'.get_class($emailMessage).'" given!';
        parent::__construct();
    }
}