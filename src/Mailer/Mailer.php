<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Mailer;

use Grifix\Kit\Mailer\Exception\InvalidMessageClassException;

/**
 * Class Mailer
 *
 * @category Grifix
 * @package  Grifix\Kit\Mailer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Mailer implements MailerInterface
{
    
    /**
     * @var \Swift_Mailer
     */
    protected $swiftMailer;
    
    /**
     * @var MessageFactoryInterface
     */
    protected $messageFactory;
    
    protected $defaultFrom;
    
    protected $defaultFromName;
    
    /**
     * Mailer constructor.
     *
     * @param \Swift_Transport        $transport
     * @param MessageFactoryInterface $messageFactory
     * @param string                  $defaultFrom
     * @param string                  $defaultFromName
     */
    public function __construct(
        \Swift_Transport $transport,
        MessageFactoryInterface $messageFactory,
        string $defaultFrom,
        string $defaultFromName
    ) {
        $this->swiftMailer = new \Swift_Mailer($transport);
        $this->messageFactory = $messageFactory;
        $this->defaultFrom = $defaultFrom;
        $this->defaultFromName = $defaultFromName;
    }
    
    /**
     * @param MessageInterface $message
     *
     * @return int
     * @throws InvalidMessageClassException
     */
    public function send(MessageInterface $message): int
    {
        if (!($message instanceof Message)) {
            throw new InvalidMessageClassException($message);
        }
        
        return $this->swiftMailer->send($message->getSwiftMessage());
    }
    
    /**
     * @return MessageInterface
     */
    public function createMessage(): MessageInterface
    {
        $message = $this->messageFactory->createMessage();
        $message->setFrom($this->defaultFrom, $this->defaultFromName);
        return $message;
    }
}