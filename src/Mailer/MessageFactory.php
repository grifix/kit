<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Mailer;

use Grifix\Kit\Kernel\AbstractFactory;

/**
 * Class MessageFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Mailer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MessageFactory extends AbstractFactory implements MessageFactoryInterface
{
    
    /**
     * {@inheritdoc}
     */
    public function createMessage():MessageInterface
    {
        $class = $this->makeClassName(Message::class);
        
        return new $class(new \Swift_Message());
    }
}