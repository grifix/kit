<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Mailer;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Mailer\Exception\InvalidTransportTypeException;

/**
 * Class MailerFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Mailer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MailerFactory extends AbstractFactory implements MailerFactoryInterface
{
    /**
     * @var \Swift_Transport
     */
    protected $transport;
    
    /**
     * @var MessageFactoryInterface
     */
    protected $messageFactory;
    
    /**
     * @var string
     */
    protected $defaultFromEmail;
    
    protected $defaultFromName;
    
    /**
     * MailerFactory constructor.
     *
     * @param ClassMakerInterface     $classMaker
     * @param MessageFactoryInterface $messageFactory
     * @param string                  $defaultFromEmail <cfg:grifix.kit.mailer.fromEmail>
     * @param string                  $defaultFormName  <cfg:grifix.kit.mailer.fromName>
     * @param string                  $transportType    <cfg:grifix.kit.mailer.transport>
     * @param string                  $host             <cfg:grifix.kit.mailer.host>
     * @param int                     $port             <cfg:grifix.kit.mailer.port>
     * @param string|null             $userName         <cfg:grifix.kit.mailer.userName>
     * @param string|null             $password         <cfg:grifix.kit.mailer.password>
     *
     * @throws InvalidTransportTypeException
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        MessageFactoryInterface $messageFactory,
        string $defaultFromEmail,
        string $defaultFormName,
        string $transportType = self::TRANSPRORT_SMTP,
        string $host = 'localhost',
        int $port = 25,
        string $userName = null,
        string $password = null
    ) {
        $this->messageFactory = $messageFactory;
        switch ($transportType) {
            case self::TRANSPRORT_SMTP:
                $this->transport = new \Swift_SmtpTransport($host, $port);
                if ($userName) {
                    $this->transport->setUsername($userName);
                }
                if ($password) {
                    $this->transport->setPassword($password);
                }
                break;
            default:
                throw new InvalidTransportTypeException($transportType);
        }
        $this->defaultFromEmail = $defaultFromEmail;
        $this->defaultFromName = $defaultFormName;
        parent::__construct($classMaker);
    }
    
    /**
     * @return MailerInterface
     */
    public function createMailer(): MailerInterface
    {
        $class = $this->makeClassName(Mailer::class);
        
        return new $class($this->transport, $this->messageFactory, $this->defaultFromEmail, $this->defaultFromName);
    }
}