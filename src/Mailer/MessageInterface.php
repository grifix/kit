<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Mailer;


/**
 * Class Message
 *
 * @category Grifix
 * @package  Grifix\Kit\Mailer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface MessageInterface
{
    /**
     * @param string|array $addresses
     * @param string       $name
     *
     * @return MessageInterface
     */
    public function setFrom($addresses, string $name = null): MessageInterface;
    
    /**
     * @param string|array $addresses
     * @param string|null  $name
     *
     * @return MessageInterface
     */
    public function setTo($addresses, string $name = null): MessageInterface;
    
    /**
     * @param string $body
     *
     * @return MessageInterface
     */
    public function setBody(string $body): MessageInterface;
    
    /**
     * @param string $subject
     *
     * @return MessageInterface
     */
    public function setSubject(string $subject): MessageInterface;
}