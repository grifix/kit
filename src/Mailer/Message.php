<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Mailer;

/**
 * Class Message
 *
 * @category Grifix
 * @package  Grifix\Kit\Mailer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Message implements MessageInterface
{
    protected $swiftMessage;
    
    /**
     * Message constructor.
     *
     * @param \Swift_Message $swiftMessage
     */
    public function __construct(\Swift_Message $swiftMessage)
    {
        $this->swiftMessage = $swiftMessage;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setSubject(string $subject): MessageInterface
    {
        $this->swiftMessage->setSubject($subject);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setFrom($addresses, string $name = null): MessageInterface
    {
        $this->swiftMessage->setFrom($addresses, $name);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setTo($addresses, string $name = null): MessageInterface
    {
        $this->swiftMessage->setTo($addresses, $name);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setBody(string $body): MessageInterface
    {
        $this->swiftMessage->setBody($body);
        
        return $this;
    }
    
    /**
     * @return \Swift_Message
     */
    public function getSwiftMessage()
    {
        return $this->swiftMessage;
    }
}