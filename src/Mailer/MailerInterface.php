<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Mailer;

use Grifix\Kit\Mailer\Exception\InvalidMessageClassException;


/**
 * Class Mailer
 *
 * @category Grifix
 * @package  Grifix\Kit\Mailer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface MailerInterface
{
    /**
     * @param MessageInterface $message
     *
     * @return int
     * @throws InvalidMessageClassException
     */
    public function send(MessageInterface $message): int;
    
    /**
     * @return MessageInterface
     */
    public function createMessage(): MessageInterface;
}