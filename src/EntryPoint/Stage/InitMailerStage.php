<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Mailer\MailerFactoryInterface;
use Grifix\Kit\Mailer\MailerInterface;

/**
 * Class InitMailerStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitMailerStage implements StageInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        $context->getIocContainer()->set(MailerInterface::class, function () use ($context) {
            return $context->getIocContainer()->get(MailerFactoryInterface::class)->createMailer();
        });
        return $context;
    }
}
