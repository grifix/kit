<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\Cache\SimpleCacheFactoryInterface;
use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;

/**
 * Class InitCacheStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitCacheStage implements StageInterface
{

    /**
     * @var SimpleCacheFactoryInterface
     */
    protected $simpleCacheFactory;

    protected $clearCache;

    /**
     * InitCacheStage constructor.
     * @param SimpleCacheFactoryInterface $simpleCacheFactory
     * @param bool $clearCache
     */
    public function __construct(
        SimpleCacheFactoryInterface $simpleCacheFactory,
        bool $clearCache = false
    ) {
        $this->simpleCacheFactory = $simpleCacheFactory;
        $this->clearCache = $clearCache;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {

        $context->setQuickCache(
            $this->simpleCacheFactory->createMemCache(include($context->getRootDir() . '/memcache.php'))
        );

        $context->setSlowCache($this->simpleCacheFactory->createFileSystemCache('cache'));
        if ($this->clearCache) {
            $context->getQuickCache()->clear();
            $context->getSlowCache()->clear();
        }
        return $context;
    }
}
