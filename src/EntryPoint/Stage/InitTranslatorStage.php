<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Intl\TranslatorFactoryInterface;
use Grifix\Kit\Intl\TranslatorInterface;

/**
 * Class InitTranslatorStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitTranslatorStage implements StageInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        $context->getIocContainer()->set(
            TranslatorInterface::class,
            $context->getIocContainer()->get(TranslatorFactoryInterface::class)->createTranslator()
        );
        return $context;
    }
}