<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelperInterface;
use Grifix\Kit\Kernel\Kernel;

/**
 * Class InitKernelStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitKernelStage implements StageInterface
{
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    protected $fileSystemHelper;

    /**
     * InitKernelStage constructor.
     * @param ArrayHelperInterface $arrayHelper
     * @param FilesystemHelperInterface $filesystemHelper
     */
    public function __construct(
        ArrayHelperInterface $arrayHelper,
        FilesystemHelperInterface $filesystemHelper
    ) {
        $this->arrayHelper = $arrayHelper;
        $this->fileSystemHelper = $filesystemHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        $kernel = new Kernel(
            $context->getRootDir(),
            $context->getQuickCache(),
            $this->fileSystemHelper,
            $this->arrayHelper
        );
        $kernel->getModules(true);
        $context->setKernel($kernel);
        return $context;
    }
}
