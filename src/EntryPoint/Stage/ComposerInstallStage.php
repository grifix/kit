<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Migration\MigrationRunnerInterface;

/**
 * Class ComposerInstallStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class ComposerInstallStage implements StageInterface
{

    protected $publishAssets = true;

    /**
     * ComposerInstallStage constructor.
     * @param bool $publishAssets
     */
    public function __construct(bool $publishAssets = true)
    {
        $this->publishAssets = $publishAssets;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        $context->getIocContainer()->get(MigrationRunnerInterface::class)->down();
        $context->getIocContainer()->get(MigrationRunnerInterface::class)->up();
        $context->getIocContainer()->get(KernelInterface::class)->installModules($this->publishAssets);
        return $context;
    }
}
