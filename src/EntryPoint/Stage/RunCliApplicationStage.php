<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Cli\AbstractCommand;
use Grifix\Kit\Cli\CommandFactoryInterface;
use Grifix\Kit\Helper\FilesystemHelperInterface;
use Grifix\Kit\Kernel\Module\ModuleInterface;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputOption;

/**
 * Class CollectCliCommandsStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class RunCliApplicationStage implements StageInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        $application = new Application();
        $application->getDefinition()->addOptions([
            new InputOption('--.env', null, InputOption::VALUE_OPTIONAL, 'The environment to operate in.', 'dev'),
        ]);
        $application->addCommands($this->collectCommands($context));
        $application->run();
    }


    /**
     * @param ModuleInterface $module
     * @param PipelineContextInterface $context
     * @return AbstractCommand[]
     */
    protected function collectModuleCommands(ModuleInterface $module, PipelineContextInterface $context)
    {
        $filesystemHelper = $context->getIocContainer()->get(FilesystemHelperInterface::class);
        $dir = $module->getVendorDir() . '/src/Ui/Cli/Command';
        $result = [];
        $files = [];
        if ($filesystemHelper->isDir($dir)) {
            $files = array_merge($files, $filesystemHelper->scanDir($dir));
        }
        $dir = $module->getAppDir() . '/src/Ui/Cli/Command';
        if ($filesystemHelper->isDir($dir)) {
            $files = array_merge($files, $filesystemHelper->scanDir($dir));
        }
        foreach ($files as $file) {
            if (isset($file['extension']) && $file['extension'] == 'php') {
                $result[] = $context->getIocContainer()->get(CommandFactoryInterface::class)
                    ->createCommand($module->getNamespace() . '\\Ui\\Cli\\Command\\' . ucfirst($file['filename']));
            }
        }

        return $result;
    }

    /**
     * @param PipelineContextInterface $context
     *
     * @return AbstractCommand[]
     */
    protected function collectCommands(PipelineContextInterface $context): array
    {
        $result = [];
        foreach ($context->getKernel()->getModules() as $module) {
            $result = array_merge($result, $this->collectModuleCommands($module, $context));
        }

        return $result;
    }
}
