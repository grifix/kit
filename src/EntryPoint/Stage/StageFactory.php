<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\Cache\SimpleCacheFactoryInterface;
use Grifix\Kit\Config\ConfigFactoryInterface;
use Grifix\Kit\EventBus\EventBusFactoryInterface;
use Grifix\Kit\Filesystem\FilesystemFactoryInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\DateHelperInterface;
use Grifix\Kit\Helper\FilesystemHelperInterface;
use Grifix\Kit\Ioc\IncContainerFactoryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Serializer\SerializerInterface;
use Grifix\Kit\Session\SessionFactoryInterface;

/**
 * Class StageFactory
 * @package Grifix\Kit\Pipeline\Stage
 */
class StageFactory extends AbstractFactory implements StageFactoryInterface
{

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var ConfigFactoryInterface
     */
    protected $configFactory;

    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;

    /**
     * @var EventBusFactoryInterface
     */
    protected $eventBusFactory;

    /**
     * @var FilesystemFactoryInterface
     */
    protected $filesystemFactory;

    /**
     * @var SessionFactoryInterface
     */
    protected $sessionFactory;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var DateHelperInterface
     */
    protected $dateHelper;

    /**
     * @var IncContainerFactoryInterface
     */
    protected $iocContainerFactory;

    /**
     * @var SimpleCacheFactoryInterface
     */
    protected $simpleCacheFactory;

    /**
     * StageFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param ArrayHelperInterface $arrayHelper
     * @param ConfigFactoryInterface $configFactory
     * @param FilesystemHelperInterface $filesystemHelper
     * @param FilesystemFactoryInterface $filesystemFactory
     * @param SessionFactoryInterface $sessionFactory
     * @param SerializerInterface $serializer
     * @param DateHelperInterface $dateHelper
     * @param IncContainerFactoryInterface $iocContainerFactory
     * @param SimpleCacheFactoryInterface $simpleCacheFactory
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        ArrayHelperInterface $arrayHelper,
        ConfigFactoryInterface $configFactory,
        FilesystemHelperInterface $filesystemHelper,
        FilesystemFactoryInterface $filesystemFactory,
        SessionFactoryInterface $sessionFactory,
        SerializerInterface $serializer,
        DateHelperInterface $dateHelper,
        IncContainerFactoryInterface $iocContainerFactory,
        SimpleCacheFactoryInterface $simpleCacheFactory
    ) {
        parent::__construct($classMaker);
        $this->arrayHelper = $arrayHelper;
        $this->configFactory = $configFactory;
        $this->filesystemHelper = $filesystemHelper;
        $this->filesystemFactory = $filesystemFactory;
        $this->sessionFactory = $sessionFactory;
        $this->serializer = $serializer;
        $this->dateHelper = $dateHelper;
        $this->iocContainerFactory = $iocContainerFactory;
        $this->simpleCacheFactory = $simpleCacheFactory;
    }

    /**
     * @param bool $publishAssets
     * @return StageInterface
     */
    public function createComposerInstallStage(bool $publishAssets = true): StageInterface
    {
        $class = $this->makeClassName(ComposerInstallStage::class);
        return new $class($publishAssets);
    }

    /**
     * @return StageInterface
     */
    public function createHandleRequestStage(): StageInterface
    {
        return $this->createSimpleStage(HandleRequestStage::class);
    }

    /**
     * @param bool $publishAssets
     * @return StageInterface
     */
    public function createComposerUpdateStage(bool $publishAssets = true): StageInterface
    {
        $class = $this->makeClassName(ComposerUpdateStage::class);
        return new $class($publishAssets);
    }


    /**
     * @param string $stageClass
     * @return StageInterface
     */
    protected function createSimpleStage(string $stageClass): StageInterface
    {
        $class = $this->makeClassName($stageClass);
        return new $class();
    }

    /**
     * @return RegisterAutoloadStage
     */
    public function createRegisterAutoloadStage(): StageInterface
    {
        $class = $this->makeClassName(RegisterAutoloadStage::class);
        return new $class($this->arrayHelper);
    }

    /**
     * @return StageInterface
     */
    public function createInitConfigStage(): StageInterface
    {
        $class = $this->makeClassName(InitConfigStage::class);
        return new $class($this->arrayHelper, $this->configFactory, $this->filesystemHelper);
    }

    /**
     * @return StageInterface
     */
    public function createInitFilesystemStage(): StageInterface
    {
        $class = $this->makeClassName(InitFilesystemStage::class);
        return new $class($this->filesystemFactory);
    }

    /**
     * @return StageInterface
     */
    public function createInitKernelStage(): StageInterface
    {
        $class = $this->makeClassName(InitKernelStage::class);
        return new $class($this->arrayHelper, $this->filesystemHelper);
    }

    /**
     * @return StageInterface
     */
    public function createInitSessionStage(): StageInterface
    {
        $class = $this->makeClassName(InitSessionStage::class);
        return new $class($this->sessionFactory);
    }

    /**
     * @return StageInterface
     */
    public function createInitRegisterAutoloadStage(): StageInterface
    {
        $class = $this->makeClassName(InitSessionStage::class);
        return new $class($this->arrayHelper);
    }

    /**
     * @return StageInterface
     */
    public function createRunCliApplicationStage(): StageInterface
    {
        return $this->createSimpleStage(RunCliApplicationStage::class);
    }

    /**
     * @return StageInterface
     */
    public function createInitDbStage(): StageInterface
    {
        return $this->createSimpleStage(InitDbStage::class);
    }

    /**
     * @return StageInterface
     */
    public function createInitMailerStage(): StageInterface
    {
        return $this->createSimpleStage(InitMailerStage::class);
    }

    /**
     * @return StageInterface
     */
    public function createInitTranslatorStage(): StageInterface
    {
        return $this->createSimpleStage(InitTranslatorStage::class);
    }

    /**
     * @return StageInterface
     */
    public function createBootstrapModulesStage(): StageInterface
    {
        return $this->createSimpleStage(BootstrapModulesStage::class);
    }

    /**
     * @return StageInterface
     */
    public function createInitIocStage(): StageInterface
    {
        $class = $this->makeClassName(InitIocStage::class);
        return new $class($this->arrayHelper, $this->filesystemHelper, $this->iocContainerFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function createInitCacheStage(bool $clearCache = false): StageInterface
    {
        $class = $this->makeClassName(InitCacheStage::class);
        return new $class($this->simpleCacheFactory, $clearCache);
    }
}
