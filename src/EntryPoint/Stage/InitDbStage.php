<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Db\ConnectionFactoryInterface;
use Grifix\Kit\Db\ConnectionInterface;

/**
 * Class InitDbStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitDbStage implements StageInterface
{

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        $context->getIocContainer()->set(ConnectionInterface::class, function () use ($context) {
            return $context->getIocContainer()->get(ConnectionFactoryInterface::class)
                ->createConnection($context->getConfig()->get('grifix.kit.db.default'));
        });
        return $context;
    }
}
