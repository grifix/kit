<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\EntryPoint\Stage\Event\AfterResponseSentEvent;
use Grifix\Kit\EntryPoint\Stage\Event\BeforeResponseSentEvent;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Exception\ExceptionPresenterInterface;
use Grifix\Kit\Exception\ExceptionProcessorInterface;
use Grifix\Kit\Http\CookieFactoryInterface;
use Grifix\Kit\Http\CookieInterface;
use Grifix\Kit\Http\ResponseFactoryInterface;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ResponseSenderInterface;
use Grifix\Kit\Http\ServerFactoryInterface;
use Grifix\Kit\Http\ServerInterface;
use Grifix\Kit\Http\ServerRequestFactoryInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Middleware\Stack;

/**
 * Class HandleRequestStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class HandleRequestStage implements StageInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        $response = $this->handleRequest($context->getRequest(), $context);
        $context->getIocContainer()->set(ResponseInterface::class, $response);
        $context->setResponse($response);
        $context->getIocContainer()->get(ResponseSenderInterface::class)->send($response);
        $context->getIocContainer()->get(EventBusInterface::class)->trigger(new AfterResponseSentEvent());
        return $context;
    }

    /**
     * @param ServerRequestInterface $request
     * @param PipelineContextInterface $context
     * @return ResponseInterface
     */
    protected function handleRequest(
        ServerRequestInterface $request,
        PipelineContextInterface $context
    ): ResponseInterface {
        $ioc = $context->getIocContainer();
        try {
            $stack = new Stack();

            foreach ($context->getKernel()->getModules() as $module) {
                $middleware = $module->makeMiddleware();
                if ($middleware) {
                    $stack->add($middleware);
                }
            }

            $context->getIocContainer()->get(EventBusInterface::class)->trigger(new BeforeResponseSentEvent());
            $response = $ioc->get(ResponseFactoryInterface::class)->createResponse();

            $ioc->set(ServerRequestInterface::class, $request);

            $ioc->set(
                CookieInterface::class,
                $context->getIocContainer()->get(CookieFactoryInterface::class)->createCookie($request)
            );
            $ioc->set(
                ServerInterface::class,
                $context->getIocContainer()->get(ServerFactoryInterface::class)->createServer($request)
            );


            $response = $stack->call($request, $response);

            /** @noinspection PhpInternalEntityUsedInspection */
            $response = $ioc->get(CookieInterface::class)->processResponse($response);

            return $response;
        } catch (\Throwable $e) {
            $e = $ioc->get(ExceptionProcessorInterface::class)->process($e);
            return $ioc->get(ExceptionPresenterInterface::class)->present($e, $request);
        }
    }
}
