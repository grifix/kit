<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Session\SessionFactoryInterface;
use Grifix\Kit\Session\SessionInterface;

/**
 * Class InitSessionStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitSessionStage implements StageInterface
{

    /**
     * @var SessionFactoryInterface
     */
    protected $sessionFactory;

    /**
     * InitSessionStage constructor.
     * @param SessionFactoryInterface $sessionFactory
     */
    public function __construct(SessionFactoryInterface $sessionFactory)
    {
        $this->sessionFactory = $sessionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        $handler = $context->getConfig()->get('grifix.kit.session.handler');
        $options = $context->getConfig()->get('grifix.kit.session.options', []);
        switch ($handler) {
            case 'native':
                $session = $this->sessionFactory->createNativeSession($options);
                break;
            default:
                $session = $this->sessionFactory->createNativeSession($options);
        }

        $context->getIocContainer()->set(SessionInterface::class, $session);
        return $context;
    }
}