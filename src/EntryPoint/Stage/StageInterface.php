<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;

/**
 * Interface StageInterface
 * @package Grifix\Kit\Pipeline\Stage
 */
interface StageInterface
{
    /**
     * @param PipelineContextInterface $context
     * @return void
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface;
}