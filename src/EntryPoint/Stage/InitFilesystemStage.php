<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Filesystem\FilesystemFactoryInterface;

/**
 * Class InitFilesystemStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitFilesystemStage implements StageInterface
{
    protected $fileSystemFactory;

    /**
     * InitFilesystemStage constructor.
     * @param FilesystemFactoryInterface $filesystemFactory
     */
    public function __construct(FilesystemFactoryInterface $filesystemFactory)
    {
        $this->fileSystemFactory = $filesystemFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        $context->setFileSystem($this->fileSystemFactory->createLocal($context->getRootDir()));
        return $context;
    }
}
