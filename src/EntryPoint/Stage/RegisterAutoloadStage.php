<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;

/**
 * Class RegisterAutoladStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class RegisterAutoloadStage implements StageInterface
{
    protected $arrayHelper;

    /**
     * RegisterAutoloadStage constructor.
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(ArrayHelperInterface $arrayHelper)
    {
        $this->arrayHelper = $arrayHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        spl_autoload_register(function ($className) use ($context) {
            $arr = explode('\\', $className);
            if ($arr[0] == 'App') {
                for ($i = 0; $i < 3; $i++) {
                    if (!isset($arr[$i])) {
                        trigger_error('Can\'t load class "' . $className . '"!', E_USER_ERROR);
                    }
                    $arr[$i] = strtolower($arr[$i]);
                }
                $this->arrayHelper->insert($arr, 'src', 3);
                $path = $context->getRootDir() . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $arr) . '.php';

                if (is_file($path)) {
                    /** @noinspection PhpIncludeInspection */
                    require_once $path;
                }
            }
        });

        return $context;
    }
}
