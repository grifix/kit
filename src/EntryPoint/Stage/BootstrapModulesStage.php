<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;


use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;

/**
 * Class BootstrapModulesStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class BootstrapModulesStage implements StageInterface
{
    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        $context->getKernel()->bootstrapModules();
        return $context;
    }
}
