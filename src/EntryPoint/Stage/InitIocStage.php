<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelperInterface;
use Grifix\Kit\Ioc\IncContainerFactoryInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Psr\SimpleCache\CacheInterface;
use Grifix\Kit\Alias;

/**
 * Class InitIocStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitIocStage implements StageInterface
{

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var FilesystemHelperInterface
     */
    protected $fileSystemHelper;

    /**
     * @var IncContainerFactoryInterface
     */
    protected $iocContainerFactory;

    /**
     * InitIocStage constructor.
     * @param ArrayHelperInterface $arrayHelper
     * @param FilesystemHelperInterface $filesystemHelper
     * @param IncContainerFactoryInterface $iocContainerFactory
     */
    public function __construct(
        ArrayHelperInterface $arrayHelper,
        FilesystemHelperInterface $filesystemHelper,
        IncContainerFactoryInterface $iocContainerFactory
    ) {
        $this->arrayHelper = $arrayHelper;
        $this->fileSystemHelper = $filesystemHelper;
        $this->iocContainerFactory = $iocContainerFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        $mainDefinitionsPath = $context->getRootDir() . '/config/ioc.php';
        $envDefinitionsPath = $context->getRootDir() . '/config/' . $context->getAppMode() . '/ioc.php';
        $definitions = [];

        foreach ($context->getKernel()->getModules() as $module) {
            $config = $module->getConfig();
            if (isset($config['ioc'])) {
                $definitions = $this->arrayHelper->merge($config['ioc'], $definitions);
            }
        }

        if ($this->fileSystemHelper->fileExists($mainDefinitionsPath)) {
            /** @noinspection PhpIncludeInspection */
            $definitions = $this->arrayHelper->merge($definitions, include $mainDefinitionsPath);
        }
        if ($this->fileSystemHelper->fileExists($envDefinitionsPath)) {
            /** @noinspection PhpIncludeInspection */
            $definitions = $this->arrayHelper->merge($definitions, include $envDefinitionsPath);
        }
        $iocContainer = $this->iocContainerFactory->createIocContainer(
            $context->getQuickCache(),
            $context->getConfig(),
            $definitions
        );

        $iocContainer->set(CacheInterface::class, $context->getQuickCache());
        $iocContainer->set(Alias::SLOW_CACHE, $context->getSlowCache());
        $iocContainer->set(Alias::QUICK_CACHE, $context->getQuickCache());
        $iocContainer->set(Alias::APP_CONTEXT, $context);
        $iocContainer->set(Alias::ROOT_DIR, $context->getRootDir());
        $iocContainer->set(Alias::APP_MODE, $context->getAppMode());
        $iocContainer->set(KernelInterface::class, $context->getKernel());
        $iocContainer->set(ConfigInterface::class, $context->getConfig());
        $iocContainer->set(FilesystemInterface::class, $context->getFileSystem());
        $iocContainer->set(IocContainerInterface::class, $iocContainer);
        $context->getKernel()->setIoc($iocContainer);
        $context->setIocContainer($iocContainer);
        return $context;
    }
}
