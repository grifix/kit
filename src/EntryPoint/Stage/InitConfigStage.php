<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Stage;

use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Cache\CacheKeyTrait;
use Grifix\Kit\Config\ConfigFactoryInterface;
use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelperInterface;

/**
 * Class InitConfigStage
 * @package Grifix\Kit\Pipeline\Stage
 */
class InitConfigStage implements StageInterface
{
    use CacheKeyTrait;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * @var ConfigFactoryInterface
     */
    protected $configFactory;

    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;

    /**
     * InitConfigStage constructor.
     * @param ArrayHelperInterface $arrayHelper
     * @param ConfigFactoryInterface $configFactory
     * @param FilesystemHelperInterface $filesystemHelper
     */
    public function __construct(
        ArrayHelperInterface $arrayHelper,
        ConfigFactoryInterface $configFactory,
        FilesystemHelperInterface $filesystemHelper
    ) {
        $this->arrayHelper = $arrayHelper;
        $this->configFactory = $configFactory;
        $this->filesystemHelper = $filesystemHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function __invoke(PipelineContextInterface $context): PipelineContextInterface
    {
        $cacheKey = $this->makeCacheKey('config');
        $cachedConfig = $context->getQuickCache()->get($cacheKey);
        if ($cachedConfig) {
            $context->setConfig($this->configFactory->createConfig($cachedConfig));
        } else {
            $config = $this->createConfig($context);
            $context->setConfig($config);
            $context->getQuickCache()->set($cacheKey, $config->toArray(), 0);
        }
        return $context;
    }

    /**
     * @param PipelineContextInterface $context
     * @return ConfigInterface
     */
    protected function createConfig(PipelineContextInterface $context): ConfigInterface
    {
        $configValues = [];

        $modules = $context->getKernel()->getModules();
        foreach ($modules as $module) {
            $this->arrayHelper->set(
                $configValues,
                $module->getVendor() . '.' . $module->getName(),
                $module->getConfig()
            );
        }
        $result = $this->configFactory->createConfig($configValues);

        $mainConfigPath = $context->getRootDir() . '/config/app.php';
        $modeConfigPath = $context->getRootDir() . '/config/' . $context->getAppMode() . '/app.php';

        /** @noinspection PhpIncludeInspection */
        $result->merge(include $mainConfigPath);
        if ($context->getAppMode() && $this->filesystemHelper->fileExists($modeConfigPath)) {
            /** @noinspection PhpIncludeInspection */
            $result->merge(include $modeConfigPath);
        }
        return $result;
    }
}
