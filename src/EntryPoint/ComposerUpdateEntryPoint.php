<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;

use League\Pipeline\PipelineInterface;

/**
 * Class ComposerUpdateEntryPoint
 * @package Grifix\Kit\EntryPoint
 */
class ComposerUpdateEntryPoint extends AbstractEntryPoint
{
    /**
     * {@inheritdoc}
     */
    protected function createPipeline(): PipelineInterface
    {
        return $this->pipelineFactory->createComposerUpdatePipeline();
    }
}
