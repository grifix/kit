<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;

use Grifix\Kit\Cache\Adapter\AdapterFactory;
use Grifix\Kit\Cache\SimpleCacheFactory;
use Grifix\Kit\Config\ConfigFactory;
use Grifix\Kit\EventBus\EventBusFactory;
use Grifix\Kit\Filesystem\FilesystemFactory;
use Grifix\Kit\Filesystem\FilesystemFactoryInterface;
use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Helper\ClassHelper;
use Grifix\Kit\Helper\DateHelper;
use Grifix\Kit\Helper\FilesystemHelper;
use Grifix\Kit\Ioc\IncContainerFactory;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMaker;
use Grifix\Kit\EntryPoint\Context\PipelineContext;
use Grifix\Kit\EntryPoint\Stage\StageFactory;
use Grifix\Kit\EntryPoint\Stage\StageFactoryInterface;
use Grifix\Kit\Queue\QueueFactory;
use Grifix\Kit\Serializer\Serializer;
use Grifix\Kit\Serializer\SerializerInterface;
use Grifix\Kit\Session\SessionFactory;
use League\Pipeline\Pipeline;
use League\Pipeline\PipelineBuilder;
use League\Pipeline\PipelineBuilderInterface;
use League\Pipeline\PipelineInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Composition root
 *
 * @category Grifix
 * @package  Grifix\Kit\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class PipelineFactory extends AbstractFactory implements PipelineFactoryInterface
{

    /**
     * @var PipelineBuilderInterface
     */
    protected $pipelineBuilder;

    /**
     * @var ArrayHelper
     */
    protected $arrayHelper;

    /**
     * @var string
     */
    protected $rootDir;

    /**
     * @var PipelineContext
     */
    protected $context;

    /**
     * @var StageFactoryInterface
     */
    protected $stageFactory;

    /**
     * @var FilesystemFactoryInterface
     */
    protected $filesystemFactory;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * Composition root
     * PipelineFactory constructor.
     * @param string $rootDir <root_dir>
     */
    public function __construct(string $rootDir)
    {
        parent::__construct(new ClassMaker());
        $dateHelper = new DateHelper();
        $this->rootDir = $rootDir;
        $this->arrayHelper = new ArrayHelper();
        $this->filesystemFactory = new FilesystemFactory($this->classMaker);
        $this->serializer = new Serializer();
        $this->stageFactory = new StageFactory(
            $this->classMaker,
            $this->arrayHelper,
            new ConfigFactory($this->arrayHelper, $this->classMaker),
            new FilesystemHelper(new Filesystem()),
            new FilesystemFactory($this->classMaker),
            new SessionFactory($this->classMaker),
            new $this->serializer,
            new $dateHelper,
            new IncContainerFactory($this->classMaker),
            new SimpleCacheFactory(
                $this->serializer,
                $this->classMaker,
                new AdapterFactory($this->classMaker, $this->filesystemFactory->createLocal($rootDir)),
                $dateHelper
            )
        );
    }

    /**
     * @return PipelineInterface
     */
    public function createEmptyPipeline(): PipelineInterface
    {
        return new Pipeline();
    }

    /**
     * @param bool $clearCache
     * @return PipelineBuilderInterface
     */
    protected function createGenericPipelineBuilder(bool $clearCache = false): PipelineBuilderInterface
    {
        $builder = new PipelineBuilder();
        $builder->add($this->stageFactory->createRegisterAutoloadStage());
        $builder->add($this->stageFactory->createInitFilesystemStage());
        $builder->add($this->stageFactory->createInitCacheStage($clearCache));
        $builder->add($this->stageFactory->createInitKernelStage());
        $builder->add($this->stageFactory->createInitConfigStage());
        $builder->add($this->stageFactory->createInitIocStage());
        $builder->add($this->stageFactory->createInitMailerStage());
        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function createWebApplicationPipeline(bool $clearCache = false): PipelineInterface
    {
        $builder = $this->createGenericPipelineBuilder($clearCache);
        $builder->add($this->stageFactory->createInitDbStage());
        $builder->add($this->stageFactory->createInitSessionStage());
        $builder->add($this->stageFactory->createInitTranslatorStage());
        $builder->add($this->stageFactory->createBootstrapModulesStage());
        $builder->add($this->stageFactory->createHandleRequestStage());
        return $builder->build();
    }

    /**
     * {@inheritdoc}
     */
    public function createCliApplicationPipeline(bool $clearCache = false): PipelineInterface
    {
        $builder = $this->createCliPipelineBuilder($clearCache);
        $builder->add($this->stageFactory->createRunCliApplicationStage());
        return $builder->build();
    }

    /**
     * {@inheritdoc}
     */
    public function createCliPipeline(bool $clearCache = false): PipelineInterface
    {
        return $this->createCliPipelineBuilder($clearCache)->build();
    }

    /**
     * @param bool $clearCache
     * @return PipelineBuilderInterface
     */
    protected function createCliPipelineBuilder(bool $clearCache = false): PipelineBuilderInterface
    {
        $builder = $this->createGenericPipelineBuilder($clearCache);
        $builder->add($this->stageFactory->createInitDbStage());
        $builder->add($this->stageFactory->createInitTranslatorStage());
        $builder->add($this->stageFactory->createBootstrapModulesStage());
        return $builder;
    }

    /**
     * @param bool $clearCache
     * @return PipelineInterface
     */
    public function createComposerInstallPipeline(bool $clearCache = true): PipelineInterface
    {
        $builder = $this->createCliPipelineBuilder($clearCache);
        $builder->add($this->stageFactory->createComposerInstallStage());
        return $builder->build();
    }

    /**
     * @param bool $clearCache
     * @return PipelineInterface
     */
    public function createComposerUpdatePipeline(bool $clearCache = true): PipelineInterface
    {
        $builder = $this->createCliPipelineBuilder($clearCache);
        $builder->add($this->stageFactory->createComposerUpdateStage());
        return $builder->build();
    }
}
