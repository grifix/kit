<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\EntryPoint;

use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Kernel\Module\ModuleInterface;
use Grifix\Kit\Migration\MigrationRunnerInterface;

/**
 * Class ComposerBootstrap
 *
 * @category Grifix
 * @package  Grifix\Kit\Pipeline
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ComposerBootstrap extends AbstractBootstrap implements ComposerBootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    protected function init()
    {
        $this->initDb();
        $this->initTranslator();
        $this->kernel->bootstrapModules();
    }
    
    /**
     * {@inheritdoc}
     */
    public function install(bool $publishAssets = true): void
    {
        $this->getShared(MigrationRunnerInterface::class)->down();
        $this->getShared(MigrationRunnerInterface::class)->up();
        $this->getShared(KernelInterface::class)->installModules($publishAssets);
    }
    
    /**
     * {@inheritdoc}
     */
    public function update(bool $publishAssets = true): void
    {
        $this->getShared(MigrationRunnerInterface::class)->up();
        $this->getShared(KernelInterface::class)->updateModules($publishAssets);
    }
}
