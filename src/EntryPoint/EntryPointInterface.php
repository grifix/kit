<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;

use Grifix\Kit\Context\ContextInterface;
use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\EntryPoint\Stage\StageInterface;

/**
 * Interface EntryPointInterface
 * @package Grifix\Kit\EntryPoint
 */
interface EntryPointInterface
{
    /**
     * @return PipelineContextInterface
     */
    public function enter(): PipelineContextInterface;
}
