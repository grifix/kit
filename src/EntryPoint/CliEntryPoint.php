<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;

use Grifix\Kit\Context\ContextFactory;
use Grifix\Kit\EntryPoint\Context\PipelineContextFactory;
use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Http\ServerRequestFactory;
use Grifix\Kit\Kernel\ClassMaker;
use League\Pipeline\PipelineInterface;

/**
 * Class WebEntryPoint
 * @package Grifix\Kit\EntryPoint
 */
class CliEntryPoint extends AbstractEntryPoint
{

    protected $rootDir;

    /**
     * {@inheritdoc}
     */
    protected function createPipeline(): PipelineInterface
    {
        return $this->pipelineFactory->createCliApplicationPipeline(!boolval(getenv('GRIFIX_CACHE_ENABLED')));
    }
}
