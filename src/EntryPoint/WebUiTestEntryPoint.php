<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;

use Grifix\Kit\Context\ContextFactory;
use Grifix\Kit\EntryPoint\Context\PipelineContextFactory;
use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Http\ServerRequestFactory;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Kernel\ClassMaker;
use League\Pipeline\PipelineInterface;

/**
 * Class WebEntryPoint
 * @package Grifix\Kit\EntryPoint
 */
class WebUiTestEntryPoint extends AbstractEntryPoint
{

    /***
     * @var ServerRequestInterface
     */
    protected $request;

    /**
     * WebUiTestEntryPoint constructor.
     * @param string $rootDir
     * @param ServerRequestInterface $request
     * @throws \Grifix\Kit\Context\Exception\ContextPropertyAlreadyExistsException
     */
    public function __construct(string $rootDir, ServerRequestInterface $request)
    {
        parent::__construct($rootDir);
        $this->request = $request;
    }

    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * {@inheritdoc}
     */
    protected function createContext(): PipelineContextInterface
    {
        return $this->contextFactory->createContext(
            $this->rootDir,
            'test',
            intval(getenv('GRIFIX_CACHE_TTL')),
            getenv('GRIFIX_MEMCACHED_HOST'),
            intval(getenv('GRIFIX_MEMCACHED_PORT')),
            $this->request
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function createPipeline(): PipelineInterface
    {
        return $this->pipelineFactory->createWebApplicationPipeline();
    }

    protected function install(): void
    {
        $pipeline = $this->pipelineFactory->createComposerInstallPipeline();
        $pipeline($this->createContext());
    }

    /**
     * {@inheritdoc}
     */
    public function enter(): PipelineContextInterface
    {
        $this->install();
        return parent::enter();
    }
}
