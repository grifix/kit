<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;

use Dotenv\Dotenv;
use Grifix\Kit\Context\ContextFactory;
use Grifix\Kit\Context\ContextInterface;
use Grifix\Kit\EntryPoint\Context\PipelineContextFactory;
use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Kernel\ClassMaker;
use League\Pipeline\PipelineInterface;

/**
 * Class AbstractEntryPoint
 * @package Grifix\Kit\EntryPoint
 */
abstract class AbstractEntryPoint implements EntryPointInterface
{

    /**
     * @var string
     */
    protected $rootDir;

    /**
     * @var PipelineContextFactory
     */
    protected $contextFactory;

    /**
     * @var ClassMaker
     */
    protected $classMaker;

    /**
     * @var PipelineFactory
     */
    protected $pipelineFactory;

    /**
     * @var string
     */
    protected $appMode;

    /**
     * AbstractEntryPoint constructor.
     * @param string $rootDir
     */
    public function __construct(string $rootDir, string $appMode = null)
    {
        $this->rootDir = $rootDir;
        (new Dotenv($this->rootDir))->overload();
        $this->classMaker = new ClassMaker();
        $this->contextFactory = new PipelineContextFactory($this->classMaker, new ContextFactory($this->classMaker));
        $this->pipelineFactory = new PipelineFactory($this->rootDir);
        $this->appMode = $appMode;
    }

    /**
     * @return PipelineInterface
     */
    abstract protected function createPipeline(): PipelineInterface;

    /**
     * @return ContextInterface
     */
    public function enter(): PipelineContextInterface
    {
        $pipeline = $this->createPipeline();
        $context = $this->createContext();
        $pipeline($context);
        return $context;
    }

    /**
     * @return PipelineContextInterface
     */
    protected function createContext(): PipelineContextInterface
    {
        return $this->contextFactory->createContext(
            $this->rootDir,
            $this->appMode ?? getenv('GRIFIX_APP_MODE')
        );
    }
}
