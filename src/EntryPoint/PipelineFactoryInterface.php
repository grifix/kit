<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;


use League\Pipeline\Pipeline;
use League\Pipeline\PipelineInterface;

/**
 * Class BootstrapFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Command
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface PipelineFactoryInterface
{
    /**
     * @param bool $clearCache
     * @return PipelineInterface
     */
    public function createWebApplicationPipeline(bool $clearCache = false): PipelineInterface;

    /**
     * @param bool $clearCache
     * @return PipelineInterface
     */
    public function createCliApplicationPipeline(bool $clearCache = false): PipelineInterface;

    /**
     * @param bool $clearCache
     * @return PipelineInterface
     */
    public function createCliPipeline(bool $clearCache = false): PipelineInterface;

    /**
     * @param bool $clearCache
     * @return PipelineInterface
     */
    public function createComposerInstallPipeline(bool $clearCache = true): PipelineInterface;

    /**
     * @param bool $clearCache
     * @return PipelineInterface
     */
    public function createComposerUpdatePipeline(bool $clearCache = true): PipelineInterface;

    /**
     * @return PipelineInterface
     */
    public function createEmptyPipeline(): PipelineInterface;
}