<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Context;

use Dotenv\Dotenv;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Context\ContextFactoryInterface as GenericContextFactoryInterface;

/**
 * Class ContextFactory
 * @package Grifix\Kit\Pipeline\Context
 */
class PipelineContextFactory extends AbstractFactory implements PipelineContextFactoryInterface
{

    /**
     * @var GenericContextFactoryInterface
     */
    protected $genericContextFactory;

    /**
     * ContextFactory constructor.
     * @param ClassMakerInterface $classMaker
     * @param GenericContextFactoryInterface $genericContextFactory
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        GenericContextFactoryInterface $genericContextFactory
    ) {
        $this->genericContextFactory = $genericContextFactory;
        parent::__construct($classMaker);
    }

    /**
     * {@inheritdoc}
     */
    public function createContext(
        string $rootDir,
        string $appMode,
        ServerRequestInterface $request = null
    ): PipelineContextInterface {
        $class = $this->makeClassName(PipelineContext::class);
        return new $class(
            $this->genericContextFactory->createContext(),
            $rootDir,
            $appMode,
            $request
        );
    }
}