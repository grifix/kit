<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Context;

use Grifix\Kit\Http\ServerRequestInterface;

/**
 * Class ContextFactory
 * @package Grifix\Kit\Pipeline\Context
 */
interface PipelineContextFactoryInterface
{
    /**
     * @param string $rootDir
     * @param string $appMode
     * @param ServerRequestInterface|null $request
     * @return PipelineContextInterface
     */
    public function createContext(
        string $rootDir,
        string $appMode,
        ServerRequestInterface $request = null
    ): PipelineContextInterface;
}