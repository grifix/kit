<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Context;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class Context
 * @package Grifix\Kit\Pipeline\Context
 */
interface PipelineContextInterface
{
    /**
     * @return string
     */
    public function getRootDir(): string;

    /**
     * @param FilesystemInterface $filesystem
     */
    public function setFileSystem(FilesystemInterface $filesystem): void;

    /**
     * @return FilesystemInterface
     */
    public function getFileSystem(): FilesystemInterface;

    /**
     * @param IocContainerInterface $iocContainer
     */
    public function setIocContainer(IocContainerInterface $iocContainer): void;

    /**
     * @return IocContainerInterface
     */
    public function getIocContainer(): IocContainerInterface;

    /**
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel): void;

    /**
     * @return KernelInterface
     */
    public function getKernel(): KernelInterface;

    /**
     * @param CacheInterface $cache
     */
    public function setQuickCache(CacheInterface $cache): void;

    /**
     * @return CacheInterface
     */
    public function getQuickCache(): CacheInterface;

    /**
     * @param CacheInterface $cache
     */
    public function setSlowCache(CacheInterface $cache): void;

    /**
     * @return CacheInterface
     */
    public function getSlowCache(): CacheInterface;

    /**
     * @return string|null
     */
    public function getMemCachedHost();

    /**
     * @return string|null
     */
    public function getMemCachedPort();

    /**
     * @return int
     */
    public function getCacheTtl(): int;

    /**
     * @param ConfigInterface $config
     */
    public function setConfig(ConfigInterface $config): void;

    /**
     * @return ConfigInterface
     */
    public function getConfig(): ConfigInterface;

    /**
     * @return string
     */
    public function getAppMode(): string;

    /**
     * @return ServerRequestInterface
     */
    public function getRequest(): ServerRequestInterface;

    /**
     * @param ResponseInterface $response
     */
    public function setResponse(ResponseInterface $response): void;

    /**
     * @return ResponseInterface
     */
    public function getResponse(): ResponseInterface;

    /**
     * @param ServerRequestInterface $request
     */
    public function setRequest(ServerRequestInterface $request): void;
}
