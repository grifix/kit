<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint\Context;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Filesystem\FilesystemInterface;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Context\ContextInterface as GenericContextInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class Context
 * @package Grifix\Kit\Pipeline\Context
 */
class PipelineContext implements PipelineContextInterface
{
    protected const MODE_PROD = 'prod';
    protected const MODE_DEV = 'dev';

    protected const ROOT_DIR = 'rootDir';
    protected const APP_MODE = 'app_mode';
    protected const QUICK_CACHE = 'quick_cache';
    protected const SLOW_CACHE = 'slow_cache';
    protected const MEM_CACHED_HOST = 'mem_cached_host';
    protected const MEM_CACHED_PORT = 'mem_cached_port';
    protected const CACHE_TTL = 'cache_ttl';

    /**
     * @var GenericContextInterface
     */
    protected $context;

    /**
     * PipelineContext constructor.
     * @param GenericContextInterface $context
     * @param string $rootDir
     * @param string $appMode
     * @param ServerRequestInterface|null $request
     * @throws \Grifix\Kit\Context\Exception\ContextPropertyAlreadyExistsException
     */
    public function __construct(
        GenericContextInterface $context,
        string $rootDir,
        string $appMode,
        ServerRequestInterface $request = null
    ) {
        $this->context = $context;
        $this->context->set(self::ROOT_DIR, $rootDir);
        $this->context->set(self::APP_MODE, $appMode);
        if ($request) {
            $this->context->set(ServerRequestInterface::class, $request);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRootDir(): string
    {
        return $this->context->get(self::ROOT_DIR);
    }

    /**
     * {@inheritdoc}
     */
    public function getAppMode(): string
    {
        return $this->context->get(self::APP_MODE);
    }

    /**
     * {@inheritdoc}$factory = new SimpleCacheFactory(
     * $this->filesystem,
     * new ArrayHelper(),
     * new DateHelper(),
     * new Serializer(),
     * new ClassMaker()
     * );
     */
    public function setFileSystem(FilesystemInterface $filesystem): void
    {
        $this->context->set(FilesystemInterface::class, $filesystem);
    }

    /**
     * {@inheritdoc}
     */
    public function getFileSystem(): FilesystemInterface
    {
        return $this->context->get(FilesystemInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function setIocContainer(IocContainerInterface $iocContainer): void
    {
        $this->context->set(IocContainerInterface::class, $iocContainer);
    }

    /**
     * {@inheritdoc}
     */
    public function getIocContainer(): IocContainerInterface
    {
        return $this->context->get(IocContainerInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function setKernel(KernelInterface $kernel): void
    {
        $this->context->set(KernelInterface::class, $kernel);
    }

    /**
     * {@inheritdoc}
     */
    public function getKernel(): KernelInterface
    {
        return $this->context->get(KernelInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function setQuickCache(CacheInterface $cache): void
    {
        $this->context->set(self::QUICK_CACHE, $cache);
    }

    /**
     * {@inheritdoc}
     */
    public function getQuickCache(): CacheInterface
    {
        return $this->context->get(self::QUICK_CACHE);
    }

    /**
     * {@inheritdoc}
     */
    public function setSlowCache(CacheInterface $cache): void
    {
        $this->context->set(self::SLOW_CACHE, $cache);
    }

    /**
     * {@inheritdoc}
     */
    public function getSlowCache(): CacheInterface
    {
        return $this->context->get(self::SLOW_CACHE);
    }

    /**
     * {@inheritdoc}
     */
    public function getMemCachedHost()
    {
        return $this->context->get(self::MEM_CACHED_HOST);
    }

    /**
     * {@inheritdoc}
     */
    public function getMemCachedPort()
    {
        return $this->context->get(self::MEM_CACHED_PORT);
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheTtl(): int
    {
        return $this->context->get(self::CACHE_TTL);
    }

    /**
     * {@inheritdoc}
     */
    public function setConfig(ConfigInterface $config): void
    {
        $this->context->set(ConfigInterface::class, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(): ConfigInterface
    {
        return $this->context->get(ConfigInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest(): ServerRequestInterface
    {
        return $this->context->get(ServerRequestInterface::class);
    }

    /**
     * {@inheritdoc}
     */
    public function setRequest(ServerRequestInterface $request): void
    {
        $this->context->set(ServerRequestInterface::class, $request, true);
    }

    /**
     * {@inheritdoc}
     */
    public function setResponse(ResponseInterface $response): void
    {
        $this->context->set(ResponseInterface::class, $response, true);
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse(): ResponseInterface
    {
        return $this->context->get(ResponseInterface::class);
    }
}
