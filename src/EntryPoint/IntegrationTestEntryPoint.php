<?php
declare(strict_types=1);

namespace Grifix\Kit\EntryPoint;

use Grifix\Kit\Context\ContextFactory;
use Grifix\Kit\EntryPoint\Context\PipelineContextFactory;
use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Http\ServerRequestFactory;
use Grifix\Kit\Kernel\ClassMaker;
use League\Pipeline\PipelineInterface;

/**
 * Class WebEntryPoint
 * @package Grifix\Kit\EntryPoint
 */
class IntegrationTestEntryPoint extends AbstractEntryPoint
{
    /**
     * {@inheritdoc}
     */
    protected function createPipeline(): PipelineInterface
    {
        return $this->pipelineFactory->createCliPipeline(true);
    }

    /**
     * @return PipelineContextInterface
     */
    protected function createContext(): PipelineContextInterface
    {
        return $this->contextFactory->createContext(
            $this->rootDir,
            'test'
        );
    }

    protected function install()
    {
        $context = $this->createContext();
        $pipeline = $this->pipelineFactory->createComposerInstallPipeline();
        $pipeline($context);
    }

    /**
     * @return PipelineContextInterface
     */
    public function enter(): PipelineContextInterface
    {
        $this->install();
        return parent::enter();
    }
}
