<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Helper;

use Grifix\Kit\Helper\Exception\CantWriteToFileException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class FilesystemHelper
 *
 * @category Grifix
 * @package  Grifix\Kit\Helper
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FilesystemHelper implements FilesystemHelperInterface
{
    
    protected $filesystem;
    
    /**
     * FilesystemHelper constructor.
     *
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }
    
    /**
     * {@inheritdoc}
     */
    public function scanDir(
        string $dir,
        bool $recursive = false,
        int $order = SCANDIR_SORT_ASCENDING,
        array &$result = []
    ) {
        $dir = $this->replaceSeparator($dir);
        $items = scandir($dir, $order);
        
        foreach ($items as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }
            $path = $dir . DIRECTORY_SEPARATOR . $item;
            $type = self::TYPE_FILE;
            if (is_dir($path)) {
                $type = self::TYPE_DIR;
            }
            $result[] = array_merge(['type' => $type, 'path' => $path], pathinfo($path));
            if ($type === self::TYPE_DIR && $recursive !== false) {
                $result = $this->scanDir($path, $recursive, $order, $result);
            }
        }
        
        return $result;
    }
    
    /**
     * {@inheritdoc}
     */
    public function replaceSeparator(string $path): string
    {
        return str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
    }
    
    /**
     * {@inheritdoc}
     */
    public function deleteDir($dir)
    {
        $dir = $this->replaceSeparator($dir);
        if (!is_dir($dir)) {
            return false;
        }
        $dh = opendir($dir);
        /** @noinspection PhpAssignmentInConditionInspection */
        while ($item = readdir($dh)) {
            if ($item != '..' && $item != '.') {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $item)) {
                    self::deleteDir($dir . DIRECTORY_SEPARATOR . $item);
                } else {
                    unlink($dir . DIRECTORY_SEPARATOR . $item);
                }
            }
        }
        closedir($dh);
        
        return rmdir($dir);
    }
    
    /**
     * {@inheritdoc}
     */
    public function writeToFile(
        string $path,
        string $content,
        string $mode = 'a',
        int $perms = 0664,
        int $dirPerms = 0750
    ) {
        $path = $this->replaceSeparator($path);
        $dir = pathinfo($path, PATHINFO_DIRNAME);
        if (!is_dir($dir)) {
            mkdir($dir, $dirPerms, true);
        }
        $fp = fopen($path, $mode);
        flock($fp, 2);
        $result = fwrite($fp, $content);
        
        if ($result === false) {
            throw new CantWriteToFileException($path);
        }
        flock($fp, 3);
        fclose($fp);
        chmod($path, $perms);
        
        return $result;
    }
    
    /**
     * @param string $path
     *
     * @return string
     */
    public function readFromFile(string $path): string
    {
        return file_get_contents($this->replaceSeparator($path));
    }
    
    /**
     * {@inheritdoc}
     */
    public function fileExists(string $path): bool
    {
        return file_exists($this->replaceSeparator($path));
    }
    
    /**
     * {@inheritdoc}
     */
    public function isDir(string $path): bool
    {
        return is_dir($this->replaceSeparator($path));
    }
    
    /**
     * {@inheritdoc}
     */
    public function mkDir(string $path, int $mode = 0777, bool $recursive = true): bool
    {
        return mkdir($path, $mode, $recursive);
    }
    
    /**
     * {@inheritdoc}
     */
    public function copyDir(string $originDir, string $targetDir): void
    {
        $originDir = $this->replaceSeparator($originDir);
        $targetDir = $this->replaceSeparator($targetDir);
        $this->filesystem->mirror($originDir, $targetDir);
    }
}
