<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Helper;

/**
 * Class ArrayHelper
 *
 * @category Grifix
 * @package  Grifix\Utils
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/utils/arr
 */
class ArrayHelper implements ArrayHelperInterface
{

    /**
     * {@inheritdoc}
     */
    public function get(array $array, $pathKey, $defaultValue = null)
    {
        $path = explode('.', strval($pathKey));
        $result = $array;
        foreach ($path as $k) {
            if (is_array($result) && array_key_exists($k, $result)) {
                $result = $result[$k];
            } else {
                return $defaultValue;
            }
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function has(array $array, $pathKey): bool
    {
        return boolval($this->get($array, $pathKey));
    }

    /**
     * {@inheritdoc}
     */
    public function set(array &$array, $pathKey, $val)
    {
        $path = explode('.', $pathKey);
        $firstKey = array_shift($path);
        if (count($path)) {
            if (!isset($array[$firstKey]) || !is_array($array[$firstKey])) {
                $array[$firstKey] = [];
            }
            self::set($array[$firstKey], implode('.', $path), $val);
        } else {
            $array[$firstKey] = $val;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function merge(array $arr1, array $arr2)
    {
        foreach ($arr2 as $k => $v) {
            if (is_array($v) && isset($arr1[$k]) && is_array($arr1[$k])) {
                $arr1[$k] = static::merge($arr1[$k], $arr2[$k]);
            } else {
                $arr1[$k] = $arr2[$k];
            }
        }

        return $arr1;
    }

    /**
     * {@inheritdoc}
     */
    public function insert(&$arr, $value, $position)
    {

        $length = count($arr);
        if ($position < 0 || $position > $length) {
            return;
        }

        for ($i = $length; $i > $position; $i--) {
            $arr[$i] = $arr[$i - 1];
        }

        $arr[$position] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function insertAssoc(&$arr, $key, $value, $index, $position = self::POSITION_BEFORE)
    {
        $keys = array_keys($arr);
        $i = array_search($index, $keys);
        if ($position == self::POSITION_AFTER) {
            $i--;
        }
        $this->insert($keys, $key, $i);
        $arr[$key] = $value;
        $result = [];
        foreach ($keys as $k) {
            $result[$k] = $arr[$k];
        }
        $arr = $result;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray($var)
    {

        $result = [];
        if ($var instanceof \DateTimeInterface) {
            $result = [
                'timestamp' => $var->getTimestamp(),
                'timezone' => $var->getTimezone()->getName(),
                'offset' => $var->getOffset()
            ];
        } elseif (is_object($var)) {
            $reflectionClass = new \ReflectionClass(get_class($var));
            foreach ($reflectionClass->getProperties() as $property) {
                $property->setAccessible(true);
                $value = $property->getValue($var);
                if (is_object($value)) {
                    $value = $this->toArray($value);
                }
                $result[$property->getName()] = $value;

                $property->setAccessible(false);
            }
        } elseif (is_array($var)) {
            foreach ($var as $key => $val) {
                $result[$key] = $this->toArray($val);
            }
        } else {
            $result = $var;
        }

        return $result;
    }

    /**
     * @param array $array1
     * @param array $array2
     * @return array
     */
    public function diff(array $array1, array $array2): array
    {
        foreach ($array1 as $key => $value) {
            if (is_array($value)) {
                if (!isset($array2[$key])) {
                    $difference[$key] = $value;
                } elseif (!is_array($array2[$key])) {
                    $difference[$key] = $value;
                } else {
                    $new_diff = $this->diff($value, $array2[$key]);
                    if ($new_diff != false) {
                        $difference[$key] = $new_diff;
                    }
                }
            } elseif ((!isset($array2[$key]) || $array2[$key] != $value)
                && !($this->get($array2, $key) === null && $value === null)) {
                $difference[$key] = $value;
            }
        }
        return !isset($difference) ? [] : $difference;
    }
}
