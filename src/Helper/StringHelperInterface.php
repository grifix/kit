<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Helper;

/**
 * Class StringHelper
 *
 * @category Grifix
 * @package  Grifix\Utils\Str
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface StringHelperInterface
{
    /**
     * @param $string
     * @param $number
     *
     * @return string
     */
    public function crop($string, $number);

    /**
     * @param string $string
     * @return bool
     */
    public function isJson(string $string): bool;
}
