<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Helper;

/**
 * Class ArrayHelper
 *
 * @category Grifix
 * @package  Grifix\Utils
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/utils/arr
 */
interface ArrayHelperInterface
{
    const POSITION_BEFORE = 'before';
    const POSITION_AFTER = 'after';
    
    /**
     * Returns array value
     *
     * @param array      $array        array
     * @param string|int    $pathKey      path to array key e.g. 'db.host'
     * @param mixed|null $defaultValue default value
     *
     * @return mixed
     */
    public function get(array $array, $pathKey, $defaultValue = null);
    
    /**
     * Sets array value
     *
     * @param array  $array   array
     * @param string|int $pathKey path to array key e.g 'db.host'
     * @param mixed  $val     value
     *
     * @return void
     */
    public function set(array &$array, $pathKey, $val);
    
    /**
     * Merge two arrays
     *
     * @param array $arr1
     * @param array $arr2
     *
     * @return array
     */
    public function merge(array $arr1, array $arr2);
    
    /**
     * @param $arr
     * @param $value
     * @param $position
     *
     * @return void
     */
    public function insert(&$arr, $value, $position);
    
    /**
     * @param $arr
     * @param $key
     * @param $value
     * @param $index
     * @param $position
     *
     * @return void
     */
    public function insertAssoc(&$arr, $key, $value, $index, $position = self::POSITION_BEFORE);

    /**
     * @param $var
     * @return array
     * @throws \ReflectionException
     */
    public function toArray($var);

    /**
     * @param array $array
     * @param $pathKey
     * @return bool
     */
    public function has(array $array, $pathKey): bool;

    /**
     * @param array $array1
     * @param array $array2
     * @return array
     */
    public function diff(array $array1, array $array2): array;
}
