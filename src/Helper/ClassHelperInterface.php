<?php
declare(strict_types=1);

namespace Grifix\Kit\Helper;

/**
 * Class ObjectHelper
 * @package Grifix\Kit\Helper
 */
interface ClassHelperInterface
{
    /**
     * @param string $classA
     * @param string $classB
     * @return bool
     */
    public function isInstanceOf(string $classA, string $classB): bool;

    /**
     * @param string $className
     * @return string
     */
    public function makeFullClassName(string $className): string;
}