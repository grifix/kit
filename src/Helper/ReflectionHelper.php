<?php
declare(strict_types=1);

namespace Grifix\Kit\Helper;

/**
 * Class ReflectionHelper
 * @package Grifix\Kit\Helper
 */
class ReflectionHelper implements ReflectionHelperInterface
{

    /**
     * @param object $object
     * @param string $propertyName
     * @return mixed
     * @throws \ReflectionException
     */
    public function getPropertyValue($object, string $propertyName)
    {
        $objectAndProperty = $this->getObjectAndProperty($object, $propertyName);
        return $this->getValue($objectAndProperty[0], $objectAndProperty[1]);
    }

    /**
     * @param $object
     * @param string $propertyName
     * @return mixed
     * @throws \ReflectionException
     */
    protected function getValue($object, string $propertyName)
    {
        $reelectionClass = new \ReflectionClass($object);
        $property = $reelectionClass->getProperty($propertyName);
        $property->setAccessible(true);
        $result = $property->getValue($object);
        $property->setAccessible(false);
        return $result;
    }

    /**
     * @param object $object
     * @param string $propertyName
     * @return array
     * @throws \ReflectionException
     */
    protected function getObjectAndProperty($object, string $propertyName): array
    {
        if (strpos($propertyName, '.') === false) {
            return [$object, $propertyName];
        }
        $propertyNameArray = explode('.', $propertyName);
        $childPropertyName = array_shift($propertyNameArray);
        $childObject = $this->getValue($object, $childPropertyName);
        return $this->getObjectAndProperty($childObject, implode('.', $propertyNameArray));
    }
}
