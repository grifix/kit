<?php
declare(strict_types=1);

namespace Grifix\Kit\Helper;

/**
 * Class ObjectHelper
 * @package Grifix\Kit\Helper
 */
class ClassHelper implements ClassHelperInterface
{

    /**
     * {@inheritdoc}
     */
    public function isInstanceOf(string $classA, string $classB): bool
    {
        $classA = $this->makeFullClassName($classA);
        $classB = $this->makeFullClassName($classB);
        $reflection = new \ReflectionClass($classA);
        if ($reflection->isInstantiable()) {
            return $reflection->newInstanceWithoutConstructor() instanceof $classB;
        } else {
            return $reflection->isSubclassOf($classB) || $classB == $classA;
        }
    }

    /**
     * @param string $className
     * @return string
     */
    public function makeFullClassName(string $className): string
    {
        if (strpos($className, '\\') !== 0) {
            $className = '\\' . $className;
        }
        return $className;
    }
}
