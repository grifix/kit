<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Helper\Exception;

use Exception;

/**
 * Class CantWriteToFileException
 *
 * @category Grifix
 * @package  Grifix\Kit\Helper\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CantWriteToFileException extends \Exception
{
    
    protected $path;
    
    /**
     * CantWriteToFileException constructor.
     *
     * @param string         $path
     * @param Exception|null $previous
     */
    public function __construct($path, Exception $previous = null)
    {
        $this->message = 'Can\'t write to file "'.$path.'""';
        parent::__construct($previous);
    }
}