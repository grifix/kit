<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Helper;

/**
 * Class ClosureHelper
 *
 * @category Grifix
 * @package  Grifix\Utils\Closure
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ClosureHelperInterface
{
    /**
     * Replaces all Closures in array to SerializableClosures
     *
     * @param array $array
     *
     * @return array
     */
    public function serializeArray(array $array): array;
    
    /**
     * Replaces all SuperClosures in array to Closures
     *
     * @param array $array
     *
     * @return array
     */
    public function unSerializeArray(array $array): array;
}
