<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Helper;

use Grifix\Kit\Helper\Exception\CantWriteToFileException;

/**
 * Class FilesystemHelper
 *
 * @category Grifix
 * @package  Grifix\Kit\Helper
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface FilesystemHelperInterface
{
    const TYPE_DIR = 1;
    const TYPE_FILE = 2;
    
    /**
     * @param string $dir
     * @param bool   $recursive
     * @param int    $order
     * @param array  $result
     *
     * @return array
     */
    public function scanDir(
        string $dir,
        bool $recursive = false,
        int $order = SCANDIR_SORT_ASCENDING,
        array &$result = []
    );
    
    /**
     * @param string $path
     *
     * @return string
     */
    public function replaceSeparator(string $path): string;
    
    /**
     * @param $dir
     *
     * @return bool
     */
    public function deleteDir($dir);
    
    /**
     * @param string $path
     * @param string $content
     * @param string $mode
     * @param int    $perms
     * @param int    $dirPerms
     *
     * @return int
     * @throws CantWriteToFileException
     */
    public function writeToFile(
        string $path,
        string $content,
        string $mode = 'a',
        int $perms = 0664,
        int $dirPerms = 0750
    );
    
    /**
     * @param string $path
     *
     * @return bool
     */
    public function fileExists(string $path): bool ;
    
    /**
     * @param string $path
     *
     * @return string
     */
    public function readFromFile(string $path): string;
    
    /**
     * @param $path
     *
     * @return bool
     */
    public function isDir(string $path): bool;
    
    /**
     * @param string $path
     * @param int    $mode
     * @param bool   $recursive
     *
     * @return bool
     */
    public function mkDir(string $path, int $mode = 0777, bool $recursive = true): bool;
    
    /**
     * @param string $originDir
     * @param string $targetDir
     *
     * @return void
     */
    public function copyDir(string $originDir, string $targetDir): void;
}