<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Helper;

/**
 * Class DateHelper
 *
 * @category Grifix
 * @package  Grifix\Utils\Date
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class DateHelper implements DateHelperInterface
{
    /**
     * {@inheritdoc}
     */
    public function dateIntervalToSeconds(\DateInterval $dateInterval)
    {
        $reference = new \DateTimeImmutable;
        $endTime = $reference->add($dateInterval);
        
        return $endTime->getTimestamp() - $reference->getTimestamp();
    }
}
