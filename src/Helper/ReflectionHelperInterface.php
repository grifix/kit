<?php
declare(strict_types=1);

namespace Grifix\Kit\Helper;

/**
 * Class ReflectionHelper
 * @package Grifix\Kit\Helper
 */
interface ReflectionHelperInterface
{
    /**
     * @param object $object
     * @param string $propertyName
     * @return mixed
     * @throws \ReflectionException
     */
    public function getPropertyValue($object, string $propertyName);
}