<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ioc;

/**
 * Class DefinitionMaker
 *
 * @category Grifix
 * @package  Grifx\Ioc
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface DefinitionMakerInterface
{
    /**
     * @param string $alias
     * @return Definition
     */
    public function makeDefinition(string $alias): Definition;

    /**
     * @param string $alias
     * @param Definition $definition
     * @return Definition
     */
    public function complementDefinition(Definition $definition): Definition;
}
