<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Ioc;

use Grifix\Kit\Ioc\Exception\ConstructorException;
use Psr\SimpleCache\CacheInterface;

/**
 * Class DefinitionMaker
 *
 * @category Grifix
 * @package  Grifx\Ioc
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class DefinitionMaker implements DefinitionMakerInterface
{
    protected $cache;

    /**
     * DefinitionMaker constructor.
     *
     * @param CacheInterface $cache
     */
    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * {@inheritdoc}
     */
    public function makeDefinition(string $alias): Definition
    {
        if ($this->cache->has($this->makeCacheKey($alias))) {
            return $this->cache->get($this->makeCacheKey($alias));
        }

        $className = $this->defineClassName($alias);
        $definition = new Definition($className, $this->defineConstructorArgs($className));

        $this->cache->set($this->makeCacheKey($alias), $definition);

        return $definition;
    }

    /**
     * {@inheritdoc}
     */
    public function complementDefinition(Definition $definition):Definition
    {
        $autoDefinition = $this->makeDefinition($definition->getClassName());
        $autoArgs = $autoDefinition->getArgs();
        $args = $definition->getArgs();
        foreach ($autoArgs as $i => $arg) {
            if (!array_key_exists($i, $args)) {
                $args[$i] = $autoArgs[$i];
            }
        }
        ksort($args);
        return $definition->withArgs($args);
    }

    /**
     * @param string $className
     * @return array
     * @throws \ReflectionException
     */
    protected function defineConstructorArgs(string $className)
    {
        $constructor = (new \ReflectionClass($className))->getConstructor();
        $result = [];

        if ($constructor) {
            $params = $constructor->getParameters();
            $doc = $constructor->getDocComment();
            foreach ($params as $param) {
                $matches = [];
                $pattern = '/\$' . $param->getName() . '\s*<(.*)>/';
                if (preg_match($pattern, $doc, $matches)) {
                    $result[] = $matches[1];
                } elseif ($param->getClass()) {
                    $result[] = $param->getClass()->getName();
                }
            }
        }
        return $result;
    }

    /**
     * @param string $alias
     * @return string
     */
    protected function defineClassName(string $alias): string
    {
        $result = $alias;
        $pattern = '/Interface$/';
        if (preg_match($pattern, $alias)) {
            $result = preg_replace($pattern, '', $alias);
        }

        if (class_exists('\\App\\' . $result)) {
            $result = '\\App\\' . $result;
        }
        return $result;
    }


    /**
     * @param string $alias
     *
     * @return string
     */
    protected function makeCacheKey(string $alias): string
    {
        return str_replace('\\', '.', get_class($this) . '.' . $alias);
    }
}
