<?php


namespace Grifix\Kit\Ioc;

/**
 * Class Definition
 * @package Grifix\Kit\Ioc
 */
class Definition
{

    /**
     * @var string
     */
    protected $className;

    /**
     * @var array
     */
    protected $args = [];

    /**
     * Definiton constructor.
     * @param string $className
     * @param array $args
     */
    public function __construct(string $className, array $args = [])
    {
        $this->className = $className;
        $this->args = $args;
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }

    /**
     * @return array
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * @param array $args
     * @return Definition
     */
    public function withArgs(array $args)
    {
        return new self($this->className, $args);
    }
}
