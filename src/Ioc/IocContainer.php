<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Ioc;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Ioc\Exception\InterDependencyException;
use Grifix\Kit\Ioc\Exception\RecursiveDependencyException;
use phpDocumentor\Reflection\Types\Object_;

/**
 * Class IocContainer
 *
 * @category Grifix
 * @package  Grifx\Ioc
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class IocContainer implements IocContainerInterface
{
    protected $resolvers = [];
    protected $definitionMaker;
    protected $config;
    protected $recursion = [];
    protected $definitions = [];

    /**
     * IocContainer constructor.
     *
     * @param array $resolvers
     * @param DefinitionMakerInterface $definitionMaker
     * @param ConfigInterface $config
     */
    public function __construct(array $resolvers, DefinitionMakerInterface $definitionMaker, ConfigInterface $config)
    {
        $resolvers[IocContainerInterface::class] = function () {
            return $this;
        };
        $this->definitionMaker = $definitionMaker;
        $this->config = $config;
        foreach ($resolvers as $alias => $resolver) {
            $this->set($alias, $resolver);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $alias)
    {
        if (!array_key_exists($alias, $this->resolvers)) {
            $this->resolvers[$alias] = $this->resolveDependency($alias);
        }

        $resolver = $this->resolvers[$alias];

        if ($resolver instanceof \Closure) {
            $this->resolvers[$alias] = $resolver->call($this);
        }

        if ($resolver instanceof Definition) {
            $this->resolvers[$alias] = $this->createNewInstance($alias);
        }


        return $this->resolvers[$alias];
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $alias, $value): IocContainerInterface
    {
        $this->resolvers[$alias] = $value;
        if ($value instanceof Definition) {
            $this->definitions[$alias] = $value;
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function has(string $alias): bool
    {
        return isset($this->resolvers[$alias]);
    }

    /**
     * @param string $alias
     * @return array|mixed|object
     * @throws \ReflectionException
     */
    protected function resolveDependency(string $alias)
    {
        if ($this->recursion && $alias == $this->recursion[0]) {
            $this->recursion[] = $alias;
            throw new RecursiveDependencyException($this->recursion);
        }


        if ($this->isConfig($alias)) {
            return $this->config->get(str_replace('cfg:', '', $alias));
        }

        return $this->createNewInstance($alias);
    }

    /**
     * @param string $alias
     * @return bool
     */
    protected function isConfig(string $alias): bool
    {
        return strpos($alias, 'cfg:') === 0;
    }

    /**
     * {@inheritdoc}
     */
    public function createNewInstance(string $alias, array $args = [])
    {
        if (isset($this->definitions[$alias])) {
            $definition = $this->definitionMaker->complementDefinition($this->definitions[$alias]);
        } else {
            $definition = $this->definitionMaker->makeDefinition($alias);
        }

        $reflectionClass = new \ReflectionClass($definition->getClassName());

        foreach ($definition->getArgs() as $i => $argAlias) {
            if (!array_key_exists($i, $args)) {
                $args[$i] = $this->get($argAlias);
            }
        }
        ksort($args);

        return $reflectionClass->newInstanceArgs($args);
    }
}
