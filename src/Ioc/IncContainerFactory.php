<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ioc;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Psr\SimpleCache\CacheInterface;

/**
 * Class IocContainerFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Ioc
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class IncContainerFactory extends AbstractFactory implements IncContainerFactoryInterface
{
    
    /**
     * @param CacheInterface  $cache
     * @param ConfigInterface $config
     * @param array           $definitions
     *
     * @return IocContainerInterface
     */
    public function createIocContainer(
        CacheInterface $cache,
        ConfigInterface $config,
        array $definitions = []
    ): IocContainerInterface {
        $IocContainer = $this->makeClassName(IocContainer::class);
        $DefinitionMaker = $this->makeClassName(DefinitionMaker::class);
        return new $IocContainer($definitions, new $DefinitionMaker($cache), $config);
    }
}
