<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Ioc;

/**
 * Interface IocContainerInterface
 *
 * @category Grifix
 * @package  Grifx\Ioc
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface IocContainerInterface
{
    /**
     * Returns dependency
     *
     * @param string $alias
     *
     * @return mixed
     */
    public function get(string $alias);

    /**
     * @param string $alias
     * @param mixed $value
     *
     * @return $this
     */
    public function set(string $alias, $value): IocContainerInterface;

    /**
     * @param string $alias
     *
     * @return bool
     */
    public function has(string $alias): bool;

    /**
     * Creates new class instance and fills constructor arguments from Di
     *
     * @param string $className
     * @param array $args
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function createNewInstance(string $className, array $args = []);
}
