<?php
declare(strict_types=1);

namespace Grifix\Kit\Ioc;


use Grifix\Kit\Config\ConfigInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class IocContainerFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Ioc
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface IncContainerFactoryInterface
{
    /**
     * @param CacheInterface $cache
     * @param ConfigInterface $config
     * @param array $definitions
     *
     * @return IocContainerInterface
     */
    public function createIocContainer(
        CacheInterface $cache,
        ConfigInterface $config,
        array $definitions = []
    ): IocContainerInterface;
}