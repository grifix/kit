<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ioc\Exception;

/**
 * Class RecursiveDependencyException
 *
 * @category Grifix
 * @package  Grifix\Kit\Ioc\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RecursiveDependencyException extends \LogicException
{
    /**
     * RecursiveDependencyException constructor.
     *
     * @param null $previous
     */
    
    protected $recursion = [];
    
    /**
     * RecursiveDependencyException constructor.
     *
     * @param array $recursion
     * @param null  $previous
     */
    public function __construct(array $recursion, $previous = null)
    {
        $this->message = 'Recursion in dependencies "' . implode(' -> ', $recursion) . ' -> ..."';
        $this->recursion = $recursion;
        parent::__construct($previous);
    }
}
