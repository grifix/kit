<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Ioc\Exception;

use Exception;

/**
 * Class ConstructorException
 *
 * @category Grifix
 * @package  Grifix\Kit\View\Skin\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ConstructorException extends \Exception
{
    protected $className;
    
    /**
     * ConstructorException constructor.
     *
     * @param Exception|null $className
     * @param Exception      $previous
     */
    public function __construct($className, \Exception $previous)
    {
        $this->className = $className;
        $this->message = 'There is an error in "'.$className.'" constructor dependencies!';
        parent::__construct($previous);
    }
}