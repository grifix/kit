<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Serializer;

use Opis\Closure\SerializableClosure;

use function Opis\Closure\{
    serialize as s, unserialize as u
};

/**
 * Class Serializer
 *
 * @category Grifix
 * @package  Grifix\Kit\Serializer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Serializer implements SerializerInterface
{
    
    /**
     * @param mixed $var
     *
     * @return string
     */
    public function serialize($var): string
    {
        return serialize($this->doSerialize($var));
    }
    
    /**
     * @param mixed $var
     *
     * @return SerializableClosure|string
     */
    protected function doSerialize($var)
    {
        if ($var instanceof \Closure) {
            $var = new SerializableClosure($var);
        }
        if ($var instanceof ClosureContainerInterface) {
            $var = s($var);
            $var = new ClosureContainerWrapper($var);
        }
        if (is_array($var) || ($var instanceof \ArrayAccess)) {
            foreach ($var as &$v) {
                $v = $this->doSerialize($v);
            }
        }
        
        return $var;
    }
    
    /**
     * @param mixed $var
     *
     * @return mixed
     */
    public function unSerialize($var)
    {
        return $this->doUnserialize(unserialize($var));
    }
    
    /**
     * @param mixed $var
     *
     * @return mixed
     */
    protected function doUnserialize($var)
    {
        if ($var instanceof SerializableClosure) {
            return $var->getClosure();
        }
        
        if ($var instanceof ClosureContainerWrapper) {
            return u($var->getString());
        }
        
        if (is_array($var) || ($var instanceof \ArrayAccess)) {
            foreach ($var as &$v) {
                $v = $this->doUnserialize($v);
            }
        }
        
        return $var;
    }
}
