<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Serializer;

/**
 * Class Serializer
 *
 * @category Grifix
 * @package  Grifix\Kit\Serializer
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface SerializerInterface
{
    /**
     * @param mixed $var
     *
     * @return string
     */
    public function serialize($var): string;
    
    /**
     * @param mixed $var
     *
     * @return mixed
     */
    public function unSerialize($var);
}