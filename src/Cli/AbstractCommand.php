<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Cli;

use Grifix\Kit\Ioc\IocContainerInterface;
use Symfony\Component\Console\Command\Command;

/**
 * Class AbstractCommand
 *
 * @category Grifix
 * @package  Grifix\Kit\Cli
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class AbstractCommand extends Command
{
    /**
     * @var IocContainerInterface
     */
    private $iocContainer;

    /**
     * AbstractCommand constructor.
     * @param IocContainerInterface $iocContainer
     * @param string|null $name
     */
    public function __construct(IocContainerInterface $iocContainer, string $name = null)
    {
        $this->iocContainer = $iocContainer;
        $this->init();
        parent::__construct($name);
    }


    protected function init()
    {
        //TODO implement in child class
    }

    /**
     * @param string $alias
     *
     * @return mixed
     */
    public function getShared(string $alias)
    {
        return $this->iocContainer->get($alias);
    }
}
