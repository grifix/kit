<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Cli;

use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class CommandFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Cli
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CommandFactory extends AbstractFactory implements CommandFactoryInterface
{
    
    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;
    
    /**
     * CommandFactory constructor.
     *
     * @param ClassMakerInterface   $classMaker
     * @param IocContainerInterface $iocContainer
     */
    public function __construct(ClassMakerInterface $classMaker, IocContainerInterface $iocContainer)
    {
        $this->iocContainer = $iocContainer;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createCommand(string $className): AbstractCommand
    {
        $className = $this->makeClassName($className);
        
        return new $className($this->iocContainer);
    }
}
