<?php
declare(strict_types=1);

namespace Grifix\Kit\Behat\Stub\Http;


use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Session\SessionInterface;

/**
 * Class Session
 * @package Grifix\Kit\Behat\Stub\Http
 */
class Session implements SessionInterface
{

    /**
     * @var array
     */
    protected $values = [];

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;

    /**
     * Session constructor.
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(ArrayHelperInterface $arrayHelper)
    {
        $this->arrayHelper = $arrayHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key)
    {
        return $this->arrayHelper->get($this->values, $key);
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $key, $val): SessionInterface
    {
        $this->arrayHelper->set($this->values, $key, $val);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return 'session_id';
    }
}