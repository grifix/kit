<?php
declare(strict_types=1);

namespace Grifix\Kit\Behat\Stub\Http;


use Grifix\Kit\Exception\ExceptionPresenterInterface;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;

/**
 * Class ExceptionPresenter
 * @package Grifix\Kit\Behat\Stub\Http
 */
class ExceptionPresenter implements ExceptionPresenterInterface
{
    /**
     * {@inheritdoc}
     */
    public function present(\Throwable $exception, ServerRequestInterface $request): ResponseInterface
    {
        throw $exception;
    }
}
