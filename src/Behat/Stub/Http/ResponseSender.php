<?php
declare(strict_types=1);

namespace Grifix\Kit\Behat\Stub\Http;


use Grifix\Kit\Http\ResponseSenderInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class ResponseSender
 * @package Grifix\Kit\Behat\Stub\Http
 */
class ResponseSender implements ResponseSenderInterface
{
    /**
     * @param ResponseInterface $response
     */
    public function send(ResponseInterface $response)
    {
        //Do nothing
    }
}
