<?php
declare(strict_types=1);

namespace Grifix\Kit\Behat\Stub\Http;


use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Session\SessionFactoryInterface;
use Grifix\Kit\Session\SessionInterface;

/**
 * Class SessionFactory
 * @package Grifix\Kit\Behat\Stub\Http
 */
class SessionFactory implements SessionFactoryInterface
{

    protected $arrayHelper;

    /**
     * SessionFactory constructor.
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(ArrayHelperInterface $arrayHelper)
    {
        $this->arrayHelper = $arrayHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function createNativeSession(array $options = []): SessionInterface
    {
        return new Session($this->arrayHelper);
    }
}