<?php


namespace Grifix\Kit\Behat\Stub\Http;

/**
 * Class Server
 * @package Grifix\Kit\Behat\Stub
 */
class Server extends \Grifix\Kit\Http\Server
{
    /** @noinspection PhpMissingParentCallCommonInspection */
    /**
     * @return mixed|null|string
     */
    public function getClientIp()
    {
        return '127.0.0.1';
    }
}
