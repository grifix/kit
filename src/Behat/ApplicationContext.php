<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Behat;

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Behat\Behat\Hook\Scope\BeforeStepScope;
use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\EntryPoint\IntegrationTestEntryPoint;
use Grifix\Kit\Cqrs\Command\CommandBusInterface;
use Grifix\Kit\Cqrs\Query\QueryBusInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Ui\Request\RequestDispatcherInterface;
use PHPUnit_Framework_Assert as PhpUnit;

/**
 * Class AbstractFeatureContext
 *
 * @category Grifix
 * @package  Grifix\Kit\Behat
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class ApplicationContext
{

    /**
     * @var IocContainerInterface
     */
    private $iocContainer;

    /**
     * @var \Throwable
     */
    protected $error;

    /**
     * @var string
     */
    protected $rootDir;

    /**
     * @var PipelineContextInterface
     */
    protected $context;

    /**
     * @return \Throwable|null
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * AbstractFeatureContext constructor.
     */
    public function __construct()
    {
        $this->rootDir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
        include $this->rootDir . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
    }

    /**
     * @param BeforeScenarioScope $scope
     * @throws \Grifix\Kit\Context\Exception\ContextPropertyAlreadyExistsException
     */
    public function beforeScenario(BeforeScenarioScope $scope)
    {
        $this->context = (new IntegrationTestEntryPoint($this->rootDir))->enter();
        $this->iocContainer = $this->context->getIocContainer();
    }

    /**
     * @param string $alias
     *
     * @return mixed
     */
    protected function getShared(string $alias)
    {
        return $this->iocContainer->get($alias);
    }

    /**
     * @param string $alias
     * @param        $value
     *
     * @return void
     */
    protected function setShared(string $alias, $value)
    {
        $this->iocContainer->set($alias, $value);
    }

    /**
     * @param \Throwable $error
     *
     * @return void
     */
    protected function setError(\Throwable $error): void
    {
        $this->error = $error;
    }

    /**
     * @param string $errorClass
     *
     * @return void
     */
    protected function assertError(string $errorClass)
    {
        PhpUnit::assertInstanceOf($errorClass, $this->error);
        $this->error = null;
    }


    /**
     * @BeforeStep
     *
     * @param BeforeStepScope $scope
     *
     * @return void
     * @throws \Throwable
     */
    public function beforeStep(BeforeStepScope $scope)
    {
        if (strpos($scope->getStep()->getText(), '<error>') === false && $this->error) {
            throw $this->error;
        }
    }

    /**
     * @param       $requestAlias
     * @param array $request
     *
     * @return array|null
     * @throws \Throwable
     */
    protected function dispatchRequest($requestAlias, array $request = [])
    {
        //TODO revmove after WebUiContext will be prepared
        if ($this->error) {
            throw $this->error;
        }
        try {
            return $this->getShared(RequestDispatcherInterface::class)->dispatch($requestAlias, $request);
        } catch (\Exception $e) {
            $this->setError($e);
            return null;
        }
    }

    /**
     * @param object $command
     *
     * @return mixed|null
     * @throws \Throwable
     */
    protected function executeCommand($command)
    {
        if ($this->error) {
            throw $this->error;
        }
        try {
            return $this->getShared(CommandBusInterface::class)->execute($command);
        } catch (\Exception $e) {
            $this->setError($e);

            return null;
        }
    }

    /**
     * @param object $query
     *
     * @return mixed|null
     */
    protected function executeQuery($query)
    {
        try {
            return $this->getShared(QueryBusInterface::class)->execute($query);
        } catch (\Exception $e) {
            $this->setError($e);

            return null;
        }
    }
}
