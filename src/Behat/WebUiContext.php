<?php


namespace Grifix\Kit\Behat;

use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Grifix\Kit\Behat\Stub\Http\ExceptionPresenter;
use Grifix\Kit\Behat\Stub\Http\ResponseSender;
use Grifix\Kit\Behat\Stub\Http\ServerFactory;
use Grifix\Kit\Behat\Stub\Http\SessionFactory;
use Grifix\Kit\EntryPoint\Context\PipelineContextInterface;
use Grifix\Kit\EntryPoint\IntegrationTestEntryPoint;
use Grifix\Kit\EntryPoint\PipelineFactoryInterface;
use Grifix\Kit\EntryPoint\Stage\StageFactoryInterface;
use Grifix\Kit\Exception\ExceptionPresenterInterface;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ResponseSenderInterface;
use Grifix\Kit\Http\ServerFactoryInterface;
use Grifix\Kit\Http\ServerRequestFactoryInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Ioc\Definition;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Session\SessionFactoryInterface;
use PHPUnit_Framework_Assert as PhpUnit;

/**
 * Class WebUiContext
 * @package Grifix\Kit\Behat
 */
abstract class WebUiContext
{

    /**
     * @var ResponseInterface
     */
    protected $lastResponse;

    /**
     * @var string
     */
    protected $rootDir;

    /**
     * @var PipelineContextInterface
     */
    protected $context;


    protected $entryPoint;

    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    /**
     * @var PipelineFactoryInterface
     */
    protected $pipelineFactory;

    /**
     * @var StageFactoryInterface
     */
    protected $stageFactory;

    protected $debug = false;

    public function __construct()
    {
        $this->rootDir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
        include $this->rootDir . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
    }

    /**
     * @param BeforeScenarioScope $scope
     */
    public function beforeScenario(BeforeScenarioScope $scope)
    {
        $this->context = (new IntegrationTestEntryPoint($this->rootDir))->enter();
        $this->iocContainer = $this->context->getIocContainer();

        $this->setShared(SessionFactoryInterface::class, new Definition(SessionFactory::class));
        $this->setShared(ServerFactoryInterface::class, new Definition(ServerFactory::class));
        $this->setShared(ResponseSenderInterface::class, new Definition(ResponseSender::class));
        if ($this->debug) {
            $this->setShared(ExceptionPresenterInterface::class, new Definition(ExceptionPresenter::class));
            echo "\033[31mWARNING, DEBUG MODE IS ON!\033[0m";
            ob_flush();
        }

        $this->pipelineFactory = $this->iocContainer->get(PipelineFactoryInterface::class);
        $this->stageFactory = $this->iocContainer->get(StageFactoryInterface::class);

        $pipeline = $this->pipelineFactory->createEmptyPipeline();
        $pipeline = $pipeline->pipe($this->stageFactory->createInitSessionStage());
        $pipeline($this->context);
    }

    /**
     * @param string $uri
     * @param array $params
     * @return ResponseInterface
     */
    protected function get(string $uri, array $params = []): ResponseInterface
    {
        $request = $this->getShared(ServerRequestFactoryInterface::class)->create('GET', $uri)
            ->withParsedBody($params);
        return $this->sendRequest($request);
    }

    /**
     * @param string $uri
     * @param array $params
     * @return ResponseInterface
     */
    protected function post(string $uri, array $params = []): ResponseInterface
    {
        $request = $this->getShared(ServerRequestFactoryInterface::class)->create('POST', $uri)
            ->withQueryParams($params);
        return $this->sendRequest($request);
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Grifix\Kit\Context\Exception\ContextPropertyAlreadyExistsException
     */
    protected function sendRequest(ServerRequestInterface $request): ResponseInterface
    {
        $request = $request->withAddedHeader('Accept', 'application/json');
        $this->context->setRequest($request);
        $pipeline = $this->pipelineFactory->createEmptyPipeline();
        $pipeline = $pipeline->pipe($this->stageFactory->createHandleRequestStage());
        $pipeline($this->context);
        $this->lastResponse = $this->context->getResponse();
        return $this->lastResponse;
    }

    /**
     * @return array|null
     */
    protected function getResponseArray()
    {
        return json_decode($this->lastResponse->getBody()->getContents(), true);
    }

    /**
     * @param string $alias
     *
     * @return mixed
     */
    protected function getShared(string $alias)
    {
        return $this->iocContainer->get($alias);
    }

    /**
     * @return ResponseInterface|null
     */
    protected function getLastResponse(): ?ResponseInterface
    {
        return $this->lastResponse;
    }

    /**
     * @param string $alias
     * @param        $value
     *
     * @return void
     */
    protected function setShared(string $alias, $value)
    {
        $this->iocContainer->set($alias, $value);
    }

    protected function assertSuccessResponse()
    {
        PhpUnit::assertEquals(200, $this->lastResponse->getStatusCode());
    }
}