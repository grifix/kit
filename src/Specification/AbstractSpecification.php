<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Specification;

use Grifix\Kit\Expression\Builder\ExpressionBuilderInterface;
use Grifix\Kit\Expression\ExpressionFactoryInterface;
use Grifix\Kit\Expression\ExpressionInterface;
use Grifix\Kit\Specification\Exception\PropertyNotExistsException;

/**
 * Class NestedSpecification
 *
 * @category Grifix
 * @package  Grifix\Kit\Orm\Specification
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractSpecification implements SpecificationInterface
{
    /**
     * @var ExpressionFactoryInterface
     */
    protected $expressionFactory;

    /**
     * @var ExpressionInterface|mixed
     */
    protected $expression;

    /**
     * @var NestedSpecificationInterface[]
     */
    protected $nestedSpecifications = [];

    /**
     * @var ExpressionBuilderInterface
     */
    protected $builder;

    /**
     * @var SpecificationFactoryInterface
     */
    protected $specificationFactory;

    /**
     * NestedSpecification constructor.
     *
     * @param ExpressionFactoryInterface $expressionFactory
     * @param ExpressionBuilderInterface $builder
     * @param SpecificationFactoryInterface $specificationFactory
     */
    public function __construct(
        ExpressionFactoryInterface $expressionFactory,
        ExpressionBuilderInterface $builder,
        SpecificationFactoryInterface $specificationFactory
    ) {
        $this->specificationFactory = $specificationFactory;
        $this->expressionFactory = $expressionFactory;
        $this->builder = $builder;
        $this->expression = $this->createExpression();
    }

    /**
     * {@inheritdoc}
     */
    public function orSpecification(
        SpecificationInterface $specification,
        string $property = null
    ): SpecificationInterface {

        $this->nestedSpecifications[] = $this->specificationFactory->createOrSpecification($specification, $property);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function andSpecification(
        SpecificationInterface $specification,
        string $property = null
    ): SpecificationInterface {
        $this->nestedSpecifications[] = $this->specificationFactory->createAndSpecification($specification, $property);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getExpression(): ExpressionInterface
    {
        return clone ($this->expression);
    }

    /**
     * @return mixed
     */
    abstract protected function createExpression(): ExpressionInterface;

    /**
     * @param ExpressionInterface $leftOperand
     * @param ExpressionInterface $rightOperand
     * @param ExpressionInterface ...$operands
     * @return ExpressionInterface
     */
    protected function andX(
        ExpressionInterface $leftOperand,
        ExpressionInterface $rightOperand,
        ExpressionInterface ...$operands
    ): ExpressionInterface {
        return $this->expressionFactory->andX($leftOperand, $rightOperand, ...$operands);
    }

    /**
     * @param ExpressionInterface $leftOperand
     * @param ExpressionInterface $rightOperand
     * @param ExpressionInterface ...$operands
     * @return ExpressionInterface
     */
    protected function orX(
        ExpressionInterface $leftOperand,
        ExpressionInterface $rightOperand,
        ExpressionInterface ...$operands
    ): ExpressionInterface {
        return $this->expressionFactory->orX($leftOperand, $rightOperand, ...$operands);
    }

    /**
     * @param $leftOperand
     * @param $rightOperand
     * @return ExpressionInterface
     */
    protected function eq($leftOperand, $rightOperand): ExpressionInterface
    {
        return $this->expressionFactory->eq($leftOperand, $rightOperand);
    }

    /**
     * @param $leftOperand
     * @param $rightOperand
     * @return ExpressionInterface
     */
    protected function ne($leftOperand, $rightOperand): ExpressionInterface
    {
        return $this->expressionFactory->ne($leftOperand, $rightOperand);
    }

    /**
     * @param $needle
     * @param array $haystack
     * @return ExpressionInterface
     */
    protected function in($needle, array $haystack): ExpressionInterface
    {
        return $this->expressionFactory->in($needle, $haystack);
    }

    /**
     * @param $leftOperand
     * @param $rightOperand
     * @return ExpressionInterface
     */
    protected function lt($leftOperand, $rightOperand): ExpressionInterface
    {
        return $this->expressionFactory->lt($leftOperand, $rightOperand);
    }

    /**
     * @param $leftOperand
     * @param $rightOperand
     * @return ExpressionInterface
     */
    protected function lte($leftOperand, $rightOperand): ExpressionInterface
    {
        return $this->expressionFactory->lte($leftOperand, $rightOperand);
    }

    /**
     * @param $leftOperand
     * @param $rightOperand
     * @return ExpressionInterface
     */
    protected function gte($leftOperand, $rightOperand): ExpressionInterface
    {
        return $this->expressionFactory->gte($leftOperand, $rightOperand);
    }

    /**
     * @param $leftOperand
     * @param $rightOperand
     * @return ExpressionInterface
     */
    protected function gt($leftOperand, $rightOperand): ExpressionInterface
    {
        return $this->expressionFactory->gt($leftOperand, $rightOperand);
    }

    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy($object): bool
    {
        $reflectionClass = new \ReflectionClass($object);
        $expression = $this->prepareExpression($this->getExpression(), $object, $reflectionClass);
        $result = false;
        $code = '$result = ' . $this->builder->buildExpression($expression);
        foreach ($this->nestedSpecifications as $specification) {
            $bool = 'false';
            if ($specification->isSatisfiedBy(
                $this->chooseNestedSpecificationObject($object, $reflectionClass, $specification)
            )) {
                $bool = 'true';
            }

            if ($specification instanceof OrSpecification) {
                $code .= ' || ' . $bool;
            } else {
                $code .= ' && ' . $bool;
            }
        }
        eval($code . ';');


        return $result;
    }

    /**
     * @param object $object
     * @param \ReflectionClass $reflectionClass
     * @param NestedSpecificationInterface $specification
     *
     * @return object
     */
    protected function chooseNestedSpecificationObject(
        $object,
        \ReflectionClass $reflectionClass,
        NestedSpecificationInterface $specification
    ) {
        if (!$specification->getProperty()) {
            return $object;
        }
        $property = $reflectionClass->getProperty($specification->getProperty());
        $property->setAccessible(true);
        $result = $property->getValue($object);
        $property->setAccessible(false);

        return $result;
    }

    /**
     * @return SpecificationInterface[]
     */
    public function getNestedSpecifications(): array
    {
        return $this->nestedSpecifications;
    }

    /**
     * @param ExpressionInterface $expression
     * @param $object
     * @param \ReflectionClass $reflectionClass
     * @return ExpressionInterface
     * @throws PropertyNotExistsException
     * @throws \Grifix\Kit\Expression\Exception\OperandNotExistsException
     * @throws \ReflectionException
     */
    protected function prepareExpression(
        ExpressionInterface $expression,
        $object,
        \ReflectionClass $reflectionClass
    ): ExpressionInterface {
        $clone = clone $expression;
        $key = $clone->getOperand(0);
        if (!($key instanceof ExpressionInterface) && is_scalar($key)) {
            $clone->setOperand(0, $this->getPropertyValue($key, $object, $reflectionClass));
        }
        foreach ($clone->getOperands() as $i => $operand) {
            if ($operand instanceof ExpressionInterface) {
                $clone->setOperand($i, $this->prepareExpression($operand, $object, $reflectionClass));
            }
        }

        return $clone;
    }

    /**
     * @param $propertyName
     * @param $object
     * @param \ReflectionClass $reflectionClass
     * @return mixed
     * @throws PropertyNotExistsException
     * @throws \ReflectionException
     */
    protected function getPropertyValue($propertyName, $object, \ReflectionClass $reflectionClass)
    {
        $arrPropertyName = explode('.', $propertyName);
        $propertyName = array_shift($arrPropertyName);
        if (!$reflectionClass->hasProperty($propertyName)) {
            throw new PropertyNotExistsException($propertyName, $object);
        }
        $property = $reflectionClass->getProperty($propertyName);
        $property->setAccessible(true);
        $value = $property->getValue($object);
        if ($property->isProtected() || $property->isPrivate()) {
            $property->setAccessible(false);
        }

        if (count($arrPropertyName) > 0 && is_object($value)) {
            $value = $this->getPropertyValue(implode('.', $arrPropertyName), $value, new \ReflectionClass($value));
        }

        return $value;
    }
}