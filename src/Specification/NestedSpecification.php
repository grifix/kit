<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Specification;

use Grifix\Kit\Expression\ExpressionInterface;

/**
 * Class NestedSpecification
 *
 * @category Grifix
 * @package  Grifix\Kit\Specification
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class NestedSpecification implements NestedSpecificationInterface
{
    /**
     * @var SpecificationInterface
     */
    protected $specification;
    
    /**
     * @var string
     */
    protected $property;
    
    /**
     * NestedSpecification constructor.
     *
     * @param SpecificationInterface $specification
     * @param string|null            $property
     */
    public function __construct(SpecificationInterface $specification, string $property = null)
    {
        $this->specification = $specification;
        $this->property = $property;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getExpression(): ExpressionInterface
    {
        return $this->specification->getExpression();
    }
    
    /**
     * {@inheritdoc}
     */
    public function isSatisfiedBy($object): bool
    {
        return $this->specification->isSatisfiedBy($object);
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function orSpecification(
        SpecificationInterface $specification,
        string $property = null
    ): SpecificationInterface {
        $this->specification->orSpecification($specification);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function andSpecification(
        SpecificationInterface $specification,
        string $property = null
    ): SpecificationInterface {
        $this->specification->andSpecification($specification, $property);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getNestedSpecifications(): array
    {
        return $this->specification->getNestedSpecifications();
    }
    
    /**
     * @return string|null
     */
    public function getProperty()
    {
        return $this->property;
    }
}