<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Specification\Exception;



/**
 * Class PropertyNotExistsException
 *
 * @category Grifix
 * @package  Grifix\Kit\Specification\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class PropertyNotExistsException extends \Exception
{
    protected $property;
    
    protected $object;
    
    /**
     * PropertyNotExistsException constructor.
     *
     * @param string $property
     * @param        $object
     */
    public function __construct(string $property, $object)
    {
        $this->property = $property;
        $this->object = $object;
        $this->message = 'Type ' . $property . ' does not exits in class ' . get_class($object) . '!';
        parent::__construct();
    }
}