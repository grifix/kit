<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Specification\Exception;


use Grifix\Kit\Specification\SimpleSpecification;

/**
 * Class InvalidLogicException
 *
 * @category Grifix
 * @package  Grifix\Kit\Specification\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 *
 * Недопустимая логика в SimpleSpecification
 */
class InvalidLogicException extends \LogicException
{
    
    /**
     * @var SimpleSpecification
     */
    protected $specification;
    
    /**
     * InvalidLogicException constructor.
     *
     * @param SimpleSpecification $specification
     */
    public function __construct(SimpleSpecification $specification)
    {
        parent::__construct();
        $this->specification = $specification;
    }
    
    /**
     * @return SimpleSpecification
     */
    public function getSpecification(){
        return $this->specification;
    }
}