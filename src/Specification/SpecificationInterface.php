<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Specification;

use bar\baz\source_with_namespace;
use Grifix\Kit\Expression\ExpressionInterface;

/**
 * Class SpecificationInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Orm\Specification
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface SpecificationInterface
{
    
    /**
     * @return ExpressionInterface
     */
    public function getExpression(): ExpressionInterface;
    
    /**
     * @param $object
     *
     * @return bool
     */
    public function isSatisfiedBy($object): bool;
    
    /**
     * @param SpecificationInterface $specification
     * @param string|null            $property
     *
     * @return SpecificationInterface
     */
    public function orSpecification(
        SpecificationInterface $specification,
        string $property = null
    ): SpecificationInterface;
    
    /**
     * @param SpecificationInterface $specification
     * @param string|null            $property
     *
     * @return SpecificationInterface
     *
     */
    public function andSpecification(
        SpecificationInterface $specification,
        string $property = null
    ): SpecificationInterface;
    
    /**
     * @return NestedSpecificationInterface[]
     */
    public function getNestedSpecifications(): array;
}