<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector;

use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Expression\Builder\ExpressionBuilderFactory;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Orm\Serializer\ObjectSerializerInterface;
use Grifix\Kit\Specification\Injector\ColumnNameCreator\ColumnNameCreatorFactoryInterface;
use Grifix\Kit\Specification\Injector\ValueConverter\ValueConverterFactoryInterface;

/**
 * Class SpecificationInjectorFactory
 * @package Grifix\Kit\Orm\Collection\SpecificationInjector
 */
class SpecificationInjectorFactory extends AbstractFactory implements SpecificationInjectorFactoryInterface
{

    /**
     * @var ExpressionBuilderFactory
     */
    protected $expressionBuilderFactory;

    /**
     * @var ValueConverterFactoryInterface
     */
    protected $valueConverterFactory;

    /**
     * @var ColumnNameCreatorFactoryInterface
     */
    protected $columnNameCreatorFactory;

    /**
     * SpecificationInjectorFactory constructor.
     * @param ExpressionBuilderFactory $expressionBuilderFactory
     * @param ClassMakerInterface $classMaker
     * @param ValueConverterFactoryInterface $valueConverterFactory
     * @param ColumnNameCreatorFactoryInterface $columnNameCreatorFactory
     */
    public function __construct(
        ExpressionBuilderFactory $expressionBuilderFactory,
        ClassMakerInterface $classMaker,
        ValueConverterFactoryInterface $valueConverterFactory,
        ColumnNameCreatorFactoryInterface $columnNameCreatorFactory
    ) {
        parent::__construct($classMaker);
        $this->expressionBuilderFactory = $expressionBuilderFactory;
        $this->valueConverterFactory = $valueConverterFactory;
        $this->columnNameCreatorFactory = $columnNameCreatorFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function createJsonbSpecificationInjector(
        QueryInterface $query,
        array $columnMap = [],
        array $converters = []
    ): SpecificationInjectorInterface {
        $class = $this->makeClassName(SpecificationInjector::class);
        return new $class(
            $query,
            $this->expressionBuilderFactory->createSqlBuilder(),
            $this->columnNameCreatorFactory->createJsonbCreator($columnMap),
            $this->valueConverterFactory->createJsonValueConverter($converters)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function createSqlSpecificationInjector(
        QueryInterface $query,
        array $columnMap = [],
        array $converters = []
    ): SpecificationInjectorInterface {
        $class = $this->makeClassName(SpecificationInjector::class);
        return new $class(
            $query,
            $this->expressionBuilderFactory->createSqlBuilder(),
            $this->columnNameCreatorFactory->createSqlCreator($columnMap),
            $this->valueConverterFactory->createValueConverter($converters)
        );
    }
}
