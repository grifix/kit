<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector\ColumnNameCreator;

use Grifix\Kit\Kernel\AbstractFactory;

/**
 * Class ColumnNameCreatorFactory
 * @package Grifix\Kit\Specification\Injector\ColumnNameCreator
 */
class ColumnNameCreatorFactory extends AbstractFactory implements ColumnNameCreatorFactoryInterface
{
    /**
     * @param array $map
     * @return ColumnNameCreatorInterface
     */
    public function createJsonbCreator(array $map = []): ColumnNameCreatorInterface
    {
        $class = $this->makeClassName(JsonbColumnNameCreator::class);
        return new $class($map);
    }

    /**
     * @param array $map
     * @return ColumnNameCreatorInterface
     */
    public function createSqlCreator(array $map = []): ColumnNameCreatorInterface
    {
        $class = $this->makeClassName(SqlColumnNameCreator::class);
        return new $class($map);
    }
}
