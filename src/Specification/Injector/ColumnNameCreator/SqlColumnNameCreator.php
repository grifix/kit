<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector\ColumnNameCreator;

/**
 * Class SqlCoumnNameCreator
 * @package Grifix\Kit\Specification\Injector\ColumnNameCreator
 */
class SqlColumnNameCreator implements ColumnNameCreatorInterface
{
    protected $map = [];

    /**
     * SqlColumnNameCreator constructor.
     * @param array $map
     */
    public function __construct(array $map = [])
    {
        $this->map = $map;
    }

    /**
     * @param string $varName
     * @return string
     */
    public function crateColumnName(string $varName): string
    {
        if (isset($this->map[$varName])) {
            return $this->map[$varName];
        }
        return $varName;
    }
}
