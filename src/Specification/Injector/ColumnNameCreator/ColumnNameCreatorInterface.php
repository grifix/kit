<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector\ColumnNameCreator;

/**
 * Class ColumnNameCreater
 * @package Grifix\Kit\Specification\Injector\ColumnNameCreator
 */
interface ColumnNameCreatorInterface
{
    /**
     * @param string $varName
     * @return string
     */
    public function crateColumnName(string $varName): string;
}