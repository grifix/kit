<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector\ColumnNameCreator;


/**
 * Class ColumnNameCreatorFactory
 * @package Grifix\Kit\Specification\Injector\ColumnNameCreator
 */
interface ColumnNameCreatorFactoryInterface
{
    /**
     * @param array $map
     * @return ColumnNameCreatorInterface
     */
    public function createJsonbCreator(array $map = []): ColumnNameCreatorInterface;

    /**
     * @param array $map
     * @return ColumnNameCreatorInterface
     */
    public function createSqlCreator(array $map = []): ColumnNameCreatorInterface;
}