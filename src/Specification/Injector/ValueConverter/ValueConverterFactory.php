<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector\ValueConverter;


use Grifix\Kit\Kernel\AbstractFactory;

/**
 * Class ValueConverterFactory
 * @package Grifix\Kit\Specification\Injector\ValueConverter
 */
class ValueConverterFactory extends AbstractFactory implements ValueConverterFactoryInterface
{
    /**
     * @param \Closure[] $converters
     * @return ValueConverterInterface
     */
    public function createValueConverter(array $converters = []): ValueConverterInterface
    {
        $class = $this->makeClassName(ValueConverter::class);
        return new $class($converters);
    }

    /**
     * @param array $converters
     * @return ValueConverterInterface
     */
    public function createJsonValueConverter(array $converters = []): ValueConverterInterface
    {
        $class = $this->makeClassName(JsonValueConverter::class);
        return new $class($converters);
    }
}