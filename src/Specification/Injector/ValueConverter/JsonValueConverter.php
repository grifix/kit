<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector\ValueConverter;


/**
 * Class JsonValueConverter
 * @package Grifix\Kit\Specification\Injector\ValueConverter
 */
class JsonValueConverter extends ValueConverter
{
    /**
     * {@inheritdoc}
     */
    public function convertValue(string $varName, $value)
    {
        return json_encode(parent::convertValue($varName, $value));
    }
}