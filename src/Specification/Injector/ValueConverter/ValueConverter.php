<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector\ValueConverter;

use Grifix\Kit\Type\ValuableTypeInterface;

/**
 * Class ValueConverter
 * @package Grifix\Kit\Specification\Injector\ValueConverter
 */
class ValueConverter implements ValueConverterInterface
{
    /***
     * @var \Closure[]
     */
    protected $converters = [];

    /**
     * ValueConverter constructor.
     * @param \Closure[] $converters
     */
    public function __construct(array $converters)
    {
        $this->converters = $converters;
    }

    /**
     * @param string $varName
     * @param $value
     * @return mixed|string
     */
    public function convertValue(string $varName, $value)
    {
        $result = $value;
        if (isset($this->converters[$varName])) {
            $closure = $this->converters[$varName];
            $result = $closure($value);
        }
        if ($value instanceof \DateTimeInterface) {
            $result = $value->format('Y-m-d H:i:s');
        }
        if ($value instanceof ValuableTypeInterface) {
            $result = $value->getValue();
        }
        if (is_null($result)) {
            $result = 'NULL';
        }
        return $result;
    }
}
