<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector;

use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Expression\Builder\ExpressionBuilderInterface;
use Grifix\Kit\Expression\ExpressionInterface;
use Grifix\Kit\Orm\Serializer\ObjectSerializerInterface;
use Grifix\Kit\Specification\AndSpecification;
use Grifix\Kit\Specification\Injector\ColumnNameCreator\ColumnNameCreatorInterface;
use Grifix\Kit\Specification\Injector\ValueConverter\ValueConverterInterface;
use Grifix\Kit\Specification\SpecificationInterface;

/**
 * Class SpecificationInjector
 * @package Grifix\Kit\Orm\Collection\SpecificationInjector
 *
 * Injects specification into collection query
 */
class SpecificationInjector implements SpecificationInjectorInterface
{
    /**
     * @var QueryInterface
     */
    protected $query;

    /**
     * @var ExpressionBuilderInterface
     */
    protected $expressionBuilder;

    /**
     * @var int
     */
    protected $bindingsCounter = 0;

    /**
     * @var ColumnNameCreatorInterface
     */
    protected $columnNameCreator;

    /**
     * @var ValueConverterInterface
     */
    protected $valueConverter;

    /**
     * SpecificationInjector constructor.
     * @param QueryInterface $query
     * @param ExpressionBuilderInterface $expressionBuilder
     * @param ColumnNameCreatorInterface $columnNameCreator
     * @param ValueConverterInterface $valueConverter
     */
    public function __construct(
        QueryInterface $query,
        ExpressionBuilderInterface $expressionBuilder,
        ColumnNameCreatorInterface $columnNameCreator,
        ValueConverterInterface $valueConverter
    ) {
        $this->query = $query;
        $this->expressionBuilder = $expressionBuilder;
        $this->columnNameCreator = $columnNameCreator;
        $this->valueConverter = $valueConverter;
    }


    /**
     * @param SpecificationInterface $specification
     * @throws \Grifix\Kit\Expression\Exception\OperandNotExistsException
     * @throws \Grifix\Kit\Expression\Exception\UnknownExpressionException
     * @throws \ReflectionException
     */
    public function inject(SpecificationInterface $specification)
    {
        $this->query->where($this->buildConditionSql($specification));
    }

    /**
     * @param SpecificationInterface $specification
     * @return string
     * @throws \Grifix\Kit\Expression\Exception\OperandNotExistsException
     * @throws \Grifix\Kit\Expression\Exception\UnknownExpressionException
     * @throws \ReflectionException
     */
    protected function buildConditionSql(SpecificationInterface $specification): string
    {
        $expression = $this->prepareExpression($specification->getExpression());
        $result = '(' . $this->expressionBuilder->buildExpression($expression);
        foreach ($specification->getNestedSpecifications() as $nestedSpecification) {
            if ($nestedSpecification instanceof AndSpecification) {
                $result .= ' AND ' . $this->buildConditionSql($nestedSpecification);
            } else {
                $result .= ' OR ' . $this->buildConditionSql($nestedSpecification);
            }
        }
        return $result . ')';
    }

    /**
     * @param ExpressionInterface $expression
     * @return ExpressionInterface
     * @throws \Grifix\Kit\Expression\Exception\OperandNotExistsException
     * @throws \ReflectionException
     */
    protected function prepareExpression(ExpressionInterface $expression): ExpressionInterface
    {
        foreach ($expression->getOperands() as $operand) {
            if ($operand instanceof ExpressionInterface) {
                $this->prepareExpression($operand);
            }
        }
        $property = $expression->getOperand(0);

        if (is_string($property)) {
            foreach ($expression->getOperands() as $i => $operand) {
                if ($i > 0) {
                    $bindingName = $this->createBindingName($property);
                    $this->query->bindValue($bindingName, $this->valueConverter->convertValue($property, $operand));
                    $expression->setOperand($i, ':' . $bindingName);
                }
            }

            $expression->setOperand(0, $this->columnNameCreator->crateColumnName($property));
        }

        return $expression;
    }

    /**
     * @param string $property
     * @return string
     */
    protected function createBindingName(string $property): string
    {
        $result = str_replace('.', '_', $property) . '_' . $this->bindingsCounter;
        $this->bindingsCounter++;
        return $result;
    }
}
