<?php
declare(strict_types=1);

namespace Grifix\Kit\Specification\Injector;

use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Orm\Serializer\ObjectSerializerInterface;

/**
 * Class SpecificationInjectorFactory
 * @package Grifix\Kit\Orm\Collection\SpecificationInjector
 */
interface SpecificationInjectorFactoryInterface
{
    /**
     * @param QueryInterface $query
     * @param array $columnMap
     * @param \Closure[] $converters
     * @return SpecificationInjectorInterface
     */
    public function createJsonbSpecificationInjector(
        QueryInterface $query,
        array $columnMap = [],
        array $converters = []
    ): SpecificationInjectorInterface;

    /**
     * @param QueryInterface $query
     * @param array $columnMap
     * @param \Closure[] $converters
     * @return SpecificationInjectorInterface
     */
    public function createSqlSpecificationInjector(
        QueryInterface $query,
        array $columnMap = [],
        array $converters = []
    ): SpecificationInjectorInterface;
}