<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Exception;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;

/**
 * Class ExceptionExceptionHandler
 *
 * @category Grifix
 * @package  Grifix\Kit\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ExceptionPresenterInterface
{
    /**
     * @param \Throwable $exception
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function present(\Throwable $exception, ServerRequestInterface $request):ResponseInterface;
}