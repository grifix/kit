<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Exception;

use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ResponseFactoryInterface;
use Grifix\Kit\Http\ServerRequestFactoryInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\View\ViewFactoryInterface;

/**
 * Class ExceptionExceptionHandler
 *
 * @category Grifix
 * @package  Grifix\Kit\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ExceptionPresenter implements ExceptionPresenterInterface
{
    /**
     * @var ViewFactoryInterface
     */
    protected $viewFactory;

    /**
     * @var ResponseFactoryInterface
     */
    protected $responseFactory;

    /**
     * @var array
     */
    protected $views;

    /**
     * @var bool
     */
    protected $debug;

    /**
     * @var bool
     */
    protected $enabled;

    /**
     * http codes for exception classes
     *
     * @var array
     */
    protected $codes = [];

    /**
     * messages for exception classes
     *
     * @var array
     */
    protected $messages = [];

    /**
     * exceptions types that must be not convert to userException
     *
     * @var array
     */
    protected $ignore;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * ExceptionExceptionHandler constructor.
     *
     * @param TranslatorInterface $translator
     * @param ViewFactoryInterface $viewFactory
     * @param ResponseFactoryInterface $responseFactory
     * @param bool $debug <cfg:grifix.kit.exception.presenter.debug>
     * @param bool $enabled <cfg:grifix.kit.exception.presenter.enabled>
     * @param array $views <cfg:grifix.kit.exception.presenter.views>
     */
    public function __construct(
        TranslatorInterface $translator,
        ViewFactoryInterface $viewFactory,
        ResponseFactoryInterface $responseFactory,
        bool $debug,
        bool $enabled,
        array $views
    )
    {
        $this->viewFactory = $viewFactory;
        $this->responseFactory = $responseFactory;
        $this->debug = $debug;
        $this->views = $views;
        $this->enabled = $enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function present(\Throwable $exception, ServerRequestInterface $request): ResponseInterface
    {
        if (!$this->enabled) {
            throw  $exception;
        }

        $accept = $request->getHeaderLine('accept');
        $accept = explode(',', $accept);
        $view = $this->views['text/html'];
        foreach ($this->views as $k => $v) {
            if (in_array($k, $accept)) {
                $view = $v;
                break;
            }
        }
        if ($request->isAjax()) {
            $view = $this->views['application/json'];
        }

        $code = $exception->getCode();
        if (!$code) {
            $code = 500;
        }

        return $this->responseFactory->createResponse($code)->withContent(
            $this->viewFactory->create($view)
                ->render([
                    'message' => $exception->getMessage(),
                    'exception' => $exception,
                    'debug' => $this->debug,
                    'code' => $code,
                ])
        );
    }
}
