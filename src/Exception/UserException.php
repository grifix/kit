<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Exception;

use Exception;

/**
 * Class UserException
 *
 * @category Grifix
 * @package  Grifix\Kit\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UserException extends \Exception
{
    /**
     * UserException constructor.
     *
     * @param string    $message
     * @param int       $code
     * @param \Throwable $previous
     */
    public function __construct($message, $code, \Throwable $previous)
    {
        parent::__construct($message, $code, $previous);
    }
}
