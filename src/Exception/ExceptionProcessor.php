<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Exception;

use Grifix\Kit\Kernel\KernelInterface;

/**
 * Class ExceptionProcessor
 *
 * @category Grifix
 * @package  Grifix\Kit\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ExceptionProcessor implements ExceptionProcessorInterface
{
    
    /**
     * @var KernelInterface
     */
    protected $kernel;
    
    /**
     * ExceptionProcessor constructor.
     *
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }
    
    /**
     * {@inheritdoc}
     */
    public function process(\Throwable $exception): \Throwable
    {
        $modules = $this->kernel->getModules();
        foreach ($modules as $module) {
            $exception = $module->processException($exception);
        }
        
        return $exception;
    }
}
