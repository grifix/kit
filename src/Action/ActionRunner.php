<?php
declare(strict_types=1);

namespace Grifix\Kit\Action;

use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class ActionRunner
 * @package Grifix\Kit\Action
 */
class ActionRunner implements ActionRunnerInterface
{
    /**
     * @var ClassMakerInterface
     */
    protected $classMaker;

    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    /**
     * ActionRunner constructor.
     * @param ClassMakerInterface $classMaker
     * @param IocContainerInterface $iocContainer
     */
    public function __construct(ClassMakerInterface $classMaker, IocContainerInterface $iocContainer)
    {
        $this->classMaker = $classMaker;
        $this->iocContainer = $iocContainer;
    }

    /**
     * @param $actionClass
     * @param array $params
     * @return mixed
     * @throws \ReflectionException
     */
    public function runAction($actionClass, array $params = [])
    {
        $actionClass = $this->classMaker->makeClassName($actionClass);
        $action = $this->iocContainer->createNewInstance($actionClass);
        return $action($params);
    }
}
