<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Expression\Exception;

/**
 * Class OperandNotExistsException
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class OperandNotExistsException extends \Exception
{
    protected $index;
    
    /**
     * OperandNotExistsException constructor.
     *
     * @param int $index
     */
    public function __construct(int $index)
    {
        $this->message = 'Operand with index "'.$index.'" does not exists!';
        parent::__construct();
    }
}