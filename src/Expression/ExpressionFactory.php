<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Expression;

use Grifix\Kit\Kernel\AbstractFactory;


/**
 * Class ExpressionFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ExpressionFactory extends AbstractFactory implements ExpressionFactoryInterface
{

    /**
     * {@inheritdoc}
     */
    public function eq($leftOperand, $rightOperand): ExpressionInterface
    {
        return (new EqualExpression($leftOperand, $rightOperand));
    }

    /**
     * {@inheritdoc}
     */
    public function ne($leftOperand, $rightOperand): ExpressionInterface
    {
        return new NotEqualExpression($leftOperand, $rightOperand);

    }

    /**
     * {@inheritdoc}
     */
    public function andX($leftOperand, $rightOperand, ...$operands): ExpressionInterface
    {
        $result = new AndExpression($leftOperand, $rightOperand);
        foreach ($operands as $operand) {
            $result->addOperand($operand);
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function orX($leftOperand, $rightOperand, ...$operands): ExpressionInterface
    {
        $result = new OrExpression($leftOperand, $rightOperand);
        foreach ($operands as $operand) {
            $result->addOperand($operand);
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function in($needle, array $haystack): ExpressionInterface
    {
        return new InExpression($needle, $haystack);
    }

    /**
     * {@inheritdoc}
     */
    public function gt($leftOperand, $rightOperand): ExpressionInterface
    {
        return new GreaterThanExpression($leftOperand, $rightOperand);
    }

    /**
     * {@inheritdoc}
     */
    public function gte($leftOperand, $rightOperand): ExpressionInterface
    {
        return new GreaterThanOrEqualToExpression($leftOperand, $rightOperand);
    }

    /**
     * {@inheritdoc}
     */
    public function lt($leftOperand, $rightOperand): ExpressionInterface
    {
        return new LessThanExpression($leftOperand, $rightOperand);
    }

    /**
     * {@inheritdoc}
     */
    public function lte($leftOperand, $rightOperand): ExpressionInterface
    {
        return new LessThanOrEqualToExpression($leftOperand, $rightOperand);
    }
}