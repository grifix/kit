<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Expression;


/**
 * Class InExpression
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InExpression extends AbstractExpression
{
    /**
     * InExpression constructor.
     *
     * @param       $needle
     * @param array $haystack
     */
    public function __construct($needle, array $haystack)
    {
        $this->operands[0] = $needle;
        $this->operands[1] = $haystack;
    }
    
    /**
     * @return mixed
     */
    public function getNeedle()
    {
        return $this->operands[0];
    }
    
    /**
     * @return mixed
     */
    public function getHaystack(){
        return $this->operands[1];
    }
    
    /**
     * @param mixed $needle
     *
     * @return $this
     */
    public function setNeedle($needle){
        $this->operands[0] = $needle;
        return $this;
    }
    
    /**
     * @param array $haystack
     *
     * @return $this
     */
    public function setHaystack(array $haystack){
        $this->operands[1] = $haystack;
        return $this;
    }
}