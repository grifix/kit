<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Expression\Builder;

use Grifix\Kit\Expression\AndExpression;
use Grifix\Kit\Expression\EqualExpression;
use Grifix\Kit\Expression\GreaterThanExpression;
use Grifix\Kit\Expression\GreaterThanOrEqualToExpression;
use Grifix\Kit\Expression\InExpression;
use Grifix\Kit\Expression\LessThanExpression;
use Grifix\Kit\Expression\LessThanOrEqualToExpression;
use Grifix\Kit\Expression\NotEqualExpression;
use Grifix\Kit\Expression\OrExpression;
use Grifix\Kit\Expression\Parser\Php;
use Grifix\Kit\Expression\Parser\Sql;

/**
 * Class BuilderFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ExpressionBuilderFactoryInterface
{
    /**
     * @return ExpressionBuilderInterface
     */
    public function createBuilder(): ExpressionBuilderInterface;
    
    /**
     * @return ExpressionBuilderInterface
     */
    public function createPhpBuilder(): ExpressionBuilderInterface;
    
    /**
     * @return ExpressionBuilderInterface
     */
    public function createSqlBuilder(): ExpressionBuilderInterface;
}