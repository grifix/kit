<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Expression\Builder;

use Grifix\Kit\Expression\AndExpression;
use Grifix\Kit\Expression\EqualExpression;
use Grifix\Kit\Expression\GreaterThanExpression;
use Grifix\Kit\Expression\GreaterThanOrEqualToExpression;
use Grifix\Kit\Expression\InExpression;
use Grifix\Kit\Expression\LessThanExpression;
use Grifix\Kit\Expression\LessThanOrEqualToExpression;
use Grifix\Kit\Expression\NotEqualExpression;
use Grifix\Kit\Expression\OrExpression;
use Grifix\Kit\Expression\Parser\ParserFactoryInterface;
use Grifix\Kit\Expression\Parser\Php;
use Grifix\Kit\Expression\Parser\Sql;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class BuilderFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ExpressionBuilderFactory extends AbstractFactory implements ExpressionBuilderFactoryInterface
{
    
    protected $parserFactory;
    
    /**
     * BuilderFactory constructor.
     *
     * @param ParserFactoryInterface $parserFactory
     * @param ClassMakerInterface    $classMaker
     */
    public function __construct(ParserFactoryInterface $parserFactory, ClassMakerInterface $classMaker)
    {
        $this->parserFactory = $parserFactory;
        parent::__construct($classMaker);
    }
    
    /**
     * (@inheritdoc)
     */
    public function createBuilder(): ExpressionBuilderInterface
    {
        $class = $this->makeClassName(ExpressionBuilder::class);
        
        return new $class();
    }
    
    /**
     * @return ExpressionBuilderInterface
     */
    public function createPhpBuilder(): ExpressionBuilderInterface
    {
        $builder = $this->createBuilder();
        $builder
            ->setParser(
                AndExpression::class,
                $this->parserFactory->createParser(Php\SimpleParser::class, $builder)
            )
            ->setParser(
                EqualExpression::class,
                $this->parserFactory->createParser(Php\SimpleParser::class, $builder)
            )
            ->setParser(
                OrExpression::class,
                $this->parserFactory->createParser(Php\SimpleParser::class, $builder)
            )
            ->setParser(
                NotEqualExpression::class,
                $this->parserFactory->createParser(Php\SimpleParser::class, $builder)
            )
            ->setParser(
                GreaterThanExpression::class,
                $this->parserFactory->createParser(Php\SimpleParser::class, $builder)
            )
            ->setParser(
                GreaterThanOrEqualToExpression::class,
                $this->parserFactory->createParser(Php\SimpleParser::class, $builder)
            )
            ->setParser(
                LessThanExpression::class,
                $this->parserFactory->createParser(Php\SimpleParser::class, $builder)
            )
            ->setParser(
                LessThanOrEqualToExpression::class,
                $this->parserFactory->createParser(Php\SimpleParser::class, $builder)
            )
            ->setParser(
                InExpression::class,
                $this->parserFactory->createParser(Php\InParser::class, $builder)
            );
        
        return $builder;
    }
    
    /**
     * {@inheritdoc}
     */
    public function createSqlBuilder(): ExpressionBuilderInterface
    {
        $builder = $this->createBuilder();
        $builder
            ->setParser(
                AndExpression::class,
                $this->parserFactory->createParser(Sql\SimpleParser::class, $builder)
            )
            ->setParser(
                EqualExpression::class,
                $this->parserFactory->createParser(Sql\SimpleParser::class, $builder)
            )
            ->setParser(
                OrExpression::class,
                $this->parserFactory->createParser(Sql\SimpleParser::class, $builder)
            )
            ->setParser(
                NotEqualExpression::class,
                $this->parserFactory->createParser(Sql\SimpleParser::class, $builder)
            )
            ->setParser(
                GreaterThanExpression::class,
                $this->parserFactory->createParser(Sql\SimpleParser::class, $builder)
            )
            ->setParser(
                GreaterThanOrEqualToExpression::class,
                $this->parserFactory->createParser(Sql\SimpleParser::class, $builder)
            )
            ->setParser(
                LessThanExpression::class,
                $this->parserFactory->createParser(Sql\SimpleParser::class, $builder)
            )
            ->setParser(
                LessThanOrEqualToExpression::class,
                $this->parserFactory->createParser(Sql\SimpleParser::class, $builder)
            )
            ->setParser(
                InExpression::class,
                $this->parserFactory->createParser(Sql\InParser::class, $builder)
            );
        
        return $builder;
    }
}