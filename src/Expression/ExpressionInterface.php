<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Expression;

use Grifix\Kit\Expression\Exception\OperandNotExistsException;


/**
 * Class AbstractExpression
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ExpressionInterface
{
    
    /**
     * @param mixed|ExpressionInterface $operand
     *
     * @return ExpressionInterface
     */
    public function addOperand($operand): ExpressionInterface;
    
    /**
     * @return array
     */
    public function getOperands();
    
    /**
     * @param array $operands
     *
     * @return ExpressionInterface
     */
    public function setOperands(array $operands): ExpressionInterface;
    
    /**
     * @param $index
     * @param $operand
     *
     * @return ExpressionInterface
     */
    public function setOperand($index, $operand): ExpressionInterface;
    
    /**
     * @param $index
     *
     * @return mixed
     * @throws OperandNotExistsException
     */
    public function getOperand($index);
}