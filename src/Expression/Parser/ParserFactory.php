<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Expression\Parser;


use Grifix\Kit\Expression\Builder\ExpressionBuilderInterface;
use Grifix\Kit\Kernel\AbstractFactory;

/**
 * Class ParserFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression\Parser
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ParserFactory extends AbstractFactory implements ParserFactoryInterface
{
    
    /**
     * {@inheritdoc}
     */
    public function createParser($class, ExpressionBuilderInterface $expressionBuilder)
    {
        $class = $this->makeClassName($class);
        
        return new $class($expressionBuilder);
    }
}