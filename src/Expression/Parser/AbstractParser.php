<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Expression\Parser;


use Grifix\Kit\Expression\Builder\ExpressionBuilderInterface;
use Grifix\Kit\Expression\EqualExpression;
use Grifix\Kit\Expression\Exception\UnknownExpressionException;
use Grifix\Kit\Expression\ExpressionInterface;
use Grifix\Kit\Expression\NotEqualExpression;

/**
 * Class AbstractParser
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression\AbstractParser
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractParser implements ParserInterface
{
    protected $builder;
    protected $operatorsMap = [];
    
    /**
     * FunctionParser constructor.
     *
     * @param \Grifix\Kit\Expression\Builder\ExpressionBuilderInterface $builder
     */
    public function __construct(ExpressionBuilderInterface $builder)
    {
        $this->builder = $builder;
    }
    
    /**
     * @param ExpressionInterface $expression
     *
     * @return array
     */
    protected function prepareOperands(ExpressionInterface $expression)
    {
        $result = [];
        foreach ($expression->getOperands() as $operand) {
            $result[] = $this->prepareOperand($operand);
        }
        return $result;
    }
    
    /**
     * @param mixed $operand
     *
     * @return string
     */
    protected function prepareOperand($operand){
        if ($operand instanceof ExpressionInterface) {
            $operand = '('.$this->builder->buildExpression($operand).')';
        }
        return $operand;
    }
    
    /**
     * @param ExpressionInterface $expression
     *
     * @return mixed
     * @throws UnknownExpressionException
     */
    protected function makeOperator(ExpressionInterface $expression){
        $class = get_class($expression);
        if(!isset($this->operatorsMap[$class])){
            throw new UnknownExpressionException($expression);
        }
        return $this->operatorsMap[$class];
    }
    
    /**
     * @param ExpressionInterface $expression
     *
     * @return string
     */
    public function parse(ExpressionInterface $expression)
    {
        $operands = $this->prepareOperands($expression);
        return implode(' '.$this->makeOperator($expression).' ', $operands);
    }
}