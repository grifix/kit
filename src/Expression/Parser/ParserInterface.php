<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Expression\Parser;

use Grifix\Kit\Expression\ExpressionInterface;


/**
 * Class FunctionParser
 *
 * @category Grifix
 * @package  Grifix\Kit\Expression\AbstractParser
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ParserInterface
{
    /**
     * @param ExpressionInterface $expression
     *
     * @return string
     */
    public function parse(ExpressionInterface $expression);
}