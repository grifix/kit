<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Migration\Exception;




/**
 * Class InvalidMigrationName
 *
 * @category Grifix
 * @package  Grifix\Kit\Migration\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InvalidMigrationName extends \Exception
{
    protected $name;
    
    /**
     * InvalidMigrationName constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
        $this->message = 'Invalid migration name "' . $name . '"';
        parent::__construct();
    }
}