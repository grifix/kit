<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Migration;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Helper\FilesystemHelperInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Kernel\Module\ModuleInterface;
use Grifix\Kit\Migration\Event\MigrateDownEvent;
use Grifix\Kit\Migration\Event\MigrateUpEvent;

/**
 * Class MigrationRunner
 *
 * @category Grifix
 * @package  Grifix\Kit\Migration
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MigrationRunner implements MigrationRunnerInterface
{

    const MIGRATION_TABLE = 'grifix_kit.migration';

    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * @var ConnectionInterface
     */
    protected $connection;

    /**
     * @var MigrationFactoryInterface
     */
    protected $migrationFactory;

    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;

    /**
     * @var EventBusInterface
     */
    protected $eventBus;

    /**
     * MigrationRunner constructor.
     *
     * @param KernelInterface $kernel
     * @param ConnectionInterface $connection
     * @param MigrationFactoryInterface $migrationFactory
     * @param FilesystemHelperInterface $filesystemHelper
     * @param EventBusInterface $eventBus
     */
    public function __construct(
        KernelInterface $kernel,
        ConnectionInterface $connection,
        MigrationFactoryInterface $migrationFactory,
        FilesystemHelperInterface $filesystemHelper,
        EventBusInterface $eventBus
    ) {
        $this->kernel = $kernel;
        $this->migrationFactory = $migrationFactory;
        $this->filesystemHelper = $filesystemHelper;
        $this->connection = $connection;
        $this->eventBus = $eventBus;
    }

    /**
     * {@inheritdoc}
     */
    public function up(string $migrationName = null): void
    {
        $this->createMigrationsTable();
        if ($migrationName) {
            $this->upMigration($this->migrationFactory->createMigration($migrationName));
        } else {
            foreach ($this->kernel->getModules() as $module) {
                foreach ($this->getModuleMigrations($module) as $migration) {
                    $this->upMigration($migration);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function down(string $migrationName = null): void
    {
        $this->createMigrationsTable();
        if ($migrationName) {
            $this->downMigration($this->migrationFactory->createMigration($migrationName));
        } else {
            $migrations = $this->connection->createQuery()
                ->select('*')
                ->from(self::MIGRATION_TABLE)
                ->orderBy('"created_at" DESC, "name" DESC')
                ->fetchAll();
            foreach ($migrations as $migrationData) {
                $this->downMigration($this->migrationFactory->createMigrationFromClassName($migrationData['name']));
            }
        }
    }

    /**
     * @param MigrationInterface $migration
     *
     * @return void
     */
    protected function upMigration(MigrationInterface $migration): void
    {
        if (!$this->getMigration($migration)) {
            $this->connection->beginTransaction();
            $migration->up();
            $this->connection->createQuery()
                ->insert([
                    'name' => get_class($migration),
                    'created_at' => (new \DateTime())->format('Y-m-d H:i:s.u'),
                ])
                ->into(self::MIGRATION_TABLE)->execute();
            $this->connection->commitTransaction();
            $this->eventBus->trigger(new MigrateUpEvent(get_class($migration)));
        }

    }

    /**
     * @param MigrationInterface $migration
     *
     * @return void
     */
    protected function downMigration(MigrationInterface $migration): void
    {
        if ($this->getMigration($migration)) {
            $this->connection->beginTransaction();
            $migration->down();
            $this->connection->createQuery()
                ->delete()
                ->from(self::MIGRATION_TABLE)
                ->where('"name" = :name')
                ->bindValue('name', get_class($migration))
                ->execute();
            $this->connection->commitTransaction();
            $this->eventBus->trigger(new MigrateDownEvent(get_class($migration)));
        }
    }

    /**
     * @param $migration
     *
     * @return array
     *
     * @throws \Throwable
     */
    protected function getMigration(MigrationInterface $migration)
    {
        return $this->connection->createQuery()
            ->select('*')
            ->from(self::MIGRATION_TABLE)
            ->where('"name" = :name')
            ->bindValue('name', get_class($migration))->fetch();
    }

    /**
     * @param ModuleInterface $module
     *
     * @return array
     *
     * @throws \Throwable
     */
    protected function getModuleMigrations(ModuleInterface $module): array
    {
        $dir = $module->getVendorDir() . '/src/Infrastructure/Migration';
        $files = [];
        $result = [];
        if ($this->filesystemHelper->isDir($dir)) {
            $files = $this->filesystemHelper->scanDir($dir);
        }
        $dir = $module->getAppDir() . '/src/Infrastructure/Migration';
        if ($this->filesystemHelper->isDir($dir)) {
            $files = array_merge($files, $this->filesystemHelper->scanDir($dir));
        }

        foreach ($files as $file) {
            if ($file['extension'] == 'php') {
                $result[] = $this->migrationFactory->createMigration($module->getVendor() . '.' . $module->getName() . '.' . $file['filename']);
            }
        }

        return $result;
    }

    protected function createMigrationsTable()
    {
        $this->connection->execute('
            CREATE SCHEMA IF NOT EXISTS "grifix_kit"; 
            CREATE TABLE IF NOT EXISTS grifix_kit.migration (
              "name" VARCHAR NOT NULL,
              "created_at" TIMESTAMP(20) WITHOUT TIME ZONE NOT NULL,
              PRIMARY KEY("name")
            )
            WITH (oids = false);
        ');
    }
}