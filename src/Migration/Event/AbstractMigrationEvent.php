<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Migration\Event;

use Grifix\Kit\Migration\MigrationInterface;

/**
 * Class AbstractMigrationEvent
 *
 * @category Grifix
 * @package  Grifix\Kit\Migration
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class AbstractMigrationEvent
{
    /**
     * @var MigrationInterface
     */
    protected $migrationClass;

    /**
     * AbstractMigrationEvent constructor.
     * @param string $migrationClass
     */
    public function __construct(string $migrationClass)
    {
        $this->migrationClass = $migrationClass;
    }

    /**
     * @return string
     */
    public function getMigrationClass(): string
    {
        return $this->migrationClass;
    }
}