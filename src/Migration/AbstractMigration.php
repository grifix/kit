<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Migration;


use Grifix\Kit\Db\ConnectionInterface;

/**
 * Class AbstractMigration
 *
 * @category Grifix
 * @package  Grifix\Kit\Migration
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractMigration implements MigrationInterface
{
    protected $connection;

    /**
     * AbstractMigration constructor.
     *
     * @param ConnectionInterface $connection
     */
    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $sql
     * @param array $bindings
     */
    protected function execute(string $sql, array $bindings = []): void
    {
        $this->connection->execute($sql, $bindings);
    }
}
