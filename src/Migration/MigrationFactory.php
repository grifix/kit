<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Migration;


use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Migration\Exception\InvalidMigrationName;

/**
 * Class MigrationFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Migration
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MigrationFactory extends AbstractFactory implements MigrationFactoryInterface
{
    
    /**
     * @var ConnectionInterface
     */
    protected $connection;
    
    /**
     * MigrationFactory constructor.
     *
     * @param ClassMakerInterface $classMaker
     * @param ConnectionInterface $connection
     */
    public function __construct(ClassMakerInterface $classMaker, ConnectionInterface $connection)
    {
        $this->connection = $connection;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createMigration(string $name): MigrationInterface
    {
        $arr = explode('.', $name);
        if (count($arr) != 3) {
            throw new InvalidMigrationName($name);
        }
        $className = $this->makeClassName(
            '\\' . ucfirst(basename($arr[0])) . '\\'
            . ucfirst(basename($arr[1]))
            . '\\Infrastructure\\Migration\\'
            . ucfirst(basename($arr[2]))
        );
        
        return new $className($this->connection);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createMigrationFromClassName(string $className): MigrationInterface
    {
        return new $className($this->connection);
    }
}