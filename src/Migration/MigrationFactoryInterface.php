<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Migration;

use Grifix\Kit\Migration\Exception\InvalidMigrationName;


/**
 * Class MigrationFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Migration
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface MigrationFactoryInterface
{
    /**
     * @param string $name
     *
     * @return MigrationInterface
     * @throws InvalidMigrationName
     */
    public function createMigration(string $name):MigrationInterface;
    
    /**
     * @param $className
     *
     * @return MigrationInterface
     */
    public function createMigrationFromClassName(string $className): MigrationInterface;
}