<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Migration;


/**
 * Class MigrationRunner
 *
 * @category Grifix
 * @package  Grifix\Kit\Migration
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface MigrationRunnerInterface
{
    /**
     * @param string|null $migrationName
     *
     * @return void
     */
    public function up(string $migrationName = null): void;
    
    /**
     * @param string|null $migrationName
     *
     * @return void
     */
    public function down(string $migrationName = null): void;
}