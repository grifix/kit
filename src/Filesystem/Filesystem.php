<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Filesystem;

/**
 * Class Filesystem
 *
 * @category Grifix
 * @package  Grifix\Kit\Filesystem
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Filesystem extends \League\Flysystem\Filesystem implements FilesystemInterface
{
    /**
     * @param       $path
     * @param       $contents
     * @param array $config
     *
     * @return bool
     */
    public function overwrite($path, $contents, array $config = []): bool
    {
        if ($this->has($path)) {
            $this->delete($path);
        }
        
        return $this->write($path, $contents, $config);
    }
}
