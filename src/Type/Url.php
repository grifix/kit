<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Type;

use Grifix\Kit\Type\Exception\InvalidUrlException;

/**
 * Class Url
 *
 * @category Grifix
 * @package  Grifix\Kit\Type
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Url extends AbstractValuableType implements UrlInterface
{
    /**
     * Url constructor.
     *
     * @param string $value
     *
     * @throws InvalidUrlException
     */
    public function __construct(string $value)
    {
        if (filter_var($value, FILTER_VALIDATE_URL) === false) {
            throw new InvalidUrlException();
        }

        parent::__construct($value);
    }
}
