<?php
declare(strict_types = 1);

namespace Grifix\Kit\Type;

/**
 * Class Email
 *
 * @category Grifix
 * @package  Grifix\Kit\Type
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface EmailInterface extends ValuableTypeInterface
{
}
