<?php
declare(strict_types=1);

namespace Grifix\Kit\Type;


/**
 * Class TypeFactory
 * @package Grifix\Kit\Type
 */
interface TypeFactoryInterface
{
    /**
     * @param string $email
     * @return EmailInterface
     */
    public function createEmail(string $email): EmailInterface;

    /**
     * @param string $ip
     * @return IpAddressInterface
     */
    public function createIpAddress(string $ip): IpAddressInterface;

    /**
     * @param int $from
     * @param int $to
     * @return RangeInterface
     */
    public function createRange(int $from, int $to): RangeInterface;

    /**
     * @param string $url
     * @return UrlInterface
     */
    public function createUrl(string $url): UrlInterface;
}