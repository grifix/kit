<?php
declare(strict_types=1);

namespace Grifix\Kit\Type;

/**
 * Interface SingleValueTypeInterface
 * @package Grifix\Kit\Type\Exception
 */
interface ValuableTypeInterface
{
    public function getValue();

    public function __toString();
}
