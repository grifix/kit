<?php
declare(strict_types=1);

namespace Grifix\Kit\Type;

use Grifix\Kit\Kernel\AbstractFactory;

/**
 * Class TypeFactory
 * @package Grifix\Kit\Type
 */
class TypeFactory extends AbstractFactory implements TypeFactoryInterface
{

    /**
     * {@inheritdoc}
     */
    public function createEmail(string $email): EmailInterface
    {
        $class = $this->makeClassName(Email::class);
        return new $class($email);
    }

    /**
     * {@inheritdoc}
     */
    public function createIpAddress(string $ip): IpAddressInterface
    {
        $class = $this->makeClassName(IpAddress::class);
        return new $class($ip);
    }

    /**
     * {@inheritdoc}
     */
    public function createRange(int $from, int $to): RangeInterface
    {
        $class = $this->makeClassName(Range::class);
        return new $class($from, $to);
    }

    /**
     * {@inheritdoc}
     */
    public function createUrl(string $url): UrlInterface
    {
        $class = $this->makeClassName(UrlInterface::class);
        return new $class($url);
    }
}
