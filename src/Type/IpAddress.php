<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Type;

use Grifix\Kit\Type\Exception\InvalidIpAddressException;

/**
 * Class IpAddress
 *
 * @category Grifix
 * @package  Grifix\Kit\Type
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class IpAddress extends AbstractValuableType implements IpAddressInterface
{
    /**
     * IpAddress constructor.
     *
     * @param string $value
     *
     * @throws InvalidIpAddressException
     */
    public function __construct(string $value)
    {
        if (!is_null($value) && filter_var($value, FILTER_VALIDATE_IP)===false) {
            throw new InvalidIpAddressException();
        }
        parent::__construct($value);
    }
}
