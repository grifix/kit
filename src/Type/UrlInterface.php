<?php
declare(strict_types = 1);

namespace Grifix\Kit\Type;

/**
 * Class Url
 *
 * @category Grifix
 * @package  Grifix\Kit\Type
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface UrlInterface extends ValuableTypeInterface
{
}