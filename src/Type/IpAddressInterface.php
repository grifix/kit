<?php

namespace Grifix\Kit\Type;

/**
 * Class IpAddress
 *
 * @category Grifix
 * @package  Grifix\Kit\Type
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface IpAddressInterface extends ValuableTypeInterface
{
}