<?php

declare(strict_types = 1);

namespace Grifix\Kit\Type;

/**
 * Class Range
 * @package Grifix\Kit\Type
 */
class Range implements RangeInterface
{
    /**
     * @var int
     */
    protected $from;

    /**
     * @var int
     */
    protected $to;

    /**
     * Range constructor.
     * @param int $from
     * @param int $to
     */
    public function __construct(int $from, int $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

    /**
     * @return int
     */
    public function getFrom(): int
    {
        return $this->from;
    }

    /**
     * @return int
     */
    public function getTo(): int
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->from.'-'.$this->to;
    }
}
