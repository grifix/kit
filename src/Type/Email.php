<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Type;

use Grifix\Kit\Type\Exception\InvalidEmailException;

/**
 * Class Email
 *
 * @category Grifix
 * @package  Grifix\Kit\Type
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Email extends AbstractValuableType implements EmailInterface
{
    /**
     * Email constructor.
     *
     * @param string $value
     *
     * @throws InvalidEmailException
     */
    public function __construct(string $value)
    {
        if (filter_var($value, FILTER_VALIDATE_EMAIL)===false) {
            throw new InvalidEmailException();
        }
        parent::__construct($value);
    }
}
