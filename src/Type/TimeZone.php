<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Type;

use Grifix\Kit\Type\Exception\InvalidTimeZoneException;

/**
 * Class TimeZone
 *
 * @category Grifix
 * @package  Grifix\Kit\Type
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class TimeZone extends AbstractValuableType implements TimeZoneInterface
{
    /**
     * TimeZone constructor.
     *
     * @param string $value
     *
     * @throws InvalidTimeZoneException
     */
    public function __construct(string $value)
    {
        if (!in_array($value, timezone_identifiers_list())) {
            throw new InvalidTimeZoneException();
        }
        parent::__construct($value);
    }
}