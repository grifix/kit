<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Kernel\Exception;

/**
 * Class ModuleNotExistsException
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ModuleNotExistsException extends \Exception
{
    protected $vendor;
    
    protected $name;
    
    /**
     * ModuleNotExistsException constructor.
     *
     * @param string $vendor
     * @param string $name
     */
    public function __construct(string $vendor, string $name)
    {
        $this->vendor = $vendor;
        $this->name = $name;
        $this->message = 'Module "'.$vendor.'.'.$name.'" is not exists!';
        
        parent::__construct();
    }
}