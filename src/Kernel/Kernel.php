<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Kernel;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Helper\FilesystemHelperInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\Exception\ModuleNotExistsException;
use Grifix\Kit\Kernel\Module\Module;
use Grifix\Kit\Kernel\Module\ModuleInterface;
use Psr\SimpleCache\CacheInterface;
use League\Flysystem\NotSupportedException;

/**
 * Class Kernel
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Kernel implements KernelInterface
{

    /**
     * @var string
     */
    protected $rootDir;

    /**
     * @var array of application modules
     */
    protected $modules;

    /**
     * @var IocContainerInterface
     */
    protected $ioc;

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @var FilesystemHelperInterface
     */
    protected $filesystemHelper;

    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;


    /**
     * Kernel constructor.
     *
     * @param string $rootDir
     * @param CacheInterface $cache
     * @param FilesystemHelperInterface $filesystemHelper
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(
        string $rootDir,
        CacheInterface $cache,
        FilesystemHelperInterface $filesystemHelper,
        ArrayHelperInterface $arrayHelper
    ) {
        $this->rootDir = $rootDir;
        $this->cache = $cache;
        $this->filesystemHelper = $filesystemHelper;
        $this->arrayHelper = $arrayHelper;
    }

    /**
     * {@inheritdoc}
     *
     * @return ModuleInterface []
     */
    public function getModules(bool $force = false): array
    {
        if (is_null($this->modules) || $force) {
            $this->modules = $this->defineModules();
        }

        return $this->modules;
    }

    /**
     * {@inheritdoc}
     */
    public function getIoc(): IocContainerInterface
    {
        return $this->ioc;
    }

    /**
     * {@inheritdoc}
     */
    public function setIoc(IocContainerInterface $ioc): void
    {
        $this->ioc = $ioc;
    }

    /**
     * {@inheritdoc}
     */
    public function getRootDir(): string
    {
        return $this->rootDir;
    }

    /**
     * {@inheritdoc}
     */
    public function getAssetsDir(): string
    {
        return $this->rootDir . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'assets';
    }


    /**
     * @param string $vendor
     * @param string $name
     *
     * @return ModuleInterface
     * @throws ModuleNotExistsException
     */
    public function getModule(string $vendor, string $name): ModuleInterface
    {
        foreach ($this->getModules() as $module) {
            if ($module->getVendor() == $vendor && $module->getName() == $name) {
                return $module;
            }
        }

        throw new ModuleNotExistsException($vendor, $name);
    }

    /**
     * {@inheritdoc}
     */
    public function hasModule(string $vendor, string $name): bool
    {
        try {
            $this->getModule($vendor, $name);

            return true;
        } catch (ModuleNotExistsException $e) {
            return false;
        }
    }

    /**
     * @return void
     */
    public function bootstrapModules(): void
    {
        foreach ($this->getModules() as $module) {
            $module->bootstrap();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function installModules(bool $publishAssets = true): void
    {
        foreach ($this->getModules() as $module) {
            $module->install($publishAssets);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function updateModules(bool $publishAssets = true): void
    {
        foreach ($this->getModules() as $module) {
            $module->update($publishAssets);
        }
    }

    /**
     * @param $path
     *
     * @return array
     */
    protected function scanModules($path): array
    {
        $result = [];
        $vendors = $this->filesystemHelper->scanDir($path);
        foreach ($vendors as $vendor) {
            if ($vendor['type'] == FilesystemHelperInterface::TYPE_DIR) {
                $modules = $this->filesystemHelper->scanDir($vendor['path']);
                foreach ($modules as $module) {
                    if ($this->filesystemHelper->fileExists($module['path'] . '/composer.json')) {
                        $json = json_decode(
                            $this->filesystemHelper->readFromFile($module['path'] . '/composer.json'),
                            true
                        );
                        if (isset($json['keywords'])
                            && is_array($json['keywords'])
                            && in_array('grifix', $json['keywords'])
                            && in_array('module', $json['keywords'])
                        ) {
                            $result[$json['name']] = $json;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $module
     * @param $raw
     *
     * @return bool
     */
    protected function mergeModuleRequire(&$module, $raw)
    {
        if ($this->arrayHelper->get($module, 'merged')) {
            return false;
        }
        if ($this->arrayHelper->get($module, 'require')) {
            foreach ($module['require'] as $name => $version) {
                if ($this->arrayHelper->get($raw, $name) && $this->arrayHelper->get($raw[$name], 'require')) {
                    $this->mergeModuleRequire($raw[$name], $raw);
                    $module['require'] = array_merge($module['require'], $raw[$name]['require']);
                }
            }
        }
        $module['merged'] = true;

        return true;
    }

    /**
     * @param array $a
     * @param array $b
     *
     * @return int
     */
    protected function sortModules(array $a, array $b)
    {
        if (in_array($a['name'], array_keys($this->arrayHelper->get($b, 'require', [])))) {
            return -1;
        }

        if (in_array($b['name'], array_keys($this->arrayHelper->get($a, 'require', [])))) {
            return 1;
        }

        return 0;
    }

    /**
     * {@inheritdoc}
     */
    public function getVendorDir(): string
    {
        return $this->rootDir . DIRECTORY_SEPARATOR . 'vendor';
    }

    /**
     * {@inheritdoc}
     */
    public function getAppDir(): string
    {
        return $this->rootDir . DIRECTORY_SEPARATOR . 'app';
    }

    /**
     * @return array
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    protected function defineModules(): array
    {
        $cacheKey = $this->makeCacheKey() . '.modules';
        $raw = $this->cache->get($cacheKey);
        if (is_null($raw)) {
            $raw = $this->arrayHelper->merge(
                $this->scanModules($this->getVendorDir()),
                $this->scanModules($this->getAppDir())
            );

            foreach ($raw as &$mod) {
                $this->mergeModuleRequire($mod, $raw);
            }

            uasort($raw, [$this, "sortModules"]);
            $this->cache->set($cacheKey, $raw);
        }

        return $this->makeModules($raw);
    }

    /**
     * @param array $raw
     *
     * @return array
     */
    protected function makeModules(array $raw)
    {
        $result = [];
        $priority = 1;
        foreach ($raw as $mod) {
            $arr = explode('/', $mod['name']);
            $result[] = new Module($arr[0], $arr[1], $priority, $this, $this->filesystemHelper, $this->arrayHelper);
            $priority++;
        }

        return $result;
    }

    /**
     * @return string
     */
    protected function makeCacheKey(): string
    {
        return str_replace('\\', '.', get_class($this));
    }
}
