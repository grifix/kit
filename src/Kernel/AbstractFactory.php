<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Kernel;

use Grifix\Acl\Domain\User\Password\Password;
use Grifix\Acl\Domain\User\Password\PasswordFactory;
use Grifix\Acl\Domain\User\Password\PasswordInterface;
use Grifix\Kit\EntryPoint\Stage\InitCacheStage;
use Grifix\Kit\EntryPoint\Stage\InitIocStage;
use Grifix\Kit\EntryPoint\Stage\StageFactory;
use Grifix\Kit\EntryPoint\Stage\StageInterface;

/**
 * Class AbstractFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class AbstractFactory
{
    protected $classMaker;
    
    /**
     * AbstractFactory constructor.
     *
     * @param ClassMakerInterface $classMaker
     */
    public function __construct(ClassMakerInterface $classMaker)
    {
        $this->classMaker = $classMaker;
    }

    /**
     * @param string $className
     *
     * @return string
     */
    protected function makeClassName(string $className): string
    {
        return $this->classMaker->makeClassName($className);
    }



    /**
     * @param string $childClass
     * @param string $parentClass
     *
     * @return bool
     */
    protected function isSubClassOf(string $childClass, string $parentClass): bool
    {
        return $this->classMaker->isSubClassOf($childClass, $parentClass);
    }
    
    /**
     * @param string $childClass
     * @param string $parentClass
     *
     * @return void
     */
    protected function isSubClassOrFail(string $childClass, string $parentClass)
    {
        $this->classMaker->isSubClassOrFail($childClass, $parentClass);
    }
}