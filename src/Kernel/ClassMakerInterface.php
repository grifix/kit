<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Kernel;

use Grifix\Kit\Kernel\Exception\InvalidSubclassException;


/**
 * Class ClassMaker
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ClassMakerInterface
{
    /**
     * @param string $className
     *
     * @return string
     */
    public function makeClassName(string $className): string;
    
    /**
     * @param $childClass
     * @param $parentClass
     *
     * @return bool
     */
    public function isSubClassOf(string $childClass, string $parentClass): bool;
    
    /**
     * @param string $childClass
     * @param string $parentClass
     *
     * @return void
     * @throws InvalidSubclassException
     */
    public function isSubClassOrFail(string $childClass, string $parentClass);
}