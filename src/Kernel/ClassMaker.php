<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Kernel;

use Grifix\Kit\Kernel\Exception\InvalidSubclassException;

/**
 * Class ClassMaker
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ClassMaker implements ClassMakerInterface
{

    /**
     * @var array
     */
    protected $classMap = [];

    /**
     * ClassMaker constructor.
     *
     * @param array $classMap <cfg:grifix.kit.classMap>
     */
    public function __construct(array $classMap = [])
    {
        $this->classMap = $classMap;
    }

    /**
     * @param string $className
     *
     * @return string
     */
    public function makeClassName(string $className): string
    {
        if (strpos($className, '\\') !== 0) {
            $className = '\\' . $className;
        }

        if (isset($this->classMap[$className])) {
            return $this->classMap[$className];
        }

        if (strpos($className, '\\App') === 0) {
            $appClassName = $className;
        } else {
            $appClassName = '\\App' . $className;
        }

        if (class_exists($appClassName)) {
            return $appClassName;
        }

        return $className;
    }

    /**
     * {@inheritdoc}
     */
    public function isSubClassOf(string $childClass, string $parentClass): bool
    {
        return ($childClass == $parentClass)
            || ($childClass == '\\' . $parentClass)
            || ('\\' . $childClass == $parentClass)
            || is_subclass_of($childClass, $parentClass);
    }

    /**
     * {@inheritdoc}
     */
    public function isSubClassOrFail(string $childClass, string $parentClass)
    {
        if (!$this->isSubClassOf($childClass, $parentClass)) {
            throw new InvalidSubclassException($childClass, $parentClass);
        }
    }
}