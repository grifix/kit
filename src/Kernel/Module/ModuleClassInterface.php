<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Kernel\Module;

use Grifix\Kit\Ioc\IocContainerInterface;

/**
 * Class ModuleClassInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel\Module
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ModuleClassInterface
{
    /**
     * @return ModuleInterface
     */
    public function getModule();
    
    /**
     * @return IocContainerInterface
     */
    public function getIoc();
    
    /**
     * @param string $alias
     *
     * @return mixed
     */
    public function getShared(string $alias);
}
