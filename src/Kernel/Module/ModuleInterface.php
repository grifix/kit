<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Kernel\Module;

use Grifix\Kit\Exception\ExceptionProcessorInterface;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Kernel\Module\Exception\InvalidExceptionProcessorException;
use Grifix\Kit\Middleware\MiddlewareInterface;

/**
 * Class Module
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ModuleInterface
{
    /**
     * @return KernelInterface
     */
    public function getKernel();
    
    /**
     * @return string
     */
    public function getNamespace(): string;
    
    /**
     * @return string
     */
    public function getName(): string;
    
    /**
     * @return string
     */
    public function getVendor(): string;
    
    /**
     * @return int
     */
    public function getPriority(): int;
    
    /**
     * @return void
     */
    public function bootstrap(): void;

    /**
     * @param bool $publishAssets
     */
    public function install(bool $publishAssets = true): void;
    
    /**
     * @param bool $publishAssets
     */
    public function update(bool $publishAssets = true): void;
    
    /**
     * @return string
     */
    public function getVendorDir(): string;
    
    /**
     * @return string
     */
    public function getAppDir(): string;
    
    /**
     * @return bool
     */
    public function isVendorModule(): bool;
    
    /**
     * @return bool
     */
    public function isAppModule(): bool;
    
    /**
     * @return array
     */
    public function getConfig();
    
    /**
     * @return MiddlewareInterface|null
     */
    public function makeMiddleware();
    
    /**
     * @return \Grifix\Kit\Ioc\IocContainerInterface
     */
    public function getIoc();
    
    /**
     * @param \Throwable $exception
     *
     * @return \Throwable
     */
    public function processException(\Throwable $exception): \Throwable;
    
    /**
     * @return string
     */
    public function getAssetsDir(): string;
    
    /**
     * @return string
     */
    public function getVendorPublicDir(): string;
    
    /**
     * @return string
     */
    public function getAppPublicDir(): string;
    
}