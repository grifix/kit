<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Kernel\Module\Exception;

use Grifix\Kit\Exception\ExceptionPresenterInterface;
use Grifix\Kit\Exception\ExceptionProcessorInterface;

/**
 * Class InvalidExceptionProcessorException
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel\Module\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InvalidExceptionProcessorException extends KernelException
{
    protected $processor;
    
    /**
     * InvalidExceptionProcessorException constructor.
     *
     * @param mixed $processor
     */
    public function __construct($processor)
    {
        $this->processor = $processor;
        $this->message = 'Exception processor '
            .get_class($this->processor).' not implements '.ExceptionProcessorInterface::class.'!';
        parent::__construct();
    }
}
