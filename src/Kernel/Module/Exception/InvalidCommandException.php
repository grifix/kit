<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Kernel\Module\Exception;

use Grifix\Kit\Kernel\Module\ModuleClassInterface;

/**
 * Class InvalidCommandException
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel\Module\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InvalidCommandException extends KernelException
{
    protected $command;
    
    /**
     * InvalidCommandException constructor.
     *
     * @param object $command
     */
    public function __construct(object $command)
    {
        $this->message = 'Command ' . get_class($command) . ' not implements "' . ModuleClassInterface::class . '"';
        parent::__construct();
    }
}
