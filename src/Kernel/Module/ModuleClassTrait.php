<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Kernel\Module;

use Grifix\Kit\Ioc\Definition;
use Grifix\Kit\Ioc\IocContainerInterface;

/**
 * Class ModuleClassTrait
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel\Module
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
trait ModuleClassTrait
{
    protected $module;

    /**
     * AbstractBootstrap constructor.
     *
     * @param ModuleInterface $module
     */
    public function __construct(ModuleInterface $module)
    {
        $this->module = $module;
    }

    /**
     * @return ModuleInterface
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @return IocContainerInterface
     */
    public function getIoc()
    {
        return $this->module->getIoc();
    }

    /**
     * @param string $alias
     *
     * @return mixed
     */
    public function getShared(string $alias)
    {
        return $this->getIoc()->get($alias);
    }

    /**
     * @param $alias
     * @param $value
     *
     * @return void
     */
    public function setShared($alias, $value): void
    {
        $this->getIoc()->set($alias, $value);
    }
}
