<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Kernel;

use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\Exception\ModuleNotExistsException;
use Grifix\Kit\Kernel\Module\ModuleInterface;

/**
 * Class Kernel
 *
 * @category Grifix
 * @package  Grifix\Kit\Kernel
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface KernelInterface
{
    
    /**
     * @param bool $force
     *
     * @return ModuleInterface []
     */
    public function getModules(bool $force = false): array;
    
    /**
     * @return IocContainerInterface
     */
    public function getIoc(): IocContainerInterface;
    
    /**
     * @return string
     */
    public function getRootDir(): string;
    
    /**
     * @return string
     */
    public function getVendorDir(): string ;
    
    /**
     * @return string
     */
    public function getAppDir(): string ;
    
    /**
     * @param IocContainerInterface $ioc
     *
     * @return void
     */
    public function setIoc(IocContainerInterface $ioc): void;
    
    /**
     * @param string $vendor
     * @param string $name
     *
     * @return ModuleInterface
     * @throws ModuleNotExistsException
     */
    public function getModule(string $vendor, string $name): ModuleInterface;
    
    /**
     * @return string
     */
    public function getAssetsDir(): string;
    
    /**
     * @param string $vendor
     * @param string $name
     *
     * @return bool
     */
    public function hasModule(string $vendor, string $name): bool;

    /**
     * @return void
     */
    public function bootstrapModules(): void;

    /**
     * @param bool $publishAssets
     */
    public function installModules(bool $publishAssets = true): void;

    /**
     * @param bool $publishAssets
     */
    public function updateModules(bool $publishAssets = true): void;
}
