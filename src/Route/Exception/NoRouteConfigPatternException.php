<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Route\Exception;

/**
 * Class NoRouteConfigNameException
 *
 * @category Grifix
 * @package  Grifix\Kit\Route\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class NoRouteConfigPatternException extends RouteException
{
    protected $message = 'Route config must have a pattern!';
    protected $config;
    
    /**
     * NoRouteConfigPatternException constructor.
     *
     * @param array $config
     * @param null  $previous
     */
    public function __construct(array $config, $previous = null)
    {
        $this->config = $config;
        parent::__construct($previous);
    }
}
