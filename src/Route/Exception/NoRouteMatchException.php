<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Route\Exception;

use Exception;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;

/**
 * Class NoRouteMatchException
 *
 * @category Grifix
 * @package  Grifix\Kit\Route\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class NoRouteMatchException extends RouteException
{
    protected $request;
    protected $response;
    protected $message = 'No route match!';
    
    /**
     * NoRouteMatchException constructor.
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     * @param Exception|null         $previous
     */
    public function __construct(
        ServerRequestInterface $request,
        ResponseInterface $response,
        Exception $previous = null
    ) {
        $this->request = $request;
        $this->response = $response;
        parent::__construct($this->message, 0, $previous);
    }
}
