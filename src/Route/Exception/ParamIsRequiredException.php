<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Route\Exception;


/**
 * Class ParamIsRequiredException
 *
 * @category Grifix
 * @package  Grifix\Kit\Route\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ParamIsRequiredException extends \Exception
{
    protected $paramName;
    
    protected $pattern;
    
    /**
     * ParamIsRequiredException constructor.
     *
     * @param string $paramName
     * @param string $pattern
     */
    public function __construct(string $paramName, string $pattern)
    {
        $this->paramName = $paramName;
        $this->pattern = $pattern;
        
        $this->message = 'Parameter "' . $paramName . '" is required for pattern "' . $pattern . '"!';
        
        parent::__construct();
    }
}
