<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Route\Exception;

use Exception;
use Grifix\Kit\Route\RouteInterface;

/**
 * Class NoRouteConfigNameException
 *
 * @category Grifix
 * @package  Grifix\Kit\Route\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class NoRouteHandlerException extends RouteException
{
    protected $route;
    protected $code = 404;
    
    /**
     * NoRouteHandlerException constructor.
     *
     * @param RouteInterface $route
     */
    public function __construct(RouteInterface $route)
    {
        $this->route = $route;
        $this->message = 'Route handler was not defined for route "'.$route->getPattern().'" !';
        parent::__construct();
    }
    
}
