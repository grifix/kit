<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Route\Exception;

use Exception;
use Grifix\Kit\Route\RouteInterface;

/**
 * Class MatcherNotExistsException
 *
 * @category Grifix
 * @package  Grifix\Kit\Route\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MatcherNotExistsException extends RouteException
{
    protected $route;
    
    protected $matherType;
    
    /**
     * MatcherNotExistsException constructor.
     *
     * @param RouteInterface $route
     * @param                $matherType
     * @param Exception|null $previous
     */
    public function __construct(RouteInterface $route, $matherType, Exception $previous = null)
    {
        $this->route = $route;
        $this->matherType = $matherType;
        $this->message = 'Mather "' . $matherType . '" not exists!';
        parent::__construct($previous);
    }
}
