<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Route\Exception;

use Exception;

/**
 * Class RouteNotExistsException
 *
 * @category Grifix
 * @package  Grifix\Kit\Route\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RouteNotExistsException extends RouteException
{
    protected $routeName;
    
    /**
     * RouteNotExistsException constructor.
     *
     * @param string         $routeName
     * @param Exception|null $previous
     */
    public function __construct($routeName, Exception $previous = null)
    {
        $this->routeName = $routeName;
        $this->message = 'Route ' . $routeName . ' not exists!';
        parent::__construct($previous);
    }
}
