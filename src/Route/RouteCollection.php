<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Route;

use Grifix\Kit\Collection\ArrayWrapperTrait;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Orm\QueryBuilder\QueryBuilderInterface;

/**
 * Class RouteCollection
 *
 * @category Grifix
 * @package  Grifix\Kit\Route
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RouteCollection implements RouteCollectionInterface
{

    use ArrayWrapperTrait;

    /**
     * @var RouteInterface[]
     */
    protected $routes = [];

    /**
     * {@inheritdoc}
     */
    protected function getCollectionArray(): array
    {
        return $this->routes;
    }

    /**
     * {@inheritdoc}
     */
    protected function setCollectionArray(array $elements)
    {
        $this->routes = $elements;
    }

    /**
     * @param ServerRequestInterface $request
     *
     * @return RouteInterface|null
     */
    public function match(ServerRequestInterface $request)
    {
        foreach ($this->routes as $route) {
            /**@var $route RouteInterface */
            if ($route->match($request)) {
                return $route;
            }
        }

        return null;
    }
}
