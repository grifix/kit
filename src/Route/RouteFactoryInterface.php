<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Route;

use Grifix\Kit\Route\Handler\RouteHandlerInterface;


/**
 * Class RouteFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Route
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface RouteFactoryInterface
{
    
    /**
     * @param string                                          $pattern
     * @param \Grifix\Kit\Route\Handler\RouteHandlerInterface $dispatcher
     * @param array                                           $methods
     * @param bool                                            $secured
     * @param string|null                                     $host
     * @param string|null                                     $resource
     *
     * @return RouteInterface
     */
    public function create(
        string $pattern,
        RouteHandlerInterface $dispatcher = null,
        array $methods = [],
        bool $secured = false,
        string $host = null,
        string $resource = null
    );
}