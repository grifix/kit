<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Route;

use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Http\ServerRequestInterface;

/**
 * Class RouteCollection
 *
 * @category Grifix
 * @package  Grifix\Kit\Route
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface RouteCollectionInterface extends CollectionInterface
{
    /**
     * @param ServerRequestInterface $request
     *
     * @return RouteInterface|null
     */
    public function match(ServerRequestInterface $request);
}