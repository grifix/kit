<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Route\Handler;

use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class RouteRouteHandlerFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Http
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RouteHandlerFactory extends AbstractFactory implements RouteHandlerFactoryInterface
{
    
    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;
    
    /**
     * QueryHandlerFactory constructor.
     *
     * @param ClassMakerInterface   $classMaker
     * @param IocContainerInterface $iocContainer
     */
    public function __construct(ClassMakerInterface $classMaker, IocContainerInterface $iocContainer)
    {
        parent::__construct($classMaker);
        $this->iocContainer = $iocContainer;
    }
    
    /**
     * {@inheritdoc}
     */
    public function createHandler(string $handlerAlias): RouteHandlerInterface
    {
        $arr = explode('.', $handlerAlias);
        foreach ($arr as &$v) {
            $v = ucfirst($v);
        }
        $vendor = array_shift($arr);
        $module = array_shift($arr);
        $className = $this->makeClassName($vendor . '\\' . $module . '\\Ui\\Http\\Route\\' . implode('\\', $arr).'RouteHandler');
        
        return new $className($this->iocContainer);
    }
}