<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Route;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Route\Exception\ParamIsRequiredException;
use Grifix\Kit\Route\Handler\RouteHandlerInterface;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Route\Exception\MatcherNotExistsException;
use Grifix\Kit\Route\Exception\NoRouteHandlerException;
use Grifix\Kit\Route\Matcher\MatcherInterface;
use League\Flysystem\Exception;

/**
 * Class Route
 *
 * @category Grifix
 * @package  Grifix\Kit\Route
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Route implements RouteInterface
{
    /**
     * @var array
     */
    protected $params = [];
    
    /**
     * @var array
     */
    protected $paramsNames = [];
    
    /**
     * @var string
     */
    protected $pattern;
    
    /**
     * @var array
     */
    protected $methods;
    
    /**
     * @var string
     */
    protected $host;
    
    /**
     * @var bool
     */
    protected $secured;
    
    /**
     * @var string
     */
    protected $regExp;
    
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;
    
    /**
     * @var MatcherInterface[]
     */
    protected $matchers;
    
    /**
     * @var RouteHandlerInterface
     */
    protected $handler;
    
    /**
     * Route constructor.
     *
     * @param ArrayHelperInterface  $arrayHelper
     * @param string                $pattern
     * @param RouteHandlerInterface $handler
     * @param array                 $methods
     * @param MatcherInterface[]    $matchers
     * @param bool                  $secured
     * @param string|null           $host
     *
     * @internal param null|string $resource
     */
    public function __construct(
        ArrayHelperInterface $arrayHelper,
        string $pattern,
        RouteHandlerInterface $handler = null,
        array $methods = [],
        array $matchers = [],
        bool $secured = false,
        string $host = null
    ) {
        $this->pattern = $pattern;
        $this->methods = $methods;
        array_walk($this->methods, function (&$v) {
            $v = strtoupper($v);
        });
        $this->secured = $secured;
        $this->host = $host;
        $this->arrayHelper = $arrayHelper;
        $this->matchers = $matchers;
        $this->handler = $handler;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getParams(): array
    {
        return $this->params;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getPattern(): string
    {
        return $this->pattern;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getParam(string $paramName, $default = null)
    {
        $result = $this->arrayHelper->get($this->params, $paramName, $default);
        if (is_string($result)) {
            $result = urldecode($result);
        }
        
        return $result;
    }
    
    /**
     * {@inheritdoc}
     */
    public function makeUrl(array $params = []): string
    {
        preg_match_all('/\{([^}]*)\}/', $this->pattern, $matches);
        $replaces = $matches[0];
        $matches = $matches[1];
        $result = $this->pattern;
        foreach ($matches as $i => $match) {
            if (strpos($match, '?')) {
                $required = false;
            } else {
                $required = true;
            }
            $paramName = explode(':', $match)[0];
            if ($required && !array_key_exists($paramName, $params)) {
                throw new ParamIsRequiredException($paramName, $this->pattern);
            }
            $result = str_replace($replaces[$i], $this->arrayHelper->get($params, $paramName), $result);
        }
        
        return str_replace('//', '/', $result);
    }
    
    /**
     * {@inheritdoc}
     */
    public function match(ServerRequestInterface $request)
    {
        if ($this->secured && !$request->isSecured()) {
            return false;
        }
        
        if ($this->methods && !in_array($request->getMethod(), $this->methods)) {
            return false;
        }
        
        if ($this->host && $this->host != $request->getHost()) {
            return false;
        }
        
        $params = [];
        if (preg_match($this->getRegExp(), $request->getUri()->getPath(), $params)) {
            array_shift($params);
            foreach ($this->paramsNames as $i => $name) {
                if (isset($params[$i])) {
                    $this->params[$name] = $params[$i];
                } else {
                    $this->params[$name] = null;
                }
            }
            
            return true;
        }
        
        return false;
    }
    
    /**
     * @param string $type
     *
     * @return MatcherInterface
     * @throws MatcherNotExistsException
     */
    protected function getMatcher(string $type): MatcherInterface
    {
        $result = $this->arrayHelper->get($this->matchers, $type);
        if (!$result) {
            throw new MatcherNotExistsException($this, $type);
        }
        
        return $result;
    }
    
    /**
     * {@inheritdoc}
     */
    public function handle(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface {
        if (!$this->handler) {
            throw new NoRouteHandlerException($this);
        }
        
        return $this->handler->handle($this, $request, $response);
    }
    
    /**
     * @return string
     */
    protected function getRegExp()
    {
        if (is_null($this->regExp)) {
            $regExp = $this->pattern;
            preg_match_all('/{(.[^}]*)}/', $regExp, $matches);
            $delimiters = [];
            $positions = [];
            foreach ($matches[0] as $i => $match) {
                $paramName = str_replace(['?', '{', '}'], '', $match);
                $position = strpos($regExp, $match);
                $positions[$i] = $position + strlen($match) - 1;
                
                if ($position > 0) {
                    $delimiter = $regExp[$position - 1];
                    
                    
                    if ($delimiter != '/') {
                        $delimiter = substr($regExp, $positions[$i - 1], $position - $positions[$i - 1]);
                    }
                    
                    $regExp = substr_replace($regExp, '', $position - strlen($delimiter), strlen($delimiter));
                    
                    if (in_array($delimiter, ['/'])) {
                        $delimiter = '\\' . $delimiter;
                    }
                    $delimiters[$paramName] = $delimiter;
                } else {
                    $delimiters[$paramName] = '';
                }
            }
            $regExp = str_replace(['/', '}'], ['\/', ''], $regExp);
            
            foreach ($matches[1] as $paramString) {
                $paramString = preg_replace('/\?$/', '', $paramString);
                $arr = explode(':', $paramString);
                $this->paramsNames[] = $arr[0];
                $matcherType = 'mixed';
                if (isset($arr[1])) {
                    $matcherType = $arr[1];
                }
                if (strpos($matcherType, '(') === 0) {
                    $varPattern = preg_replace('/^(|)$/', '', $matcherType);
                } else {
                    $matcher = $this->getMatcher($matcherType);
                    $varPattern = $matcher->getPattern();
                }
                
                if ($delimiters[$paramString]) {
                    $varPattern = '(?:' . $delimiters[$paramString] . $varPattern . ')';
                }
                $regExp = str_replace('{' . $paramString, $varPattern, $regExp);
            }
            $regExp = '/^' . $regExp . '\/?$/';
            $this->regExp = $regExp;
        }
        
        return $this->regExp;
    }
}
