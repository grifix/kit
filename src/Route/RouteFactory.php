<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Route;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Route\Handler\RouteHandlerInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Route\Matcher\MatcherFactoryInterface;

/**
 * Class RouteFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Route
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RouteFactory extends AbstractFactory implements RouteFactoryInterface
{
    /**
     * @var MatcherFactoryInterface
     */
    protected $matcherFactory;
    
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;
    
    /**
     * RouteFactory constructor.
     *
     * @param MatcherFactoryInterface $matcherFactory
     * @param ArrayHelperInterface    $arrayHelper
     * @param ClassMakerInterface     $classMaker
     */
    public function __construct(
        MatcherFactoryInterface $matcherFactory,
        ArrayHelperInterface $arrayHelper,
        ClassMakerInterface $classMaker
    ) {
        $this->matcherFactory = $matcherFactory;
        $this->arrayHelper = $arrayHelper;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function create(
        string $pattern,
        RouteHandlerInterface $dispatcher = null,
        array $methods = [],
        bool $secured = false,
        string $host = null,
        string $resource = null
    ): RouteInterface {
        $class = $this->makeClassName(Route::class);
        
        return new $class(
            $this->arrayHelper,
            $pattern,
            $dispatcher,
            $methods,
            $this->matcherFactory->createAllMatchers(),
            $secured,
            $host,
            $resource
        );
    }
}
