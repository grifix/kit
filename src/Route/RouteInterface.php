<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Route;

use Grifix\Kit\Route\Exception\ParamIsRequiredException;
use Grifix\Kit\Serializer\ClosureContainerInterface;
use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;

/**
 * Class Route
 *
 * @category Grifix
 * @package  Grifix\Kit\Route
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface RouteInterface extends ClosureContainerInterface
{
    /**
     * @param ServerRequestInterface $request
     *
     * @return bool
     */
    public function match(ServerRequestInterface $request);
    
    /**
     * @param string $paramName
     * @param mixed  $default
     *
     * @return mixed
     */
    public function getParam(string $paramName, $default = null);
    
    /**
     * @return array
     */
    public function getParams(): array;
    
    /**
     * @param array $params
     *
     * @return string
     *
     * @throws ParamIsRequiredException
     */
    public function makeUrl(array $params = []): string;
    
    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface      $response
     *
     * @return ResponseInterface
     */
    public function handle(
        ServerRequestInterface $request,
        ResponseInterface $response
    ): ResponseInterface;
    
    /**
     * @return string
     */
    public function getPattern(): string;
}
