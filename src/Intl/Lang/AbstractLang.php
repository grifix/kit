<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Intl\Lang;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Intl\Exception\TranslationNotExistsException;


/**
 * Class AbstractLang
 *
 * @category Grifix
 * @package  Grifix\Kit\Intl
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractLang implements LangInterface
{
    
    /**
     * @var string ISO 639-3
     */
    protected $code;
    
    /**
     * @var array
     */
    protected $dictionary = [];
    
    /**
     * @var string
     */
    protected $name;
    
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;
    
    /**
     * AbstractLang constructor.
     *
     * @param ArrayHelperInterface $arrayHelper
     * @param array                $dictionary
     */
    final public function __construct(
        ArrayHelperInterface $arrayHelper,
        array $dictionary = []
    ) {
        $this->dictionary = $dictionary;
        $this->arrayHelper = $arrayHelper;
    }
    
    /**
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        return $this->code;
    }
    
    /**
     * @param int $quantity
     * @param string $case
     *
     * @return array
     */
    protected function makeQuantitative(int $quantity, string $case=null): array
    {
        if($case){
            if($quantity % 10 == 1 && $quantity % 100 != 11){
                return [self::FORM_SINGULAR, $case];
            }
            else{
                return [self::FORM_PLURAL, $case];
            }
        }
        if ($quantity % 10 == 1 && $quantity % 100 != 11) {
            return [self::FORM_SINGULAR, self::CASE_NOMINATIVE];
        }
        if ($quantity % 10 >= 2 && $quantity % 10 <= 4 && ($quantity % 10 < 10 || $quantity % 100 >= 20)) {
            return [self::FORM_SINGULAR, self::CASE_GENITIVE];
        }
        
        return [self::FORM_PLURAL, self::CASE_GENITIVE];
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCode(): string
    {
        return $this->code;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTranslation(string $key)
    {
        $result = $this->arrayHelper->get($this->dictionary, $key);
        if (!$result) {
            throw new TranslationNotExistsException($this, $key);
        }
        
        return $result;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setTranslation(string $key, mixed $val): LangInterface
    {
        $this->arrayHelper->set($this->dictionary, $key, $val);
        
        return $this;
    }
    
    /**
     * @return array
     */
    protected function getCases()
    {
        return [
            self::CASE_ABLATIVE,
            self::CASE_ACCUSATIVE,
            self::CASE_DATIVE,
            self::CASE_GENITIVE,
            self::CASE_INSTRUMENTAL,
            self::CASE_NOMINATIVE,
            self::CASE_PREPOSITIONAL,
        ];
    }
    
    /**
     * @param $key
     *
     * @return array
     */
    protected function extractParamsFromKey(&$key)
    {
        $arr = explode('.', $key);
        $params = [];
        foreach ($arr as $k => $v) {
            
            if (is_numeric($v)) {
                $params['quantity'] = $v;
                unset($arr[$k]);
            }
            
            
            if (in_array($v, ['s', 'p'])) {
                $params['form'] = $v;
                if ($params['form'] == 'p') {
                    $params['form'] = 1;
                } else {
                    $params['form'] = 0;
                }
                unset($arr[$k]);
            }
            
            //Если часть это падеж
            if (in_array($v, $this->getCases())) {
                $params['case'] = $v;
                unset($arr[$k]);
            }
        }
        $key = implode('.', $arr);
        
        return $params;
    }
    
    /**
     * {@inheritdoc}
     */
    public function translate(
        string $key,
        array $vars = [],
        int $quantity = 1,
        string $case = null,
        int $form = null
    ): string {
        
        
        extract($this->extractParamsFromKey($key), EXTR_OVERWRITE);
        
        $translation = $this->getTranslation($key);
        $result = $translation;
        
        if (!$form) {
            $form = $this->makeQuantitative(intval($quantity), $case)[0];
        }
        
        if (!$case) {
            $case = $this->makeQuantitative(intval($quantity))[1];
        }
        
        if (is_array($translation)) {
            if (isset($translation['cases'])) {
                $result = strval($this->arrayHelper->get($translation, 'cases.' . $case . '.' . $form));
            } elseif (isset($translation['forms'])) {
                if ($quantity > 1) {
                    $form = self::FORM_PLURAL;
                }
                $result = strval($this->arrayHelper->get($translation, 'forms.' . $form));
            }
        }
        
        foreach ($vars as $k => $v) {
            $result = str_replace('{' . $k . '}', $v, $result);
        }
        
        preg_match_all('/_\((.[^\)]*)\)/', $result, $matches);
        $matches = $matches[1];
        foreach ($matches as $v) {
            $k = $v;
            $result = str_replace('_(' . $v . ')', $this->translate($k, $vars), $result);
        }
        
        return $result;
    }
}