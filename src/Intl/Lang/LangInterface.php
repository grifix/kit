<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Intl\Lang;

use Intl\Exception\TranslationNotExistsException;


/**
 * Class AbstractLang
 *
 * @category Grifix
 * @package  Grifix\Kit\Intl
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface LangInterface
{
    const FORM_SINGULAR = 0;
    const FORM_PLURAL = 1;
    
    const CASE_NOMINATIVE = 'nom';
    const CASE_GENITIVE = 'gen';
    const CASE_DATIVE = 'dat';
    const CASE_ACCUSATIVE = 'acc';
    const CASE_INSTRUMENTAL = 'ins';
    const CASE_PREPOSITIONAL = 'prep';
    const CASE_ABLATIVE = 'abl';
    
    /**
     * @return string
     */
    public function getCode(): string;
    
    /**
     * @return string
     */
    public function getName(): string;
    
    /**
     * @param string $key
     * @param array  $vars
     * @param int    $quantity
     * @param string $case
     * @param int    $form
     *
     * @return string
     */
    public function translate(
        string $key,
        array $vars = [],
        int $quantity = 1,
        string $case = null,
        int $form = null
    ): string;
}