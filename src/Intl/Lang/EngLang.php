<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Intl\Lang;

/**
 * Class EnLang
 *
 * @category Grifix
 * @package  Grifix\Kit\Intl\Lang
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class EngLang extends AbstractLang
{
    
    protected $code = 'eng';
    
    protected $name = 'English';
    
    /** @noinspection PhpMissingParentCallCommonInspection */
    
    /**
     * {@inheritdoc}
     */
    protected function makeQuantitative(int $quantity, string $case = null): array
    {
        if($case){
            if($quantity % 10 == 1 && $quantity % 100 != 11){
                return [self::FORM_SINGULAR, $case];
            }
            else{
                return [self::FORM_PLURAL, $case];
            }
        }
        
        if ($quantity % 10 == 1 && $quantity % 100 != 11) {
            return [self::FORM_SINGULAR, self::CASE_NOMINATIVE];
        }
        if ($quantity % 10 >= 2 && $quantity % 10 <= 4 && ($quantity % 10 < 10 || $quantity % 100 >= 20)) {
            return [self::FORM_SINGULAR, self::CASE_GENITIVE];
        }
        
        return [self::FORM_PLURAL, self::CASE_GENITIVE];
    }
}