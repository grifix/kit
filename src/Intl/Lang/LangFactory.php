<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Intl\Lang;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Intl\Exception\LangNotExistsException;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Kernel\KernelInterface;

/**
 * Class LangFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Intl\Lang
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class LangFactory extends AbstractFactory implements LangFactoryInterface
{
    /**
     * @var ArrayHelperInterface
     */
    protected $arrayHelper;
    
    /**
     * @var ConfigInterface
     */
    protected $config;
    
    /**
     * @var KernelInterface
     */
    protected $kernel;
    
    /**
     * @var DictionaryCompilerInterface
     */
    protected $dictionaryCompiler;
    
    /**
     * @var string
     */
    protected $compiledDir;
    
    /**
     * @var bool
     */
    protected $alwaysCompile;
    
    /**
     * @var array
     */
    protected $langs;
    
    /**
     * LangFactory constructor.
     *
     * @param ArrayHelperInterface        $arrayHelper
     * @param ConfigInterface             $config
     * @param KernelInterface             $kernel
     * @param DictionaryCompilerInterface $dictionaryCompiler
     * @param string                      $compiledDir   <cfg:grifix.kit.intl.config.compiledDir>
     * @param bool                        $alwaysCompile <cfg:grifix.kit.intl.config.alwaysCompile>
     * @param array                       $langs         <cfg:grifix.kit.intl.config.enabledLangs>
     * @param ClassMakerInterface         $classMaker
     */
    public function __construct(
        ArrayHelperInterface $arrayHelper,
        ConfigInterface $config,
        KernelInterface $kernel,
        DictionaryCompilerInterface $dictionaryCompiler,
        string $compiledDir,
        bool $alwaysCompile,
        array $langs,
        ClassMakerInterface $classMaker
    ) {
        $this->arrayHelper = $arrayHelper;
        $this->config = $config;
        $this->kernel = $kernel;
        $this->dictionaryCompiler = $dictionaryCompiler;
        $this->compiledDir = $compiledDir;
        $this->alwaysCompile = $alwaysCompile;
        $this->langs = $langs;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createLang($code): LangInterface
    {
        if (!in_array($code, $this->langs)) {
            throw new LangNotExistsException($code);
        }
        
        $class = $this->makeClassName('\\Grifix\\Kit\\Intl\\Lang\\' . ucfirst($code) . 'Lang');
        
        $path = $this->compiledDir . '/' . $code . '.php';
        
        if (!is_file($path) || $this->alwaysCompile) {
            $dictionary = $this->dictionaryCompiler->compile($code, true);
        } else {
            $dictionary = include $path;
        }
        
        return new $class($this->arrayHelper, $dictionary);
    }
}