<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Intl\Locale;

use Grifix\Kit\Intl\Lang\LangFactoryInterface;
use Grifix\Kit\Intl\Locale\Exception\LocaleConfigNotExistsException;
use Grifix\Kit\Intl\Locale\Exception\LocaleNotEnabledException;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class LocaleFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Intl\Locale
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class LocaleFactory extends AbstractFactory implements LocaleFactoryInterface
{
    
    protected $langs = [];
    protected $enabledLocales = [];
    protected $langFactory;
    protected $localesConfig;
    
    /**
     * LocaleFactory constructor.
     *
     * @param ClassMakerInterface  $classMaker
     * @param LangFactoryInterface $langFactory
     * @param array                $enabledLocales <cfg:grifix.kit.intl.config.enabledLocales>
     * @param array                $localesConfig  <cfg:grifix.kit.intl.locales>
     */
    public function __construct(
        ClassMakerInterface $classMaker,
        LangFactoryInterface $langFactory,
        array $enabledLocales,
        array $localesConfig
    ) {
        $this->enabledLocales = $enabledLocales;
        $this->langFactory = $langFactory;
        $this->localesConfig = $localesConfig;
        parent::__construct($classMaker);
    }
    
    /**
     * @param $code
     *
     * @return LocaleInterface
     *
     * @throws LocaleNotEnabledException
     * @throws LocaleConfigNotExistsException
     */
    public function createLocale($code): LocaleInterface
    {
        if (!in_array($code, $this->enabledLocales)) {
            throw new LocaleNotEnabledException($code);
        }
        
        if (!array_key_exists($code, $this->localesConfig)) {
            throw new LocaleConfigNotExistsException($code);
        }
        $config = $this->localesConfig[$code];
        $class = $this->makeClassName(Locale::class);
        
        return new $class($code, $this->langFactory->createLang($config['lang']));
    }
}
