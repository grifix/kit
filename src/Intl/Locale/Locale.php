<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Intl\Locale;

use Grifix\Kit\Intl\Lang\LangInterface;

/**
 * Class Locale
 *
 * @category Grifix
 * @package  Grifix\Kit\Intl\Locale
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Locale implements LocaleInterface
{
    
    protected $code;
    
    protected $lang;
    
    /**
     * Locale constructor.
     *
     * @param string        $code
     * @param LangInterface $lang
     */
    public function __construct(string $code, LangInterface $lang)
    {
        $this->code = $code;
        $this->lang = $lang;
    }
    
    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }
    
    /**
     * @return LangInterface
     */
    public function getLang(): LangInterface
    {
        return $this->lang;
    }
}
