<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Intl\Locale\Exception;

/**
 * Class LocaleConfigNotExistsException
 *
 * @category Grifix
 * @package  Grifix\Kit\Intl\Locale\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class LocaleConfigNotExistsException extends \Exception
{
    protected $localeCode;
    
    /**
     * LocaleNotEnabledException constructor.
     *
     * @param string $localeCode
     */
    public function __construct(string $localeCode)
    {
        $this->localeCode = $localeCode;
        $this->message = 'Locale "'.$localeCode.'" config is not exists!';
        parent::__construct();
    }
}