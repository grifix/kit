<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Intl;

use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Intl\Exception\LangNotExistsException;
use Grifix\Kit\Intl\Lang\LangInterface;

/**
 * Class Translator
 *
 * @category Grifix
 * @package  Grifix\Kit\Intl
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Translator implements TranslatorInterface
{
    
    
    /**
     * @var LangInterface[]
     */
    protected $langs;
    
    /**
     * @var LangInterface
     */
    protected $currentLang;
    
    /**
     * @var ArrayHelper
     */
    protected $arrayHelper;
    
    /**
     * Translator constructor.
     *
     * @param LangInterface[] $langs
     * @param ArrayHelper     $arrayHelper
     */
    public function __construct(
        array $langs,
        ArrayHelper $arrayHelper
    ) {
        $this->langs = $langs;
        $this->currentLang = $this->langs[0];
        $this->arrayHelper = $arrayHelper;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setCurrentLang(string $langCode): TranslatorInterface
    {
        $this->currentLang = $this->getLang($langCode);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCurrentLang()
    {
        return $this->currentLang;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getLang($code): LangInterface
    {
        foreach ($this->langs as $lang) {
            if ($lang->getCode() == $code) {
                return $lang;
            }
        }
        throw new LangNotExistsException($code);
    }
    
    /**
     * {@inheritdoc}
     */
    public function translate(
        string $key,
        array $vars = [],
        int $quantity = 1,
        string $case = LangInterface::CASE_NOMINATIVE,
        int $form = LangInterface::FORM_SINGULAR,
        string $langCode = null
    ): string {
        if (is_null($langCode)) {
            $langCode = $this->currentLang->getCode();
        }
        
        return $this->getLang($langCode)->translate(
            $key,
            $vars,
            $quantity,
            $case,
            $form
        );
    }
}