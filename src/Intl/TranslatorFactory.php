<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Intl;

use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Intl\Lang\LangFactoryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class TranslatorFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Intl
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class TranslatorFactory extends AbstractFactory implements TranslatorFactoryInterface
{
    
    protected $arrayHelper;
    
    protected $langFactory;
    
    protected $langCodes = [];
    
    /**
     * TranslatorFactory constructor.
     *
     * @param ArrayHelper          $arrayHelper
     * @param LangFactoryInterface $langFactory
     * @param array                $langCodes <cfg:grifix.kit.intl.config.enabledLangs>
     * @param ClassMakerInterface  $classMaker
     */
    public function __construct(
        ArrayHelper $arrayHelper,
        LangFactoryInterface $langFactory,
        array $langCodes,
        ClassMakerInterface $classMaker
    ) {
        $this->arrayHelper = $arrayHelper;
        $this->langCodes = $langCodes;
        $this->langFactory = $langFactory;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createTranslator(string $currentLangCode = null): TranslatorInterface
    {
        $class = $this->makeClassName(Translator::class);
        $langs = [];
        foreach ($this->langCodes as $code) {
            $langs[] = $this->langFactory->createLang($code);
        }
        
        /**@var $translator \Grifix\Kit\Intl\TranslatorInterface* */
        $translator = new $class($langs, $this->arrayHelper);
        if ($currentLangCode) {
            $translator->setCurrentLang($currentLangCode);
        }
        
        return $translator;
    }
}