<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Intl\Exception;

use Grifix\Kit\Intl\Lang\LangInterface;

/**
 * Class TranslationNotExistsException
 *
 * @category Grifix
 * @package  Intl\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class TranslationNotExistsException extends \Exception
{
    /**
     * @var LangInterface
     */
    protected $lang;
    
    /**
     * @var string
     */
    protected $key;
    
    /**
     * TranslationNotExistsException constructor.
     *
     * @param LangInterface $lang
     * @param               $key
     */
    public function __construct(LangInterface $lang, $key)
    {
        $this->lang = $lang;
        $this->key = $key;
        $this->message = 'Translation "'.$key.'" not exists for language "'.$lang->getCode().'"';
        parent::__construct();
    }
}