<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Intl\Exception;

/**
 * Class LangNotExistsException
 *
 * @category Grifix
 * @package  Intl\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class LangNotExistsException extends \Exception
{
    protected $langCode;
    
    /**
     * LangNotExistsException constructor.
     *
     * @param string $langCode
     */
    public function __construct(string $langCode)
    {
        $this->langCode = $langCode;
        $this->message = 'Language with code "'.$langCode.'" is not exists!';
        parent::__construct();
    }
}
