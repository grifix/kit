<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Db;

use Grifix\Kit\Db\Exception\CantCountStatementException;
use Grifix\Kit\Db\Exception\StatementExecuteException;
use PDOException;


/**
 * Class Statement
 *
 * @category Grifix
 * @package  Grifix\Kit\Db
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface StatementInterface
{
    /**
     * @param array $values
     *
     * @return static
     */
    public function bindValues(array $values);

    /**
     * @param string $key
     * @param  mixed $value
     * @param int $type
     *
     * @return static
     */
    public function bindValue(string $key, $value, $type = \PDO::PARAM_STR);

    /**
     * @param bool $withBindings
     *
     * @return mixed|string
     */
    public function getSql(bool $withBindings = true);

    /**
     * @return array
     */
    public function getBindings();

    /**
     * @return array
     */
    public function fetch();

    /**
     * @return array
     */
    public function fetchAll(): array;

    /**
     * @return int
     * @throws CantCountStatementException
     */
    public function count();

    /**
     * @return static
     * @throws StatementExecuteException
     */
    public function execute();

    /**
     * @param int $columnNumber
     *
     * @return string
     */
    public function fetchColumn(int $columnNumber);

    /**
     * @param int $ttl
     *
     * @return array|mixed
     */
    public function executeAndFetchAll(?int $ttl = null);

    /**
     * @return bool
     */
    public function isExecuted();
}