<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Exception;

use Exception;

/**
 * Class UnknownConnectionTypeException
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UnknownConnectionTypeException extends DbException
{
    protected $type;
    
    /**
     * UnknownConnectionTypeException constructor.
     *
     * @param Exception|null $method
     */
    public function __construct($method)
    {
        $this->message = 'Unknown database connection type "' . $method . '"!';
        parent::__construct();
    }
}
