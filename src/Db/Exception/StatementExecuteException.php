<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Exception;

use Grifix\Kit\Db\StatementInterface;
use PDOException;

/**
 * Class StatementExecuteException
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class StatementExecuteException extends DbException
{
    protected $statement;
    
    /**
     * StatementExecuteException constructor.
     *
     * @param StatementInterface $statement
     * @param PDOException       $previous
     */
    public function __construct(StatementInterface $statement, PDOException $previous)
    {
        $this->statement=$statement;
        $this->message = $previous->getMessage().' in query "'.$statement->getSql().'"!';
        parent::__construct($this->message, $this->code, $previous);
    }
}