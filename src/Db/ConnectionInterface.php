<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Db;

use Grifix\Kit\Db\Event\CommitTransactionEvent;
use Grifix\Kit\Db\Event\RollbackTransactionEvent;
use Grifix\Kit\Db\Query\Processor\ProcessorInterface as QueryProcessorInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use PDO;
use Grifix\Kit\Db\Exception\CantSelectQueryException;

/**
 * Class AbstractConnection
 *
 * @category Grifix
 * @package  Grifix\Kit\Db
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ConnectionInterface
{

    /**
     * @param string $sql
     *
     * @param array $bindings
     *
     * @return StatementInterface
     */
    public function createStatement(string $sql, array $bindings = []): StatementInterface;

    /**
     * @return PDO
     */
    public function getPdo();

    /**
     * @param string|null $table
     * @param string $primaryKey
     *
     * @return mixed
     */
    public function getLastInsertedId(string $table = null, string $primaryKey = 'id');

    /**
     * @param string $sql
     * @param array $bindings
     *
     * @return ConnectionInterface
     */
    public function execute(string $sql, array $bindings = []): ConnectionInterface;


    /**
     * @return ConnectionInterface
     */
    public function beginTransaction(): ConnectionInterface;

    /**
     * @return ConnectionInterface
     */
    public function commitTransaction(): ConnectionInterface;

    /**
     * @return ConnectionInterface
     */
    public function rollBackTransaction(): ConnectionInterface;

    /**
     * @return QueryInterface
     */
    public function createQuery(): QueryInterface;

    /**
     * @return QueryProcessorInterface
     */
    public function getQueryProcessor(): QueryProcessorInterface;


    /**
     * @param string $table
     *
     * @param string $column
     *
     * @return int
     */
    public function allocateId(string $table, string $column = 'id'): int;


    /**
     * @param QueryInterface $query
     *
     * @return StatementInterface
     */
    public function createStatementFromQuery(QueryInterface $query): StatementInterface;

    /**
     * @param array $data
     * @param string $table
     * @param bool $returnLastId
     *
     * @return mixed
     * @throws Exception\StatementExecuteException
     */
    public function insert(array $data, string $table, bool $returnLastId = true);

    /**
     * @param array $conditions
     * @param string $table
     * @throws Exception\StatementExecuteException
     */
    public function delete(array $conditions, string $table): void;

    /**
     * @param string $sql
     * @param array $bingings
     * @return array
     * @throws Exception\StatementExecuteException
     */
    public function select(string $sql, array $bingings = []): array;

    /**
     * @param array  $data
     * @param string $table
     * @param string $id
     *
     * @throws Exception\StatementExecuteException
     */
    public function update(array $data, string $table, string $id): void;

}