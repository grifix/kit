<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db;

use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class StatementFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Db
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class StatementFactory extends AbstractFactory implements StatementFactoryInterface
{
    
    /**
     * @var EventBusInterface
     */
    protected $eventBus;
    
    /**
     * @var CacheInterface
     */
    protected $cache;
    
    /**
     * StatementFactory constructor.
     *
     * @param EventBusInterface $eventBus
     * @param CacheInterface        $cache
     * @param ClassMakerInterface   $classMaker
     */
    public function __construct(
        EventBusInterface $eventBus,
        CacheInterface $cache,
        ClassMakerInterface $classMaker
    ) {
        $this->eventBus = $eventBus;
        $this->cache = $cache;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createStatement(ConnectionInterface $connection, string $sql, array $bindings = [])
    {
        $class = $this->makeClassName(Statement::class);
        
        return new $class($sql, $connection, $this, $this->eventBus, $this->cache, $bindings);
    }
}