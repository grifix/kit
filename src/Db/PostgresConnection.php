<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Db;

/**
 * Class PostgresConnection
 *
 * @category Grifix
 * @package  Grifix\Kit\Db
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class PostgresConnection extends AbstractConnection
{
    
    /**
     * {@inheritdoc}
     */
    protected function createDsn()
    {
        return 'pgsql:host=' . $this->params['host'] . ';dbname=' . $this->params['db'];
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getRequiredParamsNames()
    {
        return array_merge(parent::getRequiredParamsNames(), ['host', 'db']);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getSequenceName(string $table, string $column = 'id'): string
    {
        $result = $this->createStatement(
            "select pg_get_serial_sequence('" . $table . "', '" . $column . "')"
        )->execute()->fetchAll();
        
        return $result[0]['pg_get_serial_sequence'];
    }
    
    /**
     * {@inheritdoc}
     */
    public function allocateId(string $table, string $column = 'id'): int
    {
        $row = $this->createStatement("SELECT nextval('" . $this->getSequenceName($table, $column) . "')")
            ->execute()->fetchAll();
        
        return intval($row[0]['nextval']);
    }
}