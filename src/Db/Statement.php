<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Db;

use Grifix\Kit\Cache\CacheKeyTrait;
use Grifix\Kit\Db\Event\AfterStatementExecuteEvent;
use Grifix\Kit\Db\Event\BeforeStatementExecutionEvent;
use Grifix\Kit\Db\Exception\CantCountStatementException;
use Grifix\Kit\Db\Exception\StatementExecuteException;
use Grifix\Kit\EventBus\EventBusInterface;
use PDOStatement;
use Psr\SimpleCache\CacheInterface;

/**
 * Class Statement
 *
 * @category Grifix
 * @package  Grifix\Kit\Db
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Statement implements StatementInterface
{

    use CacheKeyTrait;

    /**
     * @var array
     */
    protected $bindings = [];

    /**
     * @var ConnectionInterface
     */
    protected $connection;

    /**
     * @var string
     */
    protected $sql;

    /**
     * @var PDOStatement
     */
    protected $pdoStatement;

    /**
     * @var StatementFactoryInterface
     */
    protected $statementFactory;

    /**
     * @var EventBusInterface
     */
    protected $eventBus;

    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @var bool
     */
    protected $executed = false;


    /**
     * Statement constructor.
     *
     * @param string $sql
     * @param ConnectionInterface $connection
     * @param StatementFactoryInterface $statementFactory
     * @param EventBusInterface $eventBus
     * @param CacheInterface $cache
     * @param array $bindings
     *
     * @internal param CacheInterface $cache
     */
    public function __construct(
        string $sql,
        ConnectionInterface $connection,
        StatementFactoryInterface $statementFactory,
        EventBusInterface $eventBus,
        CacheInterface $cache,
        array $bindings = []
    ) {
        $this->sql = $sql;
        $this->connection = $connection;

        $this->pdoStatement = $this->connection->getPdo()->prepare($this->sql);
        foreach ($bindings as $key => $val) {
            $this->bindValue($key, $val);
        }
        $this->statementFactory = $statementFactory;
        $this->eventBus = $eventBus;
        $this->cache = $cache;
    }

    /**
     * @param array $values
     *
     * @return static
     */
    public function bindValues(array $values)
    {
        foreach ($values as $key => $val) {
            $this->bindValue($key, $val);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isExecuted()
    {
        return $this->executed;
    }

    /**
     * {@inheritdoc}
     */
    public function bindValue(string $key, $value, $type = \PDO::PARAM_STR)
    {
        if (is_array($value)) {
            $bindNames = [];
            $bindings = [];
            foreach ($value as $i => $v) {
                $bindName = $key . '_' . $i;
                $bindings[$bindName] = $v;
                $bindNames[] = ':' . $bindName;
            }
            $bindNames = implode(', ', $bindNames);
            $this->sql = str_replace(
                ':' . $key,
                $bindNames,
                $this->sql
            );
            foreach ($bindings as $bName => $bValue) {
                $bType = $type ?? $this->detectType($bValue);
                $this->bindValue($bName, $bValue, $bType);
            }
        } else {
            $type = $type ?? $this->detectType($value);
            $this->bindings[$key] = $value;
            $this->pdoStatement->bindValue(':' . $key, $value, $type);
        }

        return $this;
    }

    /**
     * @param $value
     * @return int
     */
    protected function detectType($value): int
    {
        if (is_int($value)) {
            return \PDO::PARAM_INT;
        }
        if (is_bool($value)) {
            return \PDO::PARAM_BOOL;
        }
        return \PDO::PARAM_STR;
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $this->eventBus->trigger(new BeforeStatementExecutionEvent($this->getSql()));
        try {
            $this->pdoStatement->execute();
        } catch (\PDOException $e) {
            throw new StatementExecuteException($this, $e);
        }
        $this->executed = true;
        $this->eventBus->trigger(new AfterStatementExecuteEvent($this->getSql()));

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function fetch()
    {
        return $this->pdoStatement->fetch();
    }

    /**
     * {@inheritdoc}
     */
    public function fetchAll(): array
    {
        return $this->pdoStatement->fetchAll();
    }

    /**
     * {@inheritdoc}
     */
    public function executeAndFetchAll(?int $ttl = null)
    {
        $cacheKey = $this->makeCacheKey(base64_encode($this->getSql(true)));
        if ($this->cache->has($cacheKey)) {
            return $this->cache->get($cacheKey);
        }
        $this->execute();
        $result = $this->pdoStatement->fetchAll();
        if (!is_null($ttl)) {
            $this->cache->set($cacheKey, $result, $ttl);
        }
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function fetchColumn(int $columnNumber)
    {
        return $this->pdoStatement->fetchColumn($columnNumber);
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        if (strpos(strtolower($this->sql), 'select') !== 0) {
            throw new CantCountStatementException($this);
        }

        return $this->statementFactory->createStatement(
            $this->connection,
            'SELECT COUNT(*) FROM (' . $this->sql . ') AS "count"',
            $this->bindings
        )->execute()->fetch()['count'];
    }

    /**
     * {@inheritdoc}
     */
    public function getSql(bool $withBindings = true)
    {
        $result = $this->sql;
        if ($this->bindings && $withBindings) {
            foreach ($this->bindings as $key => $val) {
                if (!is_numeric($val) && !is_null($val)) {
                    $val = $this->connection->getPdo()->quote($val);
                }
                $result = str_replace(':' . $key, $val, $result);
            }
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getBindings()
    {
        return $this->bindings;
    }
}
