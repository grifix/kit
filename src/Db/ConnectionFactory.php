<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db;

use Grifix\Kit\Db\Exception\MissingConnectionParamException;
use Grifix\Kit\Db\Exception\UnknownConnectionTypeException;
use Grifix\Kit\Db\Query\QueryFactoryInterface;
use Grifix\Kit\Db\Query\Processor\ProcessorFactoryInterface as QueryProcessorFactoryInterface;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class ConnectionFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Db
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ConnectionFactory extends AbstractFactory implements ConnectionFactoryInterface
{
    
    /**
     * @var EventBusInterface
     */
    protected $eventBus;
    
    /**
     * @var StatementFactoryInterface
     */
    protected $statementFactory;
    
    /**
     * @var QueryProcessorFactoryInterface
     */
    protected $queryProcessorFactory;
    
    /**
     * @var QueryFactoryInterface
     */
    protected $queryFactory;
    
    
    /**
     * ConnectionFactory constructor.
     *
     * @param EventBusInterface          $eventBus
     * @param StatementFactoryInterface      $statementFactory
     * @param QueryProcessorFactoryInterface $queryProcessorFactory
     * @param QueryFactoryInterface          $queryFactory
     * @param ClassMakerInterface            $classMaker
     */
    public function __construct(
        EventBusInterface $eventBus,
        StatementFactoryInterface $statementFactory,
        QueryProcessorFactoryInterface $queryProcessorFactory,
        QueryFactoryInterface $queryFactory,
        ClassMakerInterface $classMaker
    ) {
        $this->eventBus = $eventBus;
        $this->statementFactory = $statementFactory;
        $this->queryFactory = $queryFactory;
        $this->queryProcessorFactory = $queryProcessorFactory;
        $this->classMaker = $classMaker;
        
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createConnection(array $params): ConnectionInterface
    {
        if (!isset($params['type'])) {
            throw new MissingConnectionParamException('type');
        }
        
        switch ($params['type']) {
            case 'postgres':
                $class = PostgresConnection::class;
                break;
            
            default:
                throw new UnknownConnectionTypeException($params['type']);
        }
        
        /**@var $result ConnectionInterface */
        $objectClass = $this->makeClassName($class);
        $result = new $objectClass(
            $this->eventBus,
            $this->statementFactory,
            $this->queryProcessorFactory->createProcessor($class),
            $this->queryFactory,
            $params
        );
        
        return $result;
    }
}