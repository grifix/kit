<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Event;

use Grifix\Kit\Db\ConnectionInterface;

/**
 * Class RollbackTransactionEvent
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Event
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 *
 * @method ConnectionInterface getInitiator()
 */
class RollbackTransactionEvent
{

}
