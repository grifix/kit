<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Db\Query;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Query\Exception\QueryHasPartException;
use Grifix\Kit\Db\Query\Exception\QueryWasNotExecutedException;
use Grifix\Kit\Db\Query\Part;
use Grifix\Kit\Db\StatementInterface;

/**
 * Class Query
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Criterion
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Query implements QueryInterface
{
    
    /**
     * @var Part\PartFactoryInterface
     */
    protected $partFactory;
    
    /**
     * @var array
     */
    protected $bindings = [];
    
    /**
     * @var array
     */
    protected $parts = [];
    
    /**
     * @var ConnectionInterface
     */
    protected $connection;
    
    /**
     * Query constructor.
     *
     * @param Part\PartFactoryInterface $partFactory
     * @param ConnectionInterface       $connection
     */
    public function __construct(
        Part\PartFactoryInterface $partFactory,
        ConnectionInterface $connection
    ) {
        $this->partFactory = $partFactory;
        $this->connection = $connection;
    }
    
    /**
     * @param object      $part
     * @param string|null $key
     *
     * @return QueryInterface
     */
    protected function addPart($part, string $key = null): QueryInterface
    {
        if (is_null($key)) {
            array_push($this->parts, $part);
        } else {
            $this->parts[$key] = $part;
        }
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function bindValue(string $key, $value): QueryInterface
    {
        $this->bindings[$key] = $value;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function bindValues(array $values): QueryInterface
    {
        $this->bindings = array_merge($this->bindings, $values);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setBindings(array $bindings): QueryInterface
    {
        $this->bindings = $bindings;
        
        return $this;
    }
    
    /**
     * @return array
     */
    public function getBindings()
    {
        return $this->bindings;
    }
    
    /**
     * {@inheritdoc}
     */
    public function select(string $expression, string $key = null): QueryInterface
    {
        $this->hasPartOrFail([
            Part\InsertInterface::class,
            Part\UpdateInterface::class,
            Part\DeleteInterface::class,
        ]);
        
        return $this->addPart($this->partFactory->createSelect($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function nativeSelect(string $expression, string $key = null): QueryInterface
    {
        $this->hasPartOrFail([
            Part\InsertInterface::class,
            Part\UpdateInterface::class,
            Part\DeleteInterface::class,
        ]);
        
        return $this->addPart($this->partFactory->createNativeSelect($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function having($expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createHaving($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function nativeHaving(string $expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createNativeHaving($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function orHaving($expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createOrHaving($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function nativeOrHaving(string $expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createNativeOrHaving($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function insert(array $row, string $key = null): QueryInterface
    {
        $this->hasPartOrFail([
            Part\SelectInterface::class,
            Part\UpdateInterface::class,
            Part\DeleteInterface::class,
        ]);
        
        
        return $this->addPart($this->partFactory->createInsert($row), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function nativeInsert(string $expression, string $key = null): QueryInterface
    {
        $this->hasPartOrFail([
            Part\SelectInterface::class,
            Part\UpdateInterface::class,
            Part\DeleteInterface::class,
        ]);
        
        return $this->addPart($this->partFactory->createNativeInsert($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function update(string $table, string $alias = null, string $key = null): QueryInterface
    {
        $this->hasPartOrFail([
            Part\SelectInterface::class,
            Part\InsertInterface::class,
            Part\DeleteInterface::class,
        ]);
        
        return $this->addPart($this->partFactory->createUpdate($table, $alias), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function into(string $table, string $alias = null): QueryInterface
    {
        $this->removeParts(Part\IntoInterface::class);
        
        return $this->addPart($this->partFactory->createInto($table, $alias));
    }
    
    /**
     * {@inheritdoc}
     */
    public function leftJoin(string $table, string $alias, $on, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createLeftJoin($table, $alias, $on), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function rightJoin(string $table, string $alias, $on, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createRightJoin($table, $alias, $on), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function innerJoin(string $table, string $alias, $on, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createInnerJoin($table, $alias, $on), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function nativeJoin(string $expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createNativeJoin($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function where($expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createWhere($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function nativeWhere(string $expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createNativeWhere($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function orWhere($expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createOrWhere($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function nativeOrWhere(string $expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createNativeOrWhere($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function union(QueryInterface $query, bool $all = false, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createUnion($query, $all), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function nativeUnion(string $expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createNativeUnion($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function except(array $parts, bool $all = true, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createExcept($parts, $all), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function intersect(array $parts, bool $all = true, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createIntersect($parts, $all), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function delete(): QueryInterface
    {
        $this->hasPartOrFail([
            Part\InsertInterface::class,
            Part\UpdateInterface::class,
            Part\SelectInterface::class,
        ]);
        
        $this->removeParts(Part\DeleteInterface::class);
        
        return $this->addPart($this->partFactory->createDelete());
    }
    
    /**
     * {@inheritdoc}
     */
    public function from(string $table, string $alias = null): QueryInterface
    {
        $this->removeParts(Part\FromInterface::class);
        
        return $this->addPart($this->partFactory->createFrom($table, $alias));
    }
    
    /**
     * {@inheritdoc}
     */
    public function nativeFrom(string $expression): QueryInterface
    {
        $this->removeParts(Part\FromInterface::class);
        
        return $this->addPart($this->partFactory->createNativeFrom($expression));
    }
    
    /**
     * {@inheritdoc}
     */
    public function limit(int $limit): QueryInterface
    {
        $this->removeParts(Part\LimitInterface::class);
        
        return $this->addPart($this->partFactory->createLimit($limit));
    }
    
    /**
     * {@inheritdoc}
     */
    public function offset(int $offset): QueryInterface
    {
        $this->removeParts(Part\OffsetInterface::class);
        
        return $this->addPart($this->partFactory->createOffset($offset));
    }
    
    /**
     * {@inheritdoc}
     */
    public function with(QueryInterface $query, string $alias, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createWith($query, $alias), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function nativeWith(string $expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createNativeWith($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function groupBy(string $expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createGroupBy($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function nativeGroupBy(string $expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createNativeGroupBy($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function orderBy(string $expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createOrderBy($expression), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function removeParts(string $className): QueryInterface
    {
        foreach ($this->parts as $i => $part) {
            if ($part instanceof $className
                || ($part instanceof Part\NativePartInterface && is_subclass_of($part->getType(), $className))
            ) {
                unset($this->parts[$i]);
            }
        }
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function set(array $row, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createSet($row), $key);
    }
    
    /**
     * {@inheritdoc}
     */
    public function nativeSet(string $expression, string $key = null): QueryInterface
    {
        return $this->addPart($this->partFactory->createNativeSet($expression), $key);
    }
    
    /**
     * @param array $partsClasses
     *
     * @return void
     * @throws QueryHasPartException
     */
    protected function hasPartOrFail(array $partsClasses)
    {
        foreach ($this->parts as $part) {
            foreach ($partsClasses as $partClass) {
                if ($this->isTypeOf($part, $partClass)) {
                    throw new QueryHasPartException($this, $partClass);
                }
            }
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function findParts(string $className): array
    {
        $result = [];
        foreach ($this->parts as $part) {
            if ($this->isTypeOf($part, $className)) {
                $result[] = $part;
            }
        }
        
        return $result;
    }
    
    /**
     * {@inheritdoc}
     */
    public function findPart(string $className)
    {
        $result = $this->findParts($className);
        if (count($result) > 0) {
            return $result[0];
        }
        
        return null;
    }
    
    /**
     * @param object $part
     * @param string $type
     *
     * @return bool
     */
    protected function isTypeOf($part, string $type): bool
    {
        return $part instanceof $type
            || ($part instanceof Part\NativePartInterface && is_a($part->getType(), $type, true));
    }
    
    /**
     * {@inheritdoc}
     */
    public function hasPart(string $className): bool
    {
        foreach ($this->parts as $part) {
            if ($this->isTypeOf($part, $className)) {
                return true;
            }
        }
        
        return false;
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function clear(): QueryInterface
    {
        $this->parts = [];
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function execute(): void
    {
        $this->connection->createStatementFromQuery($this)->execute();
    }
    
    /**
     * {@inheritdoc}
     */
    public function fetch()
    {
        return $this->connection->createStatementFromQuery($this)->execute()->fetch();
    }
    
    /**
     * {@inheritdoc}
     */
    public function fetchAll($ttl = null)
    {
        return $this->connection->createStatementFromQuery($this)->executeAndFetchAll($ttl);
    }
}
