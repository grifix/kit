<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Exception;

use Grifix\Kit\Db\Query\QueryInterface;

/**
 * Class QueryHasPartException
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class QueryHasPartException extends QueryException
{
    /**
     * @var QueryInterface
     */
    protected $query;
    
    
    /**
     * QueryHasSelectPartException constructor.
     *
     * @param QueryInterface $query
     */
    public function __construct(QueryInterface $query, string $partClass)
    {
        $this->message = 'Query has "' . $partClass . '"" part!';
        parent::__construct();
    }
}