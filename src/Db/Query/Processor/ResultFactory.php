<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Processor;

use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Kernel\AbstractFactory;

/**
 * Class ResultFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Processor
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ResultFactory extends AbstractFactory implements ResultFactoryInterface
{
    
    /**
     * {@inheritdoc}
     */
    public function makeResult(QueryInterface $query): ResultInterface
    {
        $class = $this->makeClassName(Result::class);
        
        return new $class($query);
    }
}