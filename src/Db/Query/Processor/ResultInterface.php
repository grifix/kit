<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Db\Query\Processor;

use Grifix\Kit\Db\Query\QueryInterface;


/**
 * Class Result
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Processor
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ResultInterface
{
    /**
     * @param string $sql
     *
     * @return ResultInterface
     */
    public function pushSql(string $sql): ResultInterface;
    
    /**
     * @param string $key
     * @param        $val
     *
     * @return ResultInterface
     */
    public function bindValue(string $key, $val): ResultInterface;
    
    /**
     * @return string
     */
    public function getSql(): string;
    
    /**
     * @return QueryInterface
     */
    public function getQuery(): QueryInterface;
    
    /**
     * @return array
     */
    public function getBindings();
    
    /**
     * @param array $binding
     *
     * @return ResultInterface
     */
    public function setBindings(array $binding): ResultInterface;
    
    /**
     * @param array $values
     *
     * @return ResultInterface
     */
    public function bindValues(array $values): ResultInterface;
}