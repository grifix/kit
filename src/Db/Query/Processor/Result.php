<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Processor;

use Grifix\Kit\Db\Query\QueryInterface;

/**
 * Class Result
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Processor
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Result implements ResultInterface
{
    
    /**
     * @var array
     */
    protected $sql = [];
    
    /**
     * @var array
     */
    protected $bindings = [];
    
    /**
     * @var QueryInterface
     */
    protected $query;
    
    /**
     * Result constructor.
     *
     * @param QueryInterface $query
     */
    public function __construct(QueryInterface $query)
    {
        $this->query = $query;
    }
    
    /**
     * {@inheritdoc}
     */
    public function pushSql(string $sql): ResultInterface
    {
        array_push($this->sql, $sql);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function bindValue(string $key, $val): ResultInterface
    {
        $this->bindings[$key] = $val;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function bindValues(array $values): ResultInterface
    {
        $this->bindings = array_merge($this->bindings, $values);
        return $this;
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function getSql(): string
    {
        return str_replace(['1=1 AND ', '1=1 OR ', '1 = 1 AND ', '1 = 1 OR '], '', implode(' ', $this->sql));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getQuery(): QueryInterface
    {
        return $this->query;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBindings()
    {
        return $this->bindings;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setBindings(array $binding): ResultInterface
    {
        $this->bindings = $binding;
        
        return $this;
    }
}
