<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Processor;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Query\Criterion\AndCriterion;
use Grifix\Kit\Db\Query\Criterion\CriterionInterface;
use Grifix\Kit\Db\Query\Criterion\OrCriterion;
use Grifix\Kit\Db\Query\NativeSqlInterface;
use Grifix\Kit\Db\Query\Part;
use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Db\StatementInterface;

/**
 * Class AbstractProcessor
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Processor
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractProcessor implements ProcessorInterface
{
    
    
    /**
     * @var ResultFactoryInterface
     */
    protected $resultFactory;
    
    /**
     * AbstractProcessor constructor.
     *
     * @param ResultFactoryInterface $resultFactory
     *
     * @internal param ConnectionInterface $connection
     */
    public function __construct(ResultFactoryInterface $resultFactory)
    {
        $this->resultFactory = $resultFactory;
    }
    
    /**
     * {@inheritdoc}
     */
    public function process(QueryInterface $query): ResultInterface
    {
        $result = $this->resultFactory->makeResult($query);
        $result->setBindings($query->getBindings());
        if ($query->hasPart(Part\SelectInterface::class)) {
            $this->buildSelect($result);
        } elseif ($query->hasPart(Part\UpdateInterface::class)) {
            $this->buildUpdate($result);
        } elseif ($query->hasPart(Part\InsertInterface::class)) {
            $this->buildInsert($result);
        } elseif ($query->hasPart(Part\DeleteInterface::class)) {
            $this->buildDelete($result);
        }
        
        return $result;
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildSelect(ResultInterface $result)
    {
        $this->buildWith($result);
        $result->pushSql('SELECT');
        $this->buildSelectFields($result);
        $this->buildFrom($result);
        $this->buildJoin($result);
        $this->buildWhere($result);
        $this->buildGroupBy($result);
        $this->buildHaving($result);
        $this->buildUnion($result);
        $this->buildOrderBy($result);
        $this->buildLimit($result);
        $this->buildOffset($result);
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildUnion(ResultInterface $result)
    {
        $unions = $result->getQuery()->findParts(Part\Union::class);
        foreach ($unions as $union) {
            if ($union instanceof Part\NativePartInterface) {
                $result->pushSql($union->getExpression());
            } elseif ($union instanceof Part\UnionInterface) {
                switch ($union->getType()) {
                    case Part\UnionInterface::TYPE_EXCEPT:
                        $result->pushSql('EXCEPT');
                        break;
                    case Part\UnionInterface::TYPE_INTERSECT:
                        $result->pushSql('INTERSECT');
                        break;
                    default:
                        $result->pushSql('UNION');
                }
                if ($union->isAll()) {
                    $result->pushSql('ALL');
                }
                $queryResult = $this->process($union->getQuery());
                $result->pushSql($queryResult->getSql());
                $result->bindValues($queryResult->getBindings());
            }
        }
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildWith(ResultInterface $result)
    {
        $sql = [];
        $withList = $result->getQuery()->findParts(Part\WithInterface::class);
        if ($withList) {
            foreach ($withList as $with) {
                if ($with instanceof Part\NativePartInterface) {
                    $sql[] = $with->getExpression();
                } elseif ($with instanceof Part\WithInterface) {
                    $withResult = $this->process($with->getQuery());
                    $sql[] = $this->quoteName($with->getAlias()) . ' AS (' . $withResult->getSql() . ')';
                    $result->setBindings(array_merge($result->getBindings(), $withResult->getBindings()));
                }
            }
            $result->pushSql('WITH')->pushSql(implode(' , ', $sql));
        }
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildLimit(ResultInterface $result)
    {
        /**@var $limit Part\LimitInterface */
        $limit = $result->getQuery()->findPart(Part\LimitInterface::class);
        if ($limit) {
            $result->pushSql('LIMIT')->pushSql(strval($limit->getValue()));
        }
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildOffset(ResultInterface $result)
    {
        /**@var $offset Part\OffsetInterface */
        $offset = $result->getQuery()->findParts(Part\OffsetInterface::class);
        if ($offset) {
            $result->pushSql('OFFSET')->pushSql(strval($offset->getValue()));
        }
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildOrderBy(ResultInterface $result)
    {
        $orderByList = $result->getQuery()->findParts(Part\OrderByInterface::class);
        if($orderByList){
            $result->pushSql('ORDER BY');
            $sql = [];
            foreach ($orderByList as $orderBy) {
                /**@var Part\OrderByInterface $orderBy */
                $sql[] = $orderBy->getExpression();
            }
            $result->pushSql(implode(', ', $sql));
        }
    }
    
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildGroupBy(ResultInterface $result)
    {
        $groupByList = $result->getQuery()->findParts(Part\GroupByInterface::class);
        if ($groupByList) {
            $result->pushSql('GROUP BY');
            $sql = [];
            foreach ($groupByList as $groupBy) {
                /**@var Part\GroupByInterface $groupBy */
                $sql[] = $groupBy->getExpression();
            }
            $result->pushSql(implode(', ', $sql));
        }
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildJoin(ResultInterface $result)
    {
        $joins = $result->getQuery()->findParts(Part\JoinInterface::class);
        foreach ($joins as $join) {
            if ($join instanceof Part\NativePartInterface) {
                $result->pushSql($join->getExpression());
            } elseif ($join instanceof Part\JoinInterface) {
                switch ($join->getType()) {
                    case Part\JoinInterface::TYPE_INNER:
                        $result->pushSql('INNER');
                        break;
                    case Part\JoinInterface::TYPE_LEFT:
                        $result->pushSql('LEFT');
                        break;
                    case Part\JoinInterface::TYPE_RIGHT:
                        $result->pushSql('RIGHT');
                        break;
                }
                $result->pushSql('JOIN');
                $result->pushSql($this->quoteName($join->getTable()));
                if ($join->getAlias()) {
                    $result->pushSql('AS');
                    $result->pushSql($this->quoteName($join->getAlias()));
                }
                if ($join->getCriterion()) {
                    $result->pushSql('ON 1=1');
                    $this->buildCriterion($result, $join->getCriterion());
                }
            }
        }
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildSelectFields(ResultInterface $result)
    {
        $selects = $result->getQuery()->findParts(Part\SelectInterface::class);
        $sql = [];
        foreach ($selects as $select) {
            /**@var  Part\SelectInterface $select */
            $sql[] = $select->getExpression();
        }
        $result->pushSql(implode(', ', $sql));
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildUpdate(ResultInterface $result)
    {
        
        $this->buildWith($result);
        $result->pushSql('UPDATE');
        
        /**@var $update Part\UpdateInterface */
        $update = $result->getQuery()->findPart(Part\UpdateInterface::class);
        $result->pushSql($this->quoteName($update->getTable()));
        if ($update->getAlias()) {
            $result->pushSql('AS')->pushSql($this->quoteName($update->getAlias()));
        }
        
        /**@var $set Part\SetInterface */
        $set = $result->getQuery()->findPart(Part\SetInterface::class);
        $values = $set->getValues();
        $result->pushSql('SET');
        $sets = [];
        foreach ($values as $key => $value) {
            $sets[] = $this->quoteName($key) . ' = :' . $key;
        }
        $result->pushSql(implode(', ', $sets));
        $this->buildWhere($result);
        $result->bindValues($set->getValues());
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildDelete(ResultInterface $result)
    {
        $this->buildWith($result);
        $result->pushSql('DELETE');
        $this->buildFrom($result);
        $this->buildWhere($result);
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildWhere(ResultInterface $result)
    {
        if ($result->getQuery()->hasPart(Part\WhereInterface::class)) {
            $result->pushSql('WHERE 1=1');
            $this->buildConditions($result, $result->getQuery()->findParts(Part\WhereInterface::class));
        }
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildHaving(ResultInterface $result)
    {
        if ($result->getQuery()->hasPart(Part\HavingInterface::class)) {
            $result->pushSql('HAVING 1=1');
            $this->buildConditions($result, $result->getQuery()->findParts(Part\HavingInterface::class));
        }
    }
    
    /**
     * @param ResultInterface $result
     * @param array           $conditions
     *
     * @return void
     */
    protected function buildConditions(ResultInterface $result, array $conditions)
    {
        foreach ($conditions as $condition) {
            if ($condition instanceof Part\ConditionInterface) {
                $this->buildCondition($result, $condition);
            } elseif ($condition instanceof Part\NativePartInterface) {
                $this->buildNativeCondition($result, $condition);
            }
        }
    }
    
    /**
     * @param ResultInterface         $result
     * @param Part\ConditionInterface $condition
     *
     * @return AbstractProcessor
     */
    protected function buildCondition(ResultInterface $result, Part\ConditionInterface $condition)
    {
        return $this->buildCriterion($result, $condition->getCriterion());
    }
    
    /**
     * @param ResultInterface    $result
     * @param CriterionInterface $criterion
     *
     * @return $this
     */
    protected function buildCriterion(ResultInterface $result, CriterionInterface $criterion)
    {
        $result->bindValues($criterion->getBindings());
        
        if ($criterion instanceof OrCriterion) {
            $result->pushSql('OR');
        } else {
            $result->pushSql('AND');
        }
        
        
        if ($criterion->hasNestedCriteria()) {
            $result->pushSql('(');
        }
        
        if ($criterion->getExpression()) {
            $result->pushSql($this->parseExpression($criterion->getExpression()));
        }
        
        if ($criterion->hasNestedCriteria()) {
            $nestedCriteria = $criterion->getNestedCriteria();
            foreach ($nestedCriteria as $nestedCriterion) {
                $this->buildCriterion($result, $nestedCriterion);
            }
            $result->pushSql(')');
        }
        
        return $this;
    }
    
    /**
     * @param string $expression
     *
     * @return string|NativeSqlInterface
     */
    protected function parseExpression($expression)
    {
        if ($expression instanceof NativeSqlInterface) {
            return $expression->__toString();
        }
        
        return $expression;
    }
    
    /**
     * @param ResultInterface          $result
     * @param Part\NativePartInterface $condition
     *
     * @return $this
     */
    protected function buildNativeCondition(ResultInterface $result, Part\NativePartInterface $condition)
    {
        if (!$condition->getLogic() || $condition->getLogic() == Part\NativePartInterface::LOGIC_AND) {
            $result->pushSql('AND');
        } elseif ($condition->getLogic() == Part\NativePartInterface::LOGIC_OR) {
            $result->pushSql('OR');
        }
        $result->pushSql($condition->getExpression());
        
        return $this;
    }
    
    
    /**
     * @param ResultInterface $result
     *
     * @return static
     */
    protected function buildFrom(ResultInterface $result)
    {
        $from = $result->getQuery()->findPart(Part\FromInterface::class);
        if ($from) {
            $result->pushSql('FROM')->pushSql($this->quoteName($from->getTable()));
            if ($from->getAlias()) {
                $result->pushSql('AS')->pushSql($this->quoteName($from->getAlias()));
            }
        }
        
        return $this;
    }
    
    /**
     * @param ResultInterface          $result
     * @param Part\NativePartInterface $from
     *
     * @return static
     */
    protected function buildNativeFrom(ResultInterface $result, Part\NativePartInterface $from)
    {
        $result->pushSql('FROM')->pushSql($from->getExpression());
        
        return $this;
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return static
     */
    protected function buildInsert(ResultInterface $result)
    {
        $result->pushSql('INSERT');
        $this->buildInto($result);
        $this->buildValues($result);
        
        return $this;
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildInto(ResultInterface $result)
    {
        $into = $result->getQuery()->findPart(Part\IntoInterface::class);
        if ($into) {
            $result->pushSql('INTO')->pushSql($this->quoteName($into->getTable()));
            if ($into->getAlias()) {
                $result->pushSql('AS')->pushSql($this->quoteName($into->getAlias()));
            }
        }
    }
    
    /**
     * @param ResultInterface $result
     *
     * @return void
     */
    protected function buildValues(ResultInterface $result)
    {
        $inserts = $result->getQuery()->findParts(Part\InsertInterface::class);
        $columns = $params = [];
        foreach ($inserts as $insert) {
            if ($insert instanceof Part\InsertInterface) {
                $values = $insert->getValues();
                foreach ($values as $key => $val) {
                    $columns[] = $this->quoteName($key);
                    $params[] = ':' . $key;
                    $result->bindValue($key, $val);
                }
            } elseif ($insert instanceof Part\NativePartInterface) {
                $result->pushSql($insert->getExpression());
            }
        }
        if ($columns && $params) {
            $result->pushSql('(' . implode(', ', $columns) . ') VALUES (' . implode(', ', $params) . ')');
        }
    }
    
    /**
     * @param string $name
     *
     * @return string
     */
    protected function quoteName(string $name)
    {
        if (strpos($name, '.') === false) {
            return '"' . $name . '"';
        }
        
        return $name;
    }
}