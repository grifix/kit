<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Processor\Exception;

/**
 * Class ProcessorNotExistsException
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Processor
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ProcessorNotExistsException extends AbstractProcessorException
{
    protected $connectionClass;
    
    /**
     * ProcessorNotExistsException constructor.
     *
     * @param string $connectionClass
     */
    public function __construct(string $connectionClass)
    {
        $this->message = 'Query processor for connection class "' . $connectionClass . '"" is not exists!';
        $this->connectionClass = $connectionClass;
        parent::__construct();
    }
}