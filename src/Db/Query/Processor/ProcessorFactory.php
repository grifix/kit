<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Processor;

use Grifix\Kit\Db\PostgresConnection;
use Grifix\Kit\Db\Query\Processor\Exception\ProcessorNotExistsException;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class ProcessorFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Processor
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ProcessorFactory extends AbstractFactory implements ProcessorFactoryInterface
{
    
    protected $resultFactory;
    
    
    /**
     * ProcessorFactory constructor.
     *
     * @param ResultFactoryInterface $resultFactory
     * @param ClassMakerInterface    $classMaker
     */
    public function __construct(ResultFactoryInterface $resultFactory, ClassMakerInterface $classMaker)
    {
        $this->resultFactory = $resultFactory;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createProcessor(string $connectionClass): ProcessorInterface
    {
        $result = null;
        $class = null;
        
        switch ($connectionClass) {
            case PostgresConnection::class:
                $class = $this->makeClassName(PostgresProcessor::class);
                break;
            default:
                throw new ProcessorNotExistsException($connectionClass);
        }
        
        return new $class($this->resultFactory);
    }
}
