<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Criterion;

use Grifix\Kit\Db\Query\NativeSqlInterface;

/**
 * Class NativeSql
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Part
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class NativeSql implements NativeSqlInterface
{
    /**
     * @var string
     */
    protected $expression;
    
    
    /**
     * NativeSql constructor.
     *
     * @param string $expression
     */
    public function __construct(string $expression)
    {
        $this->expression = $expression;
    }
    
    /**
     * {@inheritdoc}
     */
    public function __toString(): string
    {
        return $this->expression;
    }
}