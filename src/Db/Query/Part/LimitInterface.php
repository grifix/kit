<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Db\Query\Part;

/**
 * Class Limit
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface LimitInterface
{
    /**
     * @return int
     */
    public function getValue(): int;
    
    /**
     * @param int $value
     *
     * @return static
     */
    public function setValue(int $value);
}