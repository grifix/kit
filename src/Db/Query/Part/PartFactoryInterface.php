<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Db\Query\Part;

use Grifix\Kit\Db\Query\QueryInterface;


/**
 * Class PartFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Part
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface PartFactoryInterface
{
    /**
     * @param $expression
     *
     * @return SelectInterface
     */
    public function createSelect(string $expression): SelectInterface;
    
    /**
     * @param $expression
     *
     * @return NativePartInterface
     */
    public function createNativeSelect(string $expression): NativePartInterface;
    
    /**
     * @param string|callable $expression
     *
     * @return HavingInterface
     */
    public function createHaving($expression): HavingInterface;
    
    /**
     * @param string $expression
     *
     * @return NativePartInterface
     */
    public function createNativeHaving(string $expression): NativePartInterface;
    
    /**
     * @param string|callable $expression
     *
     * @return HavingInterface
     */
    public function createOrHaving($expression): HavingInterface;
    
    /**
     * @param string $expression
     *
     * @return NativePartInterface
     */
    public function createNativeOrHaving(string $expression): NativePartInterface;
    
    /**
     * @param string          $table
     * @param string          $alias
     * @param string|callable $on
     *
     * @return mixed
     */
    public function createLeftJoin(string $table, string $alias, $on): JoinInterface;
    
    /**
     * @param string $table
     * @param string $alias
     * @param        $on
     *
     * @return JoinInterface
     */
    public function createRightJoin(string $table, string $alias, $on): JoinInterface;
    
    /**
     * @param string $table
     * @param string $alias
     * @param        $on
     *
     * @return JoinInterface
     */
    public function createInnerJoin(string $table, string $alias, $on);
    
    /**
     * @param string $expression
     *
     * @return NativePartInterface
     */
    public function createNativeJoin(string $expression);
    
    /**
     * @param string|callable $expression
     *
     * @return WhereInterface
     */
    public function createWhere($expression): WhereInterface;
    
    /**
     * @param string $expression
     *
     * @return NativePartInterface
     */
    public function createNativeWhere(string $expression): NativePartInterface;
    
    /**
     * @param string|callable $expression
     *
     * @return WhereInterface
     */
    public function createOrWhere($expression): WhereInterface;
    
    /**
     * @param string $expression
     *
     * @return NativePartInterface
     */
    public function createNativeOrWhere(string $expression): NativePartInterface;
    
    /**
     * @param QueryInterface $query
     * @param bool           $all
     *
     * @return UnionInterface
     */
    public function createUnion(QueryInterface $query, bool $all = true): UnionInterface;
    
    /**
     * @param string $expression
     *
     * @return NativePartInterface
     */
    public function createNativeUnion(string $expression): NativePartInterface;
    
    /**
     * @param array $parts
     * @param bool  $all
     *
     * @return UnionInterface
     */
    public function createExcept(array $parts, bool $all = true): UnionInterface;
    
    /**
     * @param array $parts
     * @param bool  $all
     *
     * @return UnionInterface
     */
    public function createIntersect(array $parts, bool $all = true): UnionInterface;
    
    /**
     * @param string|QueryInterface $table
     * @param string                $alias
     *
     * @return FromInterface
     */
    public function createFrom($table, string $alias = null): FromInterface;
    
    /**
     * @param string $expression
     *
     * @return NativePartInterface
     */
    public function createNativeFrom(string $expression): NativePartInterface;
    
    /**
     * @param int $value
     *
     * @return LimitInterface
     */
    public function createLimit(int $value): LimitInterface;
    
    /**
     * @param int $value
     *
     * @return OffsetInterface
     */
    public function createOffset(int $value): OffsetInterface;
    
    /**
     * @param QueryInterface $query
     * @param string         $alias
     *
     * @return WithInterface
     */
    public function createWith(QueryInterface $query, string $alias): WithInterface;
    
    /**
     * @param string $expression
     *
     * @return NativePartInterface
     */
    public function createNativeWith(string $expression): NativePartInterface;
    
    /**
     * @param string $expression
     *
     * @return OrderByInterface
     */
    public function createOrderBy(string $expression): OrderByInterface;
    
    /**
     * @param string $expression
     *
     * @return GroupByInterface
     */
    public function createGroupBy(string $expression): GroupByInterface;
    
    /**
     * @param string $expression
     *
     * @return NativePartInterface
     */
    public function createNativeGroupBy(string $expression): NativePartInterface;
    
    /**
     * @param array $row
     *
     * @return InsertInterface
     */
    public function createInsert(array $row): InsertInterface;
    
    /**
     * @param string $expression
     *
     * @return NativePartInterface
     */
    public function createNativeInsert(string $expression): NativePartInterface;
    
    /**
     * @param string      $table
     * @param string|null $alias
     *
     * @return IntoInterface
     */
    public function createInto(string $table, string $alias = null): IntoInterface;
    
    /**
     * @param string      $table
     * @param string|null $alias
     *
     * @return UpdateInterface
     */
    public function createUpdate(string $table, string $alias = null): UpdateInterface;
    
    /**
     * @return DeleteInterface
     */
    public function createDelete(): DeleteInterface;
    
    /**
     * @param array $row
     *
     * @return SetInterface
     */
    public function createSet(array $row): SetInterface;
    
    /**
     * @param string $expression
     *
     * @return NativePartInterface
     */
    public function createNativeSet(string $expression): NativePartInterface;
}