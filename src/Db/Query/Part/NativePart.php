<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Part;

/**
 * Class NativePart
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Part
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class NativePart implements NativePartInterface
{
    
    /**
     * @var string
     */
    protected $expression;
    
    /**
     * @var string
     */
    protected $type;
    
    /**
     * @var int
     */
    protected $logic;
    
    /**
     * NativePart constructor.
     *
     * @param string $expression
     * @param string $type
     * @param int    $logic
     */
    public function __construct(string $expression, string $type, int $logic = null)
    {
        $this->expression = $expression;
        $this->type = $type;
        $this->logic = $logic;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getExpression(): string
    {
        return $this->expression;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getType(): string
    {
        return $this->type;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getLogic(): int
    {
        return $this->logic;
    }
}