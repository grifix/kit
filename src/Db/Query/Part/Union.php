<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Part;

use Grifix\Kit\Db\Query\NativeSqlInterface;
use Grifix\Kit\Db\Query\QueryInterface;

/**
 * Class Union
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Part
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Union implements UnionInterface
{
    /**
     * @var QueryInterface
     */
    protected $query;
    
    /**
     * @var int
     */
    protected $type = self::TYPE_UNION;
    
    /**
     * @var bool
     */
    protected $all = true;
    
    /**
     * Union constructor.
     *
     * @param QueryInterface $query
     * @param int            $type
     * @param bool           $all
     */
    public function __construct(QueryInterface $query, $type = self::TYPE_UNION, $all = false)
    {
        $this->query = $query;
        $this->type = $type;
        $this->all = $all;
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function getQuery(): QueryInterface
    {
        return $this->query;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isAll(){
        return $this->all;
    }
    
}
