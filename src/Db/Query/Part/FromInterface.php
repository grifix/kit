<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Db\Query\Part;

use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Db\Query\NativeSqlInterface;


/**
 * Class From
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface FromInterface
{
    /**
     * @return string|null
     */
    public function getAlias();
    
    /**
     * @return QueryInterface|string|\Grifix\Kit\Db\Query\NativeSqlInterface
     */
    public function getTable();
}