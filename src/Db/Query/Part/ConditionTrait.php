<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Part;

use Grifix\Kit\Db\Query\Criterion\CriterionInterface;

/**
 * Class ConditionTrait
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Part
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
trait ConditionTrait
{
    /**
     * @var CriterionInterface
     */
    protected $criterion;
    
    /**
     * ConditionTrait constructor.
     *
     * @param CriterionInterface $criterion
     */
    public function __construct(CriterionInterface $criterion)
    {
        $this->criterion = $criterion;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getCriterion(): CriterionInterface
    {
        return $this->criterion;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setCriterion(CriterionInterface $criterion)
    {
        $this->criterion = $criterion;
        
        return $this;
    }
}
