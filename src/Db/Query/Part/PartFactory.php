<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Part;

use Grifix\Kit\Db\Query\Criterion\CriterionFactoryInterface;
use Grifix\Kit\Db\Query\QueryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class PartFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Part
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class PartFactory extends AbstractFactory implements PartFactoryInterface
{
    
    /**
     * @var CriterionFactoryInterface
     */
    protected $criteriaFactory;
    
    /**
     * PartFactory constructor.
     *
     * @param CriterionFactoryInterface $criteriaFactory
     * @param ClassMakerInterface $classMaker
     */
    public function __construct(CriterionFactoryInterface $criteriaFactory, ClassMakerInterface $classMaker)
    {
        $this->criteriaFactory = $criteriaFactory;
        parent::__construct($classMaker);
    }
    
    /**
     * @param string $expression
     * @param string $type
     * @param int    $logic
     *
     * @return NativePartInterface
     */
    protected function createNativePart(
        string $expression,
        string $type,
        int $logic = null
    ): NativePartInterface {
        $class = $this->makeClassName(NativePart::class);
        
        return new $class($expression, $type, $logic);
    }
    
    /**
     * @param string $expression
     * @param string $type
     *
     * @return NativePartInterface
     */
    protected function createOrNativePart(string $expression, string $type): NativePartInterface
    {
        return $this->createNativePart($expression, $type, NativePartInterface::LOGIC_OR);
    }
    
    /**
     * @param string $expression
     * @param string $type
     *
     * @return NativePartInterface
     */
    protected function createAndNativePart(string $expression, string $type): NativePartInterface
    {
        return $this->createNativePart($expression, $type, NativePartInterface::LOGIC_AND);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createSelect(string $expression): SelectInterface
    {
        $class = $this->makeClassName(Select::class);
        
        return new $class($expression);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createNativeSelect(string $expression): NativePartInterface
    {
        return $this->createNativePart($expression, SelectInterface::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createHaving($expression): HavingInterface
    {
        $class = $this->makeClassName(Having::class);
        
        return new $class($this->criteriaFactory->createAndCriterion($expression));
    }
    
    /**
     * {@inheritdoc}
     */
    public function createNativeHaving(string $expression): NativePartInterface
    {
        return $this->createAndNativePart($expression, HavingInterface::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createOrHaving($expression): HavingInterface
    {
        $class = $this->makeClassName(Having::class);
        
        return new $class($this->criteriaFactory->createOrCriterion($expression));
    }
    
    /**
     * {@inheritdoc}
     */
    public function createNativeOrHaving(string $expression): NativePartInterface
    {
        return $this->createOrNativePart($expression, HavingInterface::class);
    }
    
    /**
     * @param     $table
     * @param     $alias
     * @param     $on
     * @param int $type
     *
     * @return JoinInterface
     */
    protected function createJoin($table, $alias, $on, $type = JoinInterface::TYPE_LEFT): JoinInterface
    {
        $class = $this->makeClassName(Join::class);
        
        return new $class($table, $alias, $this->criteriaFactory->createCriterion($on), $type);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createLeftJoin(string $table, string $alias, $on): JoinInterface
    {
        return $this->createJoin($table, $alias, $on);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createRightJoin(string $table, string $alias, $on): JoinInterface
    {
        return $this->createJoin($table, $alias, $on, JoinInterface::TYPE_RIGHT);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createInnerJoin(string $table, string $alias, $on)
    {
        return $this->createJoin($table, $alias, $on, JoinInterface::TYPE_INNER);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createNativeJoin(string $expression)
    {
        return $this->createNativePart($expression, JoinInterface::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createWhere($expression): WhereInterface
    {
        $class = $this->makeClassName(Where::class);
        
        return new $class($this->criteriaFactory->createAndCriterion($expression));
    }
    
    /**
     * {@inheritdoc}
     */
    public function createNativeWhere(string $expression): NativePartInterface
    {
        return $this->createAndNativePart($expression, WhereInterface::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createOrWhere($expression): WhereInterface
    {
        $class = $this->makeClassName(Where::class);
        
        return new $class($this->criteriaFactory->createOrCriterion($expression));
    }
    
    /**
     * {@inheritdoc}
     */
    public function createNativeOrWhere(string $expression): NativePartInterface
    {
        return $this->createOrNativePart($expression, WhereInterface::class);
    }
    
    /**
     * @param QueryInterface $query
     * @param int            $type
     * @param bool           $all
     *
     * @return UnionInterface
     */
    protected function doCreateUnion(
        QueryInterface $query,
        int $type = UnionInterface::TYPE_UNION,
        bool $all = true
    ): UnionInterface {
        $class = $this->makeClassName(Union::class);
        
        return new $class($query, $type, $all);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createUnion(QueryInterface $query, bool $all = false): UnionInterface
    {
        return $this->doCreateUnion($query, UnionInterface::TYPE_UNION, $all);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createNativeUnion(string $expression): NativePartInterface
    {
        return $this->createNativePart($expression, UnionInterface::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createExcept(array $parts, bool $all = true): UnionInterface
    {
        return $this->doCreateUnion($parts, UnionInterface::TYPE_EXCEPT, $all);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createIntersect(array $parts, bool $all = true): UnionInterface
    {
        return $this->doCreateUnion($parts, UnionInterface::TYPE_INTERSECT, $all);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createFrom($table, string $alias = null): FromInterface
    {
        $class = $this->makeClassName(From::class);
        
        return new $class($table, $alias);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createNativeFrom(string $expression): NativePartInterface
    {
        return $this->createNativePart($expression, FromInterface::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createLimit(int $value): LimitInterface
    {
        $class = $this->makeClassName(Limit::class);
        
        return new $class($value);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createOffset(int $value): OffsetInterface
    {
        $class = $this->makeClassName(Offset::class);
        
        return new $class($value);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createWith(QueryInterface $query, string $alias): WithInterface
    {
        $class = $this->makeClassName(With::class);
        
        return new $class($query, $alias);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createNativeWith(string $expression): NativePartInterface
    {
        return $this->createNativePart($expression, WithInterface::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createOrderBy(string $expression): OrderByInterface
    {
        $class = $this->makeClassName(OrderBy::class);
        
        return new $class($expression);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createGroupBy(string $expression): GroupByInterface
    {
        $class = $this->makeClassName(GroupBy::class);
        
        return new $class($expression);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createNativeGroupBy(string $expression): NativePartInterface
    {
        return $this->createNativePart($expression, GroupByInterface::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createInsert(array $row): InsertInterface
    {
        $class = $this->makeClassName(Insert::class);
        
        return new $class($row);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createNativeInsert(string $expression): NativePartInterface
    {
        return $this->createAndNativePart($expression, InsertInterface::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createSet(array $row): SetInterface
    {
        $class = $this->makeClassName(Set::class);
        
        return new $class($row);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createNativeSet(string $expression): NativePartInterface
    {
        return $this->createAndNativePart($expression, SetInterface::class);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createInto(string $table, string $alias = null): IntoInterface
    {
        $class = $this->makeClassName(Into::class);
        
        return new $class($table, $alias);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createUpdate(string $table, string $alias = null): UpdateInterface
    {
        $class = $this->makeClassName(Update::class);
        
        return new $class($table, $alias);
    }
    
    /**
     * @return DeleteInterface
     */
    public function createDelete(): DeleteInterface
    {
        $class = $this->makeClassName(Delete::class);
        
        return new $class;
    }
}
