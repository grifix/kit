<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Db\Query\Part;


/**
 * Class Join
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface JoinInterface extends ConditionInterface
{
    const TYPE_INNER = 1;
    const TYPE_LEFT = 2;
    const TYPE_RIGHT = 3;
    
    /**
     * @return string
     */
    public function getTable();
    
    /**
     * @return null|string
     */
    public function getAlias();
    
    /**
     * @return int
     */
    public function getType();
}