<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Part;

use Grifix\Kit\Db\Query\Criterion\CriterionInterface;
use Grifix\Kit\Db\Query\NativeSqlInterface;

/**
 * Class Join
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Join implements JoinInterface
{
    use ConditionTrait;
    
    /**
     * @var string
     */
    protected $table;
    
    /**
     * @var string
     */
    protected $alias;
    
    /**
     * @var CriterionInterface
     */
    protected $criterion;
    
    /**
     * @var int
     */
    protected $type;
    
    /**
     * Join constructor.
     *
     * @param string                  $table
     * @param string|null             $alias
     * @param CriterionInterface|null $criterion
     * @param int                     $type
     */
    public function __construct(
        string $table,
        string $alias = null,
        CriterionInterface $criterion = null,
        int $type = self::TYPE_LEFT
    ) {
        $this->table = $table;
        $this->alias = $alias;
        $this->criterion = $criterion;
        $this->type = $type;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTable()
    {
        return $this->table;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getAlias()
    {
        return $this->alias;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return $this->type;
    }
}
