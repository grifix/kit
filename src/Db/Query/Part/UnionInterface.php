<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Db\Query\Part;

use Grifix\Kit\Db\Query\NativeSqlInterface;
use Grifix\Kit\Db\Query\QueryInterface;


/**
 * Class Union
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Part
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface UnionInterface
{
    const TYPE_UNION = 1;
    const TYPE_INTERSECT = 2;
    const TYPE_EXCEPT = 3;
    
    /**
     * @return QueryInterface
     */
    public function getQuery(): QueryInterface;
    
    /**
     * @return int
     */
    public function getType();
    
    /**
     * @return bool
     */
    public function isAll();
}
