<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Query\Part\PartFactoryInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class QueryHandlerFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class QueryFactory extends AbstractFactory implements QueryFactoryInterface
{
    protected $partFactory;
    
    
    /**
     * QueryHandlerFactory constructor.
     *
     * @param PartFactoryInterface $partFactory
     * @param ClassMakerInterface  $classMaker
     */
    public function __construct(PartFactoryInterface $partFactory, ClassMakerInterface $classMaker)
    {
        $this->partFactory = $partFactory;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createQuery(ConnectionInterface $connection): QueryInterface
    {
        $class = $this->makeClassName(Query::class);
        
        return new $class($this->partFactory, $connection);
    }
}
