<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query;

use Grifix\Kit\Db\ConnectionInterface;

/**
 * Interface QueryFactoryInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface QueryFactoryInterface
{
    
    /**
     * @param ConnectionInterface $connection
     *
     * @return QueryInterface
     */
    public function createQuery(ConnectionInterface $connection): QueryInterface;
}
