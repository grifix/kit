<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Db\Query\Criterion;

use Grifix\Kit\Db\Query\NativeSqlInterface;

/**
 * Class Criterion
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface CriterionInterface
{
    /**
     * @param CriterionInterface $criterion
     * @param string|null        $alias
     *
     * @return CriterionInterface
     */
    public function andCriterion(CriterionInterface $criterion, string $alias = null): CriterionInterface;
    
    /**
     * @param CriterionInterface $criterion
     * @param string|null        $alias
     *
     * @return CriterionInterface
     */
    public function orCriterion(CriterionInterface $criterion, string $alias = null): CriterionInterface;
    
    /**
     * @param string|callable $expression
     * @param string|null     $alias
     *
     * @return CriterionInterface
     */
    public function andExpression($expression, string $alias = null): CriterionInterface;
    
    /**
     * @param string|callable $expression
     * @param string|null     $alias
     *
     * @return CriterionInterface
     */
    public function orExpression($expression, string $alias = null): CriterionInterface;
    
    /**
     * @return CriterionInterface[]
     */
    public function getNestedCriteria();
    
    /**
     * @return string|callable
     */
    public function getExpression();
    
    /**
     * @param string|callable $expression
     *
     * @return CriterionInterface
     */
    public function setExpression($expression): CriterionInterface;
    
    /**
     * @param string|null $alias
     *
     * @return CriterionInterface
     */
    public function removeNestedCriterion(string $alias = null): CriterionInterface;
    
    /**
     * @return bool
     */
    public function hasNestedCriteria(): bool;
    
    /**
     * @param string $key
     * @param        $value
     *
     * @return CriterionInterface
     */
    public function bindValue(string $key, $value): CriterionInterface;
    
    /**
     * @param array $values
     *
     * @return CriterionInterface
     */
    public function bindValues(array $values): CriterionInterface;
    
    /**
     * @param array $bindings
     *
     * @return CriterionInterface
     */
    public function setBindings(array $bindings): CriterionInterface;
    
    /**
     * @return array
     */
    public function getBindings(): array;
}