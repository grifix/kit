<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Db\Query\Criterion;

use Grifix\Kit\Db\Query\NativeSqlInterface;

/**
 * Class CriterionFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface CriterionFactoryInterface
{
    /**
     * @param string|callable|null $expression
     *
     * @return CriterionInterface
     */
    public function createCriterion($expression = null): CriterionInterface;
    
    /**
     * @param CriterionInterface $criterion
     *
     * @return CriterionInterface
     */
    public function createOrWrapper(CriterionInterface $criterion): CriterionInterface;
    
    /**
     * @param CriterionInterface $criterion
     *
     * @return CriterionInterface
     */
    public function createAndWrapper(CriterionInterface $criterion): CriterionInterface;
    
    /**
     * @param string|null|callable $expression
     *
     * @return CriterionInterface
     */
    public function createAndCriterion($expression = null): CriterionInterface;
    
    /**
     * @param string|null|callable $expression
     *
     * @return CriterionInterface
     */
    public function createOrCriterion($expression = null): CriterionInterface;
}