<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Criterion;

/**
 * Class Criterion
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Criterion implements CriterionInterface
{
    /**
     * @var null|string|callable
     */
    protected $expression;
    
    /**
     * @var CriterionInterface[]
     */
    protected $nestedCriteria = [];
    
    /**
     * @var CriterionFactoryInterface
     */
    protected $criterionFactory;
    
    /**
     * @var array
     */
    protected $bindings = [];
    
    /**
     * Criterion constructor.
     *
     * @param CriterionFactoryInterface $criterionFactory
     * @param string|null|callable      $expression
     */
    public function __construct(CriterionFactoryInterface $criterionFactory, $expression = null)
    {
        $this->expression = $expression;
        $this->criterionFactory = $criterionFactory;
    }
    
    /**
     * {@inheritdoc}
     */
    public function bindValue(string $key, $value): CriterionInterface
    {
        $this->bindings[$key] = $value;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function bindValues(array $values): CriterionInterface
    {
        $this->bindings = array_merge($this->bindings, $values);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setBindings(array $bindings): CriterionInterface
    {
        $this->bindings = $bindings;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBindings(): array
    {
        return $this->bindings;
    }
    
    /**
     * {@inheritdoc}
     */
    public function andCriterion(CriterionInterface $criterion, string $alias = null): CriterionInterface
    {
        if (!($criterion instanceof AndCriterion)) {
            $criterion = $this->criterionFactory->createAndWrapper($criterion);
        }
        
        return $this->addCriterion($criterion, $alias);
    }
    
    /**
     * {@inheritdoc}
     */
    public function orCriterion(CriterionInterface $criterion, string $alias = null): CriterionInterface
    {
        if (!($criterion instanceof OrCriterion)) {
            $criterion = $this->criterionFactory->createOrWrapper($criterion);
        }
        
        return $this->addCriterion($criterion, $alias);
    }
    
    /**
     * {@inheritdoc}
     */
    public function andExpression($expression, string $alias = null): CriterionInterface
    {
        return $this->andCriterion($this->criterionFactory->createAndCriterion($expression), $alias);
    }
    
    /**
     * {@inheritdoc}
     */
    public function orExpression($expression, string $alias = null): CriterionInterface
    {
        return $this->orCriterion($this->criterionFactory->createOrCriterion($expression), $alias);
    }
    
    /**
     * @param CriterionInterface $criterion
     * @param string|null        $alias
     *
     * @return $this
     */
    protected function addCriterion(CriterionInterface $criterion, string $alias = null)
    {
        if (!$alias) {
            array_push($this->nestedCriteria, $criterion);
        }
        else{
            $this->nestedCriteria[$alias] = $criterion;
        }
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getNestedCriteria()
    {
        return $this->nestedCriteria;
    }
    
    /**
     * {@inheritdoc}
     */
    public function hasNestedCriteria(): bool
    {
        return boolval(count($this->nestedCriteria));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getExpression()
    {
        return $this->expression;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setExpression($expression): CriterionInterface
    {
        $this->expression = $expression;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function removeNestedCriterion(string $alias = null): CriterionInterface
    {
        if (!$alias) {
            array_pop($this->nestedCriteria);
        } else {
            unset($this->nestedCriteria[$alias]);
        }
        
        return $this;
    }
}
