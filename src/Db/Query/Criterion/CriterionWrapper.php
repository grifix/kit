<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query\Criterion;

/**
 * Class CriterionWrapper
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query\Criterion
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class CriterionWrapper implements CriterionInterface
{
    
    /**
     * @var CriterionInterface
     */
    protected $criterion;
    
    /**
     * CriterionWrapper constructor.
     *
     * @param CriterionInterface $criterion
     */
    public function __construct(CriterionInterface $criterion)
    {
        $this->criterion = $criterion;
    }
    
    /**
     * {@inheritdoc}
     */
    public function andCriterion(CriterionInterface $criterion, string $alias = null): CriterionInterface
    {
        $this->criterion->andCriterion($criterion, $alias);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function orCriterion(CriterionInterface $criterion, string $alias = null): CriterionInterface
    {
        $this->criterion->orCriterion($criterion, $alias);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function andExpression($expression, string $alias = null): CriterionInterface
    {
        $this->criterion->andExpression($expression, $alias);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function orExpression($expression, string $alias = null): CriterionInterface
    {
        $this->criterion->orExpression($expression, $alias);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getNestedCriteria()
    {
        return $this->criterion->getNestedCriteria();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getExpression()
    {
        return $this->criterion->getExpression();
    }
    
    /**
     * {@inheritdoc}
     */
    public function setExpression($expression): CriterionInterface
    {
        $this->criterion->setExpression($expression);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function removeNestedCriterion(string $alias = null): CriterionInterface
    {
        $this->criterion->removeNestedCriterion($alias);
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function hasNestedCriteria(): bool
    {
        return $this->criterion->hasNestedCriteria();
    }
    
    /**
     * {@inheritdoc}
     */
    public function bindValue(string $key, $value): CriterionInterface
    {
        return $this->criterion->bindValue($key, $value);
    }
    
    /**
     * {@inheritdoc}
     */
    public function bindValues(array $values): CriterionInterface
    {
        return $this->criterion->bindValues($values);
    }
    
    /**
     * {@inheritdoc}
     */
    public function setBindings(array $bindings): CriterionInterface
    {
        return $this->criterion->setBindings($bindings);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBindings(): array
    {
      return $this->criterion->getBindings();
    }
}