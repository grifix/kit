<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Db\Query;

/**
 * Interface QueryInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Db\Query
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface QueryInterface
{
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function select(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function nativeSelect(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param string|callable $expression
     * @param string|null     $key
     *
     * @return QueryInterface
     */
    public function having($expression, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function nativeHaving(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param string|callable $expression
     * @param string|null     $key
     *
     * @return QueryInterface
     */
    public function orHaving($expression, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function nativeOrHaving(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param array       $row
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function insert(array $row, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function nativeInsert(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param string      $table
     * @param string|null $alias
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function update(string $table, string $alias=null, string $key = null): QueryInterface;
    
    /**
     * @param string      $table
     * @param string|null $alias
     *
     * @return QueryInterface
     */
    public function into(string $table, string $alias = null): QueryInterface;
    
    /**
     * @param string          $table
     * @param string          $alias
     * @param string|callable $on
     * @param string|null     $key
     *
     * @return QueryInterface
     */
    public function leftJoin(string $table, string $alias, $on, string $key = null): QueryInterface;
    
    /**
     * @param string          $table
     * @param string          $alias
     * @param string|callable $on
     * @param string|null     $key
     *
     * @return QueryInterface
     */
    public function rightJoin(string $table, string $alias, $on, string $key = null): QueryInterface;
    
    /**
     * @param string          $table
     * @param string          $alias
     * @param string|callable $on
     * @param string|null     $key
     *
     * @return QueryInterface
     */
    public function innerJoin(string $table, string $alias, $on, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function nativeJoin(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param string|callable $expression
     * @param string|null     $key
     *
     * @return QueryInterface
     */
    public function where($expression, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function nativeWhere(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param string|callable $expression
     * @param string|null     $key
     *
     * @return QueryInterface
     */
    public function orWhere($expression, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function nativeOrWhere(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param QueryInterface $query
     * @param bool           $all
     * @param string|null    $key
     *
     * @return QueryInterface
     */
    public function union(QueryInterface $query, bool $all = true, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function nativeUnion(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param array       $parts
     * @param bool        $all
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function except(array $parts, bool $all = true, string $key = null): QueryInterface;
    
    /**
     * @param array       $parts
     * @param bool        $all
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function intersect(array $parts, bool $all = true, string $key = null): QueryInterface;
    
    /**
     * @return QueryInterface
     */
    public function delete(): QueryInterface;
    
    /**
     * @param string $table
     * @param string $alias
     *
     * @return QueryInterface
     */
    public function from(string $table, string $alias = null): QueryInterface;
    
    /**
     * @param string $expression
     *
     * @return QueryInterface
     */
    public function nativeFrom(string $expression): QueryInterface;
    
    /**
     * @param int $limit
     *
     * @return QueryInterface
     */
    public function limit(int $limit): QueryInterface;
    
    /**
     * @param int $offset
     *
     * @return QueryInterface
     */
    public function offset(int $offset): QueryInterface;
    
    /**
     * @param QueryInterface $query
     * @param string         $alias
     * @param string|null    $key
     *
     * @return QueryInterface
     */
    public function with(QueryInterface $query, string $alias, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function nativeWith(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function groupBy(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function nativeGroupBy(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function orderBy(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param string $className
     *
     * @return QueryInterface
     */
    public function removeParts(string $className): QueryInterface;
    
    /**
     * @param $className
     *
     * @return array
     */
    public function findParts(string $className): array;
    
    /**
     * @param string $className
     *
     * @return bool
     */
    public function hasPart(string $className): bool;
    
    /**
     * @param array       $row
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function set(array $row, string $key = null): QueryInterface;
    
    /**
     * @param string      $expression
     * @param string|null $key
     *
     * @return QueryInterface
     */
    public function nativeSet(string $expression, string $key = null): QueryInterface;
    
    /**
     * @param string $className
     *
     * @return mixed|null
     */
    public function findPart(string $className);
    
    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return QueryInterface
     */
    public function bindValue(string $key, $value): QueryInterface;
    
    /**
     * @param array $values
     *
     * @return QueryInterface
     */
    public function bindValues(array $values): QueryInterface;
    
    /**
     * @param array $bindings
     *
     * @return QueryInterface
     */
    public function setBindings(array $bindings): QueryInterface;
    
    /**
     * @return array
     */
    public function getBindings();
    
    /**
     * @return QueryInterface
     */
    public function clear(): QueryInterface;
    
    /**
     * @return void
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    public function execute(): void;
    
    /**
     * @return array
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    public function fetch();
    
    /**
     * @param int $ttl
     *
     * @return array|mixed
     */
    public function fetchAll($ttl = 0);
}