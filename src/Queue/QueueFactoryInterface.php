<?php
declare(strict_types=1);

namespace Grifix\Kit\Queue;

/**
 * Class QueueFactory
 * @package Grifix\Kit\Queue
 */
interface QueueFactoryInterface
{
    /**
     * @return QueueInterface
     */
    public function createRabbitMqQueue(): QueueInterface;

    /**
     * @return QueueInterface
     */
    public function createSyncQueue(): QueueInterface;
}
