<?php
declare(strict_types=1);

namespace Grifix\Kit\Queue;

use Grifix\Kit\Queue\ConsumerFactory\ConsumerFactoryInterface;

/**
 * Class SyncQueue
 * @package Grifix\Kit\Queue
 */
class SyncQueue implements QueueInterface
{
    /**
     * @var Definition[]
     */
    protected $definitions;

    /**
     * @var ConsumerFactoryInterface
     */
    protected $consumerFactory;

    /**
     * SyncQueue constructor.
     * @param ConsumerFactoryInterface $consumerFactory
     * @param array $definitions
     */
    public function __construct(ConsumerFactoryInterface $consumerFactory, array $definitions)
    {
        $this->definitions = $definitions;
        $this->consumerFactory = $consumerFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function publish($message, string $queue)
    {
        foreach ($this->definitions as $definition) {
            if ($definition->getQueueName() == $queue) {
                $consumer = $this->consumerFactory->createConsumer($definition->getConsumerClass());
                $consumer($message);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function consume(callable $consumer, string $queue)
    {
        throw new \Exception('Not implemented!');
    }
}
