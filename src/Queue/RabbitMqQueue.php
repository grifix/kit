<?php
declare(strict_types=1);

namespace Grifix\Kit\Queue;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class Queue
 * @package Grifix\Kit\Queue
 */
class RabbitMqQueue implements QueueInterface
{
    /**
     * @var AMQPStreamConnection
     */
    protected $connection;

    /**
     * @var AMQPChannel
     */
    protected $channel;

    /**
     * RabbitMqQueue constructor.
     * @param AMQPStreamConnection $connection
     */
    public function __construct(AMQPStreamConnection $connection)
    {
        $this->connection = $connection;
        $this->channel = $connection->channel();
    }

    public function __destruct()
    {
        $this->channel->close();
    }

    /**
     * @param $message
     * @param string $queue
     */
    public function publish($message, string $queue)
    {
        $this->channel->queue_declare($queue, false, false, false, false);
        $msg = new AMQPMessage(serialize($message));
        $this->channel->basic_publish($msg, '', $queue);
    }


    /**
     * @param callable $consumer
     * @param string $queue
     */
    public function consume(callable $consumer, string $queue)
    {
        $this->channel->queue_declare($queue, false, false, false, false);
        $this->channel->basic_consume(
            $queue,
            '',
            false,
            false,
            false,
            false,
            function ($msg) use ($consumer) {
                $consumer(unserialize($msg->body));
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            }
        );
        while (count($this->channel->callbacks)) {
            $this->channel->wait();
        }
    }
}
