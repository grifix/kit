<?php
declare(strict_types=1);

namespace Grifix\Kit\Queue\ConsumerFactory;

/**
 * Class ConsumerCreator
 * @package Grifix\Kit\Queue
 */
interface ConsumerFactoryInterface
{
    /**
     * @param string $consumerClass
     * @return mixed
     * @throws \ReflectionException
     */
    public function createConsumer(string $consumerClass);
}
