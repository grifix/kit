<?php
declare(strict_types=1);

namespace Grifix\Kit\Queue\ConsumerFactory;

use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;

/**
 * Class ConsumerCreator
 * @package Grifix\Kit\Queue
 */
class ConsumerFactory extends AbstractFactory implements ConsumerFactoryInterface
{
    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    /**
     * ConsumerFactory constructor.
     * @param IocContainerInterface $iocContainer
     * @param ClassMakerInterface $classMaker
     */
    public function __construct(IocContainerInterface $iocContainer, ClassMakerInterface $classMaker)
    {
        $this->iocContainer = $iocContainer;
        parent::__construct($classMaker);
    }


    /**
     * @param string $consumerClass
     * @return mixed
     * @throws \ReflectionException
     */
    public function createConsumer(string $consumerClass)
    {
        $consumerClass = $this->makeClassName($consumerClass);
        return $this->iocContainer->createNewInstance($consumerClass);
    }
}
