<?php
declare(strict_types=1);

namespace Grifix\Kit\Queue;

use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Queue\ConsumerFactory\ConsumerFactoryInterface;
use Grifix\Kit\Queue\DefinitionsCollector\DefinitionsCollectorInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * Class QueueFactory
 * @package Grifix\Kit\Queue
 */
class QueueFactory extends AbstractFactory implements QueueFactoryInterface
{
    /**
     * @var string
     */
    protected $rabbitMqHost;

    /**
     * @var int
     */
    protected $rabbitMqPort;

    /**
     * @var string
     */
    protected $rabbitMqUser;

    /**
     * @var string
     */
    protected $rabbitMqPassword;

    /**
     * @var ConsumerFactoryInterface
     */
    protected $consumerFactory;

    /**
     * @var DefinitionsCollectorInterface
     */
    protected $definitionsCollector;

    /**
     * QueueFactory constructor.
     * @param string $rabbitMqHost <cfg:grifix.kit.queue.rabbit.host>
     * @param int $rabbitMqPort <cfg:grifix.kit.queue.rabbit.port>
     * @param string $rabbitMqUser <cfg:grifix.kit.queue.rabbit.user>
     * @param string $rabbitMqPassword <cfg:grifix.kit.queue.rabbit.pass>
     * @param ConsumerFactoryInterface $consumerFactory
     * @param DefinitionsCollectorInterface $definitionsCollector
     * @param ClassMakerInterface $classMaker
     */
    public function __construct(
        string $rabbitMqHost,
        int $rabbitMqPort,
        string $rabbitMqUser,
        string $rabbitMqPassword,
        ConsumerFactoryInterface $consumerFactory,
        DefinitionsCollectorInterface $definitionsCollector,
        ClassMakerInterface $classMaker
    ) {
        $this->rabbitMqHost = $rabbitMqHost;
        $this->rabbitMqPort = $rabbitMqPort;
        $this->rabbitMqUser = $rabbitMqUser;
        $this->rabbitMqPassword = $rabbitMqPassword;
        $this->consumerFactory = $consumerFactory;
        $this->definitionsCollector = $definitionsCollector;
        parent::__construct($classMaker);
    }

    /**
     * @return QueueInterface
     */
    public function createRabbitMqQueue(): QueueInterface
    {
        $class = $this->makeClassName(RabbitMqQueue::class);
        return new $class(
            new AMQPStreamConnection(
                $this->rabbitMqHost,
                $this->rabbitMqPort,
                $this->rabbitMqUser,
                $this->rabbitMqPassword
            )
        );
    }

    /**
     * @return QueueInterface
     */
    public function createSyncQueue(): QueueInterface
    {
        $class = $this->makeClassName(SyncQueue::class);
        return new $class($this->consumerFactory, $this->definitionsCollector->collectDefinitions());
    }
}
