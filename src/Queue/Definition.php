<?php
declare(strict_types=1);

namespace Grifix\Kit\Queue;

/**
 * Class Defintion
 * @package Grifix\Kit\Queue
 */
class Definition
{

    /**
     * @var string
     */
    protected $consumerClass;

    /**
     * @var string
     */
    protected $queueName;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * Definition constructor.
     * @param string $consumerClass
     * @param string $queueName
     * @param int $quantity
     */
    public function __construct(string $consumerClass, string $queueName, int $quantity)
    {
        $this->consumerClass = $consumerClass;
        $this->queueName = $queueName;
        $this->quantity = $quantity;
    }

    /**
     * @return string
     */
    public function getConsumerClass(): string
    {
        return $this->consumerClass;
    }

    /**
     * @return string
     */
    public function getQueueName(): string
    {
        return $this->queueName;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
