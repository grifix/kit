<?php
declare(strict_types=1);

namespace Grifix\Kit\Queue\DefinitionsCollector;

use Grifix\Kit\Queue\Definition;

/**
 * Class DefinitionsCollectorInterface
 * @package Grifix\Kit\Queue\DefinitionsCollector
 */
interface DefinitionsCollectorInterface
{
    /**
     * @return Definition[]
     */
    public function collectDefinitions(): array;
}
