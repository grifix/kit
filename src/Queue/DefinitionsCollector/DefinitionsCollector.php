<?php
declare(strict_types=1);

namespace Grifix\Kit\Queue\DefinitionsCollector;

use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Queue\Definition;

/**
 * Class DefintionsCollector
 * @package Grifix\Kit\Queue\DefinitionsCollector
 */
class DefinitionsCollector implements DefinitionsCollectorInterface
{
    /**
     * @var KernelInterface
     */
    protected $kernel;

    /**
     * DefinitionsCollector constructor.
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @return Definition[]
     */
    public function collectDefinitions(): array
    {
        $result = [];
        foreach ($this->kernel->getModules() as $module) {
            $queues = $module->getConfig()['queues'] ?? [];
            foreach ($queues as $definition) {
                $result[] = $definition;
            }
        }
        return $result;
    }
}
