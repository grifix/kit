<?php
declare(strict_types=1);

namespace Grifix\Kit\Queue;

/**
 * Class Queue
 * @package Grifix\Kit\Queue
 */
interface QueueInterface
{
    /**
     * @param object $message
     * @param string $queue
     */
    public function publish($message, string $queue);

    /**
     * @param callable $consumer
     * @param string $queue
     */
    public function consume(callable $consumer, string $queue);
}
