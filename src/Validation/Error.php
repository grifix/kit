<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation;

use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Validation\Field\FieldInterface;
use Grifix\Kit\Validation\Validator\ValidatorInterface;

/**
 * Class Error
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Error implements ErrorInterface
{
    /**
     * @var ValidatorInterface
     */
    protected $validator;
    
    /**
     * @var FieldInterface
     */
    protected $field;
    
    /**
     * @var mixed
     */
    protected $value;
    
    /**
     * @var string
     */
    protected $message;
    
    /**
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     * Error constructor.
     *
     * @param TranslatorInterface $translator
     * @param ValidatorInterface  $validator
     * @param FieldInterface      $field
     * @param string|\Closure     $message
     * @param mixed               $value
     */
    public function __construct(
        TranslatorInterface $translator,
        ValidatorInterface $validator,
        FieldInterface $field,
        $message,
        $value
    ) {
        $this->translator = $translator;
        $this->validator = $validator;
        $this->field = $field;
        $this->value = $value;
        $this->message = $message;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getValidator(): ValidatorInterface
    {
        return $this->validator;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getField(): FieldInterface
    {
        return $this->field;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMessage()
    {
        $message = $this->message;
        if ($message instanceof \Closure) {
            return $message($this);
        }
        
        return $this->message;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getTranslator(): TranslatorInterface
    {
        return $this->translator;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getValue()
    {
        return $this->value;
    }
}