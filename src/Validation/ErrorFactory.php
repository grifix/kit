<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation;

use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Validation\Field\FieldInterface;
use Grifix\Kit\Validation\Validator\ValidatorInterface;

/**
 * Class ErrorFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ErrorFactory extends AbstractFactory implements ErrorFactoryInterface
{
    
    protected $translator;
    
    /**
     * ErrorFactory constructor.
     *
     * @param TranslatorInterface $translator
     * @param ClassMakerInterface $classMaker
     */
    public function __construct(TranslatorInterface $translator, ClassMakerInterface $classMaker)
    {
        $this->translator = $translator;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createError(
        FieldInterface $field,
        ValidatorInterface $validator,
        $message,
        $value
    ): ErrorInterface {
        $class = $this->makeClassName(Error::class);
        
        return new $class($this->translator, $validator, $field, $message, $value);
    }
}