<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Validation;

use Grifix\Kit\Validation\Field\FieldInterface;
use Grifix\Kit\Validation\Validator\ValidatorInterface;
use Mockery\Matcher\Closure;


/**
 * Class ErrorFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ErrorFactoryInterface
{
    /**
     * @param FieldInterface     $field
     * @param ValidatorInterface $validator
     * @param Closure|string $message
     * @param mixed $value
     *
     * @return ErrorInterface
     */
    public function createError(
        FieldInterface $field,
        ValidatorInterface $validator,
        $message,
        $value
    ): ErrorInterface;
}