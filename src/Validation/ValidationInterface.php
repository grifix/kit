<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Validation;

use Grifix\Kit\Validation\Exception\FieldAlreadyExistsException;
use Grifix\Kit\Validation\Exception\FieldNotExistsException;
use Grifix\Kit\Validation\Exception\InvalidValidationStrategyException;
use Grifix\Kit\Validation\Exception\ValidationException;
use Grifix\Kit\Validation\Field\FieldInterface;


/**
 * Class Validation
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ValidationInterface
{
    const STRATEGY_FIELDS = 'fields'; //validate all fields defined in validation
    const STRATEGY_VALUES = 'values'; //validate only fields that existed in values array
    
    /**
     * @param FieldInterface $field
     *
     * @return ValidationInterface
     * @internal param string $key
     */
    public function addField(FieldInterface $field): ValidationInterface;
    
    /**
     * @param string $name
     * @param string $class
     *
     * @return FieldInterface
     */
    public function createField(string $name, string $class = null): FieldInterface;
    
    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasField(string $name): bool;
    
    /**
     * @param string $name
     *
     * @return FieldInterface
     * @throws FieldNotExistsException
     */
    public function getField(string $name): FieldInterface;
    
    /**
     * @param array $values
     *
     * @return bool
     */
    public function validate(array $values): bool;
    
    /**
     * @return Error[]
     */
    public function getErrors(): array;
    
    /**
     * @param array $values
     *
     * @return void
     * @throws ValidationException
     */
    public function validateOrFail(array $values);
    
    /**
     * @param string $strategy
     *
     * @return ValidationInterface
     * @throws InvalidValidationStrategyException
     */
    public function setStrategy(string $strategy): ValidationInterface;
}