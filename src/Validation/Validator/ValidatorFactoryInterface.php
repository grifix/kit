<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

use Grifix\Kit\Validation\Validator\Exception\InvalidValidatorException;

/**
 * Class ValidatorFactoryInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ValidatorFactoryInterface
{
    
    /**
     * @param string $validatorClass
     *
     * @return ValidatorInterface
     */
    public function createValidator(string $validatorClass): ValidatorInterface;
}