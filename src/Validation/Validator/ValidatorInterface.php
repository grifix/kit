<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Interface ValidatorInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Field\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ValidatorInterface
{
    /**
     * @param mixed $value
     *
     * @return bool
     */
    public function validate($value): bool;
    
    /**
     * @return bool
     */
    public function isEnabled():bool;
    
    /**
     * @return ValidatorInterface
     */
    public function enable():ValidatorInterface;
    
    /**
     * @return ValidatorInterface
     */
    public function disable():ValidatorInterface;
    
    /**
     * @return bool
     */
    public function isAllowedEmpty(): bool;
    
    /**
     * @return ValidatorInterface
     */
    public function allowEmpty(): ValidatorInterface;
    
    /**
     * @return ValidatorInterface
     */
    public function notAllowEmpty(): ValidatorInterface;
    
    /**
     * @return ValidatorInterface
     */
    public function notCancelOnFail(): ValidatorInterface;
    
    /**
     * @return bool
     */
    public function isCancelOnFail(): bool;
    
    /**
     * @return ValidatorInterface
     */
    public function cancelOnFail(): ValidatorInterface;
    
}