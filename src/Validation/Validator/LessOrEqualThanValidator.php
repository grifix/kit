<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class LessOrEqualThanValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class LessOrEqualThanValidator extends AbstractValidator
{
    protected $maxValue;
    
    /**
     * {@inheritdoc}
     */
    protected function doValidate($value): bool
    {
        return $value <= $this->maxValue;
    }
    
    /**
     * @return mixed
     */
    public function getMaxValue()
    {
        return $this->maxValue;
    }
    
    /**
     * @param mixed $maxValue
     *
     * @return LessOrEqualThanValidator
     */
    public function setMaxValue($maxValue)
    {
        $this->maxValue = $maxValue;
        
        return $this;
    }
    
    
}