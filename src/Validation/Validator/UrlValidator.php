<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class UrlValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class UrlValidator extends AbstractValidator
{
    /**
     * {@inheritdoc}
     */
    protected function doValidate($value): bool
    {
        if(filter_var($value, FILTER_VALIDATE_URL)===false){
            return false;
        }
        return true;
    }
}
