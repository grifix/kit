<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

use Grifix\Kit\Kernel\AbstractFactory;


/**
 * Class ValidatorFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ValidatorFactory extends AbstractFactory implements ValidatorFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function createValidator(string $validatorClass): ValidatorInterface
    {
        $class = $this->makeClassName($validatorClass);
        return new $class();
    }
    
}
