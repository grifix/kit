<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class MinNumOfNumbersValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MinNumOfNumbersValidator extends AbstractValidator
{
    
    /**
     * @var int
     */
    protected $numOfNumbers = 1;
    
    /**
     * {@inheritdoc}
     */
    protected function doValidate($value): bool
    {
        $exp=[];
        for($i=0; $i<$this->numOfNumbers; $i++){
            $exp[]='([0-9])';
        }
    
        $regExp="/".implode('.*',$exp)."/";
        return boolval(preg_match($regExp, $value));
    }
    
    /**
     * @param int $numOfNumbers
     *
     * @return MinNumOfNumbersValidator
     */
    public function setNumOfNumbers(int $numOfNumbers): MinNumOfNumbersValidator
    {
        $this->numOfNumbers = $numOfNumbers;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getNumOfNumbers(): int
    {
        return $this->numOfNumbers;
    }
}