<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class EqualValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class EqualValidator extends AbstractValidator
{
    protected $value;
    
    protected $strict = false;
    
    /**
     * {@inheritdoc}
     */
    protected function doValidate($value): bool
    {
        if(!$this->strict){
            return $value == $this->value;
        }
        return $value === $this->value;
    }
    
    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
    
    /**
     * @param mixed $value
     *
     * @return EqualValidator
     */
    public function setValue($value)
    {
        $this->value = $value;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isStrict(): bool
    {
        return $this->strict;
    }
    
    /**
     * @param bool $strict
     *
     * @return EqualValidator
     */
    public function setStrict(bool $strict): EqualValidator
    {
        $this->strict = $strict;
        
        return $this;
    }
    
    
}