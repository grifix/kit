<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class MinNumOfCapsValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MinNumOfCapsValidator extends AbstractValidator
{
    
    /**
     * @var int
     */
    protected $numOfCaps = 1;
    
    /**
     * {@inheritdoc}
     */
    protected function doValidate($value): bool
    {
        $exp=[];
        for($i=0; $i<$this->numOfCaps; $i++){
            $exp[]='([A-Z])';
        }
    
        $regExp="/".implode('.*',$exp)."/";
        return boolval(preg_match($regExp, $value));
    }
    
    /**
     * @param int $numOfCaps
     *
     * @return MinNumOfCapsValidator
     */
    public function setNumOfCaps(int $numOfCaps): MinNumOfCapsValidator
    {
        $this->numOfCaps = $numOfCaps;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getNumOfCaps(): int
    {
        return $this->numOfCaps;
    }
}