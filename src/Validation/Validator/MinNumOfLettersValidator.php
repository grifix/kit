<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class MinNumOfLettersValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MinNumOfLettersValidator extends AbstractValidator
{
    protected $numOfLetters = 1;
    
    /**
     * {@inheritdoc}
     */
    protected function doValidate($value): bool
    {
        $exp=[];
        for($i=0; $i<$this->numOfLetters; $i++){
            $exp[]='([a-zA-Z])';
        }
    
        $regExp="/".implode('.*',$exp)."/";
        return boolval(preg_match($regExp, $value));
    }
    
    /**
     * @return int
     */
    public function getNumOfLetters(): int
    {
        return $this->numOfLetters;
    }
    
    /**
     * @param int $numOfLetters
     *
     * @return MinNumOfLettersValidator
     */
    public function setNumOfLetters(int $numOfLetters): MinNumOfLettersValidator
    {
        $this->numOfLetters = $numOfLetters;
        
        return $this;
    }
}