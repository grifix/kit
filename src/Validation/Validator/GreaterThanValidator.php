<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class GreaterThanValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class GreaterThanValidator extends AbstractValidator
{
    /**
     * @var mixed
     */
    protected $minValue = 0;
    
    /**
     * @param mixed $value
     *
     * @return bool
     */
    protected function doValidate($value): bool
    {
        return $value>$this->minValue;
    }
    
    /**
     * @return int
     */
    public function getMinValue(): int
    {
        return $this->minValue;
    }
    
    /**
     * @param mixed $minValue
     *
     * @return GreaterThanValidator
     */
    public function setMinValue($minValue): GreaterThanValidator
    {
        $this->minValue = $minValue;
        
        return $this;
    }
    
    
}