<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Validator;

/**
 * Class AbstractValidator
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Validator
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractValidator implements ValidatorInterface
{
    /**
     * @var bool
     */
    protected $enabled = true;
    
    /**
     * @var bool
     */
    protected $allowEmpty = true;
    
    /**
     * @var bool
     */
    protected $cancelOnFail = false;
    
    /**
     * @param $value
     *
     * @return bool
     */
    abstract protected function doValidate($value): bool;
    
    /**
     *
     * {@inheritdoc}
     */
    public function validate($value): bool
    {
        if (!$this->isEnabled()) {
            return true;
        }
        
        return $this->doValidate($value);
    }
    
    /**
     * {@inheritdoc}
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }
    
    /**
     * {@inheritdoc}
     */
    public function cancelOnFail(): ValidatorInterface
    {
        $this->cancelOnFail = true;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function notCancelOnFail(): ValidatorInterface
    {
        $this->cancelOnFail = false;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isCancelOnFail(): bool
    {
        return $this->cancelOnFail;
    }
    
    /**
     * {@inheritdoc}
     */
    public function isAllowedEmpty(): bool
    {
        return $this->allowEmpty;
    }
    
    /**
     * {@inheritdoc}
     */
    public function allowEmpty(): ValidatorInterface
    {
        $this->allowEmpty = true;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function notAllowEmpty(): ValidatorInterface
    {
        $this->allowEmpty = false;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function enable(): ValidatorInterface
    {
        $this->enabled = true;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function disable(): ValidatorInterface
    {
        $this->enabled = false;
        
        return $this;
    }
}
