<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation;

use Grifix\Kit\Helper\ArrayHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Validation\Field\FieldFactoryInterface;

/**
 * Class ValidationFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ValidationFactory extends AbstractFactory implements ValidationFactoryInterface
{
    /**
     * @var FieldFactoryInterface
     */
    protected $fieldFactory;
    
    protected $arrayHelper;
    
    /**
     * ValidationFactory constructor.
     *
     * @param FieldFactoryInterface $fieldFactory
     * @param ClassMakerInterface   $classMaker
     * @param ArrayHelperInterface $arrayHelper
     */
    public function __construct(
        FieldFactoryInterface $fieldFactory,
        ClassMakerInterface $classMaker,
        ArrayHelperInterface $arrayHelper
    ) {
        
        $this->fieldFactory = $fieldFactory;
        $this->arrayHelper = $arrayHelper;
        parent::__construct($classMaker);
    }
    
    /**
     * {@inheritdoc}
     */
    public function createValidation(): ValidationInterface
    {
        $class = $this->makeClassName(Validation::class);
        
        return new $class($this->fieldFactory, $this->arrayHelper);
    }
}