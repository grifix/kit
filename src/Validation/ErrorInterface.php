<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Validation;

use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Validation\Field\FieldInterface;
use Grifix\Kit\Validation\Validator\ValidatorInterface;


/**
 * Class Error
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ErrorInterface
{
    /**
     * @return ValidatorInterface
     */
    public function getValidator(): ValidatorInterface;
    
    /**
     * @return FieldInterface
     */
    public function getField(): FieldInterface;
    
    /**
     * @return string
     */
    public function getMessage();
    
    /**
     * @return mixed
     */
    public function getValue();
    
    /**
     * @return TranslatorInterface
     */
    public function getTranslator(): TranslatorInterface;
}