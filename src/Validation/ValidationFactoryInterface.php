<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
namespace Grifix\Kit\Validation;


/**
 * Class ValidationFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface ValidationFactoryInterface
{
    /**
     * @return ValidationInterface
     */
    public function createValidation(): ValidationInterface;
}