<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Validation\Field;

use Grifix\Kit\Validation\Exception\ValidationException;
use Grifix\Kit\Validation\Exception\ValidatorAlreadyExistsException;
use Grifix\Kit\Validation\Exception\ValidatorIsNotExistsException;
use Grifix\Kit\Validation\Validator\NotEmptyValidator;
use Grifix\Kit\Validation\Validator\ValidatorInterface;

/**
 * Interface FieldInterface
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
interface FieldInterface
{
    const PLACE_APPEND = 'append';
    const PLACE_PREPEND = 'prepend';
    
    /**
     * @param ValidatorInterface $validator
     *
     * @param                    $place
     *
     * @return FieldInterface
     */
    public function addValidator(ValidatorInterface $validator, string $place = self::PLACE_APPEND): FieldInterface;
    
    /**
     * @param string $validatorClass
     *
     * @param stirng $place
     *
     * @return ValidatorInterface
     */
    public function createValidator(string $validatorClass, string $place = self::PLACE_APPEND): ValidatorInterface;
    
    /**
     * @param string $validatorClass
     *
     * @return FieldInterface
     */
    public function removeValidator(string $validatorClass): FieldInterface;
    
    /**
     * @param string $validatorClass
     *
     * @return ValidatorInterface
     * @throws ValidatorIsNotExistsException
     */
    public function getValidator(string $validatorClass): ValidatorInterface;
    
    /**
     * @param string $validatorClass
     *
     * @return bool
     */
    public function hasValidator(string $validatorClass): bool;
    
    /**
     * @return bool
     */
    public function isEnabled(): bool;
    
    /**
     * @return FieldInterface
     */
    public function enable(): FieldInterface;
    
    /**
     * @return FieldInterface
     */
    public function disable(): FieldInterface;
    
    /**
     * @return array
     */
    public function getErrors(): array;
    
    /**
     * @param $value
     *
     * @return bool
     */
    public function validate($value): bool;
    
    /**
     * @param $value
     *
     * @return void
     * @throws ValidationException
     */
    public function validateOrFail($value);
    
    /**
     * @param string $validatorClass
     * @param string|\Closure $message
     *
     * @return FieldInterface
     */
    public function setMessage(string $validatorClass, $message): FieldInterface;
    
    /**
     * @return string
     */
    public function getLabel(): string;
    
    /**
     * @param string $validatorClass
     *
     * @return string|\Closure
     */
    public function getMessage(string $validatorClass);
    
    /**
     * @return string
     */
    public function getName(): string;
    
    /**
     * @param string $name
     *
     * @return Field
     */
    public function setName(string $name): FieldInterface;
    
    /**
     * @param string $label
     *
     * @return FieldInterface
     */
    public function setLabel(string $label): FieldInterface;
    
    /**
     * @param bool $notEmpty
     *
     * @return FieldInterface
     */
    public function setNotEmpty(bool $notEmpty = true): FieldInterface;
    
    /**
     * @param ValidatorInterface $validator
     *
     * @return bool
     */
    public function addValidatorIfNotExists(ValidatorInterface $validator): bool;
    
    /**
     * @param string $validatorClass
     *
     * @param string $place
     *
     * @return ValidatorInterface
     */
    public function createValidatorIfNotExists(
        string $validatorClass,
        string $place = self::PLACE_APPEND
    ): ValidatorInterface;
}