<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Field;


use Grifix\Kit\Validation\ErrorFactoryInterface;
use Grifix\Kit\Validation\Validator\EmailValidator;
use Grifix\Kit\Validation\Validator\ValidatorFactoryInterface;

/**
 * Class EmailField
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Field
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class EmailField extends Field
{
    /**
     * IntField constructor.
     *
     * @param string                    $name
     * @param ValidatorFactoryInterface $validatorFactory
     * @param ErrorFactoryInterface     $errorFactory
     * @param null                      $label
     * @param array                     $messages
     */
    public function __construct(
        $name,
        ValidatorFactoryInterface $validatorFactory,
        ErrorFactoryInterface $errorFactory,
        $label = null,
        array $messages = []
    ) {
        parent::__construct($name, $validatorFactory, $errorFactory, $label, $messages);
        $this->createValidator(EmailValidator::class);
    }
}