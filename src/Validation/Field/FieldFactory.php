<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Field;


use Grifix\Kit\Helper\StringHelperInterface;
use Grifix\Kit\Kernel\AbstractFactory;
use Grifix\Kit\Kernel\ClassMakerInterface;
use Grifix\Kit\Validation\ErrorFactoryInterface;
use Grifix\Kit\Validation\Validator\ValidatorFactoryInterface;

/**
 * Class FieldFactory
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FieldFactory extends AbstractFactory implements FieldFactoryInterface
{
    
    /**
     * @var ValidatorFactoryInterface
     */
    protected $validatorFactory;
    
    /**
     * @var ErrorFactoryInterface
     */
    protected $errorFactory;
    
    /**
     * @var array
     */
    protected $messages = [];
    
    /**
     * @var StringHelperInterface
     */
    protected $stringHelper;
    
    /**
     * FieldFactory constructor.
     *
     * @param ValidatorFactoryInterface $validatorFactory
     * @param ErrorFactoryInterface     $errorFactory
     * @param array                     $messages <cfg:grifix.kit.validation.messages>
     * @param ClassMakerInterface       $classMaker
     */
    public function __construct(
        ValidatorFactoryInterface $validatorFactory,
        ErrorFactoryInterface $errorFactory,
        array $messages,
        ClassMakerInterface $classMaker
    ) {
        $this->validatorFactory = $validatorFactory;
        $this->errorFactory = $errorFactory;
        $this->messages = $messages;
        parent::__construct($classMaker);
        
    }
    
    /**
     * {@inheritdoc}
     */
    public function createField(string $name, string $class = null): FieldInterface
    {
        if (is_null($class)) {
            $class = Field::class;
        }
        $class = $this->makeClassName($class);
        
        return new $class(
            $name,
            $this->validatorFactory,
            $this->errorFactory,
            $name,
            $this->messages
        );
    }
}