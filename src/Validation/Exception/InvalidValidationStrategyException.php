<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Exception;

/**
 * Class InvalidValidationStrategyException
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class InvalidValidationStrategyException extends \Exception
{
    protected $message = 'Invalid validation strategy!';
}