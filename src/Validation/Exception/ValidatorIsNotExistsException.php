<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Exception;

/**
 * Class ValidatorIsNotExistsException
 *
 * @category Grifix
 * @package  Grifix\Kit\Field\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ValidatorIsNotExistsException extends \Exception
{
    protected $validatorClass;
    
    /**
     * ValidatorIsNotExistsException constructor.
     *
     * @param string $converterClass
     */
    public function __construct(string $converterClass)
    {
        $this->message = 'Validator "' . $converterClass . '" is not exists in this set!';
        parent::__construct();
    }
}