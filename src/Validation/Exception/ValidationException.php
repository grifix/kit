<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Validation\Exception;

use Grifix\Kit\Cache\Exception\InvalidArgumentException;

use Grifix\Kit\Validation\Error;

/**
 * Class ValidationException
 *
 * @category Grifix
 * @package  Grifix\Kit\Validation\Exception
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ValidationException extends InvalidArgumentException
{
    protected $errors = [];
    
    /**
     * ValidationException constructor.
     *
     * @param array           $errors
     * @param int             $code
     * @param \Exception|null $previous
     */
    public function __construct(array $errors, int $code = 0, \Exception $previous = null)
    {
        $this->errors = $errors;
        $this->message = $this->makeMessage($errors);
        parent::__construct($this->message, $code, $previous);
    }
    
    /**
     * @return Error[]
     */
    public function getErrors()
    {
        return $this->errors;
    }
    
    /**
     * @param array $errors
     *
     * @return string
     */
    protected function makeMessage(array $errors)
    {
        $result = [];
        foreach ($errors as $key => $error) {
            if (is_array($error)) {
                $result[] = $key . '(' . $this->makeMessage($error) . ')';
            } else {
                $result[] = $error->getMessage();
            }
        }
        
        return implode("\n", $result);
    }
}