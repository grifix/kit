<?php
declare(strict_types = 1);
/**@var $this \Grifix\Kit\View\View */
/**@var $exception \Exception */
$exception = $this->getVar('exception');
$result = [
    'code' => $exception->getCode(),
    'message' => $exception->getMessage(),
];

if ($exception instanceof \Grifix\Kit\Validation\Exception\ValidationException) {
    $result['errors'] = [];
    foreach ($exception->getErrors() as $error) {
        $result['errors'][] = [
            'field' => $error->getField()->getName(),
            'message' => $error->getMessage(),
        ];
    }
}

if ($this->getVar('debug')) {
    $result['file'] = $exception->getFile();
    $result['line'] = $exception->getLine();
    $result['trace'] = $exception->getTraceAsString();
    $result['class'] = get_class($exception);
    if ($exception instanceof \Grifix\Kit\Exception\UserException) {
        $result['previous'] = [
            'message' => $exception->getPrevious()->getMessage(),
            'file' => $exception->getPrevious()->getFile(),
            'line' => $exception->getPrevious()->getLine(),
            'trace' => $exception->getPrevious()->getTraceAsString(),
            'class' => get_class($exception->getPrevious()),
        ];
    }
}
echo json_encode($result);
