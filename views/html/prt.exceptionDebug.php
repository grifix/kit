<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
/**@var $this \Grifix\Kit\View\View */
/**@var $exception \Exception */
$exception = $this->getVar('exception');
if ($exception instanceof \Grifix\Kit\Exception\UserException) {
    $exception = $exception->getPrevious();
}

?>
<div class="panel panel-default">
    <div class="panel-heading">
        <?= get_class($exception) ?>:
        <?= $exception->getMessage() ?> in
        <?= $exception->getFile() ?>
        on line <?= $exception->getLine() ?>
    </div>
    <div class="panel-body">
        <pre><?= $exception->getTraceAsString() ?></pre>
    </div>
    
</div>

