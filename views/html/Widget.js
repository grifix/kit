/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/*require {src}/grifix/kit/views/{skin}/gfx.js*/
jQuery(function($) {
    "use strict";
    
    gfx.namespace('gfx');
    
    gfx.Widget = function (el, opt) {
        if(!opt){
            opt = {};
        }
        
        var that = this;
    
        //Gets widget options from attribute
        var optFromAttr = el.attr('data-' + that.name + '-opt');
        if(!optFromAttr){
            optFromAttr = el.attr('data-opt');
        }
    
        //If attribute has options
        if (optFromAttr) {
            opt = $.extend(true, {}, $.parseJSON(optFromAttr), opt);
        }
        else{
            opt = $.extend(true, {}, el.data(), opt);
        }
    
        that.cfg = $.extend(true, {}, that.cfg, opt);
    
        //Protect options from changes
        that.opt = function () {
            return opt;
        };
    
        //Protect element from changes
        that.el = function () {
            return el;
        };
    
        that.defineCfg();
    };
    
    var constructor = gfx.createConstructor(gfx.Widget, 'gfx.Widget', Object);
    var prototype = constructor.prototype;
    
    /**
     * Default options
     *
     * @type {{}}
     */
    prototype.defaults = {};
    
    prototype.cfg = {
        on: {
            init: function () {
                
            }
        }
    };
    
    /**
     * Initialize widget
     */
    prototype.init = function () {
        var that = this;
        if(!that.cfg.on){
            that.cfg.on = {};
        }
        
        $.each(that.cfg.on, function (eventName, handlers) {
            if (handlers instanceof Function) {
                that.cfg.on[eventName] = [handlers];
            }
        });
        
        gfx.initWidgets(that.el());
        that.fireEvent('init');
    };
    
    prototype.destroy = function () {
        this.el().data(this.name,null);
        return this.el();
    };
    
    /**
     * Returns widget DOM-element
     */
    prototype.el = function () {
        
    };
    
    /**
     * Возвращает опции заданный при инициализации
     */
    prototype.opt = function () {
        
    };
    
    /**
     * Ищет елемент по роли
     *
     * @param {string} role
     *
     * @return {jQuery}
     */
    prototype.find = function (role) {
        var that = this;
        var result = that.el().find('[data-'+that.name+'-role='+role+']');
        if(!result.length){
            result = that.el().find('[data-role='+role+']');
        }
        return result;
    };
    
    /**
     * Создает конфигрурацию виджета
     *
     * @param defaults
     */
    prototype.defineCfg = function (defaults) {
        var that = this;
        if(!defaults){
            defaults = {};
        }
        that.cfg = $.extend(true, {}, that.defaults, defaults, that.cfg);
    };
    
    /**
     * fire plugin event
     *
     * @param {string} eventName
     * @param {object} params
     *
     * @return {jQuery}
     */
    prototype.fireEvent = function (eventName, params) {
        var that = this;
        that.el().trigger(that.name + '.' + eventName, params);
        if (that.cfg.on[eventName]) {
            $.each(that.cfg.on[eventName], function (k, handler) {
                handler.call(that, params);
            });
        }
        return that.el();
    };
    
    /**
     * Call parent method
     *
     * @param method
     *
     * @return mixed
     */
    prototype.callParent = function (method) {
        var clone = $.extend({}, this);
        var params = Array.prototype.slice.call(arguments);
        params.shift();
        clone.parent = this.parent.prototype.parent;
        clone.parents = this.parent.prototype.parents;
        return this.parent.prototype[method].apply(clone, params);
    };
    
    /**
     *
     * @param eventName
     * @param handler
     * @return {_on}
     * @private
     */
    prototype._on = function (eventName, handler) {
        var that = this;
        
        if (!that.cfg.on[eventName]) {
            that.cfg.on[eventName] = [];
        }
        else if (!Array.isArray(that.cfg.on[eventName])) {
            that.cfg.on[eventName] = [that.cfg.on[eventName]];
        }
        that.cfg.on[eventName].push(handler);
        return that;
    };
    
    /**
     * Attach event handler
     *
     * @param {string|Array} eventName
     * @param {function} handler
     *
     * @return {Object}
     */
    prototype.on = function (eventName, handler) {
        var that = this;
        if (Array.isArray(eventName)) {
            $.each(eventName, function () {
                that._on(this, handler);
            });
        } else {
            that._on(eventName, handler);
        }
    };
    
    /**
     * Sets plugin html source
     *
     * @param {string|Object} source html-code of ajax config to get source from server
     * @param {bool|string} extend extend current source (true, false, 'recursive')
     *
     * @return {void}
     */
    prototype.setSource = function (source, extend) {
        var that = this;
        var recursive = false;
        if (extend == 'recursive') {
            recursive = true;
        }
        if ((source instanceof Object) && extend) {
            that.source = $.extend(recursive, {}, that.source, source);
        }
        else {
            that.source = source;
        }
    };
    
    /**
     * Sets plugin configuration
     *
     * @param {Object} cfg
     * @param {bool|string} extend extend current configuration (true, false, 'recursive')
     */
    prototype.setCfg = function (cfg, extend) {
        var that = this;
        var recursive = false;
        if (extend == 'recursive') {
            recursive = true;
        }
        if (extend) {
            that.cfg = $.extend(recursive, {}, that.cfg, cfg);
        }
        else {
            that.cfg = cfg;
        }
    };
    
    /**
     * Gets plugin html source
     * @param {function} handler
     * @param {Object|string|null} extendedSource ajax query config or html
     */
    prototype.getSource = function (handler, extendedSource) {
        var that = this;
        
        if ((that.source instanceof Object) || (extendedSource instanceof Object)) {
            if (!extendedSource) {
                extendedSource = {};
            }
            
            if (that.source instanceof Object) {
                extendedSource = $.extend(true, {}, that.source, extendedSource);
            }
            
            var success = extendedSource.success;
            //noinspection JSUndefinedPropertyAssignment
            extendedSource.success = function (data) {
                if (success) {
                    success.call(this, data);
                }
                handler.call(that, data);
                
            };
            $.ajax(extendedSource);
        }
        else {
            if (!extendedSource) {
                extendedSource = that.source;
            }
            if (!extendedSource) {
                throw new Error('No widget source!');
            }
            handler.call(that, extendedSource);
        }
    };
    
    /**
     * Reloads plugin (reload html and init plugin)
     *
     * @param {Object} p
     *      {Object} opt plugin options
     *      {string|Object} source html code or ajax config
     *      {bool} extendCurrentSource true false 'recursive'
     * @param handler
     */
    prototype.reload = function (p, handler) {
        
        var that = this;
        
        var def = {
            source: that.source,
            extendCurrentSource: 'recursive',
            extendOpt: 'recursive'
        };
        
        p = $.extend(true, {}, def, p);
        
        
        that.getSource(function (data) {
            that.el().html($(data).html());
            
            if (p.source) {
                that.setSource(p.source, p.extendCurrentSource);
            }
            
            if (p.opt) {
                that.setCfg(p.opt, p.extendOpt);
            }
            
            that.init();
            
            if (handler) {
                handler.call(that);
            }
            
        }, p.source);
    };
    
    /**
     *
     * @param html
     */
    prototype.setHtml = function(html){
       var that = this;
       that.el().html(html);
       that.init();
    };
    
});
