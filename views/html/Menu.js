/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
jQuery(function ($) {
    "use strict";
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        gfx.Widget,
        {
            trigger: 'click',
            items: [],
            position: 's',
            cssClass: 'menu'
        },
        {
            init: function () {
                var that = this;
                that.parent.prototype.init.call(that);
                that.el().on(that.cfg.trigger, function () {
                    that.show();
                });
                $('body').click(function (e) {
                    if(!that.isIn(e.originalEvent.clientX, e.originalEvent.clientY)){
                        that.hide();
                    }
                });
            },
            
            show: function () {
                var that = this;
                if(!that.elMenu){
                    that.render();
                }
                that.elMenu.show();
            },
            
            hide: function () {
                var that = this;
                that.elMenu.hide();
            },
            
            render: function () {
                var that = this;
                var html = '<ul class="' + that.cfg.cssClass + '">';
                $.each(that.cfg.items, function (k, v) {
                    html += that.renderItem(v);
                });
                html += '</ul>';
                that.elMenu = $(html);
                $('body').append(that.elMenu);
            },
    
            /**
             * Check if coordinates are in box
             * @protected
             * @param x
             * @param y
             * @return {boolean}
             */
            isIn: function (x, y) {
                var that = this;
                return (x > that.el().offset().left && x < that.el().offset().left + that.el().outerWidth())
                    && (y > that.el().offset().top && y < that.el().offset().top + that.el().outerHeight())
                
            },
            
            renderItem: function (item) {
                var that = this;
                var result = '<li data-id="' + item.id + '">' + item.text;
                if (item.children) {
                    result += '<ul style="display: none">';
                    $.each(item.children, function (k, v) {
                        result += that.renderItem(v);
                    });
                    result += '</ul>'
                }
                result += '</li>';
                
                return result;
            }
        }
    );
});