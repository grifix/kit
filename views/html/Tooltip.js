/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/*require {src}/grifix/kit/views/{skin}/gfx.js*/
/*require {src}/grifix/kit/views/{skin}/Widget.js*/
jQuery(function ($) {
    "use strict";
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        gfx.Widget,
        {
            content: null,
            cssClass: 'tooltip',
            position: 'n',
            offset: 5,
            arrow: true,
            autoPosition: 'clockwise' //counterclockwise
        },
        {
            /**
             * @protected
             */
            positions: [
                'n', 'ne', 'e', 'se', 's', 'sw', 'w', 'nw'
            ],
    
            /**
             * @protected
             */
            init: function () {
                var that = this;
                that.callParent('init');
                that.render();
                that.el().on('mouseover', function () {
                    that.show();
                });
                
                that.el().on('mouseout', function () {
                    that.hide();
                });
            },
    
            /**
             * @protected
             */
            render: function () {
                var that = this;
                if (!that.elTooltip) {
                    that.elTooltip = $('<div style="display:none; position: absolute; left: 0; top: 0;" class="' + that.cfg.cssClass + '">' + that.cfg.content + '</div>');
                    $('body').append(that.elTooltip);
                }
            },
    
            /**
             * @public
             * @return {*}
             */
            show: function () {
                var that = this;
                var position = that.calculatePosition(that.cfg.position);
                
                that.elTooltip.css({
                    top: position.top,
                    left: position.left,
                    display: 'inline-block'
                });
                if (that.cfg.arrow) {
                    that.addArrow(position.arrow);
                }
                return that.el();
            },
    
            /**
             * @protected
             * @param position
             */
            addArrow: function (position) {
                var that = this;
                that.elTooltip.removeClass(that.cfg.cssClass + '-arrow-top');
                that.elTooltip.removeClass(that.cfg.cssClass + '-arrow-bottom');
                that.elTooltip.removeClass(that.cfg.cssClass + '-arrow-left');
                that.elTooltip.removeClass(that.cfg.cssClass + '-arrow-right');
                that.elTooltip.addClass(that.cfg.cssClass + '-arrow-' + position);
            },
    
            /**
             * @protected
             * @param position
             * @return {{left: number, top: number, arrow: string}}
             */
            calculatePosition: function (position) {
                var that = this;
                var result = {
                    left: 0,
                    top: 0,
                    arrow: 'top'
                };
                switch (position) {
                    case 'nw':
                        result.top = that.el().offset().top - that.elTooltip.outerHeight() - that.cfg.offset;
                        result.left = that.el().offset().left - that.elTooltip.outerWidth() / 2;
                        result.arrow = 'bottom';
                        break;
                    case 'n':
                        result.top = that.el().offset().top - that.elTooltip.outerHeight() - that.cfg.offset;
                        result.left = that.el().offset().left + (that.el().outerWidth() - that.elTooltip.outerWidth()) / 2;
                        result.arrow = 'bottom';
                        break;
                    case 'ne':
                        result.top = that.el().offset().top - that.elTooltip.outerHeight() - that.cfg.offset;
                        result.left = that.el().offset().left + that.el().outerWidth() - (that.elTooltip.outerWidth() / 2);
                        result.arrow = 'bottom';
                        break;
                    case 'e':
                        result.top = that.el().offset().top + (that.el().outerHeight() / 2 - that.elTooltip.outerHeight() / 2);
                        result.left = that.el().offset().left + that.el().outerWidth() + that.cfg.offset;
                        result.arrow = 'left';
                        break;
                    case 'se':
                        result.top = that.el().offset().top + that.el().outerHeight() + that.cfg.offset;
                        result.left = that.el().offset().left + that.el().outerWidth() - (that.elTooltip.outerWidth() / 2);
                        result.arrow = 'top';
                        break;
                    case 's':
                        result.top = that.el().offset().top + that.el().outerHeight() + that.cfg.offset;
                        result.left = that.el().offset().left + (that.el().outerWidth() - that.elTooltip.outerWidth()) / 2;
                        result.arrow = 'top';
                        break;
                    case 'sw':
                        result.top = that.el().offset().top + that.el().outerHeight() + that.cfg.offset;
                        result.left = that.el().offset().left - that.elTooltip.outerWidth() / 2;
                        result.arrow = 'top';
                        break;
                    case 'w':
                        result.top = that.el().offset().top + (that.el().outerHeight() / 2 - that.elTooltip.outerHeight() / 2);
                        result.left = that.el().offset().left - (that.elTooltip.outerWidth() + that.cfg.offset);
                        result.arrow = 'right';
                        break;
                    default:
                        result.top = that.el().offset().top - that.elTooltip.outerHeight() - that.cfg.offset;
                        result.left = that.el().offset().left + (that.el().outerWidth() - that.elTooltip.outerWidth()) / 2;
                        result.arrow = 'bottom';
                }
                
                if (
                    (result.left < window.pageXOffset)
                    || (result.top < window.pageYOffset)
                    || ((result.left + that.elTooltip.outerWidth()) > window.innerWidth+window.pageXOffset)
                    || ((result.top + that.elTooltip.outerHeight()) > window.innerHeight+window.pageYOffset)
                ) {
                    result = that.calculatePosition(that.defineAutoPosition(position));
                }
                
                
                return result;
            },
    
            /**
             * @protected
             * @param position
             * @return {*}
             */
            defineAutoPosition: function (position) {
                var that = this;
                var positionIndex = null;
                for(var i in that.positions){
                    if(that.positions[i]==position){
                        positionIndex = i;
                        break;
                    }
                }
                if(that.cfg.autoPosition == 'clockwise'){
                    positionIndex++;
                }
                else{
                    positionIndex--;
                }
                if(positionIndex < 0){
                    positionIndex = that.positions.length-1;
                }
                if(positionIndex > that.positions.length-1){
                    positionIndex = 0;
                }
                return that.positions[positionIndex];
            },
    
            /**
             * @public
             * @return {*}
             */
            hide: function () {
                var that = this;
                that.elTooltip.hide();
                return that.el();
            }
        }
    );
});