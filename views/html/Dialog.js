/*
* (c) Mike Shapovalov <smikebox@gmail.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/
/*require {src}/grifix/kit/views/{skin}/Widget.js*/
jQuery(function ($) {
    "use strict";
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        gfx.Widget,
        {
            draggable: false,
            cssClass: null,
            title: null,
            isModal: false,
            closable: true,
            target: $('body'),
            width: '400px'
        },
        {
            init: function () {
                var that = this;
                that.callParent('init');
            },
            
            show: function () {
                this.adapter.show();
                return this.el();
            },
            
            hide: function () {
                this.adapter.hide();
                return this.el();
            }
        }
    );
});