<?php
declare(strict_types = 1);
/**@var $this \Grifix\Kit\View\ViewInterface */
?>
<html>
<head>
    <title><?php $this->startBlock('title') ?>Grifix<?php $this->endBlock() ?></title>
    
    <?php $this->includeJsPack('vendor', [
        'jquery/dist/jquery.js',
        'jquery-form/src/jquery.form.js',
        '{src}/grifix/kit/views/{skin}'
    ]) ?>
    
    <?php $this->includeCssPack('global', [
        '{src}/grifix/kit/views/{skin}'
    ]) ?>
    
    <?php $this->startBlock('head') ?>
    
    <?php $this->endBlock() ?>
    <?php $this->addJs('{src}/grifix/admin/views/{skin}/navBar') ?>
    <?php $this->addCss('{src}/grifix/kit/views/{skin}/lyt.default.css') ?>
    <?php $this->addCss('{src}/grifix/kit/views/{skin}/input') ?>
    
    <?php $this->includeViewJs() ?>
    <?php $this->includeViewCss() ?>
    <script type="text/javascript">
        jQuery(function () {
            gfx.initWidgets();
            gfx.setUp();
        });
    </script>
</head>
<body class="<?=$this->getCssClass()?>">
<?php $this->startBlock('content') ?>

<?php $this->endBlock() ?>
</body>
</html>