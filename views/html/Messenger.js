/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/*require {src}/grifix/kit/views/{skin}/gfx.js*/
/*require {src}/grifix/kit/views/{skin}/Widget.js*/
jQuery(function ($) {
    "use strict";
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        gfx.Widget,
        {
            position: 'n',
            timeout: 5000,
            offset: 2
        },
        {
            queue: [],
            /**
             * @protected
             */
            init: function () {
                var that = this;
                that.callParent('init');
                that.setInterval();
            },
            
            showMessage: function (content, type) {
                if (!type) {
                    type = 'info';
                }
                var that = this;
                that.setInterval();
                var elMessage = $('<div style="position:absolute; left:0; top:0" class="messenger-message messenger-' + type + '">' + content + '<a data-role="close" href="#" class="close-thik"></a> </div>');
                elMessage.find('[data-role="close"]').click(function () {
                    that.hideMessage(elMessage);
                });
                that.el().append(elMessage);
                elMessage.css(that.calculateMessagePosition(elMessage))
                that.queue.unshift(elMessage);
            },
            
            /**
             * @protected
             * @param elMessage
             */
            hideMessage: function (elMessage) {
                var that = this;
                for (var i = 0; i < that.queue.length; i++) {
                    if (that.queue[i] === elMessage) {
                        that.queue.splice(i, 1);
                    }
                }
                elMessage.hide(0, function () {
                    elMessage.remove();
                });
            },
            
            /**
             * @protected
             */
            setInterval: function () {
                var that = this;
                
                if (that.cfg.timeout) {
                    if (that.interval) {
                        clearInterval(that.interval);
                    }
                    that.interval = setInterval(function () {
                        var elMessage = that.queue.pop();
                        console.log(elMessage);
                        if (elMessage) {
                            that.hideMessage(elMessage);
                        }
                    }, that.cfg.timeout)
                }
            },
            calculateMessagePosition: function (elMessage) {
                var that = this;
                var result = {
                    top: 0,
                    left: 0
                };
                
                //TODO implement ne nw and others
                switch (that.cfg.position) {
                    case 'n':
                        console.log(elMessage.outerWidth());
                        result.left = that.el().offset().left + (that.el().outerWidth() / 2) - (elMessage.outerWidth() / 2);
                        result.top = that.el().offset().top + that.cfg.offset;
                        if (result.top < window.pageYOffset) {
                            result.top = window.pageYOffset + that.cfg.offset;
                        }
                        break;
                }
                var elLastMessage = null;
                
                if (that.queue.length) {
                    elLastMessage = that.queue[0];
                    result.top = elLastMessage.offset().top + elLastMessage.outerHeight() + that.cfg.offset;
                }
                
                return result;
            }
        }
    );
});