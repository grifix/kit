/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/*require public/assets/jquery/dist/jquery.min.js*/

var gfx = {
    widgets: {
        //Names of registered widgets
        names: [],
        
        //Constructors of registered widgets
        constructors: [],
    },
    ajaxErrorsHandlers: []
};

jQuery(function ($) {
    
    "use strict";
    
    /**
     * Заглушка для подмениы парсером PHP
     */
    gfx.createWidgetName = function () {
        
    };
    
    /**
     * Заглушка для подмены парсером PHP
     */
    gfx.createConstructorName = function () {
        
    };
    
    gfx.requestRunnerUrl = '/grifix/common/request/';
    
    /**
     *
     * @param {function} constructor - constructor function
     * @param {string} name - name of the constructor (e.g. grifix.kit.Widget)
     * @param {Array|Object} parents - parent constructor of array of parent constructors
     */
    gfx.createConstructor = function (constructor, name, parents) {
        
        //Make constructor namespace
        var arr = name.toString().split(".".toString());
        arr.pop();
        gfx.namespace(arr.join('.'));
        
        if (!parents) {
            parents = [Object];
        }
        
        if (!$.isArray(parents)) {
            parents = [parents];
        }
        
        var prototype = {};
        
        $.each(parents, function () {
            var ParentConstructor = this;
            
            //Extend only envo objects (for AbstractFeatureContext compatibility)
            if (ParentConstructor.prototype.constructorName) {
                prototype = $.extend(true, prototype, ParentConstructor.prototype);
            }
        });
        
        constructor.prototype = $.extend(true, {}, prototype, constructor.prototype);
        
        constructor.prototype.constructorName = name;
        
        constructor.prototype.parents = parents;
        
        
        constructor.prototype.parent = parents[0];
        
        //add name to global namespace
        eval(name + '=constructor;'); // jshint ignore:line
        
        return constructor;
    };
    
    /**
     * Finds elements by plugin name
     *
     * @param {jQuery} collection jquery collection
     * @param {string} plugName plugin name
     *
     * @return {Array}
     */
    gfx.findElementsByPlugin = function (collection, plugName) {
        var result = [];
        $.each(collection, function () {
            if ($(this).data(plugName)) {
                result.push($(this));
            }
        });
        return result;
    };
    
    /**
     *
     * @param {string} namespace
     */
    gfx.namespace = function (namespace) {
        var parts = namespace.split('.');
        var box = window;
        $.each(parts, function (i, part) {
            if (!box[part]) {
                box[part] = {};
            }
            box = box[part];
        });
    };
    
    /**
     * Init widgets and attach it to elements by css-class
     *
     * @param {jQuery} elContainer a container in which you want to attach plugins
     *
     * @return {void}
     */
    gfx.initWidgets = function (elContainer) {
        
        if (!elContainer) {
            elContainer = $('body');
        }
        
        $.each(gfx.widgets.names, function (k, widgetName) {
            
            var elements = elContainer.find('.wg-' + widgetName);
            
            elements.each(function () {
                
                var el = $(this);
                
                //if element exists and plugin is not attached
                if (el && el.length && !el.data(widgetName)) {
                    
                    //Attach plugin
                    el[widgetName]({});
                }
            });
        });
    };
    
    /**
     * Register new widget and makes jquery plugin
     *
     * @param {string} widgetName name of plugin
     * @param {function} widgetConstructor plugin constructor
     *
     * @return {jQuery|*}
     */
    gfx.registerWidget = function (widgetName, widgetConstructor) {
        
        //Makes jquery plugin
        $.fn[widgetName] = function (param) {
            var args = arguments;
            var result = this;
            
            this.each(function () {
                var el = $(this);
                
                //If the first params is an widget options object, the widget is not yet attached to element
                if ((typeof param == 'object' || param === undefined) && !el.data(widgetName)) {
                    
                    el.data(widgetName, new widgetConstructor(el, param));
                    
                    try {
                        el.data(widgetName).init();
                    }
                    catch (e) {
                        throw e;
                    }
                    
                }
                //If the first param is a string, run the widget method which is equal to this string
                else if (typeof param == 'string') {
                    
                    var widget = el.data(widgetName);
                    
                    if (!widget) {
                        throw new Error(widgetName + " was not attached to element!");
                    }
                    console.log(param);
                    var r = widget[param].apply(widget, Array.prototype.slice.call(args, 1));
                    
                    //If result is no jquery collection
                    if (r[0] !== el[0]) {
                        result = r;
                        return false;
                    }
                }
            });
            return result;
        };
        
        gfx.widgets.names.push(widgetName);
        
        gfx.widgets.constructors[widgetName] = widgetConstructor;
    };
    
    /**
     * Creates the widget constructor
     *
     * @param {string} constructorName constructor name e.g. demo.news.Grid
     * @param {string} widgetName name of widget e.g. demo_news_Grid
     * @param {Array | function} parentConstructors constructors of parent widgets
     *
     * @returns {Function}
     */
    gfx.createWidgetConstructor = function (constructorName, widgetName, parentConstructors) {
        //make widget constructor
        var constructor = function (el, opt) {
            gfx.Widget.call(this, el, opt);
        };
        eval("constructor=gfx.createConstructor(constructor,constructorName,parentConstructors);"); // jshint ignore:line
        
        constructor.prototype.name = widgetName;
        
        gfx.registerWidget(widgetName, constructor);
        
        return constructor;
    };
    
    /**
     * Creates tne widget prototype
     *
     * @param {string} constructorName - constructor name e.g. demo.news.Grid
     * @param {string} widgetName - widgetName name of plugin e.g. demo_news_grid
     * @param {function} parentConstructors -  constructors of parent classes
     * @param {object} defaults - plugin default options
     * @param {object} logic - logic of widget
     *
     * @returns {object}
     */
    gfx.createWidgetPrototype = function (constructorName, widgetName, parentConstructors, defaults, logic) {
        if (!logic) {
            throw new Error('Widget must have a logic!');
        }
        if (!defaults) {
            defaults = {};
        }
        var result = gfx.createWidgetConstructor(constructorName, widgetName, parentConstructors).prototype;
        
        result.defaults = $.extend(true, {}, result.defaults, defaults);
        
        return $.extend(result, logic);
    };
    
    /**
     *
     * @param {string} name e.g. grifix.kit.Validator
     * @param {function} constructor
     * @param {array|object|null} parents
     * @param {object} logic
     */
    gfx.createPrototype = function (name, parents, constructor, logic) {
        if (!logic) {
            throw new Error('Prototype must have a logic!');
        }
        var result = gfx.createConstructor(constructor, name, parents);
        $.extend(result.prototype, logic);
        return result;
    };
    
    /**
     * Makes new dom-element with attached plugin
     * @param {Object} p
     *  {string} name widget name
     *  {object} opt plugin option
     *  {object|string} source plugin source code or ajax jquery config to get source
     *  {bool|string} extendCurrentSource extend source ajax params
     *      true - extend
     *      'recursive' - extend recursively
     *      false - no extend
     * @param {function} onSuccess runs after attach plugin to dom-element
     */
    gfx.createWidget = function (p, onSuccess) {
        
        if (!p.name) {
            throw new Error("No widget name!");
        }
        
        if (!onSuccess) {
            throw new Error("Can't make " + p.name + " widget, onSuccess function is not defined!");
        }
        
        var widgetConstructor = gfx.widgets.constructors[p.name];
        
        if (!widgetConstructor) {
            throw new Error('Widget "' + p.name + '" constructor is not exists, maybe widget file was not included!');
        }
        
        p = $.extend(true, {
            name: null,
            opt: {},
            extendCurrentSource: 'recursive'
        }, p);
        
        widgetConstructor.prototype.getSource(function (html) {
            var el = $(html);
            
            eval("el." + p.name + "(p.opt);"); // jshint ignore:line
            
            if (p.source) {
                el.data(p.name).setSource(p.source, p.extendCurrentSource);
            }
            
            onSuccess(el);
        }, p.source);
    };
    
    /**
     *
     * @param requestAlias
     * @param viewAlias
     * @return {string}
     */
    gfx.makeWidgetUrl = function (requestAlias, viewAlias) {
        return gfx.requestRunnerUrl + requestAlias + '/' + viewAlias;
    };
    
    /**
     *
     * @param requestsAlias
     * @return {string}
     */
    gfx.makeRequestUrl = function (requestsAlias) {
        return gfx.requestRunnerUrl + requestsAlias + '/grifix.kit.json.tpl.default';
    };
    
    /**
     *
     * @param {array} widgets
     */
    gfx.reloadWidgets = function (widgets) {
        
        var requests = [];
        $.each(widgets, function (i, v) {
            if (!v.params) {
                v.params = {};
            }
            var widget = v.el.data(v.name);
            
            if (!widget.source && !widget.source.url) {
                throw new Error('Widget has no url');
            }
            
            if (widget.source.url.indexOf(gfx.requestRunnerUrl) === -1) {
                throw new Error('Widget source url "' + widget.source.url + '" is not a request runner url!');
            }
            
            if (!widget.source.data) {
                widget.source.data = {};
            }
            
            var url = widget.source.url.replace(gfx.requestRunnerUrl, '');
            var arr = url.split('/');
            
            requests.push({
                alias: arr[0],
                view: arr[1],
                params: $.extend(true, {}, widget.source.data, v.params)
            });
        });
        
        $.ajax({
            url: gfx.requestRunnerUrl,
            data: {
                requests: requests
            },
            success: function (r) {
                $.each(r, function (i, source) {
                    widgets[i].el.data(widgets[i].name).setHtml($(source).html());
                });
            }
        });
    };
    
    gfx.showDialog = function (message, config) {
        var config = $.extend({}, config, {
            isModal: true,
            widht: '400px'
        });
        var div = $('<div>' + message + '</div>');
        div.grifix_kit_Dialog(config);
        div.data('grifix_kit_Dialog').show();
    };
    
    gfx.showMessage = function (text, type) {
        $('body').grifix_kit_Messenger({position: 'n', timeout: 10000});
        $('body').data('grifix_kit_Messenger').showMessage(text, type);
    };
    
    gfx.hideMessage = function () {
        var div = $('#message');
        if (div.length && div.data('grifix_kit_Tooltip')) {
            div.grifix_kit_Tooltip('destroy');
        }
    };
    
    gfx.showError = function (text, timeout) {
        this.showMessage(text, 'error', timeout);
    };
    
    gfx.showSuccess = function (text, timeout) {
        this.showMessage(text, 'success', timeout);
    };
    
    gfx.showInfo = function (text, timeout) {
        this.showMessage(text, 'info', timeout);
    };
    
    gfx.showWarning = function (text, timeout) {
        this.showMessage(text, 'warning', timeout);
    };
    
    gfx.handleAjaxError = function (err) {
        var message = err.message.replace(/\n/g, '<br>');
        this.showError(message);
    };
    
    gfx.showLoader = function () {
        var loader = $('#loader');
        if (!loader.length) {
            loader = $('<div id="loader"></div>');
            $('body').append(loader);
        }
        loader.show();
    };
    
    gfx.hideLoader = function () {
        $('#loader').hide();
    };
    
    
    gfx.findMaxZindex = function (elParent) {
        if (!elParent) {
            elParent = $('body');
        }
        var result = 0;
        elParent.find('*').each(function () {
            if (parseInt($(this).css('zIndex')) > result) {
                result = parseInt($(this).css('zIndex'));
            }
        });
        return result;
    };
    
    gfx.lockElement = function (el) {
        if (!el.data('elLocker')) {
            var elLocker = $('<div class="locker"></div>');
            elLocker.css('position', 'absolute');
            elLocker.css('left', el.offset().left);
            elLocker.css('top', el.offset().top);
            elLocker.css('height', el.outerHeight());
            elLocker.css('width', el.outerWidth());
            elLocker.css('z-index', gfx.findMaxZindex());
            $('body').append(elLocker);
            el.data('locker', elLocker);
        }
    };
    
    gfx.unlockElement = function (el) {
        if (el.data('locker') && el.data('locker')[0]) {
            el.data('locker').remove();
            el.data('locker', null);
        }
    };
    
    gfx.setUp = function () {
        var that = this;
        window.onerror = function (message, file, line, col, error) {
            console.log(message, "from", error.stack);
        };
        
        $(document).ajaxStart(function () {
            that.hideMessage();
            that.showLoader();
        });
        
        $(document).ajaxStop(function () {
            that.hideLoader();
        });
        
        $(document).ajaxError(function (e, xhr) {
            try {
                var error = $.parseJSON(xhr.responseText);
                var handler = function (err) {
                    gfx.handleAjaxError(err);
                };
                if (gfx.ajaxErrorsHandlers.length && error.class) {
                    $.each(gfx.ajaxErrorsHandlers, function () {
                        if (error.class == this.class) {
                            handler = this.handler;
                            return false;
                        }
                    })
                }
                handler(error);
            }
            catch (ex) {
                alert('Exception: ' + ex.message);
                if (console) {
                    console.log('Client exception:', ex);
                    console.log('Server exception:', xhr.responseText);
                }
            }
        });
    }
    
});