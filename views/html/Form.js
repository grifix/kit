/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
/*require {src}/grifix/kit/views/{skin}/Widget.js*/
/*require {src}/grifix/kit/views/{skin}/Tooltip.js*/
jQuery(function ($) {
    "use strict";
    gfx.createWidgetPrototype(
        gfx.createConstructorName(),
        gfx.createWidgetName(),
        gfx.Widget,
        {
            errorClass: 'form-error',
            lockWhileSubmit: true,
            target: {
                url: null,
                method: 'post'
            },
            on: {
                success: function (result) {
                },
                error: function (result) {
                },
                beforeSend: function () {
                },
                complete: function () {
                }
            }
        },
        {
            init: function () {
                var that = this;
                that.errorCounter = 0;
                that.elSubmit = that.find('submit');
                that.form = that.find('form');
                that.inputBoxes = that.find('inputBox');
                
                that.inputBoxes.find('input').on('change',function () {
                    that.removeError($(this).attr('name'));
                });
                
                if (!that.form.length) {
                    that.form = that.el();
                }
                that.elSubmit.on('click', function () {
                    that.submit();
                    return false;
                });
                that.callParent('init');
            },
            
            submit: function (onSuccess) {
                var that = this;
                that.form.ajaxSubmit({
                    url: that.cfg.target.url,
                    type: that.cfg.target.method,
                    beforeSend: function (jqXHR, settings) {
                        that.fireEvent('beforeSend', {jqXHR: jqXHR, settings: settings});
                        if (that.cfg.lockWhileSubmit) {
                            gfx.lockElement(that.el());
                        }
                        that.disableSubmit();
                    },
                    beforeSubmit: function (data) {
                        //Unset $_FILES if is empty
                        $.each(data, function (k, v) {
                            if (v.type == 'file') {
                                var el = that.el().find('input[name=' + v.name + ']');
                                if (el.data('value') && !el.val()) {
                                    v.name = null;
                                }
                            }
                        });
                    },
                    success: function (r) {
                        that.fireEvent('success', {result: r});
                        if (onSuccess) {
                            onSuccess.call(that, r);
                        }
                        
                    },
                    error: function (r) {
                        that.addErrors(r);
                        that.disableSubmit();
                        that.fireEvent('error', {result: r});
                    },
                    complete: function () {
                        that.fireEvent('complete');
                        if (that.cfg.lockWhileSubmit) {
                            gfx.unlockElement(that.el());
                        }
                        if (!that.hasErrors()) {
                            that.enableSubmit();
                        }
                    }
                })
            },
            
            disableSubmit: function () {
                this.elSubmit.attr('disabled', 1);
            },
            
            enableSubmit: function () {
                this.elSubmit.removeAttr('disabled');
            },
            
            addErrors: function (r) {
                var that = this;
                var error = JSON.parse(r.responseText);
                if (error.errors && error.errors.length) {
                    $.each(error.errors, function (k, v) {
                        that.addError(v.field, v.message);
                    });
                }
            },
            
            addError: function (inputName, message) {
                var that = this;
                var inputBox = that.el().find('[data-input="' + inputName + '"]');
                inputBox.addClass(that.cfg.errorClass);
                inputBox.grifix_kit_Tooltip({
                    content: message,
                    position: 'e',
                    cssClass: 'tooltip-error'
                });
                that.errorCounter++;
            },
            
            removeError: function (inputName) {
                var that = this;
                var inputBox = that.el().find('[data-input="' + inputName + '"]');
                if(inputBox.hasClass(that.cfg.errorClass)){
                    inputBox.removeClass(that.cfg.errorClass);
                    if (inputBox.data('grifix_kit_Tooltip')) {
                        inputBox.data('grifix_kit_Tooltip').destroy();
                    }
                    that.errorCounter--;
                    if (!that.hasErrors()) {
                        that.enableSubmit();
                    }
                }
            },
            
            hasErrors: function () {
                return this.errorCounter > 0;
            }
        }
    );
});
