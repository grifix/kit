<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);
/**@var $this \Grifix\Kit\View\ViewInterface */
?>
<div data-role="inputBox" data-input="<?= $this->getVar('name') ?>" class="<?= $this->getCssClass() ?> form-box">
    <?php if ($this->getVar('label')) : ?>
        <label class="form-label"><?= $this->getTextVar('label') ?><?php if ($this->getVar('required')) : ?>*<?php endif ?></label>
    <?php endif; ?>
        <?= $this->getVar('input') ?>
</div>
