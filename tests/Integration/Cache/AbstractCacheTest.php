<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Cache;

use Grifix\Kit\Alias;
use Grifix\Kit\Test\Integration\AbstractTest;
use Psr\SimpleCache\CacheInterface;

/***
 * Class MemcacheTest
 * @package Grifix\Kit\Test\Integration\Cache
 */
abstract class AbstractCacheTest extends AbstractTest
{
    /**
     * @var CacheInterface
     */
    protected $cache;

    public function setUp()
    {
        parent::setUp();
        $this->cache = $this->createCache();
    }

    /**
     * @return CacheInterface
     */
    abstract protected function createCache(): CacheInterface;

    public function testSimpleGet()
    {
        $this->cache->set('foo', 'bar', 100);
        self::assertEquals('bar', $this->cache->get('foo'));
    }

    public function testObjectSet()
    {
        $object = new \stdClass();
        $object->foo = 'bar';
        $this->cache->set('foo', $object, 0);
        self::assertEquals($this->cache->get('foo'), $object);
    }

    public function testClosureSet()
    {
        $closure = function () {
            return 'bar';
        };
        $this->cache->set('foo', $closure, 0);
        $closure = $this->cache->get('foo');
        self::assertEquals('bar', $closure());
    }

    public function testTtl()
    {
        $this->cache->set('foo', 'bar', 3);
        sleep(1);
        self::assertEquals('bar', $this->cache->get('foo'));
        sleep(2);
        self::assertNull($this->cache->get('foo'));
    }
}