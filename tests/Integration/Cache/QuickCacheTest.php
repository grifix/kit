<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Cache;

use Grifix\Kit\Alias;
use Psr\SimpleCache\CacheInterface;

/***
 * Class MemcacheTest
 * @package Grifix\Kit\Test\Integration\Cache
 */
class QuickCacheTest extends AbstractCacheTest
{
    /***
     * {@inheritdoc}
     */
    protected function createCache(): CacheInterface
    {
        return $this->getShared(Alias::QUICK_CACHE);
    }
}