<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Intl;


use Grifix\Kit\Intl\Lang\LangFactoryInterface;
use Grifix\Kit\Intl\Lang\LangInterface;
use Grifix\Kit\Intl\TranslatorInterface;
use Grifix\Kit\Test\Integration\AbstractTest;

/**
 * Class IntlTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration\Intl
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class IntlTest extends AbstractTest
{
    public function testLanguageCreation()
    {
        $lang = $this->getShared(LangFactoryInterface::class)->createLang('rus');
        self::assertInstanceOf(LangInterface::class, $lang);
    }
    
    public function testTranslator()
    {
        $translator = $this->getShared(TranslatorInterface::class);
        $translator->setCurrentLang('eng');
        self::assertEquals(
            'Test message, do not change it!',
            $translator->translate('grifix.kit.msg_test')
        );
    }
}