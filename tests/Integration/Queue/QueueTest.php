<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Queue;

use Grifix\Demo\Application\Queue\TestMessage;
use Grifix\Demo\Ui\Cli\Command\QueueTestCommand;
use Grifix\Kit\Alias;
use Grifix\Kit\Queue\QueueInterface;
use Grifix\Kit\Test\Integration\AbstractTest;

/**
 * Class QueueTest
 * @package Grifix\Kit\Test\Integration\Queue
 */
class QueueTest extends AbstractTest
{

    public function testQueue()
    {
        $queue = $this->getShared(QueueInterface::class);
        $logPath = $this->getShared(Alias::ROOT_DIR) . '/tmp/queue.log';
        if (is_file($logPath)) {
            unlink($logPath);
        }
        $message = new TestMessage('test', new \DateTime());
        $queue->publish($message, QueueTestCommand::QUEUE_NAME);
        self::assertEquals(
            $message->getDate()->format('Y-m-d H:i:s') . "\t" . $message->getText()."\n",
            file_get_contents($logPath)
        );
    }
}
