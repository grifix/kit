<?php
declare(strict_types=1);

namespace Grifix\Acl\Test\Integration\Orm;

use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Helper\ReflectionHelperInterface;
use Grifix\Kit\Ioc\Definition;
use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Specification\SpecificationFactoryInterface;
use Grifix\Kit\Test\Integration\AbstractTest;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\AbstractVehicle;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Specification\BigBusSpecification;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Bus;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Engine;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\EngineInfrastructureInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair\Repair;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event\TechInspectionWasMadeEvent;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\GasolineTank;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\TankInfrastructureInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspection;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\VehicleInfrastructureInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\VehicleRepositoryInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Engine\EngineInfrastructure;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Engine\Repair\RepairBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Engine\Repair\RepairCollection;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Tank\TankInfrastructure;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\TechnicalInspection\TechnicalInspectionBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\TechnicalInspection\TechnicalInspectionCollection;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\VehicleBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\VehicleFactoryInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\VehicleInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\VehicleInfrastructure;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\VehicleRepositoryFactoryInterface;
use Grifix\Kit\Uuid\UuidGeneratorInterface;

/**
 * Class OrmTest
 * @package Grifix\Acl\Test\Integration\Orm
 */
class OrmTest extends AbstractTest
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var VehicleRepositoryInterface
     */
    protected $repository;

    /**
     * @var VehicleFactoryInterface
     */
    protected $vehicleFactory;

    /**
     * @var UuidGeneratorInterface
     */
    protected $uuidGenerator;

    /**
     * @var EventBusInterface
     */
    protected $eventBus;

    /**
     * @var SpecificationFactoryInterface
     */
    protected $specificationFactory;

    public function setUp()
    {
        parent::setUp();
        $this->setShared(
            VehicleInfrastructureInterface::class,
            new Definition(VehicleInfrastructure::class)
        );
        $this->setShared(EngineInfrastructureInterface::class, new Definition(EngineInfrastructure::class));
        $this->setShared(TankInfrastructureInterface::class, new Definition(TankInfrastructure::class));
        $this->eventBus = $this->getShared(EventBusInterface::class);
        $this->em = $this->getShared(EntityManagerInterface::class);
        $this->uuidGenerator = $this->getShared(UuidGeneratorInterface::class);
        $this->vehicleFactory = $this->getShared(VehicleFactoryInterface::class);
        $this->em->registerBlueprint(VehicleBlueprint::class);
        $this->em->registerBlueprint(TechnicalInspectionBlueprint::class);
        $this->em->registerBlueprint(RepairBlueprint::class);
        $this->em->getConnection()->execute(file_get_contents(dirname(__FILE__) . '/Stub/drop.sql'));
        $this->em->getConnection()->execute(file_get_contents(dirname(__FILE__) . '/Stub/create.sql'));
        $this->repository = $this->getShared(VehicleRepositoryFactoryInterface::class)
            ->createVehicleRepository();
        $this->specificationFactory = $this->getShared(SpecificationFactoryInterface::class);
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->em->getConnection()->execute(file_get_contents(dirname(__FILE__) . '/Stub/drop.sql'));
    }


    public function testInsert()
    {
        $bus = $this->vehicleFactory->createBus('1', 'Mercedes', 100, 20, 4);
        $bus->makeTechnicalInspection();
        $bus->makeTechnicalInspection();
        $bus->repairEngine(100);
        $bus->fillTank(10);
        $this->repository->save($bus);

        $truck = $this->vehicleFactory->createTruck('2', 'Skoda', 50, 100, 8);
        $truck->makeTechnicalInspection();
        $truck->makeTechnicalInspection();
        $truck->repairEngine(200);
        $truck->fillTank(20);
        $this->repository->save($truck);

        self::assertEquals([
            'id' => '1',
            'data' => json_encode([
                'id' => '1',
                'tank' => [
                    'type' => 'gasoline',
                    'fills' => [
                        10
                    ],
                    'capacity' => 100,
                    'fuelLevel' => 10,
                    'numberOfFills' => 1,
                    'fuelConsumptionPerMile' => 0.01
                ],
                'type' => 'bus',
                'model' => 'Mercedes',
                'seats' => 20,
                'engine' => [
                    'volume' => 4,
                    'mileage' => 0
                ],
                'passengersAmount' => 0
            ], JSON_PRETTY_PRINT)
        ], $this->selectVehicleRecord($bus));

        self::assertEquals([
            'id' => '2',
            'data' => json_encode([
                'id' => '2',
                'tank' => [
                    'type' => 'diesel',
                    'fills' => [
                        20
                    ],
                    'capacity' => 50,
                    'fuelLevel' => 20,
                    'numberOfFills' => 1,
                    'fuelConsumptionPerMile' => 0.01
                ],
                'type' => 'truck',
                'model' => 'Skoda',
                'engine' => [
                    'volume' => 8,
                    'mileage' => 0
                ],
                'tonnage' => 100,
                'cargoAmount' => 0
            ], JSON_PRETTY_PRINT)
        ], $this->selectVehicleRecord($truck));

        self::assertEquals(
            $this->extractTechInspectionsRecords($bus),
            $this->selectTechInspectionsRecords($bus)
        );

        self::assertEquals(
            $this->extractTechInspectionsRecords($truck),
            $this->selectTechInspectionsRecords($truck)
        );

        self::assertEquals(
            $this->extractRepairsRecords($bus),
            $this->selectRepairsRecords($bus)
        );

        self::assertEquals(
            $this->extractRepairsRecords($truck),
            $this->selectRepairsRecords($truck)
        );
    }

    public function testUpdate2()
    {
        $this->createVehicleWithoutOrm([
            'id' => '1',
            'tank' => [
                'type' => 'gasoline',
                'fills' => [],
                'capacity' => 50,
                'fuelLevel' => 10,
                'numberOfFills' => 1,
                'fuelConsumptionPerMile' => 0.01
            ],
            'type' => 'bus',
            'model' => 'Mercedes',
            'seats' => 20,
            'engine' => [
                'volume' => 4,
                'mileage' => 0,
                'repairs' => [
                    [
                        'id' => '1',
                        'date' => '2001-01-01 12:00:00',
                        'cost' => 100
                    ],
                    [
                        'id' => '2',
                        'date' => '2002-01-01 12:00:00',
                        'cost' => 30
                    ]
                ]
            ],
            'passengersAmount' => 0,
            'technicalInspections' => [
                [
                    'id' => '1',
                    'date' => '2001-01-01 12:00:00',
                    'result' => true
                ]
            ]
        ]);

        /**@var $bus Bus */
        $bus = $this->repository->find('1');

        $bus->repairEngine(100);
        $bus->makeTechnicalInspection();
        $bus->fillTank(10);
        $bus->boardPassengers(5);
        $bus->drive(100);
        $this->repository->save($bus);

        $busRecord = json_decode($this->selectVehicleRecord($bus)['data'], true);
        self::assertEquals(5, $busRecord['passengersAmount']);
        self::assertEquals(19, $busRecord['tank']['fuelLevel']);
        self::assertEquals(100, $busRecord['engine']['mileage']);
        self::assertEquals([10], $busRecord['tank']['fills']);
        self::assertEquals(2, count($this->selectTechInspectionsRecords($bus)));
        self::assertEquals(3, count($this->selectRepairsRecords($bus)));
    }

    public function testUpdate()
    {
        $bus = $this->vehicleFactory->createBus('1', 'Mercedes', 50, 20, 4);
        $truck = $this->vehicleFactory->createTruck('2', 'Volvo', 100, 20, 8);
        $this->repository->save($bus);
        $this->repository->save($truck);

        $this->assertVehicleRecordExists(1);
        $this->assertVehicleRecordExists(2);

        $bus->fillTank(10);
        $truck->fillTank(20);
        $this->repository->save($bus);
        $this->repository->save($truck);

        self::assertEquals(
            [
                'id' => '1',
                'data' => json_encode([
                    'id' => '1',
                    'tank' => [
                        'type' => 'gasoline',
                        'fills' => [
                            10
                        ],
                        'capacity' => 50,
                        'fuelLevel' => 10,
                        'numberOfFills' => 1,
                        'fuelConsumptionPerMile' => 0.01
                    ],
                    'type' => 'bus',
                    'model' => 'Mercedes',
                    'seats' => 20,
                    'engine' => [
                        'volume' => 4,
                        'mileage' => 0
                    ],
                    'passengersAmount' => 0
                ], JSON_PRETTY_PRINT)
            ],
            $this->selectVehicleRecord($bus)
        );

        self::assertEquals(
            [
                'id' => '2',
                'data' => json_encode([
                    'id' => '2',
                    'tank' => [
                        'type' => 'diesel',
                        'fills' => [
                            20
                        ],
                        'capacity' => 100,
                        'fuelLevel' => 20,
                        'numberOfFills' => 1,
                        'fuelConsumptionPerMile' => 0.01
                    ],
                    'type' => 'truck',
                    'model' => 'Volvo',
                    'engine' => [
                        'volume' => 8,
                        'mileage' => 0
                    ],
                    'tonnage' => 20,
                    'cargoAmount' => 0
                ], JSON_PRETTY_PRINT)
            ],
            $this->selectVehicleRecord($truck)
        );
    }

    public function testFindSame()
    {
        $bus = $this->vehicleFactory->createBus('1', 'Mercedes', 50, 8, 4);
        $this->repository->save($bus);

        self::assertSame($bus, $this->repository->find('1'));
    }

    public function testFindBySame()
    {
        $bus = $this->vehicleFactory->createBus('1', 'Mercedes', 50, 8, 4);
        $this->repository->save($bus);

        self::assertSame($bus, $this->repository->findByModel('Mercedes')->first());
    }

    public function testFindBy()
    {
        $mercedes = $this->vehicleFactory->createBus('1', 'Mercedes', 50, 8, 4);
        $this->repository->save($mercedes);
        $volvo = $this->vehicleFactory->createBus('2', 'Volvo', 50, 24, 2);
        $this->repository->save($volvo);

        $buses = $this->repository->findByModel('Volvo');
        self::assertEquals(1, $buses->count());
        self::assertAttributeEquals('2', 'id', $buses->first());
    }

    public function testFind()
    {
        $this->createVehicleWithoutOrm([
            'id' => '1',
            'tank' => [
                'type' => 'gasoline',
                'capacity' => 50,
                'fuelLevel' => 10,
                'numberOfFills' => 1,
                'fuelConsumptionPerMile' => 0.01
            ],
            'type' => 'bus',
            'model' => 'Mercedes',
            'seats' => 20,
            'engine' => [
                'volume' => 4,
                'mileage' => 0,
                'repairs' => [
                    [
                        'id' => '1',
                        'date' => '2001-01-01 12:00:00',
                        'cost' => 100
                    ],
                    [
                        'id' => '2',
                        'date' => '2002-01-01 12:00:00',
                        'cost' => 30
                    ]
                ]
            ],
            'passengersAmount' => 0,
            'technicalInspections' => [
                [
                    'id' => '1',
                    'date' => '2001-01-01 12:00:00',
                    'result' => true
                ]
            ]
        ]);

        $bus = $this->repository->find('1');

        /**@var TechnicalInspectionCollection $technicalInspections */
        $technicalInspections = self::getObjectAttribute($bus, 'technicalInspections');

        /**@var RepairCollection $repairs */
        $repairs = self::getObjectAttribute(self::getObjectAttribute($bus, 'engine'), 'repairs');
        $tank = self::getObjectAttribute($bus, 'tank');
        $engine = self::getObjectAttribute($bus, 'engine');


        self::assertInstanceOf(Bus::class, $bus);
        self::assertAttributeEquals('1', 'id', $bus);
        self::assertAttributeEquals('Mercedes', 'model', $bus);
        self::assertAttributeEquals(20, 'seats', $bus);
        self::assertAttributeEquals(0, 'passengersAmount', $bus);
        self::assertInstanceOf(GasolineTank::class, $tank);
        self::assertAttributeEquals(50, 'capacity', $tank);
        self::assertAttributeEquals(10, 'fuelLevel', $tank);
        self::assertAttributeEquals(1, 'numberOfFills', $tank);
        self::assertAttributeEquals(0.01, 'fuelConsumptionPerMile', $tank);
        self::assertInstanceOf(Engine::class, $engine);
        self::assertAttributeEquals(4, 'volume', $engine);
        self::assertAttributeEquals(0, 'mileage', $engine);
        self::assertEquals(1, $technicalInspections->count());
        self::assertEquals(2, $repairs->count());
    }

    public function testMatch()
    {
        $bigBus = $this->vehicleFactory->createBus('1', 'Mercedes', 200, 100, 10);
        $this->repository->save($bigBus);
        $smallBus = $this->vehicleFactory->createBus('2', 'Volvo', 50, 20, 2);
        $this->repository->save($smallBus);

        $buses = $this->repository->match($this->specificationFactory->createSpecification(BigBusSpecification::class));
        self::assertEquals(1, $buses->count());
        self::assertArrayHasKey('1', $buses);
    }


    /**
     * @param array $data
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    private function createVehicleWithoutOrm(array $data)
    {
        $vehicle = array_merge([], $data);
        unset($vehicle['technicalInspections']);
        unset($vehicle['engine']['repairs']);
        $this->em->getConnection()->beginTransaction();
        $this->em->getConnection()->insert(
            ['id' => $data['id'], 'data' => json_encode($vehicle)],
            $this->em->getBlueprint(AbstractVehicle::class)->getTable(),
            false
        );

        if (isset($data['technicalInspections'])) {
            foreach ($data['technicalInspections'] as $technicalInspection) {
                $this->em->getConnection()->insert(
                    [
                        'id' => $technicalInspection['id'],
                        'vehicle_id' => $data['id'],
                        'data' => json_encode($technicalInspection)
                    ],
                    $this->em->getBlueprint(TechnicalInspection::class)->getTable(),
                    false
                );
            }
        }

        if (isset($data['engine']['repairs'])) {
            foreach ($data['engine']['repairs'] as $repair) {
                $this->em->getConnection()->insert(
                    [
                        'id' => $repair['id'],
                        'vehicle_id' => $data['id'],
                        'data' => json_encode($repair)
                    ],
                    $this->em->getBlueprint(Repair::class)->getTable(),
                    false
                );
            }
        }
        $this->em->getConnection()->commitTransaction();
    }

    public function testDelete()
    {
        $bus = $this->vehicleFactory->createBus('1', 'Volvo', 100, 20, 3);
        $this->repository->save($bus);
        $this->assertVehicleRecordExists(1);
        $this->repository->delete($bus);
        $this->assertVehicleRecordNotExists(1);
    }

    public function testTriggerEventAfterSave()
    {
        $probe = new class
        {
            public $techInspectionWasMade = false;
        };

        $this->eventBus->listen(TechInspectionWasMadeEvent::class, function () use ($probe) {
            $probe->techInspectionWasMade = true;
        }, false);

        $bus = $this->vehicleFactory->createBus('1', 'Mercedes', 100, 20, 4);

        $bus->makeTechnicalInspection();

        self::assertFalse($probe->techInspectionWasMade);
        $this->repository->save($bus);
        self::assertTrue($probe->techInspectionWasMade);
    }

    public function testSaveOneEntity()
    {
        $bus = $this->vehicleFactory->createBus('1', 'Volvo', 50, 10, 4);
        $truck = $this->vehicleFactory->createTruck('2', 'Scania', 150, 25, 6);

        $this->repository->save($bus);
        $this->repository->save($truck);

        $bus->boardPassengers(5);
        $bus->makeTechnicalInspection();

        $truck->loadCargo(5);
        $truck->makeTechnicalInspection();

        $this->repository->save($bus);

        $busData = json_decode($this->selectVehicleRecord($bus)['data'], true);
        $truckData = json_decode($this->selectVehicleRecord($truck)['data'], true);

        self::assertEquals(5, $busData['passengersAmount']);
        self::assertEquals(0, $truckData['cargoAmount']);
        self::assertEquals(1, count($this->selectTechInspectionsRecords($bus)));
        self::assertEquals(0, count($this->selectTechInspectionsRecords($truck)));
    }

    /**
     * @param $vehicleId
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    protected function assertVehicleRecordExists($vehicleId)
    {
        $records = $this->em->getConnection()->select(
            'SELECT "id" FROM test.vehicle WHERE "id" = :id',
            ['id' => $vehicleId]
        );
        self::assertEquals($vehicleId, $records[0]['id']);
    }

    /**
     * @param $vehicleId
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    protected function assertVehicleRecordNotExists($vehicleId)
    {
        $records = $this->em->getConnection()->select(
            'SELECT "id" FROM test.vehicle WHERE "id" = :id',
            ['id' => $vehicleId]
        );
        self::assertEquals(0, count($records));
    }

    /**
     * @param VehicleInterface $vehicle
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    protected function insertVehicleWithoutOrm(VehicleInterface $vehicle)
    {
        $this->em->getConnection()->insert([
            'id' => $this->em->getEntityId($vehicle),
            'data' => json_encode($this->em->serialize($vehicle))

        ], $this->em->getBlueprint(get_class($vehicle))->getTable(), false);
    }

    /**
     * @param VehicleInterface $vehicle
     * @return array
     * @throws \ReflectionException
     */
    protected function extractTechInspectionsRecords(VehicleInterface $vehicle): array
    {
        $result = [];
        $reflection = new \ReflectionClass($vehicle);
        $property = $reflection->getProperty('technicalInspections');
        $property->setAccessible(true);
        $collection = $property->getValue($vehicle);
        foreach ($collection as $item) {
            $record['id'] = $this->em->getEntityId($item);
            $record['data'] = json_encode($this->em->serialize($item), JSON_PRETTY_PRINT);
            $record[TechnicalInspectionBlueprint::VEHICLE_COLUMN] = $this->em->getEntityId($vehicle);
            $result[] = $record;
        }
        $property->setAccessible(false);
        return $result;
    }

    /**
     * @param VehicleInterface $vehicle
     * @return array
     * @throws \ReflectionException
     */
    protected function extractRepairsRecords(VehicleInterface $vehicle): array
    {
        $result = [];
        $engineProperty = (new \ReflectionClass($vehicle))->getProperty('engine');
        $engineProperty->setAccessible(true);
        $engine = $engineProperty->getValue($vehicle);
        $engineProperty->setAccessible(false);

        $reflection = new \ReflectionClass($engine);
        $property = $reflection->getProperty('repairs');
        $property->setAccessible(true);
        $collection = $property->getValue($engine);
        foreach ($collection as $item) {
            $record['id'] = $this->em->getEntityId($item);
            $record['data'] = json_encode($this->em->serialize($item), JSON_PRETTY_PRINT);
            $record[RepairBlueprint::VEHICLE_COLUMN] = $this->em->getEntityId($vehicle);
            $result[] = $record;
        }
        $property->setAccessible(false);
        return $result;
    }

    /**
     * @param VehicleInterface $vehicle
     * @return array
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    private function selectTechInspectionsRecords(VehicleInterface $vehicle): array
    {
        return $this->em->getConnection()->select(
            'SELECT "id", "vehicle_id", jsonb_pretty("data") AS "data" FROM ' . $this->em->getBlueprint(TechnicalInspection::class)->getTable() . ' 
            WHERE "vehicle_id" = :vehicle_id',
            ['vehicle_id' => $this->em->getEntityId($vehicle)]
        );
    }

    /**
     * @param VehicleInterface $vehicle
     * @return array
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    private function selectRepairsRecords(VehicleInterface $vehicle): array
    {
        return $this->em->getConnection()->select(
            'SELECT "id", "vehicle_id", jsonb_pretty("data") AS "data" FROM ' . $this->em->getBlueprint(Repair::class)->getTable() . ' 
            WHERE "vehicle_id" = :vehicle_id',
            ['vehicle_id' => $this->em->getEntityId($vehicle)]
        );
    }

    /**
     * @param VehicleInterface $vehicle
     * @return array
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    private function selectVehicleRecord(VehicleInterface $vehicle): array
    {
        return $this->em->getConnection()->select(
            'SELECT "id", jsonb_pretty("data") AS "data" FROM test.vehicle WHERE "id" = :id',
            ['id' => $this->em->getEntityId($vehicle)]
        )[0];
    }
}
