<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain;

use Grifix\Kit\Collection\GenericCollectionInterface;
use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Specification\SpecificationInterface;

/**
 * Class VehicleRepository
 * @package Grifix\Acl\Test\Integration\Orm\Stub
 */
interface VehicleRepositoryInterface extends GenericCollectionInterface
{
    /**
     * @param string $id
     * @return VehicleInterface|null
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    public function find(string $id);

    /**
     * @param VehicleInterface $vehicle
     */
    public function save(VehicleInterface $vehicle): void;

    /**
     * @param VehicleInterface $vehicle
     */
    public function delete(VehicleInterface $vehicle): void;

    /**
     * @param SpecificationInterface $specification
     * @return CollectionInterface
     */
    public function match(SpecificationInterface $specification): CollectionInterface;

    /**
     * @param string $model
     * @return CollectionInterface
     */
    public function findByModel(string $model): CollectionInterface;

    /**
     * @return VehicleInterface|null
     */
    public function first();

    /**
     * @return VehicleInterface|null
     */
    public function last();
}
