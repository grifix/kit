<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain;

/**
 * Class VehicleFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub
 */
interface VehicleFactoryInterface
{
    /**
     * @param string $id
     * @param string $model
     * @param float $tankCapacity
     * @param float $tonnage
     * @param float $engineVolume
     * @return TruckInterface
     */
    public function createTruck(string $id, string $model, float $tankCapacity, float $tonnage, float $engineVolume): TruckInterface;

    /**
     * @param string $id
     * @param string $model
     * @param float $tankCapacity
     * @param int $seats
     * @param float $engineVolume
     * @return BusInterface
     */
    public function createBus(string $id, string $model, float $tankCapacity, int $seats, float $engineVolume): BusInterface;
}
