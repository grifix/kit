<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain;

use Grifix\Kit\Orm\Collection\CollectionFactoryInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\EngineFactoryInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\TechnicalInspection\TechnicalInspectionCollectionFactoryInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\TankFactoryInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspectionFactoryInterface;
use Grifix\Kit\Uuid\UuidGeneratorInterface;

/**
 * Class VehicleFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub
 */
class VehicleFactory implements VehicleFactoryInterface
{
    /**
     * @var TankFactoryInterface
     */
    protected $tankFactory;

    /**
     * @var TechnicalInspectionFactoryInterface
     */
    protected $technicalInspectionFactory;

    /**
     * @var UuidGeneratorInterface
     */
    protected $infrastructure;

    /**
     * @var EngineFactoryInterface
     */
    protected $engineFactory;

    /**
     * VehicleFactory constructor.
     * @param TankFactoryInterface $tankFactory
     * @param TechnicalInspectionFactoryInterface $technicalInspectionFactory
     * @param VehicleInfrastructureInterface $infrastructure
     * @param EngineFactoryInterface $engineFactory
     */
    public function __construct(
        TankFactoryInterface $tankFactory,
        TechnicalInspectionFactoryInterface $technicalInspectionFactory,
        VehicleInfrastructureInterface $infrastructure,
        EngineFactoryInterface $engineFactory
    ) {
        $this->tankFactory = $tankFactory;
        $this->technicalInspectionFactory = $technicalInspectionFactory;
        $this->infrastructure = $infrastructure;
        $this->engineFactory = $engineFactory;
    }

    /**
     * @param string $id
     * @param string $model
     * @param float $tankCapacity
     * @param float $tonnage
     * @param float $engineVolume
     * @return TruckInterface
     * @throws \Exception
     */
    public function createTruck(
        string $id,
        string $model,
        float $tankCapacity,
        float $tonnage,
        float $engineVolume
    ): TruckInterface {
        $result = new Truck(
            $this->infrastructure,
            $id,
            $model,
            $this->tankFactory->createDieselTank($tankCapacity),
            $this->engineFactory->createEngine($engineVolume),
            $this->technicalInspectionFactory,
            $tonnage
        );
        return $result;
    }

    /**
     * @param string $id
     * @param string $model
     * @param float $tankCapacity
     * @param int $seats
     * @param float $engineVolume
     * @return BusInterface
     * @throws \Exception
     */
    public function createBus(
        string $id,
        string $model,
        float $tankCapacity,
        int $seats,
        float $engineVolume
    ): BusInterface {
        $result = new Bus(
            $this->infrastructure,
            $id,
            $model,
            $this->tankFactory->createGasolineTank($tankCapacity),
            $this->engineFactory->createEngine($engineVolume),
            $this->technicalInspectionFactory,
            $seats
        );
        return $result;
    }
}
