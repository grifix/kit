<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event;

/**
 * Class TankWasFilledEvent
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event
 */
class TankWasFilledEvent
{
    private $vehicleId;

    private $fullAmount;

    /**
     * TankWasFilledEvent constructor.
     * @param string $vehicleId
     * @param float $fullAmount
     */
    public function __construct(string $vehicleId, float $fullAmount)
    {
        $this->vehicleId = $vehicleId;
        $this->fullAmount = $fullAmount;
    }
}
