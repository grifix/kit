<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event;

/**
 * Class EngineWasRepairedEvent
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event
 */
class EngineWasRepairedEvent
{
    private $vehicleId;

    private $cost;

    /**
     * EngineWasRepairedEvent constructor.
     * @param string $vehicleId
     * @param float $miles
     */
    public function __construct(string $vehicleId, float $miles)
    {
        $this->vehicleId = $vehicleId;
        $this->cost = $miles;
    }
}
