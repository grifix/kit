<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event;

/**
 * Class VehicleWasDrivenEvent
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event
 */
class VehicleWasDrivenEvent
{
    private $vehicleId;

    private $miles;

    /**
     * EngineWasRepairedEvent constructor.
     * @param string $vehicleId
     * @param float $miles
     */
    public function __construct(string $vehicleId, float $miles)
    {
        $this->vehicleId = $vehicleId;
        $this->miles = $miles;
    }
}
