<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event;

/**
 * Class TechInspectionWasMadeEvent
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event
 */
class TechInspectionWasMadeEvent
{
    private $vehicleId;

    /**
     * TechInspectionWasMadeEvent constructor.
     * @param string $vehicleId
     */
    public function __construct(string $vehicleId)
    {
        $this->vehicleId = $vehicleId;
    }
}
