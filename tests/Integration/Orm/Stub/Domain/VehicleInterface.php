<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain;

/**
 * Interface VehicleInterface
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Entity
 */
interface VehicleInterface
{

    /**
     * @param float $fuelAmount
     * @throws \Exception
     */
    public function fillTank(float $fuelAmount): void;

    public function makeTechnicalInspection(): void;

    /**
     * @param $miles
     * @throws \Exception
     */
    public function drive(float $miles): void;

    /**
     * @param float $cost
     */
    public function repairEngine(float $cost): void;
}
