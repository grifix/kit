<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine;

/**
 * Class EngineFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine
 */
interface EngineFactoryInterface
{
    /**
     * @param float $volume
     * @return EngineInterface
     * @throws \Exception
     */
    public function createEngine(float $volume): EngineInterface;
}