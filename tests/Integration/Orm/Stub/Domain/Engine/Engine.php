<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine;

use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Exception\CostOfServiceExceededException;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Exception\NeedRepairException;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Exception\RepairsLimitExceededException;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair\RepairCollectionInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair\RepairFactoryInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair\RepairInterface;

/**
 * Class Engine
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine
 */
class Engine implements EngineInterface
{
    const MAX_COST_OF_SERVICE = 10000;

    const MAX_NUMBER_OR_REPAIRS = 10;

    const REPAIR_PERIOD = 10000;

    /**
     * @var float
     */
    protected $volume;

    /**
     * @var float
     */
    protected $mileage = 0;

    /**
     * @relation
     * @var RepairCollectionInterface
     */
    protected $repairs;

    /**
     * @dependency
     * @var \Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair\RepairFactoryInterface
     */
    protected $repairFactory;

    /**
     * @dependency
     * @var \Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\EngineInfrastructureInterface
     */
    protected $infrastructure;

    /**
     * Engine constructor.
     * @param EngineInfrastructureInterface $infrastructure
     * @param RepairFactoryInterface $repairFactory
     * @param float $volume
     * @throws CostOfServiceExceededException
     * @throws RepairsLimitExceededException
     */
    public function __construct(
        EngineInfrastructureInterface $infrastructure,
        RepairFactoryInterface $repairFactory,
        float $volume
    ) {
        $this->repairFactory = $repairFactory;
        $this->volume = $volume;
        $this->infrastructure = $infrastructure;
        $this->repairs = $this->infrastructure->createRepairCollection();
        $this->repair(0);
    }

    /**
     * {@inheritdoc}
     */
    public function repair(float $cost): EngineInterface
    {
        if ($this->isCostOfRepairsExceeded($cost)) {
            throw new CostOfServiceExceededException();
        }
        if ($this->isRepairsLimitExceeded()) {
            throw new RepairsLimitExceededException();
        }
        $clone = clone($this);
        $clone->repairs->add($this->repairFactory->createRepair($this->infrastructure->generateRepairId(), $cost));
        return $clone;
    }

    /**
     * @return float
     */
    protected function calculateTotalRepairsCost(): float
    {
        $result = 0;
        foreach ($this->repairs as $repair) {
            /**@var $repair RepairInterface */
            $result += $repair->getCost();
        }
        return $result;
    }

    /**
     * @param float $repairCost
     * @return bool
     */
    protected function isCostOfRepairsExceeded(float $repairCost): bool
    {
        return $this->calculateTotalRepairsCost() + $repairCost > self::MAX_COST_OF_SERVICE;
    }

    /**
     * @return bool
     */
    protected function isRepairsLimitExceeded(): bool
    {
        return $this->repairs->count() >= self::MAX_NUMBER_OR_REPAIRS;
    }

    /**
     * @return bool
     */
    public function needRepair(): bool
    {
        return $this->mileage / $this->repairs->count() > self::REPAIR_PERIOD;
    }

    /**
     * {@inheritdoc}
     */
    public function drive(float $miles): EngineInterface
    {
        if ($this->needRepair()) {
            throw new NeedRepairException();
        }
        $clone = clone $this;
        $clone->mileage += $miles;
        return $clone;
    }
}
