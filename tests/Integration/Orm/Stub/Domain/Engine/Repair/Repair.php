<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair;

/**
 * Class Repair
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair
 */
class Repair implements RepairInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var float
     */
    protected $cost;

    /**
     * @var \DateTimeInterface
     */
    protected $date;

    /**
     * Repair constructor.
     * @param $id
     * @param $cost
     * @throws \Exception
     */
    public function __construct(string $id, float $cost)
    {
        $this->id = $id;
        $this->cost = $cost;
        $this->date = new \DateTimeImmutable();
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->cost;
    }
}
