<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair;


/**
 * Class Repair
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair
 */
interface RepairInterface
{
    /**
     * @return float
     */
    public function getCost(): float;
}
