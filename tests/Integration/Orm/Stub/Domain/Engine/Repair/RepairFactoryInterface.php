<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair;

/**
 * Class RepairFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair
 */
interface RepairFactoryInterface
{
    /**
     * @param string $id
     * @param float $cost
     * @return RepairInterface
     * @throws \Exception
     */
    public function createRepair(string $id, float $cost): RepairInterface;
}