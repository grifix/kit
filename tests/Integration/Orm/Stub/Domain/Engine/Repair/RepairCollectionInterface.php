<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair;

use Grifix\Kit\Collection\GenericCollectionInterface;

/**
 * Class RepairCollectinInterace
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair
 */
interface RepairCollectionInterface extends GenericCollectionInterface
{
    /**
     * @param RepairInterface $repair
     * @return mixed
     */
    public function add(RepairInterface $repair): void;

    /**
     * @param RepairInterface $repair
     */
    public function remove(RepairInterface $repair): void;

    /**
     * @return float
     */
    public function calculateTotalCost(): float;
}