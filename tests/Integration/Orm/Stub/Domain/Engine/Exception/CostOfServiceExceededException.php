<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Exception;


/**
 * Class CostOfServiceExceededException
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Exception
 */
class CostOfServiceExceededException extends \Exception
{
    protected $message = 'Cost of service is exceeded!';
}