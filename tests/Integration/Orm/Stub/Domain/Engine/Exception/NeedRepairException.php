<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Exception;

/**
 * Class NeedRepairException
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Exception
 */
class NeedRepairException extends \Exception
{
    protected $message = 'Need repair exception!';
}