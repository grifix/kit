<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Exception;

/**
 * Class RepairsLimitExceededException
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Exception
 */
class RepairsLimitExceededException extends \Exception
{
    protected $message = 'Limit of repairs is exceeded!';
}