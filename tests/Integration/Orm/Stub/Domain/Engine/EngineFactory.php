<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine;

use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair\RepairFactoryInterface;

/**
 * Class EngineFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine
 */
class EngineFactory implements EngineFactoryInterface
{

    /**
     * @var EngineInfrastructureInterface
     */
    protected $engineInfrastructure;

    /**
     * @var RepairFactoryInterface
     */
    protected $repairFactory;

    /**
     * EngineFactory constructor.
     * @param EngineInfrastructureInterface $infrastructure
     * @param RepairFactoryInterface $repairFactory
     */
    public function __construct(
        EngineInfrastructureInterface $infrastructure,
        RepairFactoryInterface $repairFactory
    ) {
        $this->engineInfrastructure = $infrastructure;
        $this->repairFactory = $repairFactory;
    }

    /**
     * @param float $volume
     * @return EngineInterface
     * @throws \Exception
     */
    public function createEngine(float $volume): EngineInterface
    {
        return new Engine(
            $this->engineInfrastructure,
            $this->repairFactory,
            $volume
        );
    }
}
