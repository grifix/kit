<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine;


/**
 * Class Engine
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine
 */
interface EngineInterface
{
    /**
     * @param float $cost
     * @return EngineInterface
     */
    public function repair(float $cost): EngineInterface;

    /**
     * @param float $miles
     * @return EngineInterface
     */
    public function drive(float $miles): EngineInterface;

    /**
     * @return bool
     */
    public function needRepair(): bool;
}