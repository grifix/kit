<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine;

use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair\RepairCollectionInterface;

/**
 * Class EngineInfrastructure
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine
 */
interface EngineInfrastructureInterface
{
    /**
     * @return string
     * @throws \Exception
     */
    public function generateRepairId(): string;

    /**
     * @return mixed
     */
    public function createRepairCollection(): RepairCollectionInterface;
}
