<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Specification;

use Grifix\Kit\Expression\ExpressionInterface;
use Grifix\Kit\Specification\AbstractSpecification;

/**
 * Class BigBusSpecification
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain
 */
class BigBusSpecification extends AbstractSpecification
{
    /***
     * {@inheritdoc}
     */
    protected function createExpression(): ExpressionInterface
    {
        return $this->andX(
            $this->eq('type', 'bus'),
            $this->gte('engine.volume', 4),
            $this->gte('tank.capacity', 50),
            $this->gte('seats', 30)
        );
    }
}
