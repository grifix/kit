<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection;

/**
 * Class CheckUpFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub\TechnicalInspection
 */
class TechnicalInspectionFactory implements TechnicalInspectionFactoryInterface
{
    /**
     * @param string $id
     * @param bool $result
     * @return TechnicalInspectionInterface
     * @throws \Exception
     */
    public function createTechnicalInspection(string $id, bool $result): TechnicalInspectionInterface
    {
        $result = new TechnicalInspection($id, new \DateTimeImmutable(), $result);
        return $result;
    }
}
