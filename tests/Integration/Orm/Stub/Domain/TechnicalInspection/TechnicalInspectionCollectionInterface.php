<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection;

use Grifix\Kit\Collection\GenericCollectionInterface;

/**
 * Class CheckUpCollection
 * @package Grifix\Kit\Test\Integration\Orm\Stub\TechnicalInspection
 */
interface TechnicalInspectionCollectionInterface extends GenericCollectionInterface
{
    /**
     * @param TechnicalInspectionInterface $technicalInspection
     * @return void
     */
    public function add(TechnicalInspectionInterface $technicalInspection): void ;

    /**
     * @param string $id
     * @return null|TechnicalInspectionInterface
     * @throws \Grifix\Kit\Db\Exception\StatementExecuteException
     */
    public function find(string $id);

    /**
     * @param TechnicalInspectionInterface $technicalInspection
     * @throws \Grifix\Kit\Orm\Collection\Exception\ParentEntityNotMatchException
     */
    public function remove(TechnicalInspectionInterface $technicalInspection): void;
}
