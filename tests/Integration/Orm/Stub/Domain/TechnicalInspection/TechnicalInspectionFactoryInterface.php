<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection;

/**
 * Class CheckUpFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub\TechnicalInspection
 */
interface TechnicalInspectionFactoryInterface
{
    /**
     * @param string $id
     * @param bool $result
     * @return TechnicalInspectionInterface
     */
    public function createTechnicalInspection(string $id, bool $result): TechnicalInspectionInterface;
}