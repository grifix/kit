<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection;

/**
 * Class TechnicalInspection
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Entity\TechnicalInspection
 */
class TechnicalInspection implements TechnicalInspectionInterface
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var \DateTimeInterface
     */
    protected $date;

    /**
     * @var bool
     */
    protected $result;

    /**
     * TechnicalInspection constructor.
     * @param string $id
     * @param \DateTimeInterface $date
     * @param bool $result
     */
    public function __construct(string $id, \DateTimeInterface $date, bool $result)
    {
        $this->id = $id;
        $this->date = $date;
        $this->result = $result;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->result;
    }
}
