<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain;

use Grifix\Kit\Orm\EventCollector\EventPublisherInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspectionCollectionInterface;

/**
 * Class VehicleInfrastructure
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure
 */
interface VehicleInfrastructureInterface extends EventPublisherInterface
{
    /**
     * @return string
     * @throws \Exception
     */
    public function generateTechInspectionId(): string;

    /**
     * @return TechnicalInspectionCollectionInterface
     */
    public function createTechInspectionCollection(): TechnicalInspectionCollectionInterface;
}