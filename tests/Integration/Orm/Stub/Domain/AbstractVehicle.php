<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain;

use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\EngineInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event\EngineWasRepairedEvent;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event\TankWasFilledEvent;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event\TechInspectionWasMadeEvent;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Event\VehicleWasDrivenEvent;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\TechnicalInspection\TechnicalInspectionCollectionFactoryInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspectionCollectionInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspectionFactoryInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\TankInterface;

/**
 * Class AbstractVehicle
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Entity
 */
abstract class AbstractVehicle implements VehicleInterface
{

    /**
     * @var string
     */
    protected $id;

    /**
     * @var TankInterface
     */
    protected $tank;

    /**
     * @relation
     * @var TechnicalInspectionCollectionInterface
     */
    protected $technicalInspections;

    /**
     * @var string
     */
    protected $model;

    /**
     * @dependency
     * @var \Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspectionFactoryInterface
     */
    protected $technicalInspectionFactory;

    /**
     * @var \Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\EngineInterface
     */
    protected $engine;

    /**
     * @dependency
     * @var \Grifix\Kit\Test\Integration\Orm\Stub\Domain\VehicleInfrastructureInterface
     */
    protected $infrastructure;

    /**
     * AbstractVehicle constructor.
     * @param VehicleInfrastructureInterface $infrastructure
     * @param string $id
     * @param string $model
     * @param TankInterface $tank
     * @param EngineInterface $engine
     * @param TechnicalInspectionFactoryInterface $technicalInspectionFactory
     */
    public function __construct(
        VehicleInfrastructureInterface $infrastructure,
        string $id,
        string $model,
        TankInterface $tank,
        EngineInterface $engine,
        TechnicalInspectionFactoryInterface $technicalInspectionFactory
    ) {
        $this->infrastructure = $infrastructure;
        $this->id = $id;
        $this->model = $model;
        $this->tank = $tank;
        $this->technicalInspectionFactory = $technicalInspectionFactory;
        $this->technicalInspections = $this->infrastructure->createTechInspectionCollection();
        $this->engine = $engine;
    }

    /**
     * @param float $fuelAmount
     * @throws \Exception
     */
    public function fillTank(float $fuelAmount): void
    {
        $this->tank = $this->tank->fill($fuelAmount);
        $this->infrastructure->publishEvent(new TankWasFilledEvent($this->id, $fuelAmount));
    }

    public function makeTechnicalInspection(): void
    {
        $this->technicalInspections->add(
            $this->technicalInspectionFactory->createTechnicalInspection(
                $this->infrastructure->generateTechInspectionId(),
                $this->isOperable()
            )
        );
        $this->infrastructure->publishEvent(new TechInspectionWasMadeEvent($this->id));
    }

    /**
     * @return bool
     */
    protected function isOperable(): bool
    {
        if ($this->tank->isOperable() && !$this->engine->needRepair()) {
            return true;
        }
        return false;
    }

    /**
     * @param $miles
     * @throws \Exception
     */
    public function drive(float $miles): void
    {
        $engine = $this->engine->drive($miles);
        $tank = $this->tank->useFuel($miles);

        $this->engine = $engine;
        $this->tank = $tank;

        $this->infrastructure->publishEvent(new VehicleWasDrivenEvent($this->id, $miles));
    }

    /**
     * @param float $cost
     */
    public function repairEngine(float $cost): void
    {
        $this->engine = $this->engine->repair($cost);
        $this->infrastructure->publishEvent(new EngineWasRepairedEvent($this->id, $cost));
    }
}
