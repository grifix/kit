<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank;

/**
 * Class TankFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Tank
 */
interface TankFactoryInterface
{
    /**
     * @param float $capacity
     * @return TankInterface
     */
    public function createDieselTank(float $capacity): TankInterface;

    /**
     * @param float $capacity
     * @return TankInterface
     */
    public function createGasolineTank(float $capacity): TankInterface;
}