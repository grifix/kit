<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank;

use Grifix\Kit\Collection\CollectionInterface;

/**
 * Interface TankInfrastructureInterface
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank
 */
interface TankInfrastructureInterface
{
    /**
     * @return CollectionInterface
     */
    public function createFillsCollection(): CollectionInterface;
}
