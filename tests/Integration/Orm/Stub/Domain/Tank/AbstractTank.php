<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank;

use Grifix\Kit\Collection\Collection;
use Grifix\Kit\Collection\CollectionInterface;

/**
 * Class Tank
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Entity\Tank
 */
abstract class AbstractTank implements TankInterface
{
    const MAX_NUMBER_OF_FILLS = 100;

    /**
     * @var float
     */
    protected $capacity;

    /**
     * @var float
     */
    protected $fuelLevel = 0;

    /**
     * @var int
     */
    protected $numberOfFills = 0;

    /**
     * @var int
     */
    protected $fuelConsumptionPerMile = 0.01;

    /**
     * @var CollectionInterface
     */
    protected $fills;

    /**
     * @dependency
     * @var \Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\TankInfrastructureInterface
     */
    protected $infrastructure;

    /**
     * Tank constructor.
     * @param $capacity
     */
    public function __construct(TankInfrastructureInterface $infrastructure, float $capacity)
    {
        $this->infrastructure = $infrastructure;
        $this->capacity = $capacity;
        $this->fills = $this->infrastructure->createFillsCollection();
    }

    /**
     * @param float $fuelAmount
     * @return TankInterface
     * @throws \Exception
     */
    public function fill(float $fuelAmount): TankInterface
    {
        $fuelLevel = $this->fuelLevel + $fuelAmount;
        if ($fuelLevel > $this->capacity) {
            throw new \Exception('To many fuel');
        }
        $result = clone ($this);
        $result->fuelLevel = $fuelLevel;
        $result->numberOfFills++;
        $this->fills->add($fuelAmount);
        return $result;
    }

    /**
     * @return bool
     */
    public function isOperable(): bool
    {
        if ($this->numberOfFills <= self::MAX_NUMBER_OF_FILLS) {
            return true;
        }
        return false;
    }

    /**
     * @param float $miles
     * @return float
     */
    protected function calculateFuelConsumption(float $miles): float
    {
        return $miles * $this->fuelConsumptionPerMile;
    }

    /**
     * @param int $miles
     * @return TankInterface
     * @throws \Exception
     */
    public function useFuel(float $miles): TankInterface
    {
        $needFuel = $this->calculateFuelConsumption($miles);
        if ($needFuel > $this->fuelLevel) {
            throw new \Exception('not enough fuel');
        }
        $clone = clone ($this);
        $clone->fuelLevel -= $needFuel;
        return $clone;
    }
}
