<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank;

/**
 * Class TankFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Tank
 */
class TankFactory implements TankFactoryInterface
{

    protected $infrastructure;

    /**
     * TankFactory constructor.
     * @param TankInfrastructureInterface $infrastructure
     */
    public function __construct(TankInfrastructureInterface $infrastructure)
    {
        $this->infrastructure = $infrastructure;
    }

    /**
     * {@inheritdoc}
     */
    public function createDieselTank(float $capacity): TankInterface
    {
        return new DieselTank($this->infrastructure, $capacity);
    }

    /**
     * {@inheritdoc}
     */
    public function createGasolineTank(float $capacity): TankInterface
    {
        return new GasolineTank($this->infrastructure, $capacity);
    }
}
