<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank;

/**
 * Class Tank
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Entity\Tank
 */
interface TankInterface
{

    /**
     * @param float $fuelAmount
     * @return TankInterface
     * @throws \Exception
     */
    public function fill(float $fuelAmount): TankInterface;

    /**
     * @return bool
     */
    public function isOperable(): bool;

    /**
     * @param float $miles
     * @return TankInterface
     * @throws \Exception
     */
    public function useFuel(float $miles): TankInterface;
}