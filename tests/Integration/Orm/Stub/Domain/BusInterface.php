<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain;

/**
 * Class Bus
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Entity
 */
interface BusInterface extends VehicleInterface
{
    /**
     * @param int $amount
     * @throws \Exception
     */
    public function boardPassengers(int $amount): void;
}