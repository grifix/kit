<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain;

use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\EngineInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\TankInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspectionCollectionInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspectionFactoryInterface;
use Grifix\Kit\Uuid\UuidGeneratorInterface;

/**
 * Class Truck
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Entity
 */
class Truck extends AbstractVehicle implements TruckInterface
{
    /**
     * @var float
     */
    protected $tonnage;

    /**
     * @var float
     */
    protected $cargoAmount = 0;

    /**
     * Truck constructor.
     * @param VehicleInfrastructureInterface $infrastructure
     * @param string $id
     * @param string $model
     * @param TankInterface $tank
     * @param EngineInterface $engine
     * @param TechnicalInspectionFactoryInterface $technicalInspectionFactory
     * @param float $tonnage
     * @param TechnicalInspectionCollectionInterface $technicalInspections
     */
    public function __construct(
        VehicleInfrastructureInterface $infrastructure,
        string $id,
        string $model,
        TankInterface $tank,
        EngineInterface $engine,
        TechnicalInspectionFactoryInterface $technicalInspectionFactory,
        float $tonnage
    ) {
        parent::__construct(
            $infrastructure,
            $id,
            $model,
            $tank,
            $engine,
            $technicalInspectionFactory
        );
        $this->tonnage = $tonnage;
    }

    /**
     * @param float $amount
     * @throws \Exception
     */
    public function loadCargo(float $amount): void
    {
        $cargoAmount = $this->cargoAmount + $amount;
        if ($cargoAmount > $this->tonnage) {
            throw new \Exception('Truck is overloaded!');
        }
        $this->cargoAmount = $cargoAmount;
    }
}
