<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Domain;

use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\EngineInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\TankInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspectionCollectionInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspectionFactoryInterface;
use Grifix\Kit\Uuid\UuidGeneratorInterface;

/**
 * Class Bus
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Entity
 */
class Bus extends AbstractVehicle implements BusInterface
{

    /**
     * @var int
     */
    protected $seats;

    protected $passengersAmount = 0;

    /**
     * Bus constructor.
     * @param VehicleInfrastructureInterface $infrastructure
     * @param string $id
     * @param string $model
     * @param TankInterface $tank
     * @param EngineInterface $engine
     * @param TechnicalInspectionFactoryInterface $technicalInspectionFactory
     * @param int $seats
     */
    public function __construct(
        VehicleInfrastructureInterface $infrastructure,
        string $id,
        string $model,
        TankInterface $tank,
        EngineInterface $engine,
        TechnicalInspectionFactoryInterface $technicalInspectionFactory,
        int $seats
    ) {
        parent::__construct(
            $infrastructure,
            $id,
            $model,
            $tank,
            $engine,
            $technicalInspectionFactory

        );
        $this->seats = $seats;
    }

    /**
     * @param int $amount
     * @throws \Exception
     */
    public function boardPassengers(int $amount): void
    {
        $passengersAmount = $this->passengersAmount + $amount;
        if ($passengersAmount > $this->seats) {
            throw new \Exception('Bus is overloaded');
        }
        $this->passengersAmount = $passengersAmount;
    }
}
