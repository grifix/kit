<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure;

use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\VehicleRepositoryInterface;

/**
 * Class VehicleRepositoryFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub
 */
interface VehicleRepositoryFactoryInterface
{
    /**
     * @return VehicleRepositoryInterface
     */
    public function createVehicleRepository(): VehicleRepositoryInterface;
}
