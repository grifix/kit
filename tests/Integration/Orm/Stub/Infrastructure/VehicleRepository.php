<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure;

use Grifix\Kit\Collection\CollectionWrapperTrait;
use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Orm\Repository\RepositoryInterface;
use Grifix\Kit\Specification\SpecificationInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\VehicleInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\VehicleRepositoryInterface;

/**
 * Class VehicleRepository
 * @package Grifix\Acl\Test\Integration\Orm\Stub
 */
class VehicleRepository implements VehicleRepositoryInterface
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    use CollectionWrapperTrait;

    /**
     * VehicleRepository constructor.
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function find(string $id)
    {
        return $this->repository->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function save(VehicleInterface $vehicle): void
    {
        $this->repository->save($vehicle);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(VehicleInterface $vehicle): void
    {
        $this->repository->delete($vehicle);
    }

    /**
     * {@inheritdoc}
     */
    public function match(SpecificationInterface $specification): CollectionInterface
    {
        return $this->repository->match($specification);
    }

    /**
     * {@inheritdoc}
     */
    public function findByModel(string $model): CollectionInterface
    {
        return $this->repository->findBy('model', $model);
    }

    /**
     * {@inheritdoc}
     */
    public function first()
    {
        return $this->repository->first();
    }

    /**
     * {@inheritdoc}
     */
    public function last()
    {
        return $this->repository->last();
    }
}
