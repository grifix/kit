<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure;

use Grifix\Kit\Orm\Collection\CollectionFactoryInterface;
use Grifix\Kit\Orm\EventCollector\EventCollectorInterface;
use Grifix\Kit\Orm\EventCollector\EventPublisherTrait;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspectionCollectionInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\VehicleInfrastructureInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\TechnicalInspection\TechnicalInspectionCollection;
use Grifix\Kit\Uuid\UuidGeneratorInterface;

/**
 * Class VehicleInfrastructure
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure
 */
class VehicleInfrastructure implements VehicleInfrastructureInterface
{

    use EventPublisherTrait;

    /**
     * @var UuidGeneratorInterface
     */
    private $uuidGenerator;

    /**
     * @var CollectionFactoryInterface
     */
    private $collectionFactory;

    /**
     * VehicleInfrastructure constructor.
     * @param UuidGeneratorInterface $uuidGenerator
     * @param CollectionFactoryInterface $collectionFactory
     * @param EventCollectorInterface $eventCollector
     */
    public function __construct(
        UuidGeneratorInterface $uuidGenerator,
        CollectionFactoryInterface $collectionFactory,
        EventCollectorInterface $eventCollector
    ) {
        $this->uuidGenerator = $uuidGenerator;
        $this->collectionFactory = $collectionFactory;
        $this->eventCollector = $eventCollector;
    }

    /**
     * {@inheritdoc}
     */
    public function generateTechInspectionId(): string
    {
        return $this->uuidGenerator->generate();
    }

    /**
     * {@inheritdoc}
     */
    public function createTechInspectionCollection(): TechnicalInspectionCollectionInterface
    {
        return $this->collectionFactory->createArrayCollectionWrapper(TechnicalInspectionCollection::class);
    }
}
