<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure;

use Grifix\Kit\Orm\Blueprint\AbstractBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\AbstractVehicle;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Bus;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair\Repair;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\AbstractTank;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\DieselTank;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\GasolineTank;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspection;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Engine\Repair\RepairBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Engine\Repair\RepairCollection;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\TechnicalInspection\TechnicalInspectionBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Truck;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\TechnicalInspection\TechnicalInspectionCollection;

/**
 * Class VehicleBlueprint
 * @package Grifix\Kit\Test\Integration\Orm\Stub
 */
class VehicleBlueprint extends AbstractBlueprint
{
    protected $entityClass = AbstractVehicle::class;

    protected $table = 'test.vehicle';

    protected $serializerClass = VehicleSerializer::class;

    const TECHNICAL_INSPECTIONS_PROPERTY = 'technicalInspections';

    const ENGINE_REPAIRS_PROPERTY = 'engine.repairs';

    /**
     * {@inheritdoc}
     */
    protected function init(): void
    {
        $this->addRelationToChild(
            self::TECHNICAL_INSPECTIONS_PROPERTY,
            TechnicalInspection::class,
            TechnicalInspectionBlueprint::VEHICLE_COLUMN,
            TechnicalInspectionCollection::class
        );

        $this->addRelationToChild(
            self::ENGINE_REPAIRS_PROPERTY,
            Repair::class,
            RepairBlueprint::VEHICLE_COLUMN,
            RepairCollection::class
        );

        $this->setClassDetector([
            'bus' => Bus::class,
            'truck' => Truck::class
        ]);
    }
}
