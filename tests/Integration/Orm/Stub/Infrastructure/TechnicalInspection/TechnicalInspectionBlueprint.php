<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\TechnicalInspection;

use Grifix\Kit\Orm\Blueprint\AbstractBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\AbstractVehicle;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspection;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\VehicleBlueprint;

/**
 * Class TechnicalInspectionBlueprint
 * @package Grifix\Kit\Test\Integration\Orm\Stub\TechnicalInspection
 */
class TechnicalInspectionBlueprint extends AbstractBlueprint
{
    protected $table = 'test.technical_inspection';

    protected $entityClass = TechnicalInspection::class;

    const VEHICLE_COLUMN = 'vehicle_id';

    /**
     * {@inheritdoc}
     */
    protected function init(): void
    {
        $this->addRelationToParent(
            AbstractVehicle::class,
            VehicleBlueprint::TECHNICAL_INSPECTIONS_PROPERTY,
            self::VEHICLE_COLUMN
        );
    }
}
