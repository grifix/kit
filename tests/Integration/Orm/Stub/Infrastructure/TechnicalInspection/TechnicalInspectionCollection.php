<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\TechnicalInspection;

use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Collection\CollectionWrapperTrait;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspectionCollectionInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\TechnicalInspection\TechnicalInspectionInterface;

/**
 * Class CheckUpCollection
 * @package Grifix\Kit\Test\Integration\Orm\Stub\TechnicalInspection
 */
class TechnicalInspectionCollection implements TechnicalInspectionCollectionInterface
{
    use CollectionWrapperTrait;

    protected $collection;

    /**
     * TechnicalInspectionCollection constructor.
     * @param CollectionInterface $collection
     */
    public function __construct(CollectionInterface $collection)
    {
        $this->collection = $collection;
    }

    /**
     * {@inheritdoc}
     */
    public function add(TechnicalInspectionInterface $technicalInspection): void
    {
        $this->collection->add($technicalInspection);
    }

    /**
     * {@inheritdoc}
     */
    public function find(string $id)
    {
        return $this->collection->find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function remove(TechnicalInspectionInterface $technicalInspection): void
    {
        $this->collection->remove($technicalInspection);
    }
}
