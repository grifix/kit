<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure;

use Grifix\Kit\Orm\EntityManager\EntityManagerInterface;
use Grifix\Kit\Orm\Repository\RepositoryFactoryInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\AbstractVehicle;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\VehicleRepositoryInterface;

/**
 * Class VehicleRepositoryFactory
 * @package Grifix\Kit\Test\Integration\Orm\Stub
 */
class VehicleRepositoryFactory implements VehicleRepositoryFactoryInterface
{
    /**
     * @var RepositoryFactoryInterface
     */
    protected $repositoryFactory;

    /**
     * VehicleRepositoryFactory constructor.
     * @param RepositoryFactoryInterface $repositoryFactory
     */
    public function __construct(RepositoryFactoryInterface $repositoryFactory)
    {
        $this->repositoryFactory = $repositoryFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function createVehicleRepository(): VehicleRepositoryInterface
    {
        return $this->repositoryFactory->createRepositoryWrapper(
            AbstractVehicle::class,
            VehicleRepository::class
        );
    }
}
