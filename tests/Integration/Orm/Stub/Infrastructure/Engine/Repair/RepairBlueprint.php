<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Engine\Repair;


use Grifix\Kit\Orm\Blueprint\AbstractBlueprint;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\AbstractVehicle;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair\Repair;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\VehicleBlueprint;

/**
 * Class RepairBlueprint
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Engine\Repair
 */
class RepairBlueprint extends AbstractBlueprint
{
    protected $table = 'test.repair';

    protected $entityClass = Repair::class;

    const VEHICLE_COLUMN = 'vehicle_id';

    public function init(): void
    {
        $this->addRelationToParent(
            AbstractVehicle::class,
            VehicleBlueprint::ENGINE_REPAIRS_PROPERTY,
            self::VEHICLE_COLUMN
        );
    }
}