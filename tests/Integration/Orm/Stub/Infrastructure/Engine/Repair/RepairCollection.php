<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Engine\Repair;

use Grifix\Kit\Collection\CollectionWrapperTrait;
use Grifix\Kit\Orm\Collection\CollectionInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair\RepairCollectionInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair\RepairInterface;

/**
 * Class RepairCollection
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Repair
 */
class RepairCollection implements RepairCollectionInterface
{
    use CollectionWrapperTrait;

    protected $collection;

    /***
     * RepairCollection constructor.
     * @param CollectionInterface $collection
     */
    public function __construct(CollectionInterface $collection)
    {
        $this->collection = $collection;
    }


    /**
     * {@inheritdoc}
     */
    public function remove(RepairInterface $repair): void
    {
        $this->collection->remove($repair);
    }

    /**
     * {@inheritdoc}
     */
    public function add(RepairInterface $repair): void
    {
        $this->collection->add($repair);
    }

    /**
     * {@inheritdoc}
     */
    public function calculateTotalCost(): float
    {
        $result = 0;
        foreach ($this->collection as $repair) {
            /**@var RepairInterface $repair */
            $result += $repair->getCost();
        }

        return $result;
    }
}
