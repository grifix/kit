<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Engine;

use Grifix\Kit\Orm\Collection\CollectionFactoryInterface;
use Grifix\Kit\Orm\EventCollector\EventPublisherTrait;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\EngineInfrastructureInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Repair\RepairCollectionInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Engine\Repair\RepairCollection;
use Grifix\Kit\Uuid\UuidGeneratorInterface;

/**
 * Class EngineInfrastructure
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine
 */
class EngineInfrastructure implements EngineInfrastructureInterface
{

    use EventPublisherTrait;
    /**
     * @var UuidGeneratorInterface
     */
    private $uuidGenerator;

    /**
     * @var CollectionFactoryInterface
     */
    private $collectionFactory;

    /**
     * EngineInfrastructure constructor.
     * @param UuidGeneratorInterface $uuidGenerator
     * @param CollectionFactoryInterface $collectionFactory
     */
    public function __construct(
        UuidGeneratorInterface $uuidGenerator,
        CollectionFactoryInterface $collectionFactory
    ) {
        $this->uuidGenerator = $uuidGenerator;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function generateRepairId(): string
    {
        return $this->uuidGenerator->generate();
    }

    /**
     * {@inheritdoc}
     */
    public function createRepairCollection(): RepairCollectionInterface
    {
        return $this->collectionFactory->createArrayCollectionWrapper(RepairCollection::class);
    }
}
