<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure;

use Grifix\Kit\Orm\Serializer\ObjectSerializer;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Engine\Engine;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\DieselTank;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\GasolineTank;

/**
 * Class VehicleSerializer
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure
 */
class VehicleSerializer extends ObjectSerializer
{
    public function init(): void
    {
        parent::init();

        $this->addMultiObjectSerializer('tank', [
            'gasoline' => GasolineTank::class,
            'diesel' => DieselTank::class
        ]);

        $this->addObjectSerializer('engine', Engine::class);
    }
}
