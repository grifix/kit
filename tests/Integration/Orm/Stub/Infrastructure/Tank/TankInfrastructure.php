<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Tank;

use Grifix\Kit\Collection\CollectionFactoryInterface;
use Grifix\Kit\Collection\CollectionInterface;
use Grifix\Kit\Test\Integration\Orm\Stub\Domain\Tank\TankInfrastructureInterface;

/**
 * Class TankInfrastructure
 * @package Grifix\Kit\Test\Integration\Orm\Stub\Infrastructure\Tank
 */
class TankInfrastructure implements TankInfrastructureInterface
{

    protected $collectionFactory;

    /**
     * TankInfrastructure constructor.
     * @param CollectionFactoryInterface $collectionFactory
     */
    public function __construct(CollectionFactoryInterface $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function createFillsCollection(): CollectionInterface
    {
        return $this->collectionFactory->createCollection();
    }
}
