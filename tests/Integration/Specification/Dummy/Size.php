<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Specification\Dummy;

/**
 * Class Size
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration\Specification\Stub
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class Size
{
    protected $height;
    protected $length;
    protected $width;
    
    /**
     * Size constructor.
     *
     * @param int $height
     * @param int $length
     * @param int $width
     */
    public function __construct(int $height, int $length, int $width)
    {
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
    }
    
    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }
    
    /**
     * @param int $height
     *
     * @return Size
     */
    public function setHeight(int $height): Size
    {
        $this->height = $height;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }
    
    /**
     * @param int $length
     *
     * @return Size
     */
    public function setLength(int $length): Size
    {
        $this->length = $length;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }
    
    /**
     * @param int $width
     *
     * @return Size
     */
    public function setWidth(int $width): Size
    {
        $this->width = $width;
        
        return $this;
    }
    
}