<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Specification\Dummy\Specification;


use Grifix\Kit\Expression\ExpressionInterface;
use Grifix\Kit\Specification\AbstractSpecification;

/**
 * Class BigSizeSpecification
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration\Specification\Stub
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class BigSizeSpecification extends AbstractSpecification
{
    /**
     * @return ExpressionInterface
     */
    protected function createExpression(): ExpressionInterface
    {
        return $this->andX(
            $this->gte('height', 10),
            $this->gte('width', 10),
            $this->gte('length', 10)
        );
    }
}