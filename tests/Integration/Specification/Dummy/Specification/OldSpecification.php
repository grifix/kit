<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Specification\Dummy\Specification;

use Grifix\Kit\Expression\ExpressionInterface;
use Grifix\Kit\Specification\AbstractSpecification;

/**
 * Class OldSpecification
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration\Specification\Stub
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class OldSpecification extends AbstractSpecification
{
    /**
     * {@inheritdoc}
     */
    protected function createExpression(): ExpressionInterface
    {
        return $this->lte('date', new \DateTime('01.01.2000'));
    }
}