<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Specification;


use Grifix\Kit\Expression\Builder\ExpressionBuilderFactoryInterface;
use Grifix\Kit\Expression\ExpressionFactoryInterface;
use Grifix\Kit\Specification\SpecificationFactoryInterface;
use Grifix\Kit\Test\Integration\AbstractTest;
use Grifix\Kit\Test\Integration\Specification\Dummy\Specification\BigSizeSpecification;
use Grifix\Kit\Test\Integration\Specification\Dummy\Item;
use Grifix\Kit\Test\Integration\Specification\Dummy\Specification\BoxSpecification;
use Grifix\Kit\Test\Integration\Specification\Dummy\Specification\NewBigItemSpecification;
use Grifix\Kit\Test\Integration\Specification\Dummy\Specification\OldSpecification;
use Grifix\Kit\Test\Integration\Specification\Dummy\Size;

/**
 * Class SpecificationTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration\Specification
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SpecificationTest extends AbstractTest
{
    /**
     * @var SpecificationFactoryInterface
     */
    protected $sf;

    protected function setUp()
    {
        parent::setUp();
        $this->sf = $this->getShared(SpecificationFactoryInterface::class);
    }

    public function testSpecification()
    {
        $item = new Item('Box', new \DateTime('01.01.1991'));
        $spec = $this->sf->createSpecification(OldSpecification::class);
        self::assertTrue($spec->isSatisfiedBy($item));

        $item = new Item('Box', new \DateTime('02.01.2003'));
        self::assertFalse($spec->isSatisfiedBy($item));
    }

    public function testNestedSpecification()
    {
        $spec = $this->sf->createSpecification(OldSpecification::class);
        $spec->andSpecification($this->sf->createSpecification(BoxSpecification::class));
        $spec->andSpecification($this->sf->createSpecification(BigSizeSpecification::class), 'size');
        $item = new Item('Box', new \DateTime('01.01.1991'), new Size(11, 11, 11));

        self::assertTrue($spec->isSatisfiedBy($item));

        $item->getSize()->setHeight(9);

        self::assertFalse($spec->isSatisfiedBy($item));

        $item->getSize()->setHeight(11);

        self::assertTrue($spec->isSatisfiedBy($item));

        $item->setDate(new \DateTime());

        self::assertFalse($spec->isSatisfiedBy($item));

        $item->setDate(new \DateTime('01.01.1999'));

        self::assertTrue($spec->isSatisfiedBy($item));

        $item->setName('Fox');

        self::assertFalse($spec->isSatisfiedBy($item));
    }

    public function testNestedObjectSpecification()
    {
        $spec = $this->sf->createSpecification(NewBigItemSpecification::class);
        $item = new Item('Box', new \DateTime('01.01.2020'), new Size(22, 22, 22));
        self::assertTrue($spec->isSatisfiedBy($item));

        $item->getSize()->setHeight(10);

        self::assertFalse($spec->isSatisfiedBy($item));
    }
}
