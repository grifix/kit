<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Db;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Query\Criterion\CriterionFactory;
use Grifix\Kit\Db\Query\Criterion\CriterionInterface;
use Grifix\Kit\Db\Query\Part\PartFactory;
use Grifix\Kit\Db\Query\Query;
use Grifix\Kit\Kernel\ClassMaker;
use Grifix\Kit\Test\Integration\AbstractTest;

/**
 * Class ProcessorTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration\Db
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ProcessorTest extends AbstractTest
{
    
    /**
     * @var ConnectionInterface
     */
    protected $connection;
    
    /**
     * @return Query
     */
    protected function makeQuery()
    {
        $this->connection = $this->getShared(ConnectionInterface::class);
        
        return new Query(
            new PartFactory(new CriterionFactory(new ClassMaker()), new ClassMaker()),
            $this->connection
        );
    }
    
    public function testInsert()
    {
        $query = $this->makeQuery();
        $query->insert([
            'id' => 1,
            'name' => 'test',
        ])
            ->into('table', 'tableAlias')
            ->insert(['date' => '2001-01-01']);
        
        static::assertEquals(
            'INSERT INTO "table" AS "tableAlias" ("id", "name", "date") VALUES (1, \'test\', \'2001-01-01\')',
            $this->connection->createStatementFromQuery($query)->getSql(true)
        );
    }
    
    public function testInsertRaw()
    {
        $query = $this->makeQuery();
        $query->nativeInsert('INTO "table" VALUES (:id, :name, :date)')
            ->bindValues([
                'id' => 1,
                'name' => 'test',
                'date' => '2001-01-01',
            ]);
        static::assertEquals(
            'INSERT INTO "table" VALUES (1, \'test\', \'2001-01-01\')',
            $this->connection->createStatementFromQuery($query)->getSql(true)
        );
    }
    
    public function testDelete()
    {
        $query = $this->makeQuery();
        $query->delete()->from('table', 'tableAlias')
            ->where('id = :id')
            ->orWhere('name = :name')
            ->nativeWhere('email = :email')
            ->nativeOrWhere('address = :address')
            ->where(function (CriterionInterface $criterion) {
                $criterion->setExpression('first_name = :first_name')
                    ->andExpression('last_name = :last_name')
                    ->andExpression(function (CriterionInterface $criterion) {
                        $criterion->setExpression('width = :width')->orExpression('height = :height');
                    })
                    ->orExpression('color = :color');
            });
        self::assertEquals(
            'DELETE FROM "table" AS "tableAlias" WHERE id = :id OR name = :name AND email = :email OR address = :address AND ( first_name = :first_name AND last_name = :last_name AND ( width = :width OR height = :height ) OR color = :color )',
            $this->connection->createStatementFromQuery($query)->getSql(true)
        );
    }
    
    public function testSelectFrom()
    {
        $query = $this->makeQuery();
        $query->select('id, name, MAX(id)')->from('table', 'alias');
        self::assertEquals(
            'SELECT id, name, MAX(id) FROM "table" AS "alias"',
            $this->connection->createStatementFromQuery($query)->getSql(true)
        );
    }
    
    public function testSelectJoin()
    {
        $query = $this->makeQuery();
        $query->select('*')->from('table', 't1')->innerJoin('table2', 't2', 't1.id = t2.key');
        self::assertEquals(
            'SELECT * FROM "table" AS "t1" INNER JOIN "table2" AS "t2" ON t1.id = t2.key',
            $this->connection->createStatementFromQuery($query)->getSql(true)
        );
    }
    
    public function testSelectNestedJoin()
    {
        $query = $this->makeQuery();
        $query->select('*')->from('table', 't1')->innerJoin(
            'table2',
            't2',
            function (CriterionInterface $criterion) {
                $criterion->setExpression('t1.id = t2.key')
                    ->andExpression(
                        function (CriterionInterface $criterion) {
                            $criterion->setExpression('t1.id2 = t2.key2')
                                ->orExpression('t1.id3 = t2.key3');
                        }
                    );
            }
        );
        self::assertEquals(
            'SELECT * FROM "table" AS "t1" INNER JOIN "table2" AS "t2" ON ( t1.id = t2.key AND ( t1.id2 = t2.key2 OR t1.id3 = t2.key3 ) )',
            $this->connection->createStatementFromQuery($query)->getSql(true)
        );
    }
    
    public function testSelectWhere()
    {
        $query = $this->makeQuery();
        $query->select('*')->from('table')->where('id = :id')->bindValue('id', 1);
        self::assertEquals(
            'SELECT * FROM "table" WHERE id = 1',
            $this->connection->createStatementFromQuery($query)->getSql(true)
        );
    }
    
    public function testSelectNestedWhere()
    {
        $query = $this->makeQuery();
        $query->select('*')->from('table')->where(function (CriterionInterface $criterion) {
            $criterion
                ->setExpression('id = :id')->bindValue('id', 1)
                ->andExpression('name = :name')->bindValue('name', 'Joe')
                ->andExpression(
                    function (CriterionInterface $criterion) {
                        $criterion
                            ->setExpression('date = :date')->bindValue('date', '01-01-2001')
                            ->orExpression('status = :status')->bindValue('status', 'open');
                    }
                );
        });
        self::assertEquals(
            'SELECT * FROM "table" WHERE ( id = 1 AND name = \'Joe\' AND ( date = \'01-01-2001\' OR status = \'open\' ) )',
            $this->connection->createStatementFromQuery($query)->getSql(true)
        );
    }
    
    public function testSelectGroupBy()
    {
        $query = $this->makeQuery();
        $query->select('*')->from('table')->groupBy('id, name');
        self::assertEquals(
            'SELECT * FROM "table" GROUP BY id, name',
            $this->connection->createStatementFromQuery($query)->getSql(true)
        );
    }
    
    public function testSelectUnion()
    {
        $query = $this->makeQuery();
        $query
            ->select('*')
            ->from('table')
            ->union($this->makeQuery()->select('*')->from('table2'))
            ->union($this->makeQuery()->select('*')->from('table3'), true);
        
        self::assertEquals(
            'SELECT * FROM "table" UNION SELECT * FROM "table2" UNION ALL SELECT * FROM "table3"',
            $this->connection->createStatementFromQuery($query)->getSql(true)
        );
    }
    
    public function testSelectWith()
    {
        $query = $this->makeQuery();
        $query
            ->select('*')
            ->from('table')
            ->with($this->makeQuery()->select('*')->from('table2'), 't2')
            ->with($this->makeQuery()->select('*')->from('table3'), 't3');
        
        self::assertEquals(
            'WITH "t2" AS (SELECT * FROM "table2") , "t3" AS (SELECT * FROM "table3") SELECT * FROM "table"',
            $this->connection->createStatementFromQuery($query)->getSql(true)
        );
    }
    
    public function testUpdate()
    {
        $query = $this->makeQuery();
        $query->update('table', 't')->set([
            'name' => 'Joe',
            'status' => 'open',
        ]);
        self::assertEquals(
            'UPDATE "table" AS "t" SET "name" = \'Joe\', "status" = \'open\'',
            $this->connection->createStatementFromQuery($query)->getSql(true)
        );
    }
}
