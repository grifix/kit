<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration\Db;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Test\Integration\AbstractTest;

/**
 * Class ConnectionTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration\Db
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ConnectionTest extends AbstractTest
{
    
    /**
     * @var ConnectionInterface
     */
    protected $connection;
    
    public function setUp()
    {
        parent::setUp();
        $this->connection = $this->getShared(ConnectionInterface::class);
        $this->connection->execute('
            DROP TABLE IF EXISTS test_item;
            CREATE TABLE test_item (
              id BIGSERIAL NOT NULL,
              name VARCHAR,
              description VARCHAR,
              PRIMARY KEY(id)
            ) 
            WITH (OIDS = FALSE);
            
            ALTER TABLE test_item
              ALTER COLUMN id SET STATISTICS 0;
            
            ALTER TABLE test_item
              ALTER COLUMN name SET STATISTICS 0;
        ');
    }
    
    /**
     * @return void
     */
    public function testFetchAll()
    {
        $this->connection->execute(
            'INSERT INTO test_item 
            (name, description) 
            VALUES 
            (:name, :description)',
            ['name' => 'Joe', 'description' => 'text']
        );
        
        self::assertEquals(
            [
                [
                    'id' => 1,
                    'name' => 'Joe',
                    'description' => 'text',
                ],
            
            ],
            $this->connection->createStatement('SELECT * FROM test_item')->execute()->fetchAll()
        );
    }
    
    /**
     * @return void
     */
    public function testCache()
    {
        $this->connection->execute(
            'INSERT INTO test_item 
            (name, description) 
            VALUES 
            (:name, :description)',
            ['name' => 'Joe', 'description' => 'text']
        );
        
        $r = $this->connection->createStatement('SELECT * FROM test_item')->executeAndFetchAll(10);
        
        $this->connection->execute('DELETE FROM test_item');
        
        $r2 = $this->connection->createStatement('SELECT * FROM test_item')->executeAndFetchAll(10);
        self::assertEquals($r, $r2);
    }
    
    /**
     * @return void
     */
    public function testAllocateId()
    {
        self::assertEquals(1, $this->connection->allocateId('test_item'));
        self::assertEquals(2, $this->connection->allocateId('test_item'));
        self::assertEquals(3, $this->connection->allocateId('test_item'));
        $this->connection->createQuery()->insert(['name' => 'Foo', 'description' => 'Bar'])->into('test_item');
        self::assertEquals(3, $this->connection->getLastInsertedId('test_item'));
    }
    
    public function tearDown()
    {
        parent::tearDown();
        $this->connection->execute('
            DROP TABLE IF EXISTS test_item;
        ');
    }
}
