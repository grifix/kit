<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Conversion;

use Grifix\Kit\Conversion\ConversionFactoryInterface;
use Grifix\Kit\Test\Integration\AbstractTest;
use Grifix\Kit\Conversion\Converter\HtmlToTextConverter;
use Grifix\Kit\Conversion\Converter\StringToDateConverter;

/**
 * Class AclTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration\Conversion
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ConversionTest extends AbstractTest
{
    public function testConversion()
    {
        $conversion = $this->getShared(ConversionFactoryInterface::class)->createConversion();
        $data = [
            'date' => '2012-02-01',
            'text' => '<b>test</b>',
        ];
        $conversion->createField('date')->createConverter(StringToDateConverter::class);
        $conversion->createField('text')->createConverter(HtmlToTextConverter::class);
        $conversion->convert($data);
        self::assertEquals('test', $data['text']);
        self::assertInstanceOf(\DateTimeInterface::class, $data['date']);
        self::assertEquals('2012-02-01', $data['date']->format('Y-m-d'));
    }
}