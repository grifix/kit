<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Test\Integration;

use Grifix\Kit\Config\ConfigInterface;
use Grifix\Kit\EntryPoint\IntegrationTestEntryPoint;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Queue\QueueFactoryInterface;
use Grifix\Kit\Queue\QueueInterface;
use Grifix\Kit\Queue\SyncQueue;

/**
 * Class AbstractTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
abstract class AbstractTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var IocContainerInterface
     */
    protected $iocContainer;

    protected function setUp()
    {
        $rootDir = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
        include $rootDir . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

        $context = (new IntegrationTestEntryPoint($rootDir))->enter();
        $this->iocContainer = $context->getIocContainer();
        $this->getShared(ConfigInterface::class)->set('grifix.kit.queue.async', false);
        $this->setShared(QueueInterface::class, $this->getShared(QueueFactoryInterface::class)->createSyncQueue());
    }

    /**
     * @param string $alias
     *
     * @return mixed
     */
    protected function getShared(string $alias)
    {
        return $this->iocContainer->get($alias);
    }

    /**
     * @param string $alias
     * @param        $value
     *
     * @return void
     */
    protected function setShared(string $alias, $value)
    {
        $this->iocContainer->set($alias, $value);
    }
}
