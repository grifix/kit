<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Integration\Validation;

use Grifix\Kit\Test\Integration\AbstractTest;
use Grifix\Kit\Validation\ErrorInterface;
use Grifix\Kit\Validation\ValidationFactoryInterface;
use Grifix\Kit\Validation\Validator\IntValidator;
use Grifix\Kit\Validation\Validator\MinNumOfSymbolsValidator;
use Grifix\Kit\Validation\Validator\RomanAlphabetValidator;

/**
 * Class ValidationTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Integration\Field
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ValidationTest extends AbstractTest
{
    public function testValidation()
    {
        $data = [
            'name' => 'a b c',
            'age' => 'red',
        ];
        
        $validation = $this->getShared(ValidationFactoryInterface::class)->createValidation();
        $validation->createField('name')
            ->setLabel('Name')
            ->createValidator(RomanAlphabetValidator::class)
            ->notCancelOnFail();
        
        $validation->getField('name')
            ->setMessage(RomanAlphabetValidator::class, function (ErrorInterface $error) {
                return 'invalid romanAlphabet ' . $error->getValue();
            });
        
        $validation->getField('name')
            ->createValidator(MinNumOfSymbolsValidator::class)
            ->setNumOfSymbols(25);
        
        $validation->getField('name')
            ->setMessage(MinNumOfSymbolsValidator::class, 'invalid minNum');
        
        $validation->createField('age')
            ->setLabel('Age')
            ->createValidator(IntValidator::class);
        
        
        $validation->validate($data);
        
        self::assertEquals(3, count($validation->getErrors()));
        self::assertEquals($validation->getErrors()[0]->getMessage(), 'invalid romanAlphabet a b c');
        self::assertEquals($validation->getErrors()[1]->getMessage(), 'invalid minNum');
        
        //Disable field
        $validation->getField('age')->disable();
        $validation->validate($data);
        self::assertEquals(2, count($validation->getErrors()));
        
        //Enable field
        $validation->getField('age')->enable();
        $validation->validate($data);
        self::assertEquals(3, count($validation->getErrors()));
        
        //Disable validator on field
        $validation->getField('name')->getValidator(RomanAlphabetValidator::class)->disable();
        $validation->validate($data);
        self::assertEquals(2, count($validation->getErrors()));
        
        //Enable validator on field
        $validation->getField('name')->getValidator(RomanAlphabetValidator::class)->enable();
        $validation->validate($data);
        self::assertEquals(3, count($validation->getErrors()));
        
        //Enable cancel on validator fail
        $validation->getField('name')->getValidator(RomanAlphabetValidator::class)->cancelOnFail();
        $validation->validate($data);
        self::assertEquals(2, count($validation->getErrors()));
    
        //Disable cancel on validator fail
        $validation->getField('name')->getValidator(RomanAlphabetValidator::class)->notCancelOnFail();
        $validation->validate($data);
        self::assertEquals(3, count($validation->getErrors()));
    }
}