<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Test\Unit\View;

use Grifix\Kit\EventBus\EventBus;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Helper\ClassHelper;
use Grifix\Kit\Intl\Lang\RusLang;
use Grifix\Kit\Intl\Translator;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\ClassMaker;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Queue\QueueInterface;
use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\Ui\Request\RequestDispatcher;
use Grifix\Kit\Ui\Request\RequestHandlerFactoryResolver;
use Grifix\Kit\View\Asset\AssetCombinerFactory;
use Grifix\Kit\View\Asset\AssetFactory;
use Grifix\Kit\View\Helper\ViewHelperFactory;
use Grifix\Kit\View\Input\InputBuilderFactory;
use Grifix\Kit\View\ViewFactory;
use Grifix\Kit\View\Skin\SkinFactory;
use Grifix\Kit\Helper\FilesystemHelper;
use Grifix\Kit\Helper\ArrayHelper;
use Mockery as m;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class ViewFactoryTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Unit\View
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ViewFactoryTest extends AbstractTest
{
    /**
     * @param $path
     *
     * @return mixed
     */
    protected function makePath($path)
    {
        return str_replace('/', DIRECTORY_SEPARATOR, $path);
    }


    public function testCreateView()
    {
        $rootDir = $this->makePath(dirname(__FILE__) . '/Stub');
        $appDir = $this->makePath($rootDir . '/app');
        $vendorDir = $this->makePath($rootDir . '/vendor');
        $kernel = m::mock(KernelInterface::class, [
            'getAppDir' => $appDir,
            'getVendorDir' => $vendorDir,
            'getRootDir' => $rootDir,
        ]);
        /**@var $kernel KernelInterface */

        $ioc = m::mock(IocContainerInterface::class);
        /**@var $ioc IocContainerInterface */

        $eventBus = m::mock(EventBusInterface::class, ['trigger' => null]);
        /**@var $eventBus EventBusInterface */

        $cache = m::mock(CacheInterface::class, ['get' => null, 'set' => true]);
        /**@var $cache CacheInterface */

        $skinFactory = new SkinFactory(
            $kernel,
            new FilesystemHelper(new Filesystem()),
            new ArrayHelper(),
            new ClassMaker(),
            'skin3'
        );

        $translator = new Translator([new RusLang(new ArrayHelper())], new ArrayHelper());

        $assetCombinerFactory = new AssetCombinerFactory(
            new ClassMaker(),
            new AssetFactory(
                new ClassMaker(),
                $rootDir,
                new FilesystemHelper(new Filesystem())
            ),
            new FilesystemHelper(new Filesystem()),
            new EventBus(new ClassHelper(), m::mock(QueueInterface::class)),
            new ArrayHelper()
        );

        $viewFactory = new ViewFactory(
            $ioc,
            $translator,
            $eventBus,
            $cache,
            $kernel,
            $skinFactory,
            $assetCombinerFactory,
            new FilesystemHelper(new Filesystem()),
            new RequestDispatcher(new RequestHandlerFactoryResolver(new ClassMaker(), $ioc), $eventBus),
            new ArrayHelper(),
            new ClassMaker(),
            new ViewHelperFactory(new ClassMaker(), new InputBuilderFactory(new ClassMaker())),
            $rootDir,
            $rootDir,
            true
        );

        $view = $viewFactory->create('grifix.kit.skin1.tpl.test');
        self::assertEquals(
            $this->makePath($vendorDir . '/grifix/kit/views/skin1/tpl.test.php'),
            $view->getPath()
        );

        self::assertEquals('grifix_kit_tpl_test', $view->getCssClass());

        $view = $viewFactory->create('grifix.kit.{skin}.tpl.test');
        self::assertEquals(
            $this->makePath($vendorDir . '/grifix/kit/views/skin1/tpl.test.php'),
            $view->getPath()
        );

        $view = $viewFactory->create('grifix.kit.{skin}.sub.tpl.test');
        self::assertEquals(
            $this->makePath($vendorDir . '/grifix/kit/views/skin1/sub/tpl.test.php'),
            $view->getPath()
        );

        $view = $viewFactory->create('grifix.kit.{skin}.tpl.test2');
        self::assertEquals(
            $this->makePath($appDir . '/grifix/kit/views/skin2/tpl.test2.php'),
            $view->getPath()
        );
    }
}
