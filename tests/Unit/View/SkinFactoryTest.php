<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\View;

use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Helper\FilesystemHelper;
use Grifix\Kit\Kernel\ClassMaker;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\View\Skin\SkinFactory;
use Mockery as m;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class SkinTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Unit\View
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class SkinTest extends AbstractTest
{
    public function testCreateSkin()
    {
        $kernel = m::mock(KernelInterface::class, [
            'getAppDir' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Stub' . DIRECTORY_SEPARATOR . 'app',
            'getVendorDir' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Stub' . DIRECTORY_SEPARATOR . 'vendor',
        ]);
        
        /**@var $kernel KernelInterface */
        $skinFactory = new SkinFactory(
            $kernel,
            new FilesystemHelper(new Filesystem()),
            new ArrayHelper(),
            new ClassMaker(),
            'skin1'
        );
        $skin = $skinFactory->createSkin('skin1');
        self::assertNull($skin->getParentSkin());
        self::assertTrue($skin->isPersistent());
        
        $skin = $skinFactory->createSkin('skin3');
        self::assertEquals(['skin1', 'skin2', 'skin3'], $skin->getParentSkinsNames());
        self::assertTrue($skin->isPersistent());
    }
}
