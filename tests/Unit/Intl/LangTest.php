<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Intl;

use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Intl\Lang\RusLang;
use Grifix\Kit\Test\Unit\AbstractTest;

/**
 * Class LangTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Unit\Intl
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class LangTest extends AbstractTest
{
    public function testRusTranslate()
    {
        $lang = new RusLang(
            new ArrayHelper(),
            [
                'test' => [
                    'msg_text' => 'Текст',
                    'item' => [
                        'cases' => [
                            'nom' => ['предмет', 'предметы'],
                            'gen' => ['предмета', 'предметов'],
                            'dat' => ['предмету', 'предметам'],
                            'acc' => ['предмета', 'предметов'],
                            'abl' => ['предметом', 'предметами'],
                            'prep' => ['предмете', 'предметах'],
                        ],
                    ],
                    'msg_forms' => [
                        'forms' => [
                            'p' => 'Было показано {quant} _(test.item.{quant})',
                            's' => 'Был показан {quant} _(test.item.{quant})',
                        ],
                    ],
                    'msg_vars' => 'Адрес {email} не найден',
                    'msg_eval' => '_(test.item) не найден',
                    'msg_evalWithNum' => 'Найдено {quant} _(test.item.{quant})',
                    'msg_evalMultiple' => 'Найден _(test.item.dat.p) и _(test.item) и _(test.item.p)',
                ],
            ]
        );
        
        self::assertEquals('Текст', $lang->translate('test.msg_text'));
        self::assertEquals('предмет', $lang->translate('test.item'));
        self::assertEquals('предметы', $lang->translate('test.item.p'));
        self::assertEquals('предметы', $lang->translate('test.item.p.nom'));
        self::assertEquals('предметы', $lang->translate('test.item.nom.p'));
        self::assertEquals('предмета', $lang->translate('test.item.acc'));
        self::assertEquals('предмета', $lang->translate('test.item.acc.s'));
        self::assertEquals('предметов', $lang->translate('test.item.acc.p'));
        self::assertEquals('предмет', $lang->translate('test.item.1'));
        self::assertEquals('предмета', $lang->translate('test.item.2'));
        self::assertEquals('предметов', $lang->translate('test.item.5'));
        self::assertEquals('предметов', $lang->translate('test.item.11'));
        self::assertEquals('предмет', $lang->translate('test.item.21'));
        self::assertEquals('предметам', $lang->translate('test.item.2.dat'));
        self::assertEquals('предметам', $lang->translate('test.item.11.dat'));
        self::assertEquals('предмету', $lang->translate('test.item.1.dat'));
        self::assertEquals('предмету', $lang->translate('test.item.31.dat'));
        self::assertEquals(
            'Адрес user@gmail.com не найден',
            $lang->translate('test.msg_vars', ['email' => 'user@gmail.com'])
        );
        self::assertEquals(
            'предмет не найден',
            $lang->translate('test.msg_eval')
        );
        self::assertEquals(
            'Найдено 5 предметов',
            $lang->translate('test.msg_evalWithNum', ['quant' => 5])
        );
        self::assertEquals(
            'Найден предметам и предмет и предметы',
            $lang->translate('test.msg_evalMultiple', ['quant' => 5])
        );
    }
}