<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Kernel;

use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Helper\FilesystemHelper;
use Grifix\Kit\Kernel\KernelInterface;
use Grifix\Kit\Kernel\Module\Module;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class ModuleTest
 *
 * @category Alias
 * @package  Alias\Kit\Test\Kernel
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class ModuleTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return void
     */
    public function testGetConfig()
    {
        $kernel = \Mockery::mock(
            KernelInterface::class,
            [
                'getAppDir' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Stub' . DIRECTORY_SEPARATOR . 'app',
                'getVendorDir' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Stub' . DIRECTORY_SEPARATOR . 'vendor',
            ]
        );
        
        /**@var $kernel KernelInterface */
        $module = new Module('blue', 'bravo', 1, $kernel, new FilesystemHelper(new Filesystem()), new ArrayHelper());
        self::assertEquals([
            'api' =>
                [
                    'http' =>
                        [
                            'url' => 'http://site.com',
                        ],
                    'json' =>
                        [
                            'main' =>
                                [
                                    'url' => 'http://json.main',
                                ],
                        ],
                ],
            'base' =>
                [
                    'name' => 'vendor',
                ],
            'extend' =>
                [
                    'dir' => 'home',
                ],
        ], $module->getConfig());
    }
}
