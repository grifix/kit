<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Kernel;

use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Helper\FilesystemHelper;
use Grifix\Kit\Ioc\IocContainerInterface;
use Grifix\Kit\Kernel\Kernel;
use Grifix\Kit\Kernel\Module\ModuleInterface;
use Grifix\Kit\Test\Unit\AbstractTest;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class KernelTest
 *
 * @category Alias
 * @package  Alias\Kit\Test\Kernel
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class KernelTest extends AbstractTest
{
    public function testGetModules()
    {
        
        $ioc = \Mockery::mock(IocContainerInterface::class);
        $cache = \Mockery::mock(CacheInterface::class, ['get' => null, 'set' => true, 'has' => false]);
        
        /**@var $ioc IocContainerInterface */
        /**@var $cache CacheInterface */
        $kernel = new Kernel(
            dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Stub',
            $cache,
            new FilesystemHelper(new Filesystem()),
            new ArrayHelper()
        );
        
        
        /**@var $modules ModuleInterface[] */
        $modules = $kernel->getModules();
        $result = [];
        foreach ($modules as $module) {
            $result[] = $module->getNamespace();
        }
        self::assertEquals(
            [
                'White\Gamma',
                'Green\Alpha',
                'Red\Lima',
                'Green\Beta',
                'Blue\Bravo',
            ],
            $result
        );
    }
}
