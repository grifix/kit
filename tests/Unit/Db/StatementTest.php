<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Db;

use Grifix\Kit\Db\ConnectionInterface;
use Grifix\Kit\Db\Statement;
use Grifix\Kit\Db\StatementFactoryInterface;
use Grifix\Kit\EventBus\EventBusInterface;
use Grifix\Kit\Test\Unit\AbstractTest;
use Mockery as m;
use Psr\SimpleCache\CacheInterface;

/**
 * Class StatementTest
 *
 * @category Grifix
 * @package  Grifix\Kit\Test\Unit\Db
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class StatementTest extends AbstractTest
{
    
    /**
     * @return void
     */
    public function testBindings()
    {
        
        $pdo = m::mock(
            \PDO::class,
            [
                'prepare' => m::mock(\PDOStatement::class, [
                    'bindValue' => true,
                ]),
            ]
        );
        $pdo->shouldReceive('quote')->andReturnUsing(function ($var) {
            return "'" . $var . "'";
        });
        
        /**@var $connection ConnectionInterface */
        $connection = m::mock(ConnectionInterface::class, [
            'getPdo' => $pdo,
        ]);
        
        $cache = m::mock(CacheInterface::class);
        
        /**@var $statementFactory StatementFactoryInterface */
        $statementFactory = m::mock(StatementFactoryInterface::class);
    
        /**@var $eventBus EventBusInterface */
        $eventBus = m::mock(EventBusInterface::class);
        
        $statement = new Statement(
            'SELECT * FORM "table" WHERE "id" = :id OR "color" IN (:colors)',
            $connection,
            $statementFactory,
            $eventBus,
            $cache
        );
        
        $statement->bindValue('id', 100);
        $statement->bindValue('colors', ['red', 'white', 'black']);
        self::assertEquals(
            'SELECT * FORM "table" WHERE "id" = 100 OR "color" IN (\'red\', \'white\', \'black\')',
            $statement->getSql(true)
        );
    }
}
