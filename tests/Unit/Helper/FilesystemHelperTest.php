<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Helper;

use Grifix\Kit\Helper\FilesystemHelper;
use Grifix\Kit\Test\Unit\AbstractTest;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class FilesystemHelperTest
 *
 * @category Alias
 * @package  Alias\Kit\Test\Helper
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class FilesystemHelperTest extends AbstractTest
{
    public function testScanDir()
    {
        $dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Stub' . DIRECTORY_SEPARATOR . 'dir';
        $fs = new FilesystemHelper(new Filesystem());
        static::assertEquals([
            0 =>
                [
                    'type' => FilesystemHelper::TYPE_FILE,
                    'path' =>  $dir.'/file_a.txt',
                    'dirname' =>  $dir,
                    'basename' => 'file_a.txt',
                    'extension' => 'txt',
                    'filename' => 'file_a',
                ],
            1 =>
                [
                    'type' => FilesystemHelper::TYPE_FILE,
                    'path' => $dir.'/file_x.txt',
                    'dirname' => $dir,
                    'basename' => 'file_x.txt',
                    'extension' => 'txt',
                    'filename' => 'file_x',
                ],
            2 =>
                [
                    'type' => FilesystemHelper::TYPE_DIR,
                    'path' => $dir.'/subdir_a',
                    'dirname' => $dir,
                    'basename' => 'subdir_a',
                    'filename' => 'subdir_a',
                ],
            3 =>
                [
                    'type' => FilesystemHelper::TYPE_FILE,
                    'path' => $dir.'/subdir_a/file_d.txt',
                    'dirname' => $dir.'/subdir_a',
                    'basename' => 'file_d.txt',
                    'extension' => 'txt',
                    'filename' => 'file_d',
                ],
            4 =>
                [
                    'type' => FilesystemHelper::TYPE_DIR,
                    'path' => $dir.'/subdir_a/subdir_c',
                    'dirname' => $dir.'/subdir_a',
                    'basename' => 'subdir_c',
                    'filename' => 'subdir_c',
                ],
            5 =>
                [
                    'type' => FilesystemHelper::TYPE_FILE,
                    'path' => $dir.'/subdir_a/subdir_c/file_f.txt',
                    'dirname' => $dir.'/subdir_a/subdir_c',
                    'basename' => 'file_f.txt',
                    'extension' => 'txt',
                    'filename' => 'file_f',
                ],
            6 =>
                [
                    'type' => FilesystemHelper::TYPE_DIR,
                    'path' => $dir.'/subdir_b',
                    'dirname' => $dir,
                    'basename' => 'subdir_b',
                    'filename' => 'subdir_b',
                ],
            7 =>
                [
                    'type' => FilesystemHelper::TYPE_FILE,
                    'path' => $dir.'/subdir_b/file_b.txt',
                    'dirname' => $dir.'/subdir_b',
                    'basename' => 'file_b.txt',
                    'extension' => 'txt',
                    'filename' => 'file_b',
                ],
            8 =>
                [
                    'type' => FilesystemHelper::TYPE_FILE,
                    'path' => $dir.'/subdir_b/file_c.txt',
                    'dirname' => $dir.'/subdir_b',
                    'basename' => 'file_c.txt',
                    'extension' => 'txt',
                    'filename' => 'file_c',
                ],
        ], $fs->scanDir($dir, true));
    }
}
