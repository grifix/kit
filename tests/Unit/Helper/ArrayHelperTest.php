<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Helper;

use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Test\Unit\AbstractTest;

/**
 * Class ArrayHelperTest
 *
 * @category           Alias
 * @package            Alias\Utils
 * @author             Mike Shapovalov <smikebox@gmail.com>
 * @license            http://opensource.org/licenses/MIT MIT
 * @link               http://grifix.net/utils/arr
 * @coversDefaultClass Alias\Utils\ArrayHelper
 */
class ArrayHelperTest extends AbstractTest
{
    /**
     * Tests set and get methods
     *
     * @return void
     */
    public function testSetAndGet()
    {
        $array = [];
        $helper = new ArrayHelper();
        $helper->set($array, 'foo.bar', 'test');
        self::assertEquals(
            [
                'foo' => [
                    'bar' => 'test'
                ]
            ],
            $array
        );
        self::assertEquals('test', $helper->get($array, 'foo.bar'));
    }
}
