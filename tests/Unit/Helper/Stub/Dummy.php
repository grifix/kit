<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Unit\Helper\Stub;

/**
 * Class A
 * @package Grifix\Kit\Test\Unit\Helper
 */
class Dummy
{
    /**
     * @var mixed
     */
    private $value;

    /**
     * TypeA constructor.
     * @param mixed $value
     */
    public function __construct($value = null)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}
