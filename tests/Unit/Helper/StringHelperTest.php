<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Test\Unit\Helper;

use Grifix\Kit\Helper\StringHelper;
use Grifix\Kit\Test\Unit\AbstractTest;

/**
 * Class StringHelperTestHelperTest
 * @package Grifix\Kit\Test\Unit\Helper
 */
class StringHelperTestHelperTest extends AbstractTest
{
    public function testIsJson()
    {
        $helper = new StringHelper();
        self::assertFalse($helper->isJson('text'));
        self::assertTrue($helper->isJson('{"foo":"bar"}'));
        self::assertFalse($helper->isJson("{"));
    }
}
