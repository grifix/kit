<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace Grifix\Kit\Test\Unit\Ioc;

use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\Test\Unit\Ioc\Stub\Body;
use Grifix\Kit\Test\Unit\Ioc\Stub\CarInterface;
use Grifix\Kit\Test\Unit\Ioc\Stub\Car;
use Grifix\Kit\Ioc\DefinitionMaker;
use Grifix\Kit\Test\Unit\Ioc\Stub\EngineInterface;
use Psr\SimpleCache\CacheInterface;

/**
 * Class DefinitionMakerTest
 *
 * @category Alias
 * @package  Alias\Ioc\Tests
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class DefinitionMakerTest extends AbstractTest
{
    public function testMakeDefinition()
    {
        $cache = \Mockery::mock(CacheInterface::class, [
            'has' => false,
            'get' => null,
            'set' => true
        ]);

        /**@var $cache CacheInterface */

        $definitionMaker = new DefinitionMaker($cache);
        $definition = $definitionMaker->makeDefinition(CarInterface::class);
        self::assertEquals(Car::class, $definition->getClassName());
        self::assertEquals([
            EngineInterface::class,
            Body::class,
            'cfg:cars.color',
            'cfg:cars.tonnage'
        ], $definition->getArgs());
    }
}
