<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Middleware\Stub;

use Grifix\Kit\Http\ResponseInterface;
use Grifix\Kit\Http\ServerRequestInterface;
use Grifix\Kit\Middleware\AbstractMiddleware;

/**
 * Class MiddlewareA
 *
 * @category Alias
 * @package  Alias\FirstMiddleware\Tests\Dummies
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class MiddlewareC extends AbstractMiddleware
{
    
    /**
     * {@inheritdoc}
     */
    protected function before(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $response->withAddedHeader('before', 'C');
    }
    
    /**
     * {@inheritdoc}
     */
    protected function after(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $response->withAddedHeader('after', 'C');
    }
    
}