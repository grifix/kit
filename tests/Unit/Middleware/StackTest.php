<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Middleware;

use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Http\StreamFactory;
use Grifix\Kit\Kernel\ClassMaker;
use Grifix\Kit\Middleware\Stack;
use Grifix\Kit\Http\ServerRequestFactory;
use Grifix\Kit\Http\ResponseFactory;
use Grifix\Kit\Test\Unit\AbstractTest;
use Grifix\Kit\Test\Unit\Middleware\Stub\MiddlewareA;
use Grifix\Kit\Test\Unit\Middleware\Stub\MiddlewareB;
use Grifix\Kit\Test\Unit\Middleware\Stub\MiddlewareC;

/**
 * Class StackTest
 *
 * @category Alias
 * @package  Alias\FirstMiddleware\Tests
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class StackTest extends AbstractTest
{
    /**
     * @return void
     */
    public function testStack()
    {
        $stack = new Stack();
        
        $stack->add(new MiddlewareA());
        $stack->add(new MiddlewareB());
        $stack->add(new MiddlewareC());
        $response = $stack->call(
            (new ServerRequestFactory(new ClassMaker(), new ArrayHelper()))->createFromGlobals(),
            (new ResponseFactory(new StreamFactory(), new ClassMaker()))->createResponse()
        );
        
        self::assertEquals('C, B, A', $response->getHeaderLine('before'));
        self::assertEquals('A, B, C', $response->getHeaderLine('after'));
    }
}
