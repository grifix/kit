<?php
declare(strict_types=1);

namespace Grifix\Kit\Test\Unit\Orm\Persister;

use Grifix\Kit\Orm\Persister\JsonbUpdate\JsonbUpdate;
use Grifix\Kit\Orm\Persister\JsonbUpdate\JsonbUpdateFactory;
use Grifix\Kit\Test\Unit\AbstractTest;

/**
 * Class JsonUpdaterTest
 * @package Grifix\Kit\Test\Unit\Orm\Persister
 */
class JsonbUpdateTest extends AbstractTest
{
    public function testCreateUpdate()
    {
        $updater = new JsonbUpdateFactory();
        $data = [
            'id' => 1,
            'address' => [
                'phone' => [
                    'number' => '67545654'
                ],
                'email' => 'joe@gmail.com'
            ]
        ];
        $result = $updater->createUpdates($data);
        $expected = [
            new JsonbUpdate(['id'], $data['id']),
            new JsonbUpdate(['address', 'phone', 'number'], $data['address']['phone']['number']),
            new JsonbUpdate(['address', 'email'], $data['address']['email'])
        ];

        self::assertEquals($expected, $result);
    }
}
