<?php
/**
 * PHP Version 7
 *
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types = 1);

namespace Grifix\Kit\Test\Unit\Route;

use Grifix\Kit\Helper\ArrayHelper;
use Grifix\Kit\Http\ServerRequestFactory;
use Grifix\Kit\Kernel\ClassMaker;
use Grifix\Kit\Route\Matcher\IntMatcher;
use Grifix\Kit\Route\Matcher\LocaleMatcher;
use Grifix\Kit\Route\Matcher\MixedMatcher;
use Grifix\Kit\Route\Matcher\PageMatcher;
use Grifix\Kit\Route\Route;
use Grifix\Kit\Test\Unit\AbstractTest;

/**
 * Class RouteTest
 *
 * @category Alias
 * @package  Alias\Kit\Test\Route
 * @author   Mike Shapovalov <smikebox@gmail.com>
 * @license  http://opensource.org/licenses/MIT MIT
 * @link     http://grifix.net/docs/
 */
class RouteTest extends AbstractTest
{
    
    public function testMakeUrl()
    {
        $route = new Route(
            new ArrayHelper(),
            '/{locale:locale?}/user/{id:int}/comments/{pageIndex:page}/{token}/{sort:(asc|desc)?}/{filter:(all|best)}',
            null, [], [
                'locale' => new LocaleMatcher(['ru', 'en', 'pl']),
                'int' => new IntMatcher(),
                'page' => new PageMatcher(),
                'mixed' => new MixedMatcher(),
            ]
        );
        self::assertEquals(
            '/ru_RU/user/7/comments/2/abc/asc/all',
            $route->makeUrl([
                'locale' => 'ru_RU',
                'id' => 7,
                'pageIndex' => 2,
                'token' => 'abc',
                'sort' => 'asc',
                'filter' => 'all',
            ])
        );
        
        self::assertEquals(
            '/user/7/comments/2/abc/all',
            $route->makeUrl([
                'id' => 7,
                'pageIndex' => 2,
                'token' => 'abc',
                'filter' => 'all'
            ])
        );
    }
    
    /**
     * @return void
     */
    public function testMatch()
    {
        
        $route = new Route(
            new ArrayHelper(), '/{locale:locale?}/user/{id:int}/comments/{pageIndex:page}/{token}/{sort:(asc|desc)?}',
            null, [], [
                'locale' => new LocaleMatcher(['ru', 'en', 'pl']),
                'int' => new IntMatcher(),
                'page' => new PageMatcher(),
                'mixed' => new MixedMatcher(),
            ]
        );
        
        
        $request = (new ServerRequestFactory(new ClassMaker(), new ArrayHelper()))->create(
            'get',
            '/user/18/comments/page2/xch'
        );
        self::assertTrue($route->match($request));
        self::assertEquals([
            'locale' => null,
            'id' => 18,
            'pageIndex' => 2,
            'token' => 'xch',
            'sort' => null,
        ], $route->getParams());
        
        
        $request = (new ServerRequestFactory(new ClassMaker(), new ArrayHelper()))->create(
            'get',
            '/en/user/18/comments/page2/xch/asc'
        );
        self::assertTrue($route->match($request));
        self::assertEquals([
            'locale' => 'en',
            'id' => 18,
            'pageIndex' => 2,
            'token' => 'xch',
            'sort' => 'asc',
        ], $route->getParams());
        
        
        $request = (new ServerRequestFactory(new ClassMaker(), new ArrayHelper()))->create(
            'get',
            '/fr/user/18/comments/page2/xch'
        );
        self::assertFalse($route->match($request));
        
        $request = (new ServerRequestFactory(new ClassMaker(), new ArrayHelper()))->create(
            'get',
            '/en/user/18/comments/page2/xch/ddd'
        );
        self::assertFalse($route->match($request));
    }
}
